package com.springlib.core.dto;

import java.util.List;

public class StudentDTO {
	private int total;
	private int limit;
	private int row;	
	private List<StudentInfo> rows;
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public int getRow() {
		return row;
	}
	public void setRow(int row) {
		this.row = row;
	}
	public List<StudentInfo> getRows() {
		return rows;
	}
	public void setRows(List<StudentInfo> rows) {
		this.rows = rows;
	}
	
}
