package com.springlib.core.dto;

import java.io.Serializable;

public abstract class CacheObj implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String key;
	protected String value;
	protected String getKey() {
		return key;
	}
	protected void setKey(String key) {
		this.key = key;
	}
	protected String getValue() {
		return value;
	}
	protected void setValue(String value) {
		this.value = value;
	}
}
