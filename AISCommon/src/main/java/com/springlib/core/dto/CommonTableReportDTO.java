package com.springlib.core.dto;

import java.io.Serializable;

public class CommonTableReportDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String grpHeader1;
	public String grpHeader2;
	public String grpFooter1;
	public String grpFooter2;
	public String getGrpHeader1() {
		return grpHeader1;
	}
	public void setGrpHeader1(String grpHeader1) {
		this.grpHeader1 = grpHeader1;
	}
	public String getGrpHeader2() {
		return grpHeader2;
	}
	public void setGrpHeader2(String grpHeader2) {
		this.grpHeader2 = grpHeader2;
	}
	public String getGrpFooter1() {
		return grpFooter1;
	}
	public void setGrpFooter1(String grpFooter1) {
		this.grpFooter1 = grpFooter1;
	}
	public String getGrpFooter2() {
		return grpFooter2;
	}
	public void setGrpFooter2(String grpFooter2) {
		this.grpFooter2 = grpFooter2;
	}
	
}
