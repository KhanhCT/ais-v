package com.springlib.core.dto;

import java.io.Serializable;

public class ClassInfo implements Serializable{
	private int id;
	private String name;
	private int campusID;
	private String campusName;
	private int  yearLevelID;
	private String yearLevelName;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCampusID() {
		return campusID;
	}
	public void setCampusID(int campusID) {
		this.campusID = campusID;
	}
	public String getCampusName() {
		return campusName;
	}
	public void setCampusName(String campusName) {
		this.campusName = campusName;
	}
	public int getYearLevelID() {
		return yearLevelID;
	}
	public void setYearLevelID(int yearLevelID) {
		this.yearLevelID = yearLevelID;
	}
	public String getYearLevelName() {
		return yearLevelName;
	}
	public void setYearLevelName(String yearLevelName) {
		this.yearLevelName = yearLevelName;
	}
	
}
