package com.springlib.core.dto;

import java.util.List;

public class Email extends MailObj{
	private List<String> ccLst;
	private List<String> bccLst;
	public Email() {
	}
	/**
	 * @return the ccLst
	 */
	public List<String> getCcLst() {
		return ccLst;
	}
	/**
	 * @param ccLst the ccLst to set
	 */
	public void setCcLst(List<String> ccLst) {
		this.ccLst = ccLst;
	}
	/**
	 * @return the bccLst
	 */
	public List<String> getBccLst() {
		return bccLst;
	}
	/**
	 * @param bccLst the bccLst to set
	 */
	public void setBccLst(List<String> bccLst) {
		this.bccLst = bccLst;
	}
	
}
