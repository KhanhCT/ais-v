package com.springlib.core.dto;

import java.io.Serializable;
import java.util.Date;

public class ReportDTO<T> implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String title;
	private String reportDate;
	private String fromDate;
	private String toDate;
	private ReportOwner reportOwner;
	private String ownerName;
	private String exportFile;
	private T dataSource;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getReportDate() {
		return reportDate;
	}
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getExportFile() {
		return exportFile;
	}
	public void setExportFile(String exportFile) {
		this.exportFile = exportFile;
	}
	public T getDataSource() {
		return dataSource;
	}
	public void setDataSource(T dataSource) {
		this.dataSource = dataSource;
	}
	
	public ReportOwner getReportOwner() {
		return reportOwner;
	}
	public void setReportOwner(ReportOwner reportOwner) {
		this.reportOwner = reportOwner;
	}


	public class ReportOwner
	{
		public String ownerCode;
		public String ownerName;
		public String getOwnerCode() {
			return ownerCode;
		}
		public void setOwnerCode(String ownerCode) {
			this.ownerCode = ownerCode;
		}
		public String getOwnerName() {
			return ownerName;
		}
		public void setOwnerName(String ownerName) {
			this.ownerName = ownerName;
		}
		
	}
	
}
