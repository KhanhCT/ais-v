package org.springlib.core.http;

import java.io.IOException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Arrays;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;


public class OAuthClientConfig{

	public OAuthClientConfig() {
		
	}
	public RestTemplate restTemplate(String consumerKey, String consumerSecret, String accessKey, String accessSecret) {
		return restTemplate(consumerKey, consumerSecret, accessKey, accessSecret, false);
	}
	public RestTemplate restTemplate(String accessKey) {
		return restTemplate(null, null, accessKey, null, false);
	}

	public RestTemplate restTemplate(String consumerKey, String consumerSecret, String accessKey, String accessSecret,
			boolean ignoreHttpsCert) {
		RestTemplate client = new RestTemplate(new SimpleClientHttpRequestFactory());

		// Bypass self signed HTTPS certificate
		if (ignoreHttpsCert) {
			HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier());
			HttpsURLConnection.setDefaultSSLSocketFactory(getSSLSocketFactory());
		}
		client.setInterceptors(Arrays.asList(new ClientHttpRequestInterceptor[] {
				new OAuth1RequestInterceptor(consumerKey, consumerSecret, accessKey, accessSecret)
		}));
		return client;
	}

	private SSLSocketFactory getSSLSocketFactory() {

		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager
		SSLContext sc = null;
		try {
			sc = SSLContext.getInstance("TLS");
			sc.init(null, trustAllCerts, new SecureRandom());
			return sc.getSocketFactory();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private HostnameVerifier hostnameVerifier() {
		return new javax.net.ssl.HostnameVerifier() {

			public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
				return true;
			}
		};
	}
	@SuppressWarnings("unused")
	public class OAuth1RequestInterceptor implements ClientHttpRequestInterceptor {
		
		private final String consumerKey;

		private final String consumerSecret;

		private final String accessToken;

		private final String accessTokenSecret;

		public OAuth1RequestInterceptor(String consumerKey, String consumerSecret, String accessToken,
				String accessTokenSecret) {
			this.consumerKey = consumerKey;
			this.consumerSecret = consumerSecret;
			this.accessToken = accessToken;
			this.accessTokenSecret = accessTokenSecret;

		}
		@Override
		public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
				throws IOException {
			// TODO Auto-generated method stub
			return null;
		}
	}
}
