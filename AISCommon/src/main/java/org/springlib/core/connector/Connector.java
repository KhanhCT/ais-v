package org.springlib.core.connector;

import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springlib.core.utils.CommonKey;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

@Service("connector")
public class Connector {
	private static final Logger LOGGER = LoggerFactory.getLogger(Connector.class);

   /* @Value("${generic.connect.timeout}")
    private int genericConnectTimeOut;

    @Value("${generic.read.timeout}")
    private int genericReadTimeOut;*/
    
	public Response callApi(String url, Object entity, String method, int connectTimeout, int readTimeout)
			throws Exception {
		LOGGER.debug("callApi = {}", url);
		// client config
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(JacksonJsonProvider.class);
		Client client = ClientBuilder.newClient(clientConfig);
		client.property(ClientProperties.CONNECT_TIMEOUT, connectTimeout);
		client.property(ClientProperties.READ_TIMEOUT, readTimeout);
		try {
			// build a new web resource target
			WebTarget target = client.target(url);
			// start building a request to the targeted web resource and define
			// the accepted response and invoke method
			Invocation.Builder builder = target.request(MediaType.APPLICATION_JSON_TYPE);
			// process response
			Response response = builder.build(method, entity != null ? Entity.json(entity) : null).invoke();
			return response;
		} catch (Exception e) {
			LOGGER.debug("ERROR: Calling API = {}", url, e);
			throw new Exception("ERROR: Calling API = " + url, e);
		}
	}
	
	public Response callApiWithToken(String url, Object entity, String method, int connectTimeout, int readTimeout, String token)
			throws Exception {
		LOGGER.debug("callApi = {}", url);
		// client config
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(JacksonJsonProvider.class);
		Client client = ClientBuilder.newClient(clientConfig);
		client.property(ClientProperties.CONNECT_TIMEOUT, connectTimeout);
		client.property(ClientProperties.READ_TIMEOUT, readTimeout);
		try {
			// build a new web resource target
			WebTarget target = client.target(url);
			// start building a request to the targeted web resource and define
			// the accepted response and invoke method
			Invocation.Builder builder = target.request(MediaType.APPLICATION_JSON_TYPE).header("Authorization", token);
			// process response
			Response response = builder.build(method, entity != null ? Entity.json(entity) : null).invoke();
			return response;
		} catch (Exception e) {
			LOGGER.debug("ERROR: Calling API = {}", url, e);
			throw new Exception("ERROR: Calling API = " + url, e);
		}
	}
	
	public <T> T getRequest(Class<T> valueType, String resourceUrl) {
        try {
			Response response = callApi(resourceUrl, null, CommonKey.REQUEST_METHOD.GET, 5000, 5000);
			if (response != null && response.getStatus() != 500) {
	        	return response.readEntity(valueType);
	        } else {
	        	LOGGER.debug("ERROR: API = {}, response = {}", resourceUrl, response);
	        	return null;
	        }
		} catch (Exception e) {		
			LOGGER.debug("ERROR: Exception = {}, Cause = {}", e.getMessage(), e.getCause());
			return null;
		}	
	}
	
	public <T> T getRequestWithToken(Class<T> valueType, String resourceUrl, String token) {
        try {
			Response response = callApiWithToken(resourceUrl, null, CommonKey.REQUEST_METHOD.GET, 5000, 5000, token);
			if (response != null && response.getStatus() != 500) {
	        	return response.readEntity(valueType);
	        } else {
	        	LOGGER.debug("ERROR: API = {}, response = {}", resourceUrl, response);
	        	return null;
	        }
		} catch (Exception e) {		
			LOGGER.debug("ERROR: Exception = {}, Cause = {}", e.getMessage(), e.getCause());
			return null;
		}	
	}
	
	public <T> T getRequestWithArgs(Class<T> valueType, String resourceUrl, HashMap<?, ?> args) {
        try {
			Response response = callApi(resourceUrl, args, CommonKey.REQUEST_METHOD.GET, 5000, 5000);
			if (response != null && response.getStatus() != 500) {
	        	return response.readEntity(valueType);
	        } else {
	        	LOGGER.debug("ERROR: API = {}, response = {}", resourceUrl, response);
	        	return null;
	        }
		} catch (Exception e) {		
			LOGGER.debug("ERROR: Exception = {}, Cause = {}", e.getMessage(), e.getCause());
			return null;
		}	
	}
	public <T> T postRequest(Class<T> valueType, String resourceUrl) {
        try {
			Response response = callApi(resourceUrl, null, CommonKey.REQUEST_METHOD.POST, 5000, 5000);
			if (response != null && response.getStatus() != 500) {
	        	return response.readEntity(valueType);
	        } else {
	        	LOGGER.debug("ERROR: API = {}, response = {}", resourceUrl, response);
	        	return null;
	        }
		} catch (Exception e) {		
			LOGGER.debug("ERROR: Exception = {}, Cause = {}", e.getMessage(), e.getCause());
			return null;
		}	
	}
	public <T> T postRequestWithArgs(Class<T> valueType, String resourceUrl, Object args) {
        try {
			Response response = callApi(resourceUrl, args, CommonKey.REQUEST_METHOD.POST, 5000, 5000);
			if (response != null && response.getStatus() != 500) {
	        	return response.readEntity(valueType);
	        } else {
	        	LOGGER.debug("ERROR: API = {}, response = {}", resourceUrl, response);
	        	return null;
	        }
		} catch (Exception e) {		
			LOGGER.debug("ERROR: Exception = {}, Cause = {}", e.getMessage(), e.getCause());
			return null;
		}	
	}
}
