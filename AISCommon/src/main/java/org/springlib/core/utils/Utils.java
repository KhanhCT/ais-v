package org.springlib.core.utils;

import java.util.Base64;

public class Utils {
	public static String Base64Encoder(String plainText)
	{
		return  Base64.getEncoder().encodeToString(plainText.getBytes());
	}
	public static String Base64Decoder(String encodedString)
	{
		byte[] decodedBytes = Base64.getDecoder().decode(encodedString);
		String decodedString = new String(decodedBytes);
		return decodedString;
	}
}
