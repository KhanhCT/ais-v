package org.springlib.core.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateConverter {
	private static final Logger LOGGER = LoggerFactory.getLogger(DateConverter.class);
	private static DateConverter instance;
	private  DateConverter()
	{
		
	}
	public static DateConverter getInstance() {
		if(instance == null)
			instance = new DateConverter();
		return instance;
	}
	public static Date convertDateByFormatLocal(String date, String format) {
		if (date == null || date.isEmpty() || format == null) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			return sdf.parse(date);
		} catch (ParseException e) {
			LOGGER.error("Content: "+ date + "Exception: " + e.getMessage());
			return null;
		}
	}

	public static String convertDateStringByFormatLocal(Date date, String format) {
		if (date == null || format == null) {
			return null;
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			return sdf.format(date).toString();
		}catch(Exception ex){
			LOGGER.error("Content: "+ date + "Exception: " + ex.getMessage());
			return null;
		}
	}

	public static Date getCurrentDate() {
		Calendar cal = Calendar.getInstance();
		return cal.getTime();
	}
}
