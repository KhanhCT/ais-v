package org.springlib.core.cache;

import org.springframework.beans.factory.annotation.Autowired;
import net.sf.ehcache.CacheManager;

public abstract class BaseCacheCore<T> {
	@Autowired
	protected CacheManager cacheManager;
	public abstract T getDataFromKey(String key);	
	public abstract T putData(T obj);
	public abstract T updateData(T obj);	
	public abstract T deleteCache(T obj);
	public abstract void refreshAllData();
}
