package com.daisyit.dto;

import java.io.Serializable;

public class CommonReportDTO<T> implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String grpHeader;
	private T payload;
	public String getGrpHeader() {
		return grpHeader;
	}
	public void setGrpHeader(String grpHeader) {
		this.grpHeader = grpHeader;
	}
	public T getPayload() {
		return payload;
	}
	public void setPayload(T payload) {
		this.payload = payload;
	}
	
}
