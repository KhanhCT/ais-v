package com.daisyit.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.daisyit.backend.model.RtPrice;
import com.daisyit.backend.model.RtPriceId;

public class RtPriceDTO implements Serializable{
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String prCode;
     private Integer skuId;
	 private double rtPrice;
     private Date modiDate;
     private Date openDate;
     private boolean status;
	public String getPrCode() {
		return prCode;
	}
	public void setPrCode(String prCode) {
		this.prCode = prCode;
	}
	public Integer getSkuId() {
		return skuId;
	}
	public void setSkuId(Integer skuId) {
		this.skuId = skuId;
	}
	public double getRtPrice() {
		return rtPrice;
	}
	public void setRtPrice(double rtPrice) {
		this.rtPrice = rtPrice;
	}
	public Date getModiDate() {
		return modiDate;
	}
	public void setModiDate(Date modiDate) {
		this.modiDate = modiDate;
	}
	public Date getOpenDate() {
		return openDate;
	}
	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public RtPrice dto2Model()
	{
		RtPrice rtPrice = new RtPrice();
		rtPrice.setId(new RtPriceId(this.getPrCode(), this.getSkuId()));
		rtPrice.setRtPrice(new BigDecimal(String.valueOf(this.getRtPrice())));
		rtPrice.setModiDate(this.getModiDate());
		rtPrice.setOpenDate(this.getOpenDate());
		rtPrice.setStatus(this.isStatus());
		return rtPrice;	
	}
     
	public void model2DTO(RtPrice rtPrice)
	{
		this.setPrCode(rtPrice.getId().getPrCode());
		this.setSkuId(rtPrice.getId().getSkuId());
		this.setRtPrice(rtPrice.getRtPrice().doubleValue());
		this.setModiDate(rtPrice.getModiDate());
		this.setOpenDate(rtPrice.getOpenDate());
		this.setStatus(rtPrice.getStatus());
	}
}
