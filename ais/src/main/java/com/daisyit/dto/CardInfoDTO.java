package com.daisyit.dto;

import java.io.Serializable;
import java.util.Date;

import org.springlib.core.utils.DateConverter;

import com.daisyit.backend.model.CardInfo;
import com.daisyit.backend.model.CardInfoId;

public class CardInfoDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer cardId;
    private String cardCode;
    private double balance;
    private String cardType;
    private String passcode;
    private String chrCode;
    private String hexCode;
    private Date issDate;
    private Date dueDate;
    private Double discPc;
    private Date fromDate;
    private Date toDate;
    private boolean active;
	public Integer getCardId() {
		return cardId;
	}
	public void setCardId(Integer cardId) {
		this.cardId = cardId;
	}
	public String getCardCode() {
		return cardCode;
	}
	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getPasscode() {
		return passcode;
	}
	public void setPasscode(String passcode) {
		this.passcode = passcode;
	}
	public String getChrCode() {
		return chrCode;
	}
	public void setChrCode(String chrCode) {
		this.chrCode = chrCode;
	}
	public String getHexCode() {
		return hexCode;
	}
	public void setHexCode(String hexCode) {
		this.hexCode = hexCode;
	}
	public Date getIssDate() {
		return issDate;
	}
	public void setIssDate(Date issDate) {
		this.issDate = issDate;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public Double getDiscPc() {
		return discPc;
	}
	public void setDiscPc(Double discPc) {
		this.discPc = discPc;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public CardInfo dto2Model() {
		CardInfoId id = new CardInfoId();
		id.setCardCode(this.cardCode);
		CardInfo card = new CardInfo();
		card.setId(id);
		card.setBalance(this.balance);
		card.setIssDate(DateConverter.getCurrentDate());
		card.setActive(true);
		return card;
	}
}
