package com.daisyit.dto;

import java.io.Serializable;
import java.util.Date;

import com.daisyit.backend.model.CusTrans;

public class CusTransDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String transNum;
    private String pmtCode;
    private String cys;
    private String transType;
    private Date transDate;
    private Date transTime;
    private String cusId;
    private String cusName;
    private int userId;
    private String action;
    private String remark;
    private double amount;
    private Boolean status;
    private double cashIn;
    private double cashOut;
    private String merName;
    private double balanceCount;
   
	public String getTransNum() {
		return transNum;
	}



	public void setTransNum(String transNum) {
		this.transNum = transNum;
	}



	public String getPmtCode() {
		return pmtCode;
	}



	public void setPmtCode(String pmtCode) {
		this.pmtCode = pmtCode;
	}



	public String getCys() {
		return cys;
	}



	public void setCys(String cys) {
		this.cys = cys;
	}



	public String getTransType() {
		return transType;
	}



	public void setTransType(String transType) {
		this.transType = transType;
	}



	public Date getTransDate() {
		return transDate;
	}



	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}



	public Date getTransTime() {
		return transTime;
	}

	public void setTransTime(Date transTime) {
		this.transTime = transTime;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}



	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}



	public int getUserId() {
		return userId;
	}



	public void setUserId(int userId) {
		this.userId = userId;
	}



	public String getAction() {
		return action;
	}



	public void setAction(String action) {
		this.action = action;
	}



	public String getRemark() {
		return remark;
	}



	public void setRemark(String remark) {
		this.remark = remark;
	}



	public double getAmount() {
		return amount;
	}



	public void setAmount(double amount) {
		this.amount = amount;
	}



	public Boolean getStatus() {
		return status;
	}



	public void setStatus(Boolean status) {
		this.status = status;
	}



	public double getCashIn() {
		return cashIn;
	}



	public void setCashIn(long cashIn) {
		this.cashIn = cashIn;
	}



	public double getCashOut() {
		return cashOut;
	}



	public void setCashOut(double cashOut) {
		this.cashOut = cashOut;
	}



	


	public String getMerName() {
		return merName;
	}



	public void setMerName(String merName) {
		this.merName = merName;
	}



	public double getBalanceCount() {
		return balanceCount;
	}



	public void setBalanceCount(double balanceCount) {
		this.balanceCount = balanceCount;
	}



	public void model2dto(CusTrans cusTrans)
	{
		this.transNum = cusTrans.getId().getTransNum();
		this.pmtCode = cusTrans.getId().getPmtCode();
		this.cys = cusTrans.getId().getCys();
		this.transType = cusTrans.getTransType();
		this.transDate = cusTrans.getTransDate();
		this.transTime = cusTrans.getTransTime();
		this.cusId = cusTrans.getCusId();
		this.userId = cusTrans.getUserId();
		this.remark = cusTrans.getRemark();
		this.amount = cusTrans.getAmount().doubleValue();
		this.status = cusTrans.getStatus();
		switch(this.transType)
		{
			case "01":
			{
				this.action="+"; //put
				this.cashIn = this.amount;
				this.cashOut = 0;
			}break;
			case "02":
			{
				this.action="-";//order
				this.cashIn = 0;
				this.cashOut = this.amount;
			}break;
			case "03":
			{
				this.action="-";//withdraw
				this.cashIn = 0;
				this.cashOut = this.amount;
			}break;
			case "04":
			{
				this.action="-";// transfer
				this.cashIn = 0;
				this.cashOut = this.amount;
			}break;
			default: break;
		}
		this.balanceCount = cusTrans.getCurBalance().doubleValue();
		this.merName = "Khanh";
	}
}
