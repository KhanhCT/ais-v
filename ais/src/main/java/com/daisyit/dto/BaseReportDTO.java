package com.daisyit.dto;

public class BaseReportDTO<T> {
	private String headerGroup;
	private T payload;
	private String footerGroup;
	private int totalQuantity;
	private long totalBalance;
	private long totalTopUp;
	private long totalConsumption;
	public String getHeaderGroup() {
		return headerGroup;
	}
	public void setHeaderGroup(String headerGroup) {
		this.headerGroup = headerGroup;
	}
	public T getPayload() {
		return payload;
	}
	public void setPayload(T payload) {
		this.payload = payload;
	}
	public String getFooterGroup() {
		return footerGroup;
	}
	public void setFooterGroup(String footerGroup) {
		this.footerGroup = footerGroup;
	}
	public int getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(int totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	public long getTotalBalance() {
		return totalBalance;
	}
	public void setTotalBalance(long totalBalance) {
		this.totalBalance = totalBalance;
	}
	public long getTotalTopUp() {
		return totalTopUp;
	}
	public void setTotalTopUp(long totalTopUp) {
		this.totalTopUp = totalTopUp;
	}
	public long getTotalConsumption() {
		return totalConsumption;
	}
	public void setTotalConsumption(long totalConsumption) {
		this.totalConsumption = totalConsumption;
	}
	
	
}
