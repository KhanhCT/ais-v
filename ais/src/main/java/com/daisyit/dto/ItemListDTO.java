package com.daisyit.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.springlib.core.utils.CommonKey;
import org.springlib.core.utils.DateConverter;

import com.daisyit.backend.model.ItemList;
import com.daisyit.backend.model.ItemListId;

public class ItemListDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private Integer skuId;
	private String skuCode;
	private String name;
	private String pluCode;
	private int itemGrpId;
	private String itemType;
	private String barcode;
	private Double lastPrice;
	private String priceUnit;
	private String unitDesc;
	private Double itemPc;
	private String openDate;
	private Double rtPrice;
	private String description;
	private boolean status;

	public Integer getSkuId() {
		return skuId;
	}

	public void setSkuId(Integer skuId) {
		this.skuId = skuId;
	}

	public String getSkuCode() {
		return skuCode;
	}

	public void setSkuCode(String skuCode) {
		this.skuCode = skuCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPluCode() {
		return pluCode;
	}

	public void setPluCode(String pluCode) {
		this.pluCode = pluCode;
	}

	public int getItemGrpId() {
		return itemGrpId;
	}

	public void setItemGrpId(int itemGrpId) {
		this.itemGrpId = itemGrpId;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Double getLastPrice() {
		return lastPrice;
	}

	public void setLastPrice(Double lastPrice) {
		this.lastPrice = lastPrice;
	}

	public String getPriceUnit() {
		return priceUnit;
	}

	public void setPriceUnit(String priceUnit) {
		this.priceUnit = priceUnit;
	}

	public String getUnitDesc() {
		return unitDesc;
	}

	public void setUnitDesc(String unitDesc) {
		this.unitDesc = unitDesc;
	}

	public Double getItemPc() {
		return itemPc;
	}

	public void setItemPc(Double itemPc) {
		this.itemPc = itemPc;
	}

	public String getOpenDate() {
		return openDate;
	}

	public void setOpenDate(String openDate) {
		this.openDate = openDate;
	}

	public Double getRtPrice() {
		return rtPrice;
	}

	public void setRtPrice(Double rtPrice) {
		this.rtPrice = rtPrice;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public ItemList dto2Model() {
		ItemList item = new ItemList();
		item.setId(new ItemListId(this.getSkuId(), this.getSkuCode()));
		item.setName(this.getName());
		item.setItemGrpId(this.getItemGrpId());
		item.setItemType(this.getItemType());
		item.setBarcode(this.getBarcode());
		if (this.getLastPrice() != null) {
			item.setLastPrice(new BigDecimal(this.getLastPrice()));
		}
		item.setPriceUnit(this.getPriceUnit());
		item.setUnitDesc(this.getUnitDesc());
		if (this.getItemPc() != null) {
			item.setItemPc(new BigDecimal(this.getItemPc()));
		}
		item.setOpenDate(DateConverter.convertDateByFormatLocal(this.getOpenDate(), CommonKey.DATE_FORMAT.DATE_SLASH_FORMAT));
		if (this.getRtPrice() != null) {
			item.setRtPrice(new BigDecimal(this.getRtPrice()));
		}
		item.setDescription(this.getDescription());
		item.setStatus(this.isStatus());
		return item;
	}

	public void model2DTO(ItemList item) {
		this.skuId = item.getId().getSkuId();
		this.skuCode = item.getId().getSkuCode();
		this.name = item.getName();
		this.itemGrpId = item.getItemGrpId();
		this.itemType = item.getItemType();
		this.barcode = item.getBarcode();
		this.lastPrice = item.getLastPrice().doubleValue();
		this.priceUnit = item.getPriceUnit();
		this.unitDesc = item.getUnitDesc();
		if(this.itemPc !=null)
			this.itemPc = item.getItemPc().doubleValue();
		if(this.openDate !=null)
			this.openDate = DateConverter.convertDateStringByFormatLocal(item.getOpenDate(), CommonKey.DATE_FORMAT.DATE_SLASH_FORMAT);
		this.rtPrice = item.getRtPrice().doubleValue();
		this.description = item.getDescription();
		this.status = item.isStatus();
	}

}
