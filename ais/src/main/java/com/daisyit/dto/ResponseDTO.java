package com.daisyit.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ResponseDTO {
	private boolean success;
	private int code;
	private String description;
	private Object data;

	public ResponseDTO() {
		super();
	}

	public ResponseDTO(final boolean success) {
		super();
		this.success = success;
	}

	public ResponseDTO(final boolean success, final int code) {
		this(success);
		this.code = code;
	}

	public ResponseDTO(final boolean success, final int code, String description) {
		this(success, code);
		this.description = description;
	}

	public ResponseDTO(final boolean success, final int code, String description, Object entity) {
		this(success, code, description);
		this.data = entity;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
