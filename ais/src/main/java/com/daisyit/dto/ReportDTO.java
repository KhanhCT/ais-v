package com.daisyit.dto;

import java.io.Serializable;

public class ReportDTO<T> implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cardId;
	private int classId;
	private String cusName;
	private String className;
	private double totalQty;	
	private double totalAmount;
	private double balance;
	private double totalCashOut;
	private double totalCashIn;
	private T data;
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getCusName() {
		return cusName;
	}
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	public double getTotalQty() {
		return totalQty;
	}
	public void setTotalQty(double totalQty) {
		this.totalQty = totalQty;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public double getTotalCashOut() {
		return totalCashOut;
	}
	public void setTotalCashOut(double totalCashOut) {
		this.totalCashOut = totalCashOut;
	}
	public double getTotalCashIn() {
		return totalCashIn;
	}
	public void setTotalCashIn(double totalCashIn) {
		this.totalCashIn = totalCashIn;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public int getClassId() {
		return classId;
	}
	public void setClassId(int classId) {
		this.classId = classId;
	}
	
}
