package com.daisyit.dto;

import com.daisyit.backend.model.AllCode;

public class AllCodeDTO {
	private Integer idX;
	private String codeName;
	private String type;
	private Integer codeIdX;
	private String codeVal;
	private Boolean modify;

	public AllCodeDTO() {
	}

	public AllCodeDTO(Integer idX, String codeName, String type, Integer codeIdX, String codeVal, Boolean modify) {
		super();
		this.idX = idX;
		this.codeName = codeName;
		this.type = type;
		this.codeIdX = codeIdX;
		this.codeVal = codeVal;
		this.modify = modify;
	}

	public Integer getIdX() {
		return idX;
	}

	public void setIdX(Integer idX) {
		this.idX = idX;
	}

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getCodeIdX() {
		return codeIdX;
	}

	public void setCodeIdX(Integer codeIdX) {
		this.codeIdX = codeIdX;
	}

	public String getCodeVal() {
		return codeVal;
	}

	public void setCodeVal(String codeVal) {
		this.codeVal = codeVal;
	}

	public Boolean getModify() {
		return modify;
	}

	public void setModify(Boolean modify) {
		this.modify = modify;
	}
	public void model2DTO(AllCode code)
	{
		this.idX = code.getId().getIdx();
		this.codeName = code.getId().getCodeName();
		this.type = code.getType();
		this.codeIdX = code.getCodeIdx();
		this.codeVal = code.getCodeVal();
		this.modify = code.getModify();
	}

}
