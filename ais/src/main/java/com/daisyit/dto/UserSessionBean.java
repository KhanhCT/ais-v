package com.daisyit.dto;

import java.io.Serializable;

public class UserSessionBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long userID;
	private String apiKey;
	private String apiSecretKey;
	private String tokenKey;
	private String userRoleID;
	private String userName;
	/**
	 * @return the userID
	 */
	public Long getUserID() {
		return userID;
	}
	/**
	 * @param userID the userID to set
	 */
	public void setUserID(Long userID) {
		this.userID = userID;
	}
	/**
	 * @return the apiKey
	 */
	public String getApiKey() {
		return apiKey;
	}
	/**
	 * @param apiKey the apiKey to set
	 */
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	/**
	 * @return the apiSecretKey
	 */
	public String getApiSecretKey() {
		return apiSecretKey;
	}
	/**
	 * @param apiSecretKey the apiSecretKey to set
	 */
	public void setApiSecretKey(String apiSecretKey) {
		this.apiSecretKey = apiSecretKey;
	}
	/**
	 * @return the tokenKey
	 */
	public String getTokenKey() {
		return tokenKey;
	}
	/**
	 * @param tokenKey the tokenKey to set
	 */
	public void setTokenKey(String tokenKey) {
		this.tokenKey = tokenKey;
	}
	/**
	 * @return the userRoleID
	 */
	public String getUserRoleID() {
		return userRoleID;
	}
	/**
	 * @param userRoleID the userRoleID to set
	 */
	public void setUserRoleID(String userRoleID) {
		this.userRoleID = userRoleID;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
