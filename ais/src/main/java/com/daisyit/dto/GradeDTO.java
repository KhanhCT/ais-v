package com.daisyit.dto;

import java.io.Serializable;

import com.daisyit.backend.model.Grade;

public class GradeDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int gradeId;
    private String gradeCode;
    private String name;
    private String scYearId;
    private String description;
    private PrCodeDTO prCode;
	/**
	 * @return the gradeId
	 */
	public int getGradeId() {
		return gradeId;
	}
	/**
	 * @param gradeId the gradeId to set
	 */
	public void setGradeId(int gradeId) {
		this.gradeId = gradeId;
	}
	/**
	 * @return the gradeCode
	 */
	public String getGradeCode() {
		return gradeCode;
	}
	/**
	 * @param gradeCode the gradeCode to set
	 */
	public void setGradeCode(String gradeCode) {
		this.gradeCode = gradeCode;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the scYearId
	 */
	public String getScYearId() {
		return scYearId;
	}
	/**
	 * @param scYearId the scYearId to set
	 */
	public void setScYearId(String scYearId) {
		this.scYearId = scYearId;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public PrCodeDTO getPrCode() {
		return prCode;
	}
	public void setPrCode(PrCodeDTO prCode) {
		this.prCode = prCode;
	}
	public void model2DTO(Grade grade)
	{
		this.gradeId = grade.getGradeId();
		this.name = grade.getName();
		this.scYearId = grade.getScYearId();
		this.description = grade.getDescription();
	}
    
}
