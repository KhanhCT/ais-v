package com.daisyit.dto;

import java.io.Serializable;

import com.daisyit.backend.model.PrCode;

public class PrCodeDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String prCode;
	private String type;
	private String name;
	private String cys;
	private Boolean status;

	public String getPrCode() {
		return prCode;
	}

	public void setPrCode(String prCode) {
		this.prCode = prCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCys() {
		return cys;
	}

	public void setCys(String cys) {
		this.cys = cys;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public PrCode dto2Model() {
		PrCode prCode = new PrCode();
		prCode.setPrCode(this.getPrCode());
		prCode.setType(this.getType());
		prCode.setName(this.getName());
		prCode.setCys(this.getCys());
		prCode.setStatus(this.getStatus());
		return prCode;
	}

	public void model2Dto(PrCode obj) {
		this.setPrCode(obj.getPrCode());
		this.setType(obj.getType());
		this.setName(obj.getName());
		this.setCys(obj.getCys());
		this.setStatus(obj.getStatus());
	}

}
