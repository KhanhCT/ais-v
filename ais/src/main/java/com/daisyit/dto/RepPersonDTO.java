package com.daisyit.dto;

import java.io.Serializable;

public class RepPersonDTO implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int repPersonId;
    private String repPersonCode;
    private String name;
    private int numOf;
    private String address;
    private String phoneNum;
    private String email;
    private Boolean disable;
	public int getRepPersonId() {
		return repPersonId;
	}
	public void setRepPersonId(int repPersonId) {
		this.repPersonId = repPersonId;
	}
	public String getRepPersonCode() {
		return repPersonCode;
	}
	public void setRepPersonCode(String repPersonCode) {
		this.repPersonCode = repPersonCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNumOf() {
		return numOf;
	}
	public void setNumOf(int numOf) {
		this.numOf = numOf;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Boolean getDisable() {
		return disable;
	}
	public void setDisable(Boolean disable) {
		this.disable = disable;
	}
    
}
