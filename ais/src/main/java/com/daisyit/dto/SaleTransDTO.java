package com.daisyit.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import org.springlib.core.utils.CommonKey;
import org.springlib.core.utils.DateConverter;

import com.daisyit.backend.model.SaleTrans;
import com.daisyit.backend.model.SaleTransId;

public class SaleTransDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String transNum;
    private int skuId;
    private String skuCode;
    private String skuName;
    private String transType;
    private int mealTimeId;
    private String cusId;
    private String cusName;
    private int cardId;
    private String cardCode;
    private int locationId;
    private String locationCode;
    private String locationName;
    private String transDate;
    private String transTime;
    private double qty;
    private double rtPrice;
    private double amount;
    private double cashIn; 
    private double cashOut;
    private double balance;
    private String action;
    private boolean isError = false;
    public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	private double curBalance;
    private double total;
    private double totalQty;
    private int userId;
    private String remark;
	public String getTransNum() {
		return transNum;
	}
	public void setTransNum(String transNum) {
		this.transNum = transNum;
	}
	public int getSkuId() {
		return skuId;
	}
	public void setSkuId(int skuId) {
		this.skuId = skuId;
	}
	public String getTransType() {
		return transType;
	}
	public void setTransType(String transType) {
		this.transType = transType;
	}
	public int getMealTimeId() {
		return mealTimeId;
	}
	public void setMealTimeId(int mealTimeId) {
		this.mealTimeId = mealTimeId;
	}
	public String getCusId() {
		return cusId;
	}
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
	public int getCardId() {
		return cardId;
	}
	public void setCardId(int cardId) {
		this.cardId = cardId;
	}
	public String getCardCode() {
		return cardCode;
	}
	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}
	public int getLocationId() {
		return locationId;
	}
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}
	public String getTransDate() {
		return transDate;
	}
	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}
	public String getTransTime() {
		return transTime;
	}
	public void setTransTime(String transTime) {
		this.transTime = transTime;
	}
	public double getQty() {
		return qty;
	}
	public void setQty(double qty) {
		this.qty = qty;
	}
	public double getRtPrice() {
		return rtPrice;
	}
	public void setRtPrice(double rtPrice) {
		this.rtPrice = rtPrice;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	public String getSkuCode() {
		return skuCode;
	}
	public void setSkuCode(String skuCode) {
		this.skuCode = skuCode;
	}
	public String getSkuName() {
		return skuName;
	}
	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}
	
	
	public double getCurBalance() {
		return curBalance;
	}
	public void setCurBalance(double curBalance) {
		this.curBalance = curBalance;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public double getTotalQty() {
		return totalQty;
	}
	public void setTotalQty(double totalQty) {
		this.totalQty = totalQty;
	}
	
	public double getCashIn() {
		return cashIn;
	}
	public void setCashIn(double cashIn) {
		this.cashIn = cashIn;
	}
	public double getCashOut() {
		return cashOut;
	}
	public void setCashOut(double cashOut) {
		this.cashOut = cashOut;
	}
	
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	public boolean isError() {
		return isError;
	}
	public void setError(boolean isError) {
		this.isError = isError;
	}
	public SaleTrans dto2Model()
	{
		SaleTrans saleTrans = new SaleTrans();
		SaleTransId id = new SaleTransId(this.transNum, this.skuId);
		saleTrans.setId(id);
		saleTrans.setTransType(this.transType);
		saleTrans.setCusId(this.cusId);
		saleTrans.setLocationId(this.locationId);
		saleTrans.setCardId(cardId);
		saleTrans.setTransDate(DateConverter.convertDateByFormatLocal(transDate, CommonKey.DATE_FORMAT.DATE_TIME_FORMAT));
		saleTrans.setTransTime(DateConverter.convertDateByFormatLocal(transTime, CommonKey.DATE_FORMAT.DATE_TIME_FORMAT));
		saleTrans.setQty(new BigDecimal(this.qty));
		saleTrans.setRtPrice(new BigDecimal(this.rtPrice));
		saleTrans.setUserId(this.userId);
		saleTrans.setRemark(this.remark);
		return saleTrans;
	}
	
	public void model2DTO(SaleTrans saleTrans)
	{
		this.transNum = saleTrans.getId().getTransNum();
		this.skuId = saleTrans.getId().getSkuId();
		this.transType = saleTrans.getTransType();
		this.cusId = saleTrans.getCusId();
		this.locationId = saleTrans.getLocationId();
		this.transDate = DateConverter.convertDateStringByFormatLocal(saleTrans.getTransDate(), CommonKey.DATE_FORMAT.DATE_DATA_FORMAT);
		this.transTime = DateConverter.convertDateStringByFormatLocal(saleTrans.getTransTime(), CommonKey.DATE_FORMAT.TIME_FORMAT_2);
		this.qty = saleTrans.getQty().doubleValue();
		this.rtPrice = saleTrans.getRtPrice().doubleValue();
		this.userId = saleTrans.getUserId();
		this.remark = saleTrans.getRemark();
	}
	public String getCusName() {
		return cusName;
	}
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	/**
	 * @return the locationCode
	 */
	public String getLocationCode() {
		return locationCode;
	}
	/**
	 * @param locationCode the locationCode to set
	 */
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	/**
	 * @return the locationName
	 */
	public String getLocationName() {
		return locationName;
	}
	/**
	 * @param locationName the locationName to set
	 */
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
	
}
