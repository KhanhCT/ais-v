package com.daisyit.dto;

import java.io.Serializable;
import java.util.List;

public class CommonDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<LocationDTO> canteenList;
	private List<ClassDTO> classList;
	private List<GradeDTO> gradeList;
	private List<AllCodeDTO> cusTypeList;
	/**
	 * @return the canteenList
	 */
	public List<LocationDTO> getCanteenList() {
		return canteenList;
	}
	/**
	 * @param canteenList the canteenList to set
	 */
	public void setCanteenList(List<LocationDTO> canteenList) {
		this.canteenList = canteenList;
	}

	/**
	 * @return the classList
	 */
	public List<ClassDTO> getClassList() {
		return classList;
	}
	/**
	 * @param classList the classList to set
	 */
	public void setClassList(List<ClassDTO> classList) {
		this.classList = classList;
	}
	/**
	 * @return the gradeList
	 */
	public List<GradeDTO> getGradeList() {
		return gradeList;
	}
	/**
	 * @param gradeList the gradeList to set
	 */
	public void setGradeList(List<GradeDTO> gradeList) {
		this.gradeList = gradeList;
	}
	public List<AllCodeDTO> getCusTypeList() {
		return cusTypeList;
	}
	public void setCusTypeList(List<AllCodeDTO> cusTypeList) {
		this.cusTypeList = cusTypeList;
	}
	
	
}
