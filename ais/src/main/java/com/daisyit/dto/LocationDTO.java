package com.daisyit.dto;

import java.io.Serializable;

import com.daisyit.backend.model.Location;

public class LocationDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int locationId;
	private String locationCode;
	private String name;
	private String type;
	private Integer campusId;
	private String description;
	/**
	 * @return the locationId
	 */
	public int getLocationId() {
		return locationId;
	}
	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}
	/**
	 * @return the locationCode
	 */
	public String getLocationCode() {
		return locationCode;
	}
	/**
	 * @param locationCode the locationCode to set
	 */
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the schoolId
	 */
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	public Integer getCampusId() {
		return campusId;
	}
	public void setCampusId(Integer campusId) {
		this.campusId = campusId;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	public void model2DTO(Location loc)
	{
		this.locationId = loc.getLocationId();
		this.name = loc.getName();
		this.description = loc.getDescription();
		this.campusId = loc.getCampusId();
		this.type= loc.getType();
	}
	
}
