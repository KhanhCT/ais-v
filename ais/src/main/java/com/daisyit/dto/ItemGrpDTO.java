package com.daisyit.dto;

import java.io.Serializable;

import com.daisyit.backend.model.ItemGrp;

public class ItemGrpDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer itemGrpId;
    private String itemGrpCode;
	private String name;
    private String description;
	public int getItemGrpId() {
		return itemGrpId;
	}
	public void setItemGrpId(int itemGrpId) {
		this.itemGrpId = itemGrpId;
	}
	public String getItemGrpCode() {
		return itemGrpCode;
	}
	public void setItemGrpCode(String itemGrpCode) {
		this.itemGrpCode = itemGrpCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
    public ItemGrp dto2Model()
    {
    	ItemGrp itemGrp = new ItemGrp();
    	itemGrp.setItemGrpId(itemGrpId);
    	
    	itemGrp.setName(this.getName());
    	itemGrp.setDescription(this.getDescription());
    	return itemGrp;
    }
    public void model2DTO(ItemGrp item)
    {
    	this.setItemGrpId(item.getItemGrpId());
    	this.setName(item.getName());
    	this.setDescription(item.getDescription());
    }
    
}
