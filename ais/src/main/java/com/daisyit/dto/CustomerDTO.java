package com.daisyit.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import com.daisyit.backend.model.Customer;

public class CustomerDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cusId;
    private String cusName;
    private String firstName;
    private String lastName;
	private String prCode;
	private String cusTypeId;
	private Integer classId;
	private String className;
	private int repPersonId;
	private int cardId;
	private String cardCode;
	private double balance;
	private String position;
	private Double discPc;
	private String nationality;
	private String address;
	private String phoneNum;
	private String dueDate;
	private String email;
	private int gender;
	private String sex;
	private String remark;
	private boolean active;
	private boolean isError=false;
	
	
	
	public String getSex() {
		return sex;
	}


	public void setSex(String sex) {
		this.sex = sex;
	}


	public void setBalance(double balance) {
		this.balance = balance;
	}



	public boolean isError() {
		return isError;
	}


	public void setError(boolean isError) {
		this.isError = isError;
	}


	public String getCusId() {
		return cusId;
	}


	public void setCusId(String cusId) {
		this.cusId = cusId;
	}


	public String getCusName() {
		return cusName;
	}


	public void setCusName(String cusName) {
		this.cusName = cusName;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getPrCode() {
		return prCode;
	}


	public void setPrCode(String prCode) {
		this.prCode = prCode;
	}


	public String getCusTypeId() {
		return cusTypeId;
	}


	public void setCusTypeId(String cusTypeId) {
		this.cusTypeId = cusTypeId;
	}


	public Integer getClassId() {
		return classId;
	}


	public void setClassId(Integer classId) {
		this.classId = classId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}


	public int getRepPersonId() {
		return repPersonId;
	}


	public void setRepPersonId(int repPersonId) {
		this.repPersonId = repPersonId;
	}


	public int getCardId() {
		return cardId;
	}


	public void setCardId(int cardId) {
		this.cardId = cardId;
	}


	public String getCardCode() {
		return cardCode;
	}


	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}


	public double getBalance() {
		return balance;
	}


	


	public String getPosition() {
		return position;
	}


	public void setPosition(String position) {
		this.position = position;
	}


	public Double getDiscPc() {
		return discPc;
	}


	public void setDiscPc(Double discPc) {
		this.discPc = discPc;
	}


	public String getNationality() {
		return nationality;
	}


	public void setNationality(String nationality) {
		this.nationality = nationality;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getPhoneNum() {
		return phoneNum;
	}


	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}


	public String getDueDate() {
		return dueDate;
	}


	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public int getGender() {
		return gender;
	}


	public void setGender(int gender) {
		this.gender = gender;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	

	public boolean isActive() {
		return active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}


	public Customer dto2Model() {
		Customer customer = new Customer();
		customer.setCusId(this.getCusId());
		customer.setGivenName(this.firstName);
		customer.setFamilyName(this.lastName);
		customer.setCusTypeId(this.cusTypeId);
		if(this.gender == 0)	
			customer.setGender('F');
		else
			customer.setGender('M');
		customer.setClassId(this.classId);		
		customer.setPrCode(this.prCode);
		customer.setCardId(this.cardId);
		customer.setBalance(new BigDecimal("0.0"));
		customer.setNationality(this.nationality);
		customer.setAddress(this.address);
		customer.setPhoneNum(this.phoneNum);
		customer.setEmail(this.email);
		customer.setActive(this.active);
		return customer;
	}
	public void model2DTO(Customer customer) {
		this.cusId = customer.getCusId();
		
		this.cusName = customer.getGivenName() +" " + customer.getFamilyName();
		this.balance = customer.getBalance().doubleValue();
		this.cusTypeId = customer.getCusTypeId();
		if(customer.getGender().equals('M'))
		{
			this.gender = 0;
			this.sex = "Male";
		}else {
			this.gender = 1;
			this.sex = "Female";
		}
		this.classId = customer.getClassId();
		this.prCode = customer.getPrCode();
		this.cusTypeId = customer.getCusTypeId();
		this.cardId = customer.getCardId();
		this.balance = customer.getBalance().doubleValue();
		this.active = customer.isActive();
		this.nationality = customer.getNationality();
		this.address = customer.getAddress();
		this.phoneNum = customer.getPhoneNum();
		this.email = customer.getEmail();
		this.remark = customer.getRemark();
	}
}
