package com.daisyit.dto;

import java.util.List;

public class ItemListInfo {
	private ItemListDTO itemListDTO;
	private List<RtPriceDTO> listRtPrice;

	public ItemListDTO getItemListDTO() {
		return itemListDTO;
	}

	public void setItemListDTO(ItemListDTO itemListDTO) {
		this.itemListDTO = itemListDTO;
	}

	public List<RtPriceDTO> getListRtPrice() {
		return listRtPrice;
	}

	public void setListRtPrice(List<RtPriceDTO> listRtPrice) {
		this.listRtPrice = listRtPrice;
	}

}
