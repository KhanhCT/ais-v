package com.daisyit.dto;

import java.util.List;

public class OrderInfoDTO {
	private String className;
	private String teacherName;
	private List<SaleTransDTO> saleTransDTOs;
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	public List<SaleTransDTO> getSaleTransDTOs() {
		return saleTransDTOs;
	}
	public void setSaleTransDTOs(List<SaleTransDTO> saleTransDTOs) {
		this.saleTransDTOs = saleTransDTOs;
	}
	
}
