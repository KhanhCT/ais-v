package com.daisyit.dto;

public class TransferingDTO {
	private String srcCusId;
	private int srcCardId;
	private double srcBalance;
	private String dstCusId;
	private int dstCardId; 
	private double dstBalance;
	private long amount;
	public String getSrcCusId() {
		return srcCusId;
	}
	public void setSrcCusId(String srcCusId) {
		this.srcCusId = srcCusId;
	}
	public int getSrcCardId() {
		return srcCardId;
	}
	public void setSrcCardId(int srcCardId) {
		this.srcCardId = srcCardId;
	}
	public String getDstCusId() {
		return dstCusId;
	}
	public void setDstCusId(String dstCusId) {
		this.dstCusId = dstCusId;
	}
	public int getDstCardId() {
		return dstCardId;
	}
	public void setDstCardId(int dstCardId) {
		this.dstCardId = dstCardId;
	}
	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}
	public double getSrcBalance() {
		return srcBalance;
	}
	public void setSrcBalance(double srcBalance) {
		this.srcBalance = srcBalance;
	}
	public double getDstBalance() {
		return dstBalance;
	}
	public void setDstBalance(double dstBalance) {
		this.dstBalance = dstBalance;
	}
	
	
}
