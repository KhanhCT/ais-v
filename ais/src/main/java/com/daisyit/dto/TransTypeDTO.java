package com.daisyit.dto;

public class TransTypeDTO {
	Integer transTypeId;
	String transTypeCode;
	String transTypeName;
	
	
	public TransTypeDTO(Integer transTypeId, String transTypeCode, String transTypeName) {
		super();
		this.transTypeId = transTypeId;
		this.transTypeCode = transTypeCode;
		this.transTypeName = transTypeName;
	}
	/**
	 * @return the transTypeId
	 */
	public Integer getTransTypeId() {
		return transTypeId;
	}
	/**
	 * @param transTypeId the transTypeId to set
	 */
	public void setTransTypeId(Integer transTypeId) {
		this.transTypeId = transTypeId;
	}
	/**
	 * @return the transTypeCode
	 */
	public String getTransTypeCode() {
		return transTypeCode;
	}
	/**
	 * @param transTypeCode the transTypeCode to set
	 */
	public void setTransTypeCode(String transTypeCode) {
		this.transTypeCode = transTypeCode;
	}
	/**
	 * @return the transTypeName
	 */
	public String getTransTypeName() {
		return transTypeName;
	}
	/**
	 * @param transTypeName the transTypeName to set
	 */
	public void setTransTypeName(String transTypeName) {
		this.transTypeName = transTypeName;
	}
	

}
