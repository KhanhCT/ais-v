package com.daisyit.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springlib.core.utils.MappingUtil;

import com.daisyit.dto.AllCodeDTO;
import com.daisyit.dto.GradeDTO;
import com.daisyit.dto.ItemGrpDTO;
import com.daisyit.dto.ItemListDTO;
import com.daisyit.dto.ItemListInfo;
import com.daisyit.dto.PrCodeDTO;
import com.daisyit.dto.RtPriceDTO;
import com.daisyit.frontend.service.ItemService;

@RestController
@RequestMapping(value="api/v0.1/itemController")
public class ItemController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ItemController.class);

	@Autowired
	private ItemService itemService;

	@RequestMapping(value = "/newItemList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int newItemList(@RequestBody ItemListInfo dto) {
		LOGGER.info("POST newItemList() : dto = {}", MappingUtil.writeValueAsString(dto));
		return itemService.saveItemList(dto, true);
	}
	
	@RequestMapping(value = "/updateItemList", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public int updateItemList(@RequestBody ItemListInfo dto) {
		LOGGER.info("POST newItemList() : dto = {}", MappingUtil.writeValueAsString(dto));
		return itemService.saveItemList(dto,false);
	}

	@RequestMapping(value = "/newPricePolicy", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int newPricePolicy(@RequestBody PrCodeDTO dto) {
		dto.setStatus(true);
		return itemService.newPricePolicy(dto);
	}

	@RequestMapping(value = "/modifyPricePolicy", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int modifyPricePolicy(@RequestBody PrCodeDTO dto) {
		dto.setStatus(true);
		return itemService.modifyPricePolicy(dto);
	}
	
	@RequestMapping(path = "/disablePricePolicy/prCode/{prCode}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public int disablePricePolicy(@PathVariable String prCode) {
		return itemService.disablePricePolicy(prCode);
	}

	@RequestMapping(value = "/findAllItemGrp", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ItemGrpDTO> findAllItemList() {
		return itemService.findAllItemList();
	}

	@RequestMapping(value = "/findAllItemListByGrpId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ItemListDTO> findAllItemListByGrpId(@RequestParam(value = "itemGrpId") int itemGrpId) {
		return itemService.findAllItemListByGrpId(itemGrpId);
	}

	@RequestMapping(value = "/convertCurrency", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public double convertCurrency(@RequestParam(value = "rate") double rate,
			@RequestParam(value = "amount") double amount) {
		if (rate == 0)
			return -1;
		return itemService.convertCurrency(rate, amount);
	}

	@RequestMapping(value = "/getAllPriceCode", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PrCodeDTO> getAllPriceCode() {
		LOGGER.info("GET getAllPriceCode()");
		return itemService.getAllPriceCode();
	}

	@RequestMapping(value = "/getLstPriceUnit", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AllCodeDTO> getLstPriceUnit() {
		LOGGER.info("GET getLstPriceUnit()");
		return itemService.getLstPriceUnit();
	}

	@RequestMapping(value = "/findItemtLstBySkuCode", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ItemListInfo findItemtLstBySkuCode(@RequestParam String skuCode) {
		LOGGER.info("GET findItemtLstBySkuCode() : skuCode = {}", skuCode);
		return itemService.findItemtLstBySkuCode(skuCode);
	}
	
	@RequestMapping(value = "/findRtPrice", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public RtPriceDTO findRtPrice(@RequestParam String prCode, @RequestParam Integer skuId) {
		LOGGER.info("GET findRtPrice() : prCode = {}, skuId = {}", prCode, skuId);
		return itemService.findRtPrice(prCode, skuId);
	}
	@RequestMapping(value = "/updatePricePolicy", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int updatePricePolicy(@RequestBody List<GradeDTO> dtos) {
		return itemService.updatePricePolicyForGrade(dtos);
	}
	
	@RequestMapping(value = "/getCurrencyList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AllCodeDTO> getCurrencyList() {
		return itemService.getCurrencyList();
	}
}
