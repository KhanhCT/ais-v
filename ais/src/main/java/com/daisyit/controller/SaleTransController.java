package com.daisyit.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springlib.core.utils.CommonKey;

import com.daisyit.dto.CusTransDTO;
import com.daisyit.dto.CustomerDTO;
import com.daisyit.dto.LocationDTO;
import com.daisyit.dto.ReportDTO;
import com.daisyit.dto.SaleTransDTO;
import com.daisyit.dto.TransTypeDTO;
import com.daisyit.frontend.service.SaleTransService;

@RestController
@RequestMapping("/api/v0.1/saleTrans")
public class SaleTransController {

	@Autowired
	private SaleTransService saleTransService;

	@RequestMapping(value = "newSaleTrans", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<SaleTransDTO> newSaletrans(HttpSession session, @RequestBody List<SaleTransDTO> dtos) {
		int userId = (int) session.getAttribute(CommonKey.USER_ID);
		return saleTransService.newSaleTransaction(dtos, userId);
	}

	@RequestMapping(value = "getHistoryByCusId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<SaleTransDTO> getHistoryByCusId(@RequestParam(value = "customerId") int customerId,
			@RequestParam(value = "fromDate") String fromDate, @RequestParam(value = "toDate") String toDate) {
		return saleTransService.getHistoryByCusId(fromDate, toDate, customerId);
	}

	@RequestMapping(value = "getStudentOrderInforFromFile", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<SaleTransDTO> getStudentOrderInforFromFile(@RequestParam(value = "fileUrl") String fileUrl) {
		return saleTransService.processStudentOrderFromFile(fileUrl);
	}
	@RequestMapping(value = "newSaleTransFromFiles", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<SaleTransDTO> newSaleTransFromFiles(HttpSession session, @RequestBody List<SaleTransDTO> dtos, String transDate) {
		int userId = (int) session.getAttribute(CommonKey.USER_ID);
		return saleTransService.newSaleTransaction(dtos, userId);
	}
	
	@RequestMapping(value = "initStudentOrders", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<SaleTransDTO> initStudentOrders(@RequestParam(value="classId") int classId) {
		return saleTransService.initStudentOrders(classId);
	}
	
	@RequestMapping(value = "getTransactionList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ReportDTO<List<SaleTransDTO>>> getTransactionList(
			@RequestParam(value = "fromDate") String fromDate, @RequestParam(value = "toDate") String toDate) {
		return saleTransService.getTransactionList(fromDate, toDate);
	}
	
	@RequestMapping(value = "getCardTransHistory", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ReportDTO<List<SaleTransDTO>> getCardTransHistory(@RequestParam(value = "fromDate") String fromDate, @RequestParam(value = "toDate") String toDate , 
			@RequestParam(value = "cusId") String custId) 
	{
		return saleTransService.getCardTransHistory(fromDate, toDate, custId);
	}
	
	@RequestMapping(value = "getCardBalance", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ReportDTO<List<CustomerDTO>>> getCardBalance() 
	{
		return saleTransService.getCardBalance();
	}
	
	@RequestMapping(value = "processTopUpList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CusTransDTO> processTopUpList(@RequestBody List<CusTransDTO> dtos, HttpSession session) 
	{
		return saleTransService.processTopUpList(dtos  , (int) session.getAttribute(CommonKey.USER_ID));
	}
	
	@RequestMapping(value = "pay", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int pay(@RequestBody List<SaleTransDTO> dtos, HttpSession session) 
	{
		return saleTransService.pay(dtos, (int) session.getAttribute(CommonKey.USER_ID));
	}
	@RequestMapping(value = "findAllCanteens", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<LocationDTO> findAllCanteens() 
	{
		return saleTransService.findAllCanteens();
	}
	
	@RequestMapping(value = "getSaleTransHistory", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ReportDTO<List<SaleTransDTO>> getSaleTransHistory(@RequestParam(value = "customerId") String customerId,
	@RequestParam(value = "fromDate") String fromDate, @RequestParam(value = "toDate") String toDate)
	{
		return saleTransService.getSaleTransHistory(customerId, fromDate, toDate);
	}

	@RequestMapping(value = "getAllTransType", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<TransTypeDTO> getAllTransType()
	{
		return saleTransService.getAllTransationType();
	}
}
