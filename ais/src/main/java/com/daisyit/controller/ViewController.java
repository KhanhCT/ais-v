package com.daisyit.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/")
public class ViewController {
    @RequestMapping("login")
    public ModelAndView login()
    {
        ModelAndView m = new ModelAndView("loginPage");
        return m;
    }
    /***************Order**************************/
   /* @RequestMapping("customerOrder")
    public ModelAndView customerOrder()
    {
        ModelAndView m = new ModelAndView("customerOrder");
        m.addObject("menuObj","order");
        return m;
    }
    @RequestMapping("orderFromFile")
    public ModelAndView addOrderFromFile()
    {
        ModelAndView m = new ModelAndView("orderFromFile");
        m.addObject("menuObj","order");
        return m;
    }
    @RequestMapping("studentOrder")
    public ModelAndView studentOrder()
    {
        ModelAndView m = new ModelAndView("studentOrder");
        m.addObject("menuObj","order");
        return m;
    }*/
    /***************Item**************************/
   /* @RequestMapping("itemMaster")
    public ModelAndView itemMaster()
    {
        ModelAndView m = new ModelAndView("itemMaster");
        m.addObject("menuObj","item");
        return m;
    }
    @RequestMapping("newPriceCode")
    public ModelAndView newPriceCode()
    {
        ModelAndView m = new ModelAndView("newPriceCode");
        m.addObject("menuObj","item");
        return m;
    }*/
    /***************User**************************/
    @RequestMapping("registerUser")
    public ModelAndView registerUser()
    {
        ModelAndView m = new ModelAndView("registerUser");
        m.addObject("menuObj","user");
        return m;
    }

    @RequestMapping("userManagement")
    public ModelAndView userManagement()
    {
        ModelAndView m = new ModelAndView("userManagement");
        m.addObject("menuObj","user");
        return m;
    }
    /***************Report**************************/
    @RequestMapping("transactionListReport")
    public ModelAndView transactionListReport()
    {
        ModelAndView m = new ModelAndView("transactionListReport");
        m.addObject("menuObj","report");
        return m;
    }

   /* @RequestMapping("balanceReport")
    public ModelAndView balanceReport()
    {
        ModelAndView m = new ModelAndView("balanceReport");
        m.addObject("menuObj","report");
        return m;
    }*/

    /*@RequestMapping("consumptionSumaryReport")
    public ModelAndView consumptionSumaryReport()
    {
        ModelAndView m = new ModelAndView("consumptionSumaryReport");
        m.addObject("menuObj","report");
        return m;
    }*/

    @RequestMapping("cardBalance")
    public ModelAndView cardBalance()
    {
        ModelAndView m = new ModelAndView("cardBalance");
        m.addObject("menuObj","report");
        return m;
    }
    @RequestMapping("cardTransHisReport")
    public ModelAndView cardTransHisReport()
    {
        ModelAndView m = new ModelAndView("cardTransHisReport");
        m.addObject("menuObj","report");
        return m;
    }
    
    /***************Customer**************************/
   /* @RequestMapping("registerCustomer")
    public ModelAndView registerCustomer()
    {
        ModelAndView m = new ModelAndView("registerCustomer");
        m.addObject("menuObj","customer");
        return m;
    }
    @RequestMapping("registerCustomerFromFile")
    public ModelAndView registerCustomerFromFile()
    {
        ModelAndView m = new ModelAndView("registerCustomerFromFile");
        m.addObject("menuObj","customer");
        return m;
    }
    @RequestMapping("transferMoney")
    public ModelAndView transferMoney()
    {
        ModelAndView m = new ModelAndView("transferMoney");
        m.addObject("menuObj","customer");
        return m;
    }*/
    @RequestMapping("uploadTopUpList")
    public ModelAndView uploadTopUpList()
    {
        ModelAndView m = new ModelAndView("uploadTopUpList");
        m.addObject("menuObj","customer");
        return m;
    }
    
   /* @RequestMapping("updateCardId")
    public ModelAndView updateCardId()
    {
        ModelAndView m = new ModelAndView("updateCardId");
        m.addObject("menuObj","customer");
        return m;
    }
    
    @RequestMapping("studentManagement")
    public ModelAndView studentManagement()
    {
        ModelAndView m = new ModelAndView("studentManagement");
        m.addObject("menuObj","customer");
        return m;
    }*/
    /***************system**************************/
    @RequestMapping("syncStudentList")
    public ModelAndView syncStudentList()
    {
        ModelAndView m = new ModelAndView("syncStudentList");
        m.addObject("menuObj","system");
        return m;
    }
    /*@RequestMapping("viewCampusList")
    public ModelAndView viewCampusList()
    {
        ModelAndView m = new ModelAndView("viewCampusList");
        m.addObject("menuObj","system");
        return m;
    }*/
    /***************system**************************/
    @RequestMapping("errorPage")
    public ModelAndView errorPage()
    {
        /*
        * TODO CHECK VALID SESSION OF ALL VIEW CONTROLLER AND REDIRECT TO ERROR
        * */
        ModelAndView m = new ModelAndView("errorPage");
        return m;
    }
    
    
    @RequestMapping("dailyMeals")
    public ModelAndView mealToday()
    {
        ModelAndView m = new ModelAndView("dailyMeals");
        m.addObject("menuObj","meal");
        return m;
    }
    @RequestMapping("homePage")
    public ModelAndView homePage()
    {
        ModelAndView m = new ModelAndView("homePage");
        m.addObject("menuObj","home");
        return m;
    }
    @RequestMapping("upload")
    public ModelAndView upload()
    {
        ModelAndView m = new ModelAndView("upload");
        m.addObject("menuObj","System");
        return m;
    }
}
