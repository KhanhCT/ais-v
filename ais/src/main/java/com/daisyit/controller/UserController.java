package com.daisyit.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.daisyit.dto.UserDTO;
import com.daisyit.dto.UserRoleDTO;
import com.daisyit.frontend.service.UserService;

@RestController
@RequestMapping("/api/v0.1/userController")
public class UserController {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "getLstUserActive", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UserDTO> getActiveUserList() {
		LOGGER.info("GET getLstUserActive()");
		return userService.findAll();
	}
	@RequestMapping(value = "getAllUserRoles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UserRoleDTO> getAllUserRoles()
	{
		return userService.getAllUserRoles();
	}
	@RequestMapping(value = "newUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int newUser(@RequestBody UserDTO dto)
	{
		return userService.newUser(dto);
	}
}
