package com.daisyit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.daisyit.frontend.service.CustomerService;

@Configuration
@EnableScheduling
public class ScheduleTask {
	
	@Autowired
	private CustomerService customerService;
	
	//@Scheduled(cron = "00 30 9 * * *")
	@Scheduled(cron = "*/10800 * * * * *")
	// @Scheduled(fixedRate = 6000)
	public void startSchedule() {
		System.out.println("11111111111111111");
		//customerService.updateStudentList();
	}
}
