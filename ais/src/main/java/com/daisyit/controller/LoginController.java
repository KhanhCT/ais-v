package com.daisyit.controller;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springlib.core.utils.CommonKey;
import org.springlib.core.utils.Utils;

import com.daisyit.dto.UserDTO;
import com.daisyit.frontend.service.CustomerService;
import com.daisyit.frontend.service.UserService;
import com.daisyit.utils.ConstantKey;
import com.springlib.core.dto.TokenInfo;

@Controller
@RequestMapping("/")
public class LoginController {

	@Autowired
	private UserService userService;
	@Autowired
	private CustomerService customerService;
	
	@RequestMapping(value = "authentication", method = RequestMethod.POST)
	public ModelAndView doAuthentication(@RequestParam("username") String username,
			@RequestParam("password") String password, HttpServletRequest request) {
		request.getSession().removeAttribute(ConstantKey.ACCESS_TOKEN);
		request.getSession().removeAttribute(ConstantKey.REFRESH_TOKEN);
		request.getSession().removeAttribute(ConstantKey.PARENT_ID);
		request.getSession().removeAttribute(ConstantKey.ROLE.NAME);
		TokenInfo tokenInfo = userService.getTokenInfo(username, password);
		if (tokenInfo != null ) {
			request.getSession().setAttribute(ConstantKey.ACCESS_TOKEN, tokenInfo.getAccessToken());
			request.getSession().setAttribute(ConstantKey.REFRESH_TOKEN, tokenInfo.getRefreshToken());
			request.getSession().setAttribute(ConstantKey.PARENT_ID, tokenInfo.getParentID());
			request.getSession().setAttribute(ConstantKey.ROLE.NAME, ConstantKey.ROLE.ROLE_PARENT);
			ModelAndView model = new ModelAndView("redirect:dailyMeals");
			return model;
		} else {
			String _password = Utils.Base64Encoder(password);
			UserDTO userDTO = userService.getUserByNameAndPass(username, _password);
			if(userDTO == null)
			{
				ModelAndView model = new ModelAndView("loginPage");
				model.addObject("message", "error");
				return model;
			}else {
				request.getSession().setAttribute(ConstantKey.ROLE.NAME, ConstantKey.ROLE.ROLE_ADMIN);
				request.getSession().setAttribute(CommonKey.USER_ID, userDTO.getUserId());
				ModelAndView model = new ModelAndView("redirect:dailyMeals");
				return model;
			}
		}
	}
	
	@RequestMapping(value = "api/v0.1/login", method = RequestMethod.POST)
	public ModelAndView login(@RequestParam("accessToken") String accessToken,
			@RequestParam("refreshToken") String refreshToken, @RequestParam("parentID") Integer parentId, HttpServletRequest request) {
		request.getSession().removeAttribute(ConstantKey.ACCESS_TOKEN);
		request.getSession().removeAttribute(ConstantKey.REFRESH_TOKEN);
		request.getSession().removeAttribute(ConstantKey.PARENT_ID);
		request.getSession().removeAttribute(ConstantKey.ROLE.NAME);		
		if (refreshToken != null && accessToken !=null  && parentId !=null) {
			if(customerService.verifyToken(parentId, accessToken, refreshToken))
			{
				request.getSession().setAttribute(ConstantKey.ACCESS_TOKEN, accessToken);
				request.getSession().setAttribute(ConstantKey.REFRESH_TOKEN, refreshToken);
				request.getSession().setAttribute(ConstantKey.PARENT_ID, parentId);
				request.getSession().setAttribute(ConstantKey.ROLE.NAME, ConstantKey.ROLE.ROLE_PARENT);
				ModelAndView model = new ModelAndView("redirect:dailyMeals");
				return model;
			}else {
				ModelAndView model = new ModelAndView("redirect:https://services.aisvietnam.com/");
				model.addObject("message", "error");
				return model;
			}			
		} else {
			ModelAndView model = new ModelAndView("redirect:https://services.aisvietnam.com/");
			model.addObject("message", "error");
			return model;
		}
	}

	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest request) {
		String role = (String) request.getSession().getAttribute(ConstantKey.ROLE.NAME);
		request.getSession().removeAttribute(ConstantKey.ACCESS_TOKEN);
		request.getSession().removeAttribute(ConstantKey.REFRESH_TOKEN);
		request.getSession().removeAttribute(ConstantKey.PARENT_ID);
		request.getSession().removeAttribute(ConstantKey.ROLE.NAME);
		if(role.equals(ConstantKey.ROLE.ROLE_ADMIN))
			return new ModelAndView("loginPage");
		else
			return new ModelAndView("redirect:https://services.aisvietnam.com/");
	}
	@RequestMapping(value = "api/v0.1/logout", method = RequestMethod.GET)
	public void ExeLogout(HttpServletRequest request) {
		request.getSession().removeAttribute(ConstantKey.ACCESS_TOKEN);
		request.getSession().removeAttribute(ConstantKey.REFRESH_TOKEN);
		request.getSession().removeAttribute(ConstantKey.PARENT_ID);
		request.getSession().removeAttribute(ConstantKey.ROLE.NAME);
	}

}
