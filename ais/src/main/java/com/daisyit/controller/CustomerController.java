package com.daisyit.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springlib.core.utils.CommonKey;

import com.daisyit.dto.CommonDTO;
import com.daisyit.dto.CustomerDTO;
import com.daisyit.dto.GradeDTO;
import com.daisyit.dto.ResponseDTO;
import com.daisyit.dto.TransferingDTO;
import com.daisyit.frontend.service.CustomerService;
import com.daisyit.utils.ConstantKey;
import com.springlib.core.dto.CampusDTO;

@RestController
@RequestMapping(value="/api/v0.1/customerController")
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	
	@RequestMapping(value = "newCustomer", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int newCustomer(HttpSession session, @RequestBody CustomerDTO dto) {		
		return customerService.newCustomer(dto);
	}
	@RequestMapping(value = "newCustomerList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CustomerDTO> newCustomerList(HttpSession session, @RequestBody List<CustomerDTO> dtos) {		
		return customerService.newCustomerList(dtos);
	}
	@RequestMapping(value = "getCommonDTO", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public CommonDTO getCommonDTO() {
		return customerService.getCommonDTO();
	}
	@RequestMapping(value = "updateStudentList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CustomerDTO> updateStudent() {
		return customerService.updateStudentList();
	}
	@RequestMapping(value = "findCustomerById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public CustomerDTO findCustomerById(@RequestParam(value="cusId") String cusId) {
		return customerService.findCustomerById(cusId);
	}
	@RequestMapping(value = "transferMoney", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int transferMoney(@RequestBody TransferingDTO dto, HttpSession session) {
		return customerService.transferMoney(dto, (int) session.getAttribute(CommonKey.USER_ID));
	}
	
	@RequestMapping(value = "getCampusList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CampusDTO> getCampusList() {
		return customerService.getCampusList();
	}

	@RequestMapping(value = "updateCampusEnv", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int updateCampusEnv(@RequestBody CampusDTO dto) {
		return customerService.updateCampusEnv(dto);
	}
	
	@RequestMapping(value = "getGradeList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GradeDTO> getGradeList() {
		return customerService.getGradeList();
	}
	@RequestMapping(value = "findCustomerByCardCode", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public CustomerDTO findCustomerByCardCode(@RequestParam(value="cardCode") String cardCode) {
		return customerService.findCustomerByCardCode(cardCode);
	}
	
	@RequestMapping(value = "findByClassId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CustomerDTO> findByClassId(@RequestParam(value="classId") Integer classId, @RequestParam String type) {
		return customerService.getListByClass(classId, type);
	}
	
	@RequestMapping(value = "updateCardId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO updateCardId(@RequestBody List<CustomerDTO> dtos) {
		return customerService.updateCardId(dtos);
	}
	
	@RequestMapping(value = "disableStudent", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO disableStudent(@RequestBody List<CustomerDTO> dtos) {
		return customerService.disableStudent(dtos);
	}
	@RequestMapping(value="/getChildrentByParentId", method=RequestMethod.GET, produces= MediaType.APPLICATION_JSON_VALUE)
	public List<CustomerDTO> getChildrentByParentId(HttpSession session, HttpServletResponse response, HttpServletRequest request)
	{
		String accessToken = session.getAttribute(ConstantKey.ACCESS_TOKEN).toString();
		String refreshToken = session.getAttribute(ConstantKey.REFRESH_TOKEN).toString();
		int parentId = (int) session.getAttribute(ConstantKey.PARENT_ID);
		List<CustomerDTO> customers = customerService.getAllChildrentByParentId(parentId, accessToken, refreshToken);
		return customers;
	}
	
}
