package com.daisyit.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springlib.core.utils.CommonKey;

import com.daisyit.backend.dao.SysvarDAO;
import com.daisyit.dto.CusTransDTO;
import com.daisyit.dto.CustomerDTO;
import com.daisyit.dto.SaleTransDTO;
import com.daisyit.frontend.service.CustomerService;
import com.daisyit.frontend.service.FileService;
import com.daisyit.frontend.service.SaleTransService;
import com.daisyit.utils.Constant;
import com.daisyit.utils.ConstantKey;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping(value = "/api/v0.1/fileController")
public class FileUploadController {
	private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadController.class);
	@Autowired
	private CustomerService customerService;
	@Autowired	
	private SaleTransService saleTransService;
	@Autowired
	private SysvarDAO sysvarDAO;
	@Autowired
	private FileService fileService;
	
	@RequestMapping(value = "uploadCustomerList", method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<CustomerDTO> uploadCustomerList(@RequestParam("file") MultipartFile file) {
		String fileName = null;
		String uploadDir = sysvarDAO.getValueFromName("m_UploadDir");
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				fileName = file.getOriginalFilename();
				// Creating the directory to store file
				File dir = new File(uploadDir);
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(uploadDir + File.separator + file.getOriginalFilename());
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				LOGGER.info("Server File Location=" + serverFile.getAbsolutePath());
				List<CustomerDTO>  errorList = customerService.insertCustomerFromFile(serverFile.getAbsolutePath());
				serverFile.delete();
				return errorList;
			} catch (Exception e) {
				
				LOGGER.error("You failed to upload " + fileName + " => " + e.getMessage());
				return null;
			}
		} else {
			LOGGER.error("failed to upload " + fileName + " because the file was empty.");
			return null;
		}
	}
	
	@RequestMapping(value = "uploadOrderList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<SaleTransDTO> uploadOrderList(@RequestParam("file") MultipartFile file) {
		String fileName = null;
		String uploadDir = sysvarDAO.getValueFromName(ConstantKey.UPLOAD_FOLDER);
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				fileName = file.getOriginalFilename();

				// Creating the directory to store file
				File dir = new File(uploadDir);
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(uploadDir + File.separator + file.getOriginalFilename());
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				LOGGER.info("Server File Location=" + serverFile.getAbsolutePath());
				List<SaleTransDTO> orderList = saleTransService.processStudentOrderFromFile(serverFile.getAbsolutePath());
				serverFile.delete();
				return orderList;
			} catch (Exception e) {
				
				LOGGER.error("You failed to upload " + fileName + " => " + e.getMessage());
				return null;
			}
		} else {
			LOGGER.error("failed to upload " + fileName + " because the file was empty.");
			return null;
		}
	}
	
	@RequestMapping(value = "uploadTopUpList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CusTransDTO> uploadTopUpList(@RequestParam("file") MultipartFile file,HttpSession session) {
		String fileName = null;
		String uploadDir = sysvarDAO.getValueFromName(ConstantKey.UPLOAD_FOLDER);
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				fileName = file.getOriginalFilename();

				// Creating the directory to store file
				File dir = new File(uploadDir);
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(uploadDir + File.separator + file.getOriginalFilename());
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();
				LOGGER.info("Server File Location=" + serverFile.getAbsolutePath());		
				List<CusTransDTO> orderList = saleTransService.processToUpList(serverFile.getAbsolutePath(), (int) session.getAttribute(CommonKey.USER_ID));
				serverFile.delete();
				return orderList;
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error("You failed to upload " + fileName + " => " + e.getMessage());
				return null;
			}
		} else {
			LOGGER.error("Failed to upload " + fileName + " because the file was empty.");
			return null;
		}
	}
	@RequestMapping(value = "uploadCardList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CustomerDTO> uploadCardList(@RequestParam("file") MultipartFile file,HttpSession session) {
		String fileName = null;
		String uploadDir = sysvarDAO.getValueFromName(ConstantKey.UPLOAD_FOLDER);
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				fileName = file.getOriginalFilename();

				// Creating the directory to store file
				File dir = new File(uploadDir);
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(uploadDir + File.separator + file.getOriginalFilename());
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();
				LOGGER.info("Server File Location=" + serverFile.getAbsolutePath());
				List<CustomerDTO> errorList = customerService.processCardList(serverFile.getAbsolutePath());
				serverFile.delete();
				return errorList;
			} catch (IOException e) {
				
				LOGGER.error("You failed to upload " + fileName + " => " + e.getMessage());
				return null;
			}
		} else {
			LOGGER.error("Failed to upload " + fileName + " because the file was empty.");
			return null;
		}
	}


	
	@RequestMapping(value = "getImageDir", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getImageDir()
	{
		return fileService.getImageDir();
	}
	@RequestMapping(value = "findAllImageFiles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<String> findAllImageFiles()
	{
		return fileService.findAllImageFiles();
	}
	@RequestMapping(value = "deleteImageFileByName", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public boolean deleteImageFileByName(@RequestParam(value = "imageName") String imageName)
	{
		return fileService.deleteImageFileByName(imageName);
	}
	
	@RequestMapping(value = "findAllImageFilePath", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<String> findAllImageFilePath()
	{
		return fileService.findAllImageFilePath();
	}
	
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Integer uploadFileHandler(@RequestParam("file") MultipartFile file) {
		String fileName = null;
		String uploadDir = sysvarDAO.getValueFromName(ConstantKey.IMAGE_DIR);
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				fileName = file.getOriginalFilename();

				// Creating the directory to store file
				File dir = new File(uploadDir);
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(uploadDir + File.separator + file.getOriginalFilename());
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				LOGGER.info("Server File Location=" + serverFile.getAbsolutePath());

				return CommonKey.RESPONSE_CODE.SUCCESS;
			} catch (Exception e) {
				
				LOGGER.error("You failed to upload " + fileName + " => " + e.getMessage());
				return CommonKey.RESPONSE_CODE.ERROR;
			}
		} else {
			LOGGER.error("failed to upload " + fileName + " because the file was empty.");
			return CommonKey.RESPONSE_CODE.ERROR;
		}
	}

	@RequestMapping(value = "/uploadMultipleFile", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public  List<String> uploadMultipleFileHandler(@RequestParam("file") MultipartFile[] files) {
		String uploadDir = sysvarDAO.getValueFromName(ConstantKey.IMAGE_DIR);
		if(uploadDir == null)
		{
			uploadDir = ConstantKey.DEFAULE_IMAGE_DIR;
		}
		List<String> errorFiles = new ArrayList<>();
		for (int i = 0; i < files.length; i++) {
			MultipartFile file = files[i];
			String fileName = file.getOriginalFilename();
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				File dir = new File(uploadDir);
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath() + File.separator + fileName);
				if(serverFile.exists())
					continue;
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();
				LOGGER.info("Server File Location=" + serverFile.getAbsolutePath());
			} catch (Exception e) {
				LOGGER.error("You failed to upload " + fileName + " => " + e.getMessage());
				errorFiles.add(fileName);
			}
		}
		return errorFiles;
	}
	
	
}
