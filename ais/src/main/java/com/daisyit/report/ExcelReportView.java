package com.daisyit.report;

import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.servlet.view.document.AbstractXlsView;
import org.springlib.core.utils.CommonKey;
import org.springlib.core.utils.DateConverter;

import com.daisyit.dto.CustomerDTO;
import com.daisyit.dto.ReportDTO;
import com.daisyit.dto.SaleTransDTO;

public class ExcelReportView extends AbstractXlsView {
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String nameFile = (String) model.get(CommonKey.REPORT_KEY.NAME);
		response.setHeader("Content-Disposition", "attachment; filename=\"" + nameFile + ".xls\"");

		Sheet sheet = workbook.createSheet();
		
		// FileInputStream obtains input bytes from the image file
		ClassPathResource resource = new ClassPathResource("Logo-Caterers.png");
		InputStream inputStream = resource.getInputStream();
		// Get the contents of an InputStream as a byte[].
		byte[] bytes = IOUtils.toByteArray(inputStream);
		// Adds a picture to the workbook
		int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
		// close the input stream
		inputStream.close();
		// Returns an object that handles instantiating concrete classes
		CreationHelper helper = workbook.getCreationHelper();
		// Creates the top-level drawing patriarch.
		Drawing drawing = sheet.createDrawingPatriarch();
		// Create an anchor that is attached to the worksheet
		ClientAnchor anchor = helper.createClientAnchor();
		// set top-left corner for the image
		anchor.setCol1(0);
		anchor.setRow1(0);
		// Creates a picture
		Picture pict = drawing.createPicture(anchor, pictureIdx);
		String contentExport = (String) model.get("Content");
		if (contentExport.equals(CommonKey.REPORT_KEY.TRANSACTION_LIST)) {
			rpTransactionList(sheet, workbook, pict, model);
		} else if (contentExport.equals(CommonKey.REPORT_KEY.CARD_BALANCE)) {
			rpCardBalance(sheet, workbook, pict, model);
		} else if (contentExport.equals(CommonKey.REPORT_KEY.CARD_TRANSACTION_HISTORY)) {
			rpCardTransaction(sheet, workbook, pict, model);
		}
	}
	
	private void rpTransactionList (Sheet sheet, Workbook workbook, Picture pict, Map<String, Object> model) {
		String fromDate = (String) model.get("fromDate");
		String toDate = (String) model.get("toDate");
		List<ReportDTO<List<SaleTransDTO>>> listData = (List<ReportDTO<List<SaleTransDTO>>>) model.get("data");
		
		pict.resize(2.5, 2.0);
		sheet.setColumnWidth(0, 16 * 256);
		sheet.setColumnWidth(1, 7 * 256);
		sheet.setColumnWidth(2, 20 * 256);
		sheet.setColumnWidth(3, 7 * 256);
		sheet.setColumnWidth(4, 10 * 256);
		sheet.setColumnWidth(5, 15 * 256);
		sheet.setColumnWidth(6, 15 * 256);
		sheet.setColumnWidth(7, 25 * 256);
		
		CellStyle styleHeader = cellHeader(workbook);
		Font fontHeader = fontHeader(workbook);
		CellStyle styleBold = cellStyleBold(workbook);
		CellStyle styleHeaderTable = cellStyleHeaderTable(workbook);
		
		Row rowHeader1 = sheet.createRow(0);
		rowHeader1.setHeight((short) (rowHeader1.getHeight() * 4));
		Cell cellHeader = rowHeader1.createCell(0);
		HSSFRichTextString richString = new HSSFRichTextString( "TRANSACTION LIST\nFrom : " + fromDate + " to " + toDate );
		richString.applyFont( 0, 16, fontHeader );
		cellHeader.setCellValue(richString);
		cellHeader.setCellStyle(styleHeader);
		sheet.addMergedRegion(new CellRangeAddress(0, 2, 0, 7));
		
		Row rowInfo = sheet.createRow(3);
		Cell cellInfo = rowInfo.createCell(0);
		cellInfo.setCellValue("Date");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(1);
		cellInfo.setCellValue("Time");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(2);
		cellInfo.setCellValue("Service number");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(3);
		cellInfo.setCellValue("Type");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(4);
		cellInfo.setCellValue("Quantity");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(5);
		cellInfo.setCellValue("Unit price");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(6);
		cellInfo.setCellValue("Amount");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(7);
		cellInfo.setCellValue("Item name");
		cellInfo.setCellStyle(styleHeaderTable);
		
		int temp = 0;
		if (listData != null && !listData.isEmpty()) {
			for (int i = 0; i < listData.size(); i++) {
				ReportDTO<List<SaleTransDTO>> report = listData.get(i);
				List<SaleTransDTO> listSale = report.getData();
				rowInfo = sheet.createRow(4 + temp);
				cellInfo = rowInfo.createCell(0);
				cellInfo.setCellValue(
						"Số thẻ/Card ID: (" + report.getCardId().trim() + ") " + report.getCusName().trim());
				cellInfo.setCellStyle(styleBold);
				sheet.addMergedRegion(new CellRangeAddress(4 + temp, 4 + temp, 0, 7));
				for (int j = 0; j < listSale.size(); j++) {
					SaleTransDTO sale = listSale.get(j);
					rowInfo = sheet.createRow(5 + temp + j);
					cellInfo = rowInfo.createCell(0);
					cellInfo.setCellValue(sale.getTransDate());
					cellInfo = rowInfo.createCell(1);
					cellInfo.setCellValue(sale.getTransTime());
					cellInfo = rowInfo.createCell(2);
					cellInfo.setCellValue(sale.getTransNum());
					cellInfo = rowInfo.createCell(3);
					cellInfo.setCellValue(sale.getTransType());
					cellInfo = rowInfo.createCell(4);
					cellInfo.setCellValue(sale.getQty());
					cellInfo = rowInfo.createCell(5);
					cellInfo.setCellValue(sale.getRtPrice());
					cellInfo = rowInfo.createCell(6);
					cellInfo.setCellValue(sale.getAmount());
					cellInfo = rowInfo.createCell(7);
					cellInfo.setCellValue(sale.getSkuName());
				}
				Row rowTotal = sheet.createRow(5 + temp + listSale.size());
				rowTotal.setHeight((short) (rowTotal.getHeight() * 1.5));
				cellInfo = rowTotal.createCell(0);
				cellInfo.setCellValue("Total by card ID");
				sheet.addMergedRegion(new CellRangeAddress(5 + temp + listSale.size(), 5 + temp + listSale.size(), 0, 3));
				cellInfo = rowTotal.createCell(4);
				cellInfo.setCellValue(report.getTotalQty());
				cellInfo.setCellStyle(styleBold);
				cellInfo = rowTotal.createCell(6);
				cellInfo.setCellValue(report.getTotalAmount());
				cellInfo.setCellStyle(styleBold);
				temp += listSale.size() + 2;
			}
		}
	}
	
	private void rpCashBalance (Sheet sheet, Workbook workbook, Picture pict, Map<String, Object> model) {
		List<ReportDTO<List<CustomerDTO>>> lstBalance = (List<ReportDTO<List<CustomerDTO>>>) model.get("data");
		Date currentDate = new Date();
		String printedDate = DateConverter.convertDateStringByFormatLocal(currentDate, CommonKey.DATE_FORMAT.DATE_SLASH_FORMAT);
		
		pict.resize(1.7, 2.0);
		sheet.setColumnWidth(0, 5 * 256);
		sheet.setColumnWidth(1, 40 * 256);
		sheet.setColumnWidth(2, 15 * 256);
		sheet.setColumnWidth(3, 15 * 256);
		sheet.setColumnWidth(4, 15 * 256);
		sheet.setColumnWidth(5, 15 * 256);
		sheet.setColumnWidth(6, 40 * 256);
		
		CellStyle styleHeader = workbook.createCellStyle();
		Font fontHeader1 = workbook.createFont();
		styleHeader.setAlignment(HorizontalAlignment.CENTER);
		styleHeader.setVerticalAlignment(VerticalAlignment.CENTER);
		styleHeader.setWrapText(true);
		fontHeader1.setBold(true);
		fontHeader1.setFontHeightInPoints((short) 13);
		
		CellStyle styleCardId = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setBold(true);
		styleCardId.setFont(font);
		
		CellStyle styleHeaderTable = workbook.createCellStyle();
		styleHeaderTable.setAlignment(HorizontalAlignment.CENTER);
		styleHeaderTable.setVerticalAlignment(VerticalAlignment.CENTER);
		
		Row rowHeader1 = sheet.createRow(0);
		rowHeader1.setHeight((short) (rowHeader1.getHeight() * 3));
		Cell cellHeader = rowHeader1.createCell(0);
		HSSFRichTextString richString = new HSSFRichTextString( "CASH BALANCE\nPrinted date: " + printedDate);
		richString.applyFont( 0, 12, fontHeader1 );
		cellHeader.setCellValue(richString);
		cellHeader.setCellStyle(styleHeader);
		sheet.addMergedRegion(new CellRangeAddress(0, 2, 0, 6));
		
		Row rowInfo = sheet.createRow(3);
		Cell cellInfo = rowInfo.createCell(0);
		cellInfo.setCellValue("No.");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(1);
		cellInfo.setCellValue("Name");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(2);
		cellInfo.setCellValue("Opening Amount");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(3);
		cellInfo.setCellValue("Addtional top up");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(4);
		cellInfo.setCellValue("Consumpsion");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(5);
		cellInfo.setCellValue("Balance");
		cellInfo.setCellStyle(styleCardId);
		cellInfo = rowInfo.createCell(6);
		cellInfo.setCellValue("Remark");
		cellInfo.setCellStyle(styleHeaderTable);
		
		int temp = 0;
		if (lstBalance != null && !lstBalance.isEmpty()) {
			for (int i = 0; i < lstBalance.size(); i++) {
				ReportDTO<List<CustomerDTO>> report = lstBalance.get(i);
				List<CustomerDTO> listCus = report.getData();
				
				rowInfo = sheet.createRow(4 + temp);
				cellInfo = rowInfo.createCell(0);
				cellInfo.setCellValue("Class/Title: " + String.valueOf(report.getClassId()) + " - " + report.getClassName());
				cellInfo.setCellStyle(styleCardId);
				sheet.addMergedRegion(new CellRangeAddress(4 + temp, 4 + temp, 0, 6));
				for (int j = 0; j < listCus.size(); j++) {
					CustomerDTO cus = listCus.get(j);
					rowInfo = sheet.createRow(5 + temp + j);
					cellInfo = rowInfo.createCell(0);
					cellInfo.setCellValue(j + 1);
					cellInfo = rowInfo.createCell(1);
					cellInfo.setCellValue(cus.getCusName());
					cellInfo = rowInfo.createCell(2);
					//TODO
					cellInfo.setCellValue("");
					cellInfo = rowInfo.createCell(3);
					cellInfo.setCellValue("Addtional top up");
					cellInfo = rowInfo.createCell(4);
					cellInfo.setCellValue("Consumpsion");
					cellInfo = rowInfo.createCell(5);
					cellInfo.setCellValue("Balance");
					cellInfo = rowInfo.createCell(6);
					cellInfo.setCellValue("Remark");
				}
				Row rowTotal = sheet.createRow(5 + temp + listCus.size());
				rowTotal.setHeight((short) (rowTotal.getHeight() * 1.5));
				cellInfo = rowTotal.createCell(0);
				cellInfo.setCellValue("Total by Class/Title: ");
				sheet.addMergedRegion(new CellRangeAddress(5 + temp, 5 + temp, 0, 2));
				cellInfo = rowTotal.createCell(3);
				cellInfo.setCellValue("");
				cellInfo.setCellStyle(styleCardId);
				cellInfo = rowTotal.createCell(4);
				cellInfo.setCellValue("");
				cellInfo.setCellStyle(styleCardId);
				cellInfo = rowTotal.createCell(5);
				cellInfo.setCellValue("");
				cellInfo.setCellStyle(styleCardId);
				sheet.addMergedRegion(new CellRangeAddress(5 + temp, 5 + temp, 5, 6));
				temp += listCus.size() + 2;
			}
		}
	}
	
	private void rpConsumtionSummary (Sheet sheet, Workbook workbook, Picture pict) {
		pict.resize(3.1, 2.0);
		sheet.setColumnWidth(0, 8 * 256);
		sheet.setColumnWidth(1, 14 * 256);
		sheet.setColumnWidth(2, 8 * 256);
		sheet.setColumnWidth(3, 30 * 256);
		sheet.setColumnWidth(4, 15 * 256);
		sheet.setColumnWidth(5, 10 * 256);
		sheet.setColumnWidth(6, 15 * 256);
		sheet.setColumnWidth(7, 15 * 256);
		sheet.setColumnWidth(8, 30 * 256);
		
		CellStyle styleHeader = workbook.createCellStyle();
		Font fontHeader1 = workbook.createFont();
		styleHeader.setAlignment(HorizontalAlignment.CENTER);
		styleHeader.setVerticalAlignment(VerticalAlignment.CENTER);
		styleHeader.setWrapText(true);
		fontHeader1.setBold(true);
		fontHeader1.setFontHeightInPoints((short) 13);
		
		CellStyle styleCardId = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setBold(true);
		styleCardId.setFont(font);
		
		CellStyle styleHeaderTable = workbook.createCellStyle();
		styleHeaderTable.setAlignment(HorizontalAlignment.CENTER);
		styleHeaderTable.setVerticalAlignment(VerticalAlignment.CENTER);
		
		Row rowHeader1 = sheet.createRow(0);
		rowHeader1.setHeight((short) (rowHeader1.getHeight() * 3));
		Cell cellHeader = rowHeader1.createCell(0);
		HSSFRichTextString richString = new HSSFRichTextString( "CONSUMPTION SUMMARY REPORT\nFrom : 01/08/2017 to 18/08/2017" );
		richString.applyFont( 0, 26, fontHeader1 );
		cellHeader.setCellValue(richString);
		cellHeader.setCellStyle(styleHeader);
		sheet.addMergedRegion(new CellRangeAddress(0, 2, 0, 8));
		
		Row rowInfo = sheet.createRow(3);
		Cell cellInfo = rowInfo.createCell(0);
		cellInfo.setCellValue("No.");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(1);
		cellInfo.setCellValue("Date");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(2);
		cellInfo.setCellValue("Time");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(3);
		cellInfo.setCellValue("Name");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(4);
		cellInfo.setCellValue("Service number");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(5);
		cellInfo.setCellValue("Quantity");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(6);
		cellInfo.setCellValue("Unit price");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(7);
		cellInfo.setCellValue("Amount");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(8);
		cellInfo.setCellValue("Remark");
		cellInfo.setCellStyle(styleHeaderTable);
		
		rowInfo = sheet.createRow(4);
		cellInfo = rowInfo.createCell(0);
		cellInfo.setCellValue("Card ID: ");
		cellInfo.setCellStyle(styleCardId);
		sheet.addMergedRegion(new CellRangeAddress(4, 4, 0, 8));
		
		rowInfo = sheet.createRow(5);
		cellInfo = rowInfo.createCell(0);
		cellInfo.setCellValue("1");
		cellInfo = rowInfo.createCell(1);
		cellInfo.setCellValue("18/08/2017");
		cellInfo = rowInfo.createCell(2);
		cellInfo.setCellValue("12:44");
		cellInfo = rowInfo.createCell(3);
		cellInfo.setCellValue("Noodle");
		cellInfo = rowInfo.createCell(4);
		cellInfo.setCellValue("00D100834");
		cellInfo = rowInfo.createCell(5);
		cellInfo.setCellValue("1");
		cellInfo = rowInfo.createCell(6);
		cellInfo.setCellValue("-45000");
		cellInfo = rowInfo.createCell(7);
		cellInfo.setCellValue("-45000");
		cellInfo = rowInfo.createCell(8);
		cellInfo.setCellValue("Canteen");
	}
	
	private void rpCardBalance (Sheet sheet, Workbook workbook, Picture pict, Map<String, Object> model) {
		String balanceDate = (String) model.get("balanceDate");
		String mostRecentDate = (String) model.get("mostRecentDate");
		List<ReportDTO<List<CustomerDTO>>> lstBalance = (List<ReportDTO<List<CustomerDTO>>>) model.get("data");
		pict.resize(3.0, 2.0);
		sheet.setColumnWidth(0, 8 * 256);
		sheet.setColumnWidth(1, 25 * 256);
		sheet.setColumnWidth(2, 4 * 256);
		sheet.setColumnWidth(3, 25 * 256);
		sheet.setColumnWidth(4, 20 * 256);
		sheet.setColumnWidth(5, 40 * 256);
		
		CellStyle styleHeader = cellHeader(workbook);
		Font fontHeader = fontHeader(workbook);
		CellStyle styleBold = cellStyleBold(workbook);
		
		CellStyle styleHeaderTable = cellStyleHeaderTable(workbook);
		
		Row rowHeader1 = sheet.createRow(0);
		rowHeader1.setHeight((short) (rowHeader1.getHeight() * 3));
		Cell cellHeader = rowHeader1.createCell(0);
		HSSFRichTextString richString = new HSSFRichTextString( "CARD BALANCE\nBalance @ "+ balanceDate +"\nMost recent balance date: "+ mostRecentDate );
		richString.applyFont( 0, 12, fontHeader );
		cellHeader.setCellValue(richString);
		cellHeader.setCellStyle(styleHeader);
		sheet.addMergedRegion(new CellRangeAddress(0, 2, 0, 5));
		
		Row rowInfo = sheet.createRow(4);
		Cell cellInfo = rowInfo.createCell(0);
		cellInfo.setCellValue("No.");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(1);
		cellInfo.setCellValue("Name");
		cellInfo.setCellStyle(styleHeaderTable);
		sheet.addMergedRegion(new CellRangeAddress(4, 4, 1, 3));
		cellInfo = rowInfo.createCell(4);
		cellInfo.setCellValue("Balance");
		cellInfo.setCellStyle(styleBold);
		cellInfo = rowInfo.createCell(5);
		cellInfo.setCellValue("Remark");
		cellInfo.setCellStyle(styleHeaderTable);
		
		int temp = 0;
		int totalCard = 0;
		double totalBalance = 0;
		if (lstBalance != null && !lstBalance.isEmpty()) {
			for (int i = 0; i < lstBalance.size(); i++) {
				ReportDTO<List<CustomerDTO>> report = lstBalance.get(i);
				List<CustomerDTO> listCus = report.getData();
				
				rowInfo = sheet.createRow(5 + temp);
				cellInfo = rowInfo.createCell(0);
				cellInfo.setCellValue("Class/Title: " + String.valueOf(report.getClassId()) + " - " + report.getClassName());
				cellInfo.setCellStyle(styleBold);
				sheet.addMergedRegion(new CellRangeAddress(5 + temp, 5 + temp, 0, 5));
				for (int j = 0; j < listCus.size(); j++) {
					CustomerDTO cus = listCus.get(j);
					rowInfo = sheet.createRow(6 + temp + j);
					cellInfo = rowInfo.createCell(0);
					cellInfo.setCellValue(j + 1);
					cellInfo = rowInfo.createCell(1);
					cellInfo.setCellValue(cus.getCusId() + " - " + cus.getCusName());
					sheet.addMergedRegion(new CellRangeAddress(6 + temp + j, 6 + temp + j, 1, 3));
					cellInfo = rowInfo.createCell(4);
					cellInfo.setCellValue(cus.getBalance());
					cellInfo.setCellStyle(styleBold);
					cellInfo = rowInfo.createCell(5);
					cellInfo.setCellValue(cus.getRemark());
					totalCard++;
				}
				Row rowTotalByClass = sheet.createRow(6 + temp + listCus.size());
				Cell cellTotal = rowTotalByClass.createCell(0);
				cellTotal.setCellValue("Total by Class/Title: " + String.valueOf(report.getClassId()) + " - " + report.getClassName());
				cellTotal.setCellStyle(styleBold);
				sheet.addMergedRegion(new CellRangeAddress(6 + temp + listCus.size(), 6 + temp + listCus.size(), 0, 3));
				cellTotal = rowTotalByClass.createCell(4);
				cellTotal.setCellValue(report.getTotalAmount());
				cellTotal.setCellStyle(styleBold);
				totalBalance += report.getTotalAmount();
				temp += listCus.size() + 2;
			}
		}
		Row rowTotal = sheet.createRow(7 + temp);
		Cell cellTotal = rowTotal.createCell(1);
		cellTotal.setCellValue("Grand Total : " + totalCard + " card");
		cellTotal.setCellStyle(styleBold);
		sheet.addMergedRegion(new CellRangeAddress(7 + temp, 7 + temp, 1, 3));
		cellTotal = rowTotal.createCell(4);
		cellTotal.setCellValue(totalBalance);
		cellTotal.setCellStyle(styleBold);
		
		rowTotal = sheet.createRow(8 + temp);
		cellTotal = rowTotal.createCell(3);
		cellTotal.setCellValue("Reported by");
		cellTotal.setCellStyle(styleBold);
		sheet.addMergedRegion(new CellRangeAddress(8 + temp, 8 + temp, 3, 4));
		cellTotal = rowTotal.createCell(5);
		cellTotal.setCellValue("Checked by");
		cellTotal.setCellStyle(styleBold);
	}
	
	private void rpOutstanding (Sheet sheet, Workbook workbook, Picture pict) {
		pict.resize(1.6, 2.0);
		sheet.setColumnWidth(0, 7 * 256);
		sheet.setColumnWidth(1, 35 * 256);
		sheet.setColumnWidth(2, 15 * 256);
		sheet.setColumnWidth(3, 15 * 256);
		sheet.setColumnWidth(4, 15 * 256);
		sheet.setColumnWidth(5, 12 * 256);
		sheet.setColumnWidth(6, 30 * 256);
		
		CellStyle styleHeader = workbook.createCellStyle();
		Font fontHeader1 = workbook.createFont();
		styleHeader.setAlignment(HorizontalAlignment.CENTER);
		styleHeader.setVerticalAlignment(VerticalAlignment.CENTER);
		styleHeader.setWrapText(true);
		fontHeader1.setBold(true);
		fontHeader1.setFontHeightInPoints((short) 13);
		
		CellStyle styleCardId = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setBold(true);
		styleCardId.setFont(font);
		
		CellStyle styleHeaderTable = workbook.createCellStyle();
		styleHeaderTable.setAlignment(HorizontalAlignment.CENTER);
		styleHeaderTable.setVerticalAlignment(VerticalAlignment.CENTER);
		
		Row rowHeader1 = sheet.createRow(0);
		rowHeader1.setHeight((short) (rowHeader1.getHeight() * 3));
		Cell cellHeader = rowHeader1.createCell(0);
		HSSFRichTextString richString = new HSSFRichTextString( "OUTSTANDING CASH BALANCE\nPrinted date: 18/08/2018" );
		richString.applyFont( 0, 24, fontHeader1 );
		cellHeader.setCellValue(richString);
		cellHeader.setCellStyle(styleHeader);
		sheet.addMergedRegion(new CellRangeAddress(0, 2, 0, 6));
		
		Row rowInfo = sheet.createRow(3);
		Cell cellInfo = rowInfo.createCell(0);
		cellInfo.setCellValue("No.");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(1);
		cellInfo.setCellValue("Name");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(2);
		cellInfo.setCellValue("Opening Amount");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(3);
		cellInfo.setCellValue("Addtional top up");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(4);
		cellInfo.setCellValue("Consumpsion");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(5);
		cellInfo.setCellValue("Balance");
		cellInfo.setCellStyle(styleCardId);
		cellInfo = rowInfo.createCell(6);
		cellInfo.setCellValue("Remark");
		cellInfo.setCellStyle(styleHeaderTable);
		
		rowInfo = sheet.createRow(4);
		cellInfo = rowInfo.createCell(0);
		cellInfo.setCellValue("Class/Title:");
		cellInfo.setCellStyle(styleCardId);
		sheet.addMergedRegion(new CellRangeAddress(4, 4, 0, 6));
		
		rowInfo = sheet.createRow(5);
		cellInfo = rowInfo.createCell(0);
		cellInfo.setCellValue("1");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(1);
		cellInfo.setCellValue("ABCD");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(2);
		cellInfo.setCellValue("");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(3);
		cellInfo.setCellValue("");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(4);
		cellInfo.setCellValue("-41000");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(5);
		cellInfo.setCellValue("-41000");
		cellInfo.setCellStyle(styleHeaderTable);
		cellInfo = rowInfo.createCell(6);
		cellInfo.setCellValue("");
		cellInfo.setCellStyle(styleHeaderTable);
	}
	
	private void rpCardTransaction (Sheet sheet, Workbook workbook, Picture pict, Map<String, Object> model) {
		String fromDate = (String) model.get("fromDate");
		String toDate = (String) model.get("toDate");
		ReportDTO<List<SaleTransDTO>> report =  (ReportDTO<List<SaleTransDTO>>) model.get("data");
		
		pict.resize(1.3, 2.0);
		sheet.setColumnWidth(0, 20 * 256);
		sheet.setColumnWidth(1, 16 * 256);
		sheet.setColumnWidth(2, 7 * 256);
		sheet.setColumnWidth(3, 7 * 256);
		sheet.setColumnWidth(4, 15 * 256);
		sheet.setColumnWidth(5, 15 * 256);
		sheet.setColumnWidth(6, 25 * 256);
		sheet.setColumnWidth(7, 25 * 256);
		
		CellStyle styleHeader = cellHeader(workbook);
		CellStyle styleBold = cellStyleBold(workbook);
		Font fontHeader = fontHeader(workbook);
		
		CellStyle styleWraptext= workbook.createCellStyle();
		styleWraptext.setAlignment(HorizontalAlignment.CENTER);
		styleWraptext.setVerticalAlignment(VerticalAlignment.CENTER);
		styleWraptext.setWrapText(true);
		
		Row rowHeader1 = sheet.createRow(0);
		rowHeader1.setHeight((short) (rowHeader1.getHeight() * 3));
		Cell cellHeader = rowHeader1.createCell(0);
		HSSFRichTextString richString = new HSSFRichTextString( "CARD TRANSACTION HISTORY\nFrom date "+fromDate+" to date " + toDate);
		richString.applyFont( 0, 24, fontHeader );
		cellHeader.setCellValue(richString);
		cellHeader.setCellStyle(styleHeader);
		sheet.addMergedRegion(new CellRangeAddress(0, 2, 0, 7));
		
		Row rowInfo = sheet.createRow(3);
		Cell cellInfo = rowInfo.createCell(0);
		cellInfo.setCellValue(CommonKey.REPORT_KEY.CARD_TRAN_HISTORY.DATE_TIME);
		cellInfo.setCellStyle(styleWraptext);
		cellInfo = rowInfo.createCell(1);
		cellInfo.setCellValue(CommonKey.REPORT_KEY.CARD_TRAN_HISTORY.TRANS_NUM);
		cellInfo.setCellStyle(styleWraptext);
		cellInfo = rowInfo.createCell(2);
		cellInfo.setCellValue(CommonKey.REPORT_KEY.CARD_TRAN_HISTORY.TYPE);
		cellInfo.setCellStyle(styleWraptext);
		cellInfo = rowInfo.createCell(3);
		cellInfo.setCellValue("SL\nQty");
		cellInfo.setCellStyle(styleWraptext);
		cellInfo = rowInfo.createCell(4);
		cellInfo.setCellValue("Cash-in");
		cellInfo.setCellStyle(styleWraptext);
		cellInfo = rowInfo.createCell(5);
		cellInfo.setCellValue("Cash-out");
		cellInfo.setCellStyle(styleWraptext);
		cellInfo = rowInfo.createCell(6);
		cellInfo.setCellValue(CommonKey.REPORT_KEY.CARD_TRAN_HISTORY.TRANS_TYPE);
		cellInfo.setCellStyle(styleWraptext);
		cellInfo = rowInfo.createCell(7);
		cellInfo.setCellValue("Balance count");
		cellInfo.setCellStyle(styleWraptext);
		if (report != null) {
			CellStyle styleCard1= workbook.createCellStyle();
			styleCard1.setVerticalAlignment(VerticalAlignment.CENTER);
			styleCard1.setWrapText(true);
			Font fontCard1 = workbook.createFont();
			fontCard1.setBold(true);
			styleCard1.setFont(fontCard1);
			
			CellStyle styleCard2= workbook.createCellStyle();
			styleCard2.setAlignment(HorizontalAlignment.RIGHT);
			styleCard2.setVerticalAlignment(VerticalAlignment.CENTER);
			styleCard2.setWrapText(true);
			Font fontCard2 = workbook.createFont();
			fontCard2.setColor(IndexedColors.RED.getIndex());
			fontCard2.setBold(true);
			styleCard2.setFont(fontCard2);
			
			Row rowCard = sheet.createRow(4);
			rowHeader1.setHeight((short) (rowCard.getHeight() * 2));
			cellInfo = rowCard.createCell(0);
			cellInfo.setCellValue(CommonKey.REPORT_KEY.CARD_TRAN_HISTORY.CARD_ID + report.getCardId() + " - " + report.getCusName());
			cellInfo.setCellStyle(styleCard1);
			sheet.addMergedRegion(new CellRangeAddress(4, 4, 0, 4));
			cellInfo = rowCard.createCell(5);
			cellInfo.setCellValue("Last Balace @ 31/07/2017 (758000 VND)\nCurrent Balance: (184000 VND)");
			cellInfo.setCellStyle(styleCard2);
			sheet.addMergedRegion(new CellRangeAddress(4, 4, 5, 7));
			
			List<SaleTransDTO> lstSale = report.getData();
			if (lstSale != null && !lstSale.isEmpty()) {
				for (int i = 0; i < lstSale.size(); i++) {
					SaleTransDTO sale = lstSale.get(i);
					rowInfo = sheet.createRow(5+i);
					cellInfo = rowInfo.createCell(0);
					cellInfo.setCellValue(sale.getTransDate() + "@" + sale.getTransTime());
					cellInfo.setCellStyle(styleWraptext);
					cellInfo = rowInfo.createCell(1);
					cellInfo.setCellValue(sale.getTransNum());
					cellInfo.setCellStyle(styleWraptext);
					cellInfo = rowInfo.createCell(2);
					cellInfo.setCellValue(sale.getTransType());
					cellInfo.setCellStyle(styleWraptext);
					cellInfo = rowInfo.createCell(3);
					cellInfo.setCellValue(sale.getQty());
					cellInfo.setCellStyle(styleWraptext);
					cellInfo = rowInfo.createCell(4);
					cellInfo.setCellValue(sale.getCashIn());
					cellInfo.setCellStyle(styleWraptext);
					cellInfo = rowInfo.createCell(5);
					cellInfo.setCellValue(sale.getCashOut());
					cellInfo.setCellStyle(styleWraptext);
					cellInfo = rowInfo.createCell(6);
					cellInfo.setCellValue(sale.getTransType());
					cellInfo.setCellStyle(styleWraptext);
					cellInfo = rowInfo.createCell(7);
					cellInfo.setCellValue(sale.getBalance());
					cellInfo.setCellStyle(styleWraptext);
				}
			}
			
			Row rowTotal = sheet.createRow(5 + lstSale.size());
			rowTotal.setHeight((short) (rowTotal.getHeight() * 1.5));
			cellInfo = rowTotal.createCell(0);
			cellInfo.setCellValue("Total by card ID (" + report.getCardId() + ")");
			cellInfo.setCellStyle(styleBold);
			sheet.addMergedRegion(new CellRangeAddress(5 + lstSale.size(), 5 + lstSale.size(), 0, 2));
			cellInfo = rowTotal.createCell(3);
			cellInfo.setCellValue(report.getTotalQty());
			cellInfo.setCellStyle(styleBold);
			cellInfo = rowTotal.createCell(4);
			cellInfo.setCellValue(report.getTotalCashIn());
			cellInfo.setCellStyle(styleBold);
			cellInfo = rowTotal.createCell(5);
			cellInfo.setCellValue(report.getTotalCashOut());
			cellInfo.setCellStyle(styleBold);
			cellInfo = rowTotal.createCell(7);
			cellInfo.setCellValue(report.getBalance());
			cellInfo.setCellStyle(styleBold);
			
			Row rowGrandTotal = sheet.createRow(6 + lstSale.size());
			rowGrandTotal.setHeight((short) (rowTotal.getHeight() * 1.5));
			cellInfo = rowGrandTotal.createCell(0);
			cellInfo.setCellValue("Grand total: ");
			cellInfo.setCellStyle(styleBold);
			sheet.addMergedRegion(new CellRangeAddress(6 + lstSale.size(), 6 + lstSale.size(), 0, 2));
			cellInfo = rowGrandTotal.createCell(3);
			cellInfo.setCellValue(report.getTotalQty());
			cellInfo.setCellStyle(styleBold);
			cellInfo = rowGrandTotal.createCell(4);
			cellInfo.setCellValue(report.getTotalCashIn());
			cellInfo.setCellStyle(styleBold);
			cellInfo = rowGrandTotal.createCell(5);
			cellInfo.setCellValue(report.getTotalCashOut());
			cellInfo.setCellStyle(styleBold);
			cellInfo = rowGrandTotal.createCell(7);
			cellInfo.setCellValue(report.getBalance());
			cellInfo.setCellStyle(styleBold);
			
			Row rowEnd = sheet.createRow(7 + lstSale.size());
			rowGrandTotal.setHeight((short) (rowTotal.getHeight() * 1.5));
			cellInfo = rowEnd.createCell(1);
			cellInfo.setCellValue("Reported by");
			cellInfo.setCellStyle(styleBold);
			sheet.addMergedRegion(new CellRangeAddress(7 + lstSale.size(), 7 + lstSale.size(), 1, 2));
			cellInfo = rowEnd.createCell(6);
			cellInfo.setCellValue("Checked by");
			cellInfo.setCellStyle(styleBold);
		}
	}

	private CellStyle cellHeader (Workbook workbook) {
		CellStyle styleHeader = workbook.createCellStyle();
		styleHeader.setAlignment(HorizontalAlignment.CENTER);
		styleHeader.setVerticalAlignment(VerticalAlignment.CENTER);
		styleHeader.setWrapText(true);
		return styleHeader;
	}
	
	private Font fontHeader (Workbook workbook) {
		Font fontHeader = workbook.createFont();
		fontHeader.setBold(true);
		fontHeader.setFontHeightInPoints((short) 13);
		return fontHeader;
	}
	
	private CellStyle cellStyleBold (Workbook workbook) {
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setBold(true);
		style.setFont(font);
		return style;
	}
	
	private CellStyle cellStyleHeaderTable (Workbook workbook) {
		CellStyle styleHeaderTable = workbook.createCellStyle();
		styleHeaderTable.setAlignment(HorizontalAlignment.CENTER);
		styleHeaderTable.setVerticalAlignment(VerticalAlignment.CENTER);
		return styleHeaderTable;
	}
}
