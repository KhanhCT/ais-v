package com.daisyit.report;

import com.itextpdf.text.pdf.PdfContentByte;

public class DashedLine implements LineDash {

	@Override
	public void applyLineDash(PdfContentByte canvas) {
		canvas.setLineDash(3, 3);
	}

}
