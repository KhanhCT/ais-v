package com.daisyit.report;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springlib.core.utils.CommonKey;

import com.daisyit.dto.CustomerDTO;
import com.daisyit.dto.ReportDTO;
import com.daisyit.dto.SaleTransDTO;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PDFReportView extends AbstractITextPdfView {
	private Logger logger = LoggerFactory.getLogger(PDFReportView.class);
	
	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		String nameFile = (String) model.get(CommonKey.REPORT_KEY.NAME);
		response.setHeader("Content-Disposition", "attachment; filename=\"" + nameFile + ".pdf\"");
		ClassPathResource resource = new ClassPathResource("Logo-Caterers.png");
		InputStream inputStream = resource.getInputStream();
		byte[] bytes = IOUtils.toByteArray(inputStream);
		Image image = Image.getInstance(bytes);
		image.scaleAbsolute(100f, 30f);
		image.setAlignment(Image.LEFT);
		PdfPTable table = new PdfPTable(3);
		table.setWidths(new float[] { 2.0f, 6f, 2.0f });
		PdfPCell cellImg = new PdfPCell();
		cellImg.addElement(image);
		cellImg.setBorder(PdfPCell.NO_BORDER);
		cellImg.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cellImg);
		BaseFont bf;
		Resource resFont = new ClassPathResource("FreeSans.ttf");
		bf = BaseFont.createFont(resFont.getURL().toString(), BaseFont.IDENTITY_H, true);
		
		String contentExport = (String) model.get("Content");
		if (contentExport.equals(CommonKey.REPORT_KEY.TRANSACTION_LIST)) {
			String fromDate = (String) model.get("fromDate");
			String toDate = (String) model.get("toDate");
			setTemplateImg (table, bf, "TRANSACTION LIST\n", "\nFrom: " + fromDate + " to " + toDate);
			document.add(table);
			rpTransactionList (document, bf, model);
		} else if (contentExport.equals(CommonKey.REPORT_KEY.CARD_BALANCE)) {
			String balanceDate = (String) model.get("balanceDate");
			String mostRecentDate = (String) model.get("mostRecentDate");
			setTemplateImg (table, bf, "CARD BALANCE\n", "\nBalance @ " + balanceDate +"\nMost recent balance date: " + mostRecentDate);
			document.add(table);
			rpCardBalance (document, bf, model);
		} else if (contentExport.equals(CommonKey.REPORT_KEY.CARD_TRANSACTION_HISTORY)) {
			String fromDate = (String) model.get("fromDate");
			String toDate = (String) model.get("toDate");
			setTemplateImg (table, bf, "CARD TRANSACTION HISTORY\n", "\nFrom date " + fromDate + " to date " + toDate);
			document.add(table);
			rpTransactionHistory (document, bf, model);
		}
	}

	private void rpTransactionList(Document document, BaseFont bf, Map<String, Object> model) {
		try {
			List<ReportDTO<List<SaleTransDTO>>> listData = (List<ReportDTO<List<SaleTransDTO>>>) model.get("data");
			
			Font fontNomar = new Font(bf, 10);
			Font fontBold = new Font(bf, 10, Font.BOLD);
			LineDash solid = new SolidLine();
		    LineDash dotted = new DottedLine();
		    LineDash dashed = new DashedLine();
		    
			PdfPTable tableData = new PdfPTable(8);
			tableData.setWidths(new float[] { 1.5f, 0.7f, 1.8f, 0.7f, 1.0f, 1.3f, 1.0f, 2.0f});
			tableData.setSpacingBefore(10);
			PdfPCell cell = cellAlignMiddle();
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);

			cell.setPhrase(new Phrase("Date", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("Time", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("Service number", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("Type", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("Quantity", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("Unit price", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("Amount", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("Item name", fontNomar));
			tableData.addCell(cell);
			if (listData != null && !listData.isEmpty()) {
				for (int i = 0; i < listData.size(); i++) {
					ReportDTO<List<SaleTransDTO>> report = listData.get(i);
					List<SaleTransDTO> listSale = report.getData();
					PdfPCell cellCardId = cellAlignMiddle();
					cellCardId.setPhrase(new Phrase(
							"Số thẻ/Card ID: (" + report.getCardId().trim() + ") " + report.getCusName().trim(),
							fontBold));
					cellCardId.setBackgroundColor(new BaseColor(206, 231, 255));
					cellCardId.setColspan(8);
					tableData.addCell(cellCardId);
					for (int j = 0; j < listSale.size(); j++) {
						SaleTransDTO sale = listSale.get(j);
						cell.setPhrase(new Phrase(sale.getTransDate(), fontNomar));
						tableData.addCell(cell);
						cell.setPhrase(new Phrase(sale.getTransTime(), fontNomar));
						tableData.addCell(cell);
						cell.setPhrase(new Phrase(sale.getTransNum(), fontNomar));
						tableData.addCell(cell);
						cell.setPhrase(new Phrase(sale.getTransType(), fontNomar));
						tableData.addCell(cell);
						cell.setPhrase(new Phrase(String.valueOf(sale.getQty()), fontNomar));
						tableData.addCell(cell);
						cell.setPhrase(new Phrase(String.valueOf(sale.getRtPrice()), fontNomar));
						tableData.addCell(cell);
						cell.setPhrase(new Phrase(String.valueOf(sale.getAmount()), fontNomar));
						tableData.addCell(cell);
						cell.setPhrase(new Phrase(sale.getSkuName(), fontNomar));
						tableData.addCell(cell);
					}
					PdfPCell cellTotal1 = cellNoBorder();
					cellTotal1.setPhrase(new Phrase("Total by card ID :", fontNomar));
					cellTotal1.setColspan(4);
					cellTotal1.setCellEvent(new CustomBorder(solid, null, null, solid));
					tableData.addCell(cellTotal1);
					PdfPCell cellTotal2 = cellNoBorder();
					cellTotal2.setPhrase(new Phrase(String.valueOf(report.getTotalQty()), fontBold));
					cellTotal2.setColspan(2);
					cellTotal2.setCellEvent(new CustomBorder(null, null, null, solid));
					tableData.addCell(cellTotal2);
					PdfPCell cellTotal3 = cellNoBorder();
					cellTotal3.setPhrase(new Phrase(String.valueOf(report.getTotalAmount()), fontBold));
					cellTotal3.setColspan(2);
					cellTotal3.setCellEvent(new CustomBorder(null, solid, null, solid));
					tableData.addCell(cellTotal3);
				}
			}
			document.add(tableData);
		} catch (Exception e) {
			logger.error("ERROR : " + e);
		}
	}
	
	private void rpCashBalance(Document document, BaseFont bf) {
		try {
			Font fontNomar = new Font(bf, 10);
			Font fontBold = new Font(bf, 10, Font.BOLD);
			LineDash solid = new SolidLine();
		    LineDash dotted = new DottedLine();
		    LineDash dashed = new DashedLine();
			
			PdfPTable tableData = new PdfPTable(7);
			tableData.setWidths(new float[] { 0.5f, 3.0f, 1.0f, 1.0f, 1.0f, 1.0f, 2.5f});
			tableData.setSpacingBefore(10);
			PdfPCell cell = cellAlignMiddle();
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);

			cell.setPhrase(new Phrase("No.", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("Name", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("Opening Amount", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("Addtional top up", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("Consumpsion", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("Balance", fontBold));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("Remark", fontNomar));
			tableData.addCell(cell);
			PdfPCell cellCardId = cellAlignMiddle();
			cellCardId.setPhrase(new Phrase("Class/Title :", fontBold));
			cellCardId.setColspan(7);
			tableData.addCell(cellCardId);
			cell.setPhrase(new Phrase("1", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("A0000588", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("1000000", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("246", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("754", fontBold));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("", fontNomar));
			tableData.addCell(cell);
			PdfPCell cellTotal1 = new PdfPCell(new Phrase("Total by Class/Title :", fontBold));
			cellTotal1.setMinimumHeight(20f);
			cellTotal1.setColspan(3);
			cellTotal1.setBorder(PdfPCell.NO_BORDER);
			cellTotal1.setCellEvent(new CustomBorder(solid, null, null, solid));
			tableData.addCell(cellTotal1);
			PdfPCell cellTotal2 = new PdfPCell(new Phrase("29801", fontBold));
			cellTotal2.setMinimumHeight(20f);
			cellTotal2.setBorder(PdfPCell.NO_BORDER);
			cellTotal2.setCellEvent(new CustomBorder(null, null, null, solid));
			tableData.addCell(cellTotal2);
			cellTotal2.setPhrase(new Phrase("29801", fontBold));
			tableData.addCell(cellTotal2);
			PdfPCell cellTotal3 = new PdfPCell(new Phrase("23000", fontBold));
			cellTotal3.setMinimumHeight(20f);
			cellTotal3.setColspan(2);
			cellTotal3.setBorder(PdfPCell.NO_BORDER);
			cellTotal3.setCellEvent(new CustomBorder(null, solid, null, solid));
			tableData.addCell(cellTotal3);
			
			document.add(tableData);
		} catch (Exception e) {
			logger.error("ERROR : " + e);
		}
	}
	
	private void rpCardBalance(Document document, BaseFont bf, Map<String, Object> model) {
		try {
			List<ReportDTO<List<CustomerDTO>>> lstBalance = (List<ReportDTO<List<CustomerDTO>>>) model.get("data");
			
			Font fontNomar = new Font(bf, 10);
			Font fontBold = new Font(bf, 10, Font.BOLD);
			LineDash solid = new SolidLine();
		    
			PdfPTable tableData = new PdfPTable(4);
			tableData.setWidths(new float[] { 0.7f, 4f, 2f, 3.3f});
			tableData.setSpacingBefore(10);
			PdfPCell cell = cellAlignMiddle();
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);

			cell.setPhrase(new Phrase("No.", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("Name", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("Balance", fontBold));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("Remark", fontNomar));
			tableData.addCell(cell);
			
			int totalCard = 0;
			double totalBalance = 0;
			if (lstBalance != null && !lstBalance.isEmpty()) {
				for (int i = 0; i < lstBalance.size(); i++) {
					ReportDTO<List<CustomerDTO>> report = lstBalance.get(i);
					List<CustomerDTO> listCus = report.getData();
					PdfPCell cellClass = cellAlignMiddle();
					cellClass.setPhrase(new Phrase("Class/Title: " + String.valueOf(report.getClassId()) + " - " + report.getClassName(), fontBold));
					cellClass.setColspan(4);
					tableData.addCell(cellClass);
					for (int j = 0; j < listCus.size(); j++) {
						CustomerDTO cus = listCus.get(j);
						cell.setPhrase(new Phrase(String.valueOf(j+1), fontNomar));
						tableData.addCell(cell);
						cell.setPhrase(new Phrase(cus.getCusId() + " - " + cus.getClassName(), fontNomar));
						tableData.addCell(cell);
						cell.setPhrase(new Phrase(String.valueOf(cus.getBalance()), fontNomar));
						tableData.addCell(cell);
						cell.setPhrase(new Phrase(cus.getRemark(), fontNomar));
						tableData.addCell(cell);
						totalCard ++;
					}
					cellClass = cellNoBorder();
					cellClass.setPhrase(new Phrase("Total by Class/Title: " + String.valueOf(report.getClassId()) + " - " + report.getClassName(), fontBold));
					cellClass.setColspan(2);
					cellClass.setCellEvent(new CustomBorder(solid, null, solid, solid));
					tableData.addCell(cellClass);
					cellClass = cellNoBorder();
					cellClass.setPhrase(new Phrase(String.valueOf(report.getTotalAmount()), fontBold));
					cellClass.setHorizontalAlignment(Element.ALIGN_RIGHT);
					cellClass.setCellEvent(new CustomBorder(null, null, solid, solid));
					tableData.addCell(cellClass);
					cellClass = cellNoBorder();
					cellClass.setPhrase(new Phrase("", fontBold));
					cellClass.setCellEvent(new CustomBorder(null, solid, solid, solid));
					tableData.addCell(cellClass);
					totalBalance += report.getTotalAmount();
				}
			}
			
			PdfPCell cellTotal = cellNoBorder();
			cellTotal.setPhrase(new Phrase("", fontBold));
			cellTotal.setCellEvent(new CustomBorder(null, null, solid, null));
			
			tableData.addCell(cellTotal);
			cellTotal.setPhrase(new Phrase("Grand Total: " + String.valueOf(totalCard)+ " card", fontBold));
			tableData.addCell(cellTotal);
			cellTotal.setPhrase(new Phrase(String.valueOf(totalBalance), fontBold));
			cellTotal.setHorizontalAlignment(Element.ALIGN_RIGHT);
			tableData.addCell(cellTotal);
			cellTotal.setPhrase(new Phrase("", fontBold));
			tableData.addCell(cellTotal);
			document.add(tableData);
			
			PdfPTable tableReport = new PdfPTable(3);
			tableReport.setWidths(new float[] { 3f, 4f, 3f});
			tableReport.setSpacingBefore(10);
			PdfPCell cellReport = cellNoBorder();
			cellReport.setPhrase(new Phrase("", fontBold));
			cellReport.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellReport.setCellEvent(new CustomBorder(null, null, null, null));
			tableReport.addCell(cellReport);
			cellReport.setPhrase(new Phrase("Reported by", fontBold));
			tableReport.addCell(cellReport);
			cellReport.setPhrase(new Phrase("Checked by", fontBold));
			cellReport.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellReport.setPaddingRight(20);
			tableReport.addCell(cellReport);
			document.add(tableReport);
		} catch (Exception e) {
			logger.error("ERROR : " + e);
		}
	}
	
	private void rpTransactionHistory(Document document, BaseFont bf, Map<String, Object> model) {
		try {
			ReportDTO<List<SaleTransDTO>> report = (ReportDTO<List<SaleTransDTO>>) model.get("data");
			Font fontNomar = new Font(bf, 8);
			Font fontBold = new Font(bf, 8, Font.BOLD);
			Font fontRed = new Font(bf, 8, Font.BOLD, BaseColor.RED);
			LineDash solid = new SolidLine();
			LineDash dotted = new DottedLine();
			LineDash dashed = new DashedLine();

			PdfPTable tableData = new PdfPTable(8);
			tableData.setWidths(new float[] { 1.3f, 1.0f, 0.5f, 0.5f, 1.0f, 1.0f, 1.4f, 1.3f });
			tableData.setSpacingBefore(10);
			PdfPCell cell = cellAlignMiddle();
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);

			cell.setPhrase(new Phrase(CommonKey.REPORT_KEY.CARD_TRAN_HISTORY.DATE_TIME, fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase(CommonKey.REPORT_KEY.CARD_TRAN_HISTORY.TRANS_NUM, fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase(CommonKey.REPORT_KEY.CARD_TRAN_HISTORY.TYPE, fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("SL\nQty", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("Cash-in", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("Cash-out", fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase(CommonKey.REPORT_KEY.CARD_TRAN_HISTORY.TRANS_TYPE, fontNomar));
			tableData.addCell(cell);
			cell.setPhrase(new Phrase("Balance count", fontNomar));
			tableData.addCell(cell);

			PdfPCell cellCard = cellNoBorder();
			cellCard.setMinimumHeight(40f);
			cellCard.setPhrase(new Phrase(CommonKey.REPORT_KEY.CARD_TRAN_HISTORY.CARD_ID, fontBold));
			cellCard.setColspan(5);
			cellCard.setCellEvent(new CustomBorder(solid, null, solid, solid));
			tableData.addCell(cellCard);
			PdfPCell cellBalance = cellNoBorder();
			cellBalance.setMinimumHeight(30f);
			cellBalance.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellBalance.setPhrase(
					new Phrase("Last Balance @ 31/07/2017 (758000 VND)\nCurrent Balance: (184000 VND)", fontRed));
			cellBalance.setColspan(3);
			cellBalance.setCellEvent(new CustomBorder(null, solid, solid, solid));
			tableData.addCell(cellBalance);

			List<SaleTransDTO> listSale = report.getData();
			PdfPCell cellData = cellNoBorder();
			cellData.setCellEvent(new CustomBorder(solid, solid, null, null));
			for (int i = 0; i < listSale.size(); i++) {
				SaleTransDTO sale = listSale.get(i);
				cellData.setPhrase(new Phrase(sale.getTransDate() + "@" + sale.getTransTime(), fontNomar));
				cellData.setHorizontalAlignment(Element.ALIGN_LEFT);
				tableData.addCell(cellData);
				cellData.setPhrase(new Phrase(sale.getTransNum(), fontNomar));
				tableData.addCell(cellData);
				cellData.setPhrase(new Phrase(sale.getTransType(), fontNomar));
				tableData.addCell(cellData);
				cellData.setPhrase(new Phrase(String.valueOf(sale.getTotalQty()), fontNomar));
				cellData.setHorizontalAlignment(Element.ALIGN_RIGHT);
				tableData.addCell(cellData);
				cellData.setPhrase(new Phrase(String.valueOf(sale.getCashIn()), fontNomar));
				tableData.addCell(cellData);
				cellData.setPhrase(new Phrase(String.valueOf(sale.getCashOut()), fontNomar));
				tableData.addCell(cellData);
				cellData.setPhrase(new Phrase(sale.getTransType(), fontNomar));
				cellData.setHorizontalAlignment(Element.ALIGN_LEFT);
				tableData.addCell(cellData);
				cellData.setPhrase(new Phrase(String.valueOf(sale.getBalance()), fontNomar));
				cellData.setHorizontalAlignment(Element.ALIGN_RIGHT);
				tableData.addCell(cellData);
			}
			PdfPCell cellTotal = cellNoBorder();
			cellTotal.setPhrase(new Phrase("Total by card ID (" + report.getCardId() + "):", fontBold));
			cellTotal.setColspan(3);
			cellTotal.setCellEvent(new CustomBorder(solid, null, solid, solid));
			cellTotal.setHorizontalAlignment(Element.ALIGN_LEFT);
			tableData.addCell(cellTotal);
			cellTotal = cellNoBorder();
			cellTotal.setPhrase(new Phrase(String.valueOf(report.getTotalQty()), fontBold));
			cellTotal.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellTotal.setCellEvent(new CustomBorder(null, null, solid, solid));
			tableData.addCell(cellTotal);
			cellTotal.setPhrase(new Phrase(String.valueOf(report.getTotalCashIn()), fontBold));
			cellTotal.setHorizontalAlignment(Element.ALIGN_RIGHT);
			tableData.addCell(cellTotal);
			cellTotal.setPhrase(new Phrase(String.valueOf(report.getTotalCashOut()), fontBold));
			cellTotal.setHorizontalAlignment(Element.ALIGN_RIGHT);
			tableData.addCell(cellTotal);
			cellTotal.setPhrase(new Phrase(String.valueOf(report.getBalance()), fontBold));
			cellTotal.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellTotal.setColspan(2);
			cellTotal.setCellEvent(new CustomBorder(null, solid, solid, solid));
			tableData.addCell(cellTotal);

			PdfPCell cellGrandTotal = cellNoBorder();
			cellGrandTotal.setCellEvent(new CustomBorder(null, null, solid, null));
			cellGrandTotal.setPhrase(new Phrase("Grand total", fontBold));
			cellGrandTotal.setColspan(3);
			tableData.addCell(cellGrandTotal);
			cellGrandTotal = cellNoBorder();
			cellGrandTotal.setCellEvent(new CustomBorder(null, null, solid, null));
			cellGrandTotal.setPhrase(new Phrase(String.valueOf(report.getTotalQty()), fontBold));
			cellGrandTotal.setHorizontalAlignment(Element.ALIGN_RIGHT);
			tableData.addCell(cellGrandTotal);
			cellGrandTotal.setPhrase(new Phrase(String.valueOf(report.getTotalCashIn()), fontBold));
			tableData.addCell(cellGrandTotal);
			cellGrandTotal.setPhrase(new Phrase(String.valueOf(report.getTotalCashOut()), fontBold));
			tableData.addCell(cellGrandTotal);
			cellGrandTotal.setPhrase(new Phrase("", fontBold));
			tableData.addCell(cellGrandTotal);
			cellGrandTotal.setPhrase(new Phrase(String.valueOf(report.getBalance()), fontBold));
			tableData.addCell(cellGrandTotal);
			document.add(tableData);
		} catch (Exception e) {
			logger.error("ERROR : " + e);
		}
	}
	
	private PdfPCell cellAlignMiddle () {
		PdfPCell cell = new PdfPCell();
		cell.setMinimumHeight(20f);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		return cell;
	}
	
	private PdfPCell cellNoBorder () {
		PdfPCell cell = new PdfPCell();
		cell.setMinimumHeight(20f);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}
	
	private void setTemplateImg (PdfPTable tableTitle, BaseFont bf, String header1, String header2) {
		Phrase phraseTitle = new Phrase();
		phraseTitle.add(new Chunk(header1, new Font(bf, 12, Font.BOLD)));
		phraseTitle.add(new Chunk(header2, new Font(bf, 10)));
		PdfPCell cellTitle = new PdfPCell(phraseTitle);
		cellTitle.setBorder(PdfPCell.NO_BORDER);
		cellTitle.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cellTitle.setHorizontalAlignment(Element.ALIGN_CENTER);
		tableTitle.addCell(cellTitle);
		PdfPCell cellEmpty = new PdfPCell(new Phrase(""));
		cellEmpty.setBorder(PdfPCell.NO_BORDER);
		tableTitle.addCell(cellEmpty);
	}

}
