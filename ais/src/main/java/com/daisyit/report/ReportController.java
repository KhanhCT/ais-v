package com.daisyit.report;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springlib.core.utils.CommonKey;
import org.springlib.core.utils.DateConverter;

import com.daisyit.dto.CustomerDTO;
import com.daisyit.dto.ReportDTO;
import com.daisyit.dto.SaleTransDTO;

@Controller
@RequestMapping(value = "/ais/report/")
public class ReportController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);

	@RequestMapping(value = "/exportTransactionList", method = RequestMethod.POST)
	public ModelAndView exportTransactionList(@RequestBody List<ReportDTO<List<SaleTransDTO>>> data,
			@RequestParam String fromDate, @RequestParam String toDate, @RequestParam String typeExport) {
		ModelAndView modelAndView;
		if (typeExport.equals(CommonKey.REPORT_KEY.TYPE.PDF)) {
			modelAndView = new ModelAndView("pdfView");
		} else {
			modelAndView = new ModelAndView("excelView");
		}
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute(CommonKey.REPORT_KEY.NAME, "Transaction List");
		modelMap.addAttribute("Content", CommonKey.REPORT_KEY.TRANSACTION_LIST);
		modelMap.addAttribute("fromDate", fromDate);
		modelMap.addAttribute("toDate", toDate);
		modelMap.addAttribute("data", data);
		modelAndView.addAllObjects(modelMap);
		return modelAndView;
	}

	@RequestMapping(value = "/exportCardBalance", method = RequestMethod.POST)
	public ModelAndView exportCardBalance(@RequestBody List<ReportDTO<List<CustomerDTO>>> data,
			@RequestParam String typeExport) {
		ModelAndView modelAndView;
		if (typeExport.equals(CommonKey.REPORT_KEY.TYPE.PDF)) {
			modelAndView = new ModelAndView("pdfView");
		} else {
			modelAndView = new ModelAndView("excelView");
		}
		Date currentDate = new Date();
		String dateStr = DateConverter.convertDateStringByFormatLocal(currentDate, CommonKey.DATE_FORMAT.DATE_SLASH_FORMAT);
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute(CommonKey.REPORT_KEY.NAME, "Card Balance");
		modelMap.addAttribute("Content", CommonKey.REPORT_KEY.CARD_BALANCE);
		modelMap.addAttribute("data", data);
		modelMap.addAttribute("balanceDate", dateStr);
		modelMap.addAttribute("mostRecentDate", dateStr);
		modelAndView.addAllObjects(modelMap);
		return modelAndView;
	}

	@RequestMapping(value = "/exportCardTransactionHistory", method = RequestMethod.POST)
	public ModelAndView exportCardTransactionHistory(@RequestBody ReportDTO<List<SaleTransDTO>> data,
			@RequestParam String fromDate, @RequestParam String toDate, @RequestParam String typeExport) {
		ModelAndView modelAndView;
		if (typeExport.equals(CommonKey.REPORT_KEY.TYPE.PDF)) {
			modelAndView = new ModelAndView("pdfView");
		} else {
			modelAndView = new ModelAndView("excelView");
		}
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute(CommonKey.REPORT_KEY.NAME, "Card Transaction History");
		modelMap.addAttribute("Content", CommonKey.REPORT_KEY.CARD_TRANSACTION_HISTORY);
		modelMap.addAttribute("fromDate", fromDate);
		modelMap.addAttribute("toDate", toDate);
		modelMap.addAttribute("data", data);
		modelAndView.addAllObjects(modelMap);
		return modelAndView;
	}
}
