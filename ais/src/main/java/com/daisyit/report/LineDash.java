package com.daisyit.report;

import com.itextpdf.text.pdf.PdfContentByte;

public interface LineDash {
	public void applyLineDash(PdfContentByte canvas);
}
