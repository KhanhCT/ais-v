package com.daisyit.frontend.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springlib.core.utils.CommonKey;
import org.springlib.core.utils.DateConverter;

import com.daisyit.backend.dao.CusTransDAO;
import com.daisyit.backend.model.CusTrans;
import com.daisyit.backend.model.Customer;
import com.daisyit.dto.CusTransDTO;
import com.daisyit.frontend.service.CusTransService;

@Service(value="transactionService")
public class CusTransSerivceImpl implements CusTransService {
	
	@Autowired
	private CusTransDAO cusTransDAO;
	@Override
	public List<CusTransDTO> getCusTransHistory(int customerId, String fromDate, String toDate) {
		String sqlQuery = "cusId="+ String.valueOf(customerId);
		List<CusTransDTO> cusTransDTOs = new ArrayList<>();
		Date _fromDate = DateConverter.convertDateByFormatLocal(fromDate, CommonKey.DATE_FORMAT.DATE_TIME_FORMAT);
		Date _toDate = DateConverter.convertDateByFormatLocal(toDate, CommonKey.DATE_FORMAT.DATE_TIME_FORMAT);
		List<CusTrans> cusTransLst = cusTransDAO.getCusTransHistory(customerId, _fromDate,_toDate, null);
		for(CusTrans cusTrans: cusTransLst)
		{
			CusTransDTO cusTransDTO = new CusTransDTO();
			cusTransDTO.model2dto(cusTrans);
			cusTransDTOs.add(cusTransDTO);
		}
		CusTransDTO cusTransDTO = new CusTransDTO();
		cusTransDTO.setCusId("AAAAAAAAAA");
		cusTransDTO.setAmount(11111);
		cusTransDTOs.add(cusTransDTO);
		return cusTransDTOs;
	}
	
}
