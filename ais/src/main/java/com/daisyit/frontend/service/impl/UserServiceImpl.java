package com.daisyit.frontend.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springlib.core.http.RestClient;
import org.springlib.core.http.RestClientErrorHandler;
import org.springlib.core.utils.Utils;

import com.daisyit.backend.dao.SysvarDAO;
import com.daisyit.backend.dao.UserDAO;
import com.daisyit.backend.dao.UserRoleDAO;
import com.daisyit.backend.model.Account;
import com.daisyit.backend.model.UserRole;
import com.daisyit.dto.UserDTO;
import com.daisyit.dto.UserRoleDTO;
import com.daisyit.frontend.service.UserService;
import com.daisyit.utils.ConstantKey;
import com.springlib.core.dto.TokenInfo;

@Service("userService")
public class UserServiceImpl implements UserService {
	public static List<Account> listUser = new ArrayList<Account>();

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private UserRoleDAO userRoleDAO;
	
	@Autowired 
	private SysvarDAO sysvarDAO;

	@Override
	public List<UserDTO> findAll() {
		List<Account> list = userDAO.getAllUser();
		List<UserDTO> userList = new ArrayList<>();
		for (Account account : list) {
			UserDTO user = new UserDTO();
			user.setUserId(account.getId().getUserId());
			user.setUserName(account.getId().getUserName());
			user.setPassword(account.getPassword());
			user.setUserRoleId(account.getUserRoleId());
			userList.add(user);
		}
		return userList;
	}

	@Override
	public UserDTO findById(int id) {
		Account account = userDAO.getUserById(id);
		UserDTO user = new UserDTO();
		user.setUserId(account.getId().getUserId());
		user.setUserName(account.getId().getUserName());
		user.setPassword(account.getPassword());
		user.setUserRoleId(account.getUserRoleId());
		return user;
	}

	@Override
	public int newUser(UserDTO user) {
		Account account = user.dto2Model();
		String password = account.getPassword();
		password = Utils.Base64Encoder(password);
		account.setPassword(password);
		return userDAO.addUser(account);
	}

	@Override
	public boolean delete(int id) {
		return userDAO.deleteUserById(id);
	}

	@Override
	public UserDTO loadUserByUsername(String username) {
		Account account = userDAO.getUserByName(username);
		UserDTO user = new UserDTO();
		user.setUserId(account.getId().getUserId());
		user.setUserName(account.getId().getUserName());
		user.setPassword(account.getPassword());
		user.setUserRoleId(account.getUserRoleId());
		return user;
	}

	@Override
	public boolean checkLogin(UserDTO user) {
		Account account = userDAO.getUser(user.getUserName(), user.getPassword());
		if (account != null)
			return true;
		return false;
	}

	@Override
	public String getUserRole(int userRoldeId) {
		return userRoleDAO.getUserRoleName(userRoldeId);
	}

	@Override
	public UserDTO getUserByNameAndPass(String userName, String passWord) {
		UserDTO result = null;
		Account account = userDAO.getUser(userName, passWord);
		if (account != null) {
			result = convertToDTO(account);
		}
		return result;
	}

	private UserDTO convertToDTO(Account account) {
		UserDTO bean = new UserDTO();
		bean.setUserId(account.getId().getUserId());
		bean.setUserName(account.getId().getUserName());
		bean.setPassword(account.getPassword());
		bean.setUserRoleId(account.getUserRoleId());
		return bean;
	}

	
	@Override
	public List<UserRoleDTO> getAllUserRoles() {
		List<UserRoleDTO> roleDTOs = new ArrayList<>();
		List<UserRole> roles = userRoleDAO.getAllUserRoles();
		for(UserRole role : roles)
		{
			UserRoleDTO dto= new UserRoleDTO();
			dto.model2DTO(role);
			roleDTOs.add(dto);
		}
		return roleDTOs;
	}
	@Override
	public TokenInfo getTokenInfo(String userName, String password) {
		String exeApiUrl = sysvarDAO.getValueFromName(ConstantKey.EXE_API_URL);
		RestClient restClient = new RestClient(new RestTemplate(), exeApiUrl, null);
		String jsonBody  = "{\"email\": \"" + userName + "\",\"password\": \"" +password + "\"}";
		ResponseEntity<TokenInfo> response = restClient.postRequest(TokenInfo.class, "/caterer-service/login", jsonBody);
		if (RestClientErrorHandler.hasError(response.getStatusCode())) {
			return null;
		}
		return response.getBody();
	}
	

	@Override
	public TokenInfo refreshToken(String refreshToken) {
		String exeApiUrl = sysvarDAO.getValueFromName(ConstantKey.EXE_API_URL);
		RestClient restClient = new RestClient(new RestTemplate(), exeApiUrl, null);
		String jsonBody  = "{\"refreshToken\": \"" + refreshToken +"\"}";
		ResponseEntity<TokenInfo> response = restClient.putRequest(TokenInfo.class, "/caterer-service/refreshToken", jsonBody);
		if (RestClientErrorHandler.hasError(response.getStatusCode())) {
			return null;
		}
		return response.getBody();
	}
	

}
