package com.daisyit.frontend.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daisyit.backend.dao.SysvarDAO;
import com.daisyit.frontend.service.FileService;
import com.daisyit.utils.ConstantKey;

@Service("fileService")
public class FileServiceImplm implements FileService {
	private static final Logger LOGGER = LoggerFactory.getLogger(FileServiceImplm.class);
	@Autowired
	private SysvarDAO sysvarDAO;

	@Override
	public String getImageDir() {
		return sysvarDAO.getValueFromName(ConstantKey.IMAGE_DIR);
	}

	@SuppressWarnings("finally")
	@Override
	public List<String> findAllImageFiles() {
		List<String> imageList = new ArrayList<>();
		String imageDir = sysvarDAO.getValueFromName(ConstantKey.IMAGE_DIR);
		try {
			if (imageDir != null) {
				File dir = new File(imageDir);
				File[] fList = dir.listFiles();
				for (File file : fList) {
					if (file.isFile()) {
						imageList.add(file.getName());
					}
				}
			}
		} catch (NullPointerException e) {
			LOGGER.error("Exception {}", e);
		} finally {
			return imageList;
		}

	}

	@Override
	public boolean deleteImageFileByName(String name) {
		String imageDir = sysvarDAO.getValueFromName(ConstantKey.IMAGE_DIR);
		try {
			File file = new File(imageDir + File.separator + name);
			if (file.isFile())
				return file.delete();
		} catch (NullPointerException e) {
			LOGGER.error("Exception {}", e);
			return false;
		}
		return false;
	}

	@SuppressWarnings("finally")
	@Override
	public List<String> findAllImageFilePath() {
		List<String> imageList = new ArrayList<>();
		String imageDir = sysvarDAO.getValueFromName(ConstantKey.IMAGE_DIR);
		try {
			if (imageDir != null) {
				File dir = new File(imageDir);
				File[] fList = dir.listFiles();
				for (File file : fList) {
					if (file.isFile()) {
						imageList.add(file.getAbsolutePath());
					}
				}
			}
		} catch (NullPointerException e) {
			LOGGER.error("Exception {}", e);
		} finally {
			return imageList;
		}

	}
}
