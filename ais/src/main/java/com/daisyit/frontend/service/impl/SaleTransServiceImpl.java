package com.daisyit.frontend.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springlib.core.utils.CommonKey;
import org.springlib.core.utils.DateConverter;

import com.daisyit.backend.dao.AllCodeDAO;
import com.daisyit.backend.dao.CardInfoDAO;
import com.daisyit.backend.dao.CusTransDAO;
import com.daisyit.backend.dao.CustomerDAO;
import com.daisyit.backend.dao.IdGeneratorDAO;
import com.daisyit.backend.dao.ItemListDAO;
import com.daisyit.backend.dao.LocationDAO;
import com.daisyit.backend.dao.SaleTransDAO;
import com.daisyit.backend.dao.SysvarDAO;
import com.daisyit.backend.model.AllCode;
import com.daisyit.backend.model.CusTrans;
import com.daisyit.backend.model.CusTransId;
import com.daisyit.backend.model.Customer;
import com.daisyit.backend.model.ItemList;
import com.daisyit.backend.model.Location;
import com.daisyit.backend.model.SaleTrans;
import com.daisyit.backend.model.WebTopUp;
import com.daisyit.backend.model.WebTopUpId;
import com.daisyit.dto.CusTransDTO;
import com.daisyit.dto.CustomerDTO;
import com.daisyit.dto.LocationDTO;
import com.daisyit.dto.ReportDTO;
import com.daisyit.dto.SaleTransDTO;
import com.daisyit.dto.TransTypeDTO;
import com.daisyit.frontend.service.SaleTransService;
import com.daisyit.utils.ConstantKey;
import com.opencsv.CSVReader;


@Service(value = "saleTransService")
public class SaleTransServiceImpl implements SaleTransService {
	private static final Logger LOGGER = LoggerFactory.getLogger(SaleTransServiceImpl.class);

	@Autowired
	private IdGeneratorDAO idGeneratorDAO;

	@Autowired
	private SaleTransDAO saleTransDAO;

	@Autowired
	private CusTransDAO cusTransDAO;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private ItemListDAO itemListDAO;	
	@Autowired
	private LocationDAO locationDAO; 
	@Autowired
	private CardInfoDAO cardInfoDAO;
	@Autowired
	private AllCodeDAO allCodeDAO;
	@Autowired
	private SysvarDAO sysvarDAO;
	@Override
	public List<SaleTransDTO> newSaleTransaction(List<SaleTransDTO> dtos, int userId) {
		List<SaleTransDTO> errorItemList = new ArrayList<>();
		CusTrans cusTrans = new CusTrans();
		Customer customer;
		double totalMoney = 0;
		double limittedMoney = Double.parseDouble(sysvarDAO.getValueFromName(ConstantKey.LIMITTED_MONEY));
		// Add each saleTrans to SaleTrans Table
		for (SaleTransDTO dto : dtos) {
			totalMoney = dto.getRtPrice() * dto.getQty();
			customer = customerDAO.getCustomerById(dto.getCusId(), true);
			if(customer == null)
			{	
				dto.setError(true);
				dto.setRemark("Not found customer");
				errorItemList.add(dto);
				continue;
			}
			// kiem tra so du
			if((customer.getBalance().doubleValue() - totalMoney) <= limittedMoney  )
			{
				dto.setError(true);
				dto.setRemark("Not enough Balance");
				errorItemList.add(dto);
				continue;
			}
			// gen trans num
			String transNum = idGeneratorDAO.getTransNum();
			if(transNum == null || transNum.length()!=16)
			{
				dto.setError(true);
				dto.setRemark("System Error");
				errorItemList.add(dto);
				continue;
			}
			dto.setTransNum(transNum);
			dto.setUserId(userId);
			SaleTrans saleTrans = dto.dto2Model();
			// TODO add TransDate and TransTime
			saleTrans.setTransDate(DateConverter.getCurrentDate());
			saleTrans.setTransTime(DateConverter.getCurrentDate());
			saleTrans.setTransType(ConstantKey.TRANS_TYPE.ORDER);
			
			BigDecimal curBalance = new BigDecimal(customer.getBalance().doubleValue() - totalMoney);
			saleTrans.setCurBalance(curBalance);
			try {
				saleTransDAO.newSaleTrans(saleTrans);
			} catch (Exception ex) {
				LOGGER.error("Exception: {}, Cause: {}", ex.getMessage(), ex.getCause());
				dto.setError(true);
				dto.setRemark("System Error");
				errorItemList.add(dto);
			}
			// Add all transaction to CusTrans Table
			CusTransId id = new CusTransId(transNum,ConstantKey.PAY_TYPE , ConstantKey.CASH_TYPE);
			cusTrans.setId(id);
			cusTrans.setAmount(new BigDecimal(totalMoney));
			cusTrans.setTransDate(DateConverter.getCurrentDate());
			cusTrans.setTransTime(DateConverter.getCurrentDate());
			cusTrans.setTransType(ConstantKey.TRANS_TYPE.ORDER);
			cusTrans.setCusId(dtos.get(0).getCusId());
			cusTrans.setUserId(userId);
			cusTrans.setStatus(true);
			
			//cap nhap customer trans
			cusTrans.setCurBalance(curBalance);
			cusTransDAO.newCusTran(cusTrans);
			//cap nhat so du tai khoan
			customer.setBalance(curBalance);
			customerDAO.saveOrUpdate(customer);		
			// cap nhat so du tai khoan the
			cardInfoDAO.updateCardBalance(curBalance.doubleValue(), customer.getCardId());
		}
		return errorItemList;
	}

	@Override
	public List<SaleTransDTO> getHistoryByCusId(String fromDate, String toDate, int customerId) {
		Date _fromDate = DateConverter.convertDateByFormatLocal(fromDate, CommonKey.DATE_FORMAT.DATE_TIME_FORMAT);
		Date _toDate = DateConverter.convertDateByFormatLocal(toDate, CommonKey.DATE_FORMAT.DATE_TIME_FORMAT);
		List<SaleTrans> list = saleTransDAO.getSaleTransByCusId(_fromDate, _toDate, customerId, null);
		List<SaleTransDTO> dtos = new ArrayList<>();
		for (SaleTrans saleTrans : list) {
			SaleTransDTO dto = new SaleTransDTO();
			dto.model2DTO(saleTrans);
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public List<SaleTransDTO> processStudentOrderFromFile(String url) {
		List<SaleTransDTO> saleTransDTOs = new ArrayList<>();
		CSVReader reader = null;
		String studentCode;
		Map<String, ItemList> itemMap;
		SaleTransDTO dto;
		try {
			reader = new CSVReader(new FileReader(url));
			String[] content;
			reader.readNext();
			reader.readNext();
			itemMap = itemListDAO.findAllItemMap();			
			while ((content = reader.readNext()) != null) {
				if (content.length == 6) {
					dto = new SaleTransDTO();
					studentCode = content[2];
					Customer customer = customerDAO.getCustomerById(studentCode, true);
					if (customer == null)
						continue;
					dto.setCusId(customer.getCusId());
					dto.setCusName(customer.getGivenName() + " " + customer.getFamilyName());
					dto.setCardId(customer.getCardId());
					ItemList item = itemMap.get(content[4]);
					if (item == null) {
						continue;
					} else {
						dto.setSkuName(item.getName());
						dto.setSkuCode(content[4]);
						dto.setRtPrice(item.getRtPrice().doubleValue());
					}
					try {
						dto.setQty(Integer.parseInt(content[5]));
					} catch (NumberFormatException e) {
						dto.setQty(1);
					}
					dto.setTotal(dto.getQty() * dto.getRtPrice());
					dto.setTransDate(DateConverter.convertDateStringByFormatLocal(DateConverter.getCurrentDate(),
							CommonKey.DATE_FORMAT.DATE_SLASH_FORMAT));
					dto.setTransTime(DateConverter.convertDateStringByFormatLocal(DateConverter.getCurrentDate(),
							CommonKey.DATE_FORMAT.TIME_FORMAT_2));
					saleTransDTOs.add(dto);
				} else {
					continue;
				}
			}
			reader.close();
		} catch (FileNotFoundException e) {
			LOGGER.error("Content " + url + " Error {}", e.getMessage());
		} catch (IOException ex) {
			LOGGER.error("Exception {}", ex.getMessage());
		}
		return saleTransDTOs;
	}

	
	@Override
	public List<SaleTransDTO> initStudentOrders(int classId) {
		List<Customer> customers = customerDAO.findAllStudentByClassId(classId);
		List<SaleTransDTO> dtos = new ArrayList<>();
		for(Customer cus : customers)
		{
			SaleTransDTO dto = new SaleTransDTO();
			dto.setCusId(cus.getCusId());
			dto.setCardId(cus.getCardId());
			dto.setCusName(cus.getGivenName() + " " + cus.getFamilyName());
			dto.setQty(1);
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public List<ReportDTO<List<SaleTransDTO>>> getTransactionList(String fromDate, String toDate) {
		Date _fromDate = DateConverter.convertDateByFormatLocal(fromDate, CommonKey.DATE_FORMAT.DATE_SLASH_FORMAT);
		Date _toDate = DateConverter.convertDateByFormatLocal(toDate, CommonKey.DATE_FORMAT.DATE_SLASH_FORMAT);
		List<Object[]> list = saleTransDAO.getTransactionList(_fromDate, _toDate);
		List<ReportDTO<List<SaleTransDTO>>> repostData = new ArrayList<>();
		List<SaleTransDTO> saleTransDTOs = new ArrayList<>();
		ReportDTO<List<SaleTransDTO>> payload = new ReportDTO<>();
		BigDecimal tmp;
		double totalQty = 0;
		double totalAmount = 0;
		String preCusId, curCusId;
		String cardCode="";
		preCusId = curCusId = "";
		String cusName="N/A" ;
		
		for(Object[] obj : list)
		{	
			curCusId = (String)obj[6];
			
			if(!curCusId.equals(preCusId) && !preCusId.equals(""))
			{
				payload.setCardId(preCusId);
				payload.setCusName(cusName);
				payload.setCardId(cardCode);
				payload.setTotalAmount(totalAmount);
				payload.setTotalQty(totalQty);
				payload.setData(saleTransDTOs);
				repostData.add(payload);
				totalAmount = totalQty = 0;
				saleTransDTOs = new  ArrayList<>();
				payload = new ReportDTO<>();
				
			}		
			SaleTransDTO dto = new SaleTransDTO();
			dto.setTransDate(DateConverter.convertDateStringByFormatLocal((Date)obj[0], CommonKey.DATE_FORMAT.DATE_SLASH_FORMAT));
			dto.setTransTime(DateConverter.convertDateStringByFormatLocal((Date)obj[1], CommonKey.DATE_FORMAT.TIME_FORMAT_2));
			dto.setTransNum(obj[2].toString());
			dto.setTransType(obj[3].toString());
			tmp = (BigDecimal) obj[4];
			dto.setQty(tmp.longValueExact());
			totalQty += tmp.longValueExact();
			tmp = (BigDecimal) obj[5];
			dto.setRtPrice(tmp.longValueExact());
			dto.setAmount(tmp.longValueExact() * dto.getQty());
			totalAmount += tmp.longValueExact() * dto.getQty();
			dto.setCusId((String)obj[6]);
			dto.setCardCode((String)obj[7]);
			dto.setSkuName((String)obj[8]);
			cusName = (String)obj[9] + " " + (String)obj[10];
			preCusId = curCusId ;
			cardCode = dto.getCardCode();
			saleTransDTOs.add(dto);
		}
		payload.setCardId(preCusId);
		payload.setCusName(cusName);
		payload.setCardId(cardCode);
		payload.setTotalAmount(totalAmount);
		payload.setTotalQty(totalQty);
		payload.setData(saleTransDTOs);
		repostData.add(payload);
		return repostData;
	}

	@Override
	public ReportDTO<List<SaleTransDTO>> getCardTransHistory(String fromDate, String toDate, String cusId) {
		Date _fromDate = DateConverter.convertDateByFormatLocal(fromDate, CommonKey.DATE_FORMAT.DATE_SLASH_FORMAT);
		Date _toDate = DateConverter.convertDateByFormatLocal(toDate, CommonKey.DATE_FORMAT.DATE_SLASH_FORMAT);
		List<Object[]> list = saleTransDAO.getCardTransHistory(_fromDate, _toDate, cusId);
		List<SaleTransDTO> saleTransDTOs = new ArrayList<>();
		ReportDTO<List<SaleTransDTO>> reportData = new ReportDTO<>();
		Map<Integer, String> itemMap = itemListDAO.findItemMap();
		BigDecimal tmp;
		Object[] cusInfo = customerDAO.getCustomerInfo(cusId);
		if(cusInfo == null)
			return reportData;
		reportData.setCusName((String)cusInfo[1] + " " + (String)cusInfo[2]);
		// replace cardid to cusID
		reportData.setCardId((String)cusInfo[0]);
		tmp = (BigDecimal)cusInfo[7];
		reportData.setBalance(tmp.doubleValue());
		String transType;
		double totalCashIn = 0, totalCashOut = 0;
		for(Object[] obj : list)
		{
			SaleTransDTO dto = new SaleTransDTO();
			dto.setTransDate(DateConverter.convertDateStringByFormatLocal((Date)obj[0], CommonKey.DATE_FORMAT.DATE_SLASH_FORMAT));
			dto.setTransTime(DateConverter.convertDateStringByFormatLocal((Date)obj[1], CommonKey.DATE_FORMAT.TIME_FORMAT_2));
			dto.setTransNum(obj[2].toString());
			dto.setTransType(obj[3].toString());
			tmp = (BigDecimal) obj[4];
			dto.setCurBalance(tmp.doubleValue());
			tmp = (BigDecimal) obj[5];
			dto.setAmount(tmp.doubleValue());
			String skuName = itemMap.get((int)obj[6]);
			
			if(skuName == null)
			{
				transType = dto.getTransType();
				if(transType.equals(ConstantKey.TRANS_TYPE.TOP_UP))
				{
					dto.setSkuName("Top Up");
				}else if(transType.equals(ConstantKey.TRANS_TYPE.TRASFER))
				{
					dto.setSkuName("Transfer");
				}else if(transType.equals(ConstantKey.TRANS_TYPE.WITHDRAW))
				{
					dto.setSkuName("Withdraw");
				}else {
					dto.setSkuName("N/A");
				}
			}else
			{
				dto.setSkuName(skuName);
			}
			tmp = (BigDecimal) obj[7];
			dto.setQty(tmp.doubleValue());
			transType = dto.getTransType();
			if(transType.equals(ConstantKey.TRANS_TYPE.TOP_UP))
			{
				dto.setCashIn(dto.getAmount());	
				totalCashIn +=dto.getAmount();
				dto.setCashOut(0);
			}else if(transType.equals(ConstantKey.TRANS_TYPE.ORDER))
			{
				dto.setCashOut(dto.getAmount());
				totalCashOut +=dto.getAmount();
				dto.setCashIn(0);
			}else if(transType.equals(ConstantKey.TRANS_TYPE.TRASFER))
			{
				dto.setCashOut(dto.getAmount());	
				dto.setCashIn(0);
				totalCashOut +=dto.getAmount();
			}else {
				dto.setCashOut(dto.getAmount());
				dto.setCashIn(0);
				totalCashOut +=dto.getAmount();
			}
			saleTransDTOs.add(dto);			
		}
		reportData.setTotalCashIn(totalCashIn);
		reportData.setTotalCashOut(totalCashOut);
		reportData.setData(saleTransDTOs);
		return reportData;
	}

	@Override
	public List<ReportDTO<List<CustomerDTO>>> getCardBalance() {
		List<Object[]> list = customerDAO.getCardBalance();
		List<CustomerDTO> customerDTOs = new ArrayList<>();
		ReportDTO<List<CustomerDTO>> payload = new ReportDTO<>();
		List<ReportDTO<List<CustomerDTO>>>  reportData = new ArrayList<>();
		Integer preClassId, curClassId;
		preClassId = curClassId = null;
		String className = null;
		double totalAmount = 0;
		BigDecimal tmp;
		for(Object[] obj : list)
		{
			curClassId = (int)obj[0];
			if(curClassId != preClassId && preClassId !=null)
			{
				payload.setTotalAmount(totalAmount);
				payload.setClassName(className);
				payload.setData(customerDTOs);	
				payload.setClassId(preClassId);
				reportData.add(payload);
				payload = new ReportDTO<>();
				customerDTOs = new ArrayList<>();
				totalAmount = 0;
			}
			CustomerDTO dto = new CustomerDTO();
			dto.setCusId((String)obj[1]);
			dto.setCusName((String)obj[2] + (String)obj[3]);
			tmp = (BigDecimal)obj[4];
			totalAmount +=tmp.doubleValue();
			dto.setBalance(tmp.doubleValue());
			dto.setRemark((String)obj[5]);
			dto.setClassName((String)obj[6]);
			className = (String)obj[6];
			dto.setClassId(curClassId);
			preClassId = curClassId;
			customerDTOs.add(dto);			
		}
		payload.setTotalAmount(totalAmount);
		payload.setClassName(className);
		payload.setData(customerDTOs);	
		payload.setClassId(preClassId);
		reportData.add(payload);
		return reportData;
	}

	@Override
	public List<CusTransDTO> processToUpList(String url, int userId) {
		List<CusTransDTO> cusTransDTOs = new ArrayList<>();
//		CSVReader reader = null;
//		CusTransDTO dto;
//		try {
//			reader = new CSVReader(new FileReader(url));
//			String[] content;
//			reader.readNext();		
//			reader.readNext();
//			while ((content = reader.readNext()) != null) {
//				dto = new CusTransDTO();
//				dto.setCusId(content[1]);
//				dto.setCusName(content[2]);
//				try {
//					dto.setAmount(Double.parseDouble(content[3]));
//				}catch(NumberFormatException ex) {
//					dto.setAmount(0);
//					dto.setRemark("Amount is not a number");
//					cusTransDTOs.add(dto);
//					continue;
//				}
//				dto.setRemark(content[4]);
//				dto.setUserId(userId);
//				dto.setTransType(ConstantKey.TRANS_TYPE.TOP_UP);
//				cusTransDTOs.add(dto);
//			}
//		}catch(FileNotFoundException ex) {
//			
//		}catch(IOException ex) {
//			
//		}
		try {
			Workbook workbook = WorkbookFactory.create(new File(url));
			Sheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			CusTransDTO dto;
			if(rowIterator.hasNext()) {
				rowIterator.next();
				rowIterator.next();
			}
			 while (rowIterator.hasNext()) {
				 Row row = rowIterator.next();
				 dto = new CusTransDTO();
				 Iterator<Cell> cellIterator = row.cellIterator();
				 if(cellIterator.hasNext()) {
					cellIterator.next();
					Cell cell = cellIterator.next();
					String cusId = cell.getStringCellValue();
					if(cusId ==null || cusId.equals(""))
						continue;
					dto.setCusId(cusId);
					cell = cellIterator.next();
					dto.setCusName(cell.getStringCellValue());
					cell = cellIterator.next();
					try {
						dto.setAmount(cell.getNumericCellValue());
					} catch (Exception ex) {
						dto.setAmount(0);
						dto.setRemark("Amount is not a number");
						cusTransDTOs.add(dto);
						continue;
					}
					cell = cellIterator.next();
					dto.setRemark(cell.getStringCellValue());
					dto.setUserId(userId);
					dto.setTransType(ConstantKey.TRANS_TYPE.TOP_UP);
					cusTransDTOs.add(dto);
				 }
			 }
		} catch (EncryptedDocumentException e) {
			//e.printStackTrace();
		} catch (InvalidFormatException e) {
			//e.printStackTrace();
		} catch (IOException e) {
			//e.printStackTrace();
		}
		return cusTransDTOs;
	}

	@Override
	public List<CusTransDTO> processTopUpList(List<CusTransDTO> dtos, int userId) {
		List<CusTransDTO> errorList = new ArrayList<>();
		for(CusTransDTO dto : dtos)
		{
			Customer customer = customerDAO.getCustomerById(dto.getCusId(), true);
			if(customer ==null)
			{
				dto.setRemark("Customer code is not available");
				errorList.add(dto);
				continue;
			}
			WebTopUp cusTrans = new WebTopUp();
			String transNum = idGeneratorDAO.getTransNum();
			WebTopUpId id = new WebTopUpId(transNum, DateConverter.getCurrentDate());
			cusTrans.setId(id);
			cusTrans.setAmount(new BigDecimal(dto.getAmount()));
			cusTrans.setTransTime(DateConverter.getCurrentDate());
			cusTrans.setTransType(ConstantKey.TRANS_TYPE.TOP_UP);
			cusTrans.setCusId(dto.getCusId());
			cusTrans.setUserId(userId);
			cusTrans.setStatus(false);
			cusTrans.setCurBalance(new BigDecimal(customer.getBalance().doubleValue() + dto.getAmount()));
			cusTrans.setUpdated(false);
			cusTrans.setStatus(true);
			cusTransDAO.newWebTopUp(cusTrans);
		}
		return errorList;
	}

	@Override
	public int pay(List<SaleTransDTO> dtos, int userId) {
		List<SaleTransDTO> errorItemList = new ArrayList<>();
		CusTrans cusTrans = new CusTrans();
		Customer customer;
		double totalMoney = 0;
		double limittedMoney = Double.parseDouble(sysvarDAO.getValueFromName(ConstantKey.LIMITTED_MONEY));
		customer = customerDAO.getCustomerById(dtos.get(0).getCusId(), true);
		if(customer == null)
			return ConstantKey.RESPONSE_CODE.ERROR;
		for(SaleTransDTO dto : dtos) {
			totalMoney +=dto.getAmount();
		}
		if((customer.getBalance().doubleValue() - totalMoney) < limittedMoney) {
			return ConstantKey.RESPONSE_CODE.ERROR;
		}
		String transNum;
		double totalAmount = 0;
		BigDecimal curBalance = null;
		for(SaleTransDTO dto : dtos) {
			transNum = idGeneratorDAO.getTransNum();
			if(transNum == null || transNum.length()!=16)
			{
				dto.setError(true);
				dto.setRemark("System Error");
				errorItemList.add(dto);
				continue;
			}
			dto.setTransNum(transNum);
			dto.setUserId(userId);
			SaleTrans saleTrans = dto.dto2Model();
			// TODO add TransDate and TransTime
			saleTrans.setTransDate(DateConverter.getCurrentDate());
			saleTrans.setTransTime(DateConverter.getCurrentDate());
			saleTrans.setTransType(ConstantKey.TRANS_TYPE.ORDER);
			totalMoney = dto.getRtPrice() * dto.getQty();
			totalAmount += totalMoney;
			curBalance = new BigDecimal(customer.getBalance().doubleValue() - totalMoney);
			saleTrans.setCurBalance(curBalance);
			try {
				saleTransDAO.newSaleTrans(saleTrans);
			} catch (Exception ex) {
				LOGGER.error("Exception: {}, Cause: {}", ex.getMessage(), ex.getCause());
				dto.setError(true);
				dto.setRemark("System Error");
				errorItemList.add(dto);
			}
				
		}
		transNum = idGeneratorDAO.getTransNum();
		// Add all transaction to CusTrans Table
		CusTransId id = new CusTransId(transNum, ConstantKey.PAY_TYPE, ConstantKey.CASH_TYPE);
		cusTrans.setId(id);
		cusTrans.setAmount(new BigDecimal(totalMoney));
		cusTrans.setTransDate(DateConverter.getCurrentDate());
		cusTrans.setTransTime(DateConverter.getCurrentDate());
		cusTrans.setTransType(ConstantKey.TRANS_TYPE.ORDER);
		cusTrans.setCusId(dtos.get(0).getCusId());
		cusTrans.setUserId(userId);
		cusTrans.setStatus(true);
		cusTrans.setCurBalance(curBalance);
		cusTransDAO.newCusTran(cusTrans);
		customer.setBalance(curBalance);
		customerDAO.saveOrUpdate(customer);
		return ConstantKey.RESPONSE_CODE.SUCCESS;
	}

	@Override
	public List<LocationDTO> findAllCanteens() {
		List<Location> locationList = locationDAO.getAllCanteen();
		List<LocationDTO> dtos = new ArrayList<>();
		for(Location canteen : locationList) {
			LocationDTO dto  = new LocationDTO();
			dto.model2DTO(canteen);
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public ReportDTO<List<SaleTransDTO>> getSaleTransHistory(String customerId, String fromDate, String toDate) {
		Date _fromDate = DateConverter.convertDateByFormatLocal(fromDate, CommonKey.DATE_FORMAT.DATE_SLASH_FORMAT);
		Date _toDate = DateConverter.convertDateByFormatLocal(toDate, CommonKey.DATE_FORMAT.DATE_SLASH_FORMAT);
		List<Object[]> list = saleTransDAO.getCardTransHistory(_fromDate, _toDate, customerId);
		ReportDTO<List<SaleTransDTO>> reportData = new ReportDTO<>();
		List<SaleTransDTO> saleTransDTOs = new ArrayList<>();
		BigDecimal tmp;
		String transType;
		double totalCashIn = 0, totalCashOut = 0;
		for(Object[] obj : list)
		{
			SaleTransDTO dto = new SaleTransDTO();
			dto.setTransDate(DateConverter.convertDateStringByFormatLocal((Date)obj[0], CommonKey.DATE_FORMAT.DATE_SLASH_FORMAT));
			dto.setTransTime(DateConverter.convertDateStringByFormatLocal((Date)obj[1], CommonKey.DATE_FORMAT.TIME_FORMAT_2));
			dto.setTransNum(obj[2].toString());
			dto.setTransType(obj[3].toString());
			tmp = (BigDecimal) obj[4];
			dto.setCurBalance(tmp.doubleValue());
			tmp = (BigDecimal) obj[5];
			dto.setAmount(tmp.doubleValue());
			tmp = (BigDecimal) obj[7];
			dto.setQty(tmp.doubleValue());
			transType = dto.getTransType();
			if(transType.equals(ConstantKey.TRANS_TYPE.TOP_UP))
			{
				dto.setCashIn(dto.getAmount());	
				totalCashIn +=dto.getAmount();
				dto.setAction("+");
				dto.setCashOut(0);
				dto.setRemark("Top Up");
			}else if(transType.equals(ConstantKey.TRANS_TYPE.ORDER))
			{
				dto.setCashOut(dto.getAmount());
				totalCashOut +=dto.getAmount();
				dto.setCashIn(0);
				dto.setRemark("Order");
				dto.setAction("-");
			}else if(transType.equals(ConstantKey.TRANS_TYPE.TRASFER))
			{
				dto.setCashOut(dto.getAmount());	
				dto.setCashIn(0);
				totalCashOut +=dto.getAmount();
				dto.setRemark("Transfer");
				dto.setAction("-");
			}else {
				dto.setCashOut(dto.getAmount());
				dto.setCashIn(0);
				totalCashOut +=dto.getAmount();
				dto.setRemark("Withdraw");
				dto.setAction("-");
			}
			saleTransDTOs.add(dto);				
		}
		reportData.setTotalCashIn(totalCashIn);
		reportData.setTotalCashOut(totalCashOut);
		reportData.setData(saleTransDTOs);
		return reportData;
	}
	
	@Override
	public List<TransTypeDTO> getAllTransationType() {
		List<AllCode> list = allCodeDAO.getAllCodeByType("TRANS_TYPE");
		List<TransTypeDTO> transTypeDTOs = new ArrayList<>();
		if (list != null) {
			for (AllCode code : list) {
				transTypeDTOs.add(new TransTypeDTO(code.getCodeIdx(), code.getId().getCodeName(), code.getCodeVal()));
			}
		}
		return transTypeDTOs;
	}
	

}
