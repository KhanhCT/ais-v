package com.daisyit.frontend.service.impl;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springlib.core.http.RestClient;
import org.springlib.core.http.RestClientErrorHandler;
import org.springlib.core.utils.CommonKey;
import org.springlib.core.utils.DateConverter;
import org.springlib.core.utils.MappingUtil;

import com.daisyit.backend.dao.AllCodeDAO;
import com.daisyit.backend.dao.CardInfoDAO;
import com.daisyit.backend.dao.ClassDAO;
import com.daisyit.backend.dao.CusTransDAO;
import com.daisyit.backend.dao.CustomerDAO;
import com.daisyit.backend.dao.GradeDAO;
import com.daisyit.backend.dao.IdGeneratorDAO;
import com.daisyit.backend.dao.LocationDAO;
import com.daisyit.backend.dao.SysvarDAO;
import com.daisyit.backend.model.AllCode;
import com.daisyit.backend.model.AllCodeId;
import com.daisyit.backend.model.CardInfo;
import com.daisyit.backend.model.CardInfoId;
import com.daisyit.backend.model.ClassLst;
import com.daisyit.backend.model.CusTrans;
import com.daisyit.backend.model.CusTransId;
import com.daisyit.backend.model.Customer;
import com.daisyit.backend.model.Grade;
import com.daisyit.backend.model.Location;
import com.daisyit.dto.AllCodeDTO;
import com.daisyit.dto.CardInfoDTO;
import com.daisyit.dto.ClassDTO;
import com.daisyit.dto.CommonDTO;
import com.daisyit.dto.CustomerDTO;
import com.daisyit.dto.GradeDTO;
import com.daisyit.dto.LocationDTO;
import com.daisyit.dto.ResponseDTO;
import com.daisyit.dto.TransferingDTO;
import com.daisyit.frontend.service.CustomerService;
import com.daisyit.frontend.service.UserService;
import com.daisyit.utils.ConstantKey;
import com.opencsv.CSVReader;
import com.springlib.core.dto.CampusDTO;
import com.springlib.core.dto.ClassInfo;
import com.springlib.core.dto.StudentDTO;
import com.springlib.core.dto.StudentInfo;
import com.springlib.core.dto.TokenInfo;
@Service(value="customerService")
public class CustomerServiceImpl implements CustomerService {
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerServiceImpl.class);
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private CardInfoDAO cardInfoDAO;
	@Autowired
	private AllCodeDAO allCodeDAO;
	@Autowired
	private GradeDAO gradeDAO;
	@Autowired
	private ClassDAO classDAO;
	@Autowired
	private LocationDAO locationDAO;
	@Autowired
	private UserService userService;	
	@Autowired
	private SysvarDAO sysvarDAO;
	@Autowired
	private IdGeneratorDAO idGeneratorDAO;
	@Autowired
	private CusTransDAO cusTransDAO;
	@Override
	public List<CustomerDTO> getAllChildrentByParentId(String parentId) {
		
		return null;
	}
	@Override
	public CommonDTO getCommonDTO() {
		CommonDTO commonDTO = new CommonDTO();
		List<LocationDTO> cateenDTOList = new ArrayList<>();
		List<Location> canteenList = locationDAO.getAllCanteen();
		for(Location loc : canteenList)
		{
			LocationDTO dto = new LocationDTO();
			dto.model2DTO(loc);
			cateenDTOList.add(dto);
		}
		commonDTO.setCanteenList(cateenDTOList);
		
		
		List<ClassDTO> classDTOList = new ArrayList<>();
		List<ClassLst> classList = classDAO.findAllClasses();
		for(ClassLst cls : classList)
		{
			ClassDTO dto = new ClassDTO();
			dto.model2DTO(cls);
			classDTOList.add(dto);
		}
		commonDTO.setClassList(classDTOList);
		
		List<GradeDTO> gradeDTOList = new ArrayList<>();
		List<Grade> gradeList = gradeDAO.findAllGrades();
		for(Grade grade : gradeList)
		{
			GradeDTO dto = new GradeDTO();
			dto.model2DTO(grade);
			gradeDTOList.add(dto);
		}
		commonDTO.setGradeList(gradeDTOList);
		List<AllCodeDTO> cusTypeDTOList = new ArrayList<>();
		List<AllCode> cusTypeList = allCodeDAO.getAllCodeByType(ConstantKey.ALLCODE.CUSTOMER_TYPE);
		for(AllCode code : cusTypeList)
		{
			AllCodeDTO dto = new AllCodeDTO();
			dto.model2DTO(code);
			cusTypeDTOList.add(dto);
			
		}
		commonDTO.setCusTypeList(cusTypeDTOList);
		return commonDTO;
	}
	@Override
	public List<CustomerDTO> updateStudentList() {
		List<CustomerDTO> errorList = new ArrayList<>();
		List<CustomerDTO> studentList = new ArrayList<>();
		List<AllCode> exeAccount = allCodeDAO.getAllCodeByType(ConstantKey.ALLCODE.EXE);
		AllCode campusCode = allCodeDAO.getCodeById(ConstantKey.ALLCODE.CAMPUS);
		if(exeAccount.size()!=2 || campusCode ==null)
			return studentList;
		String user =null, password =null;
		for(AllCode code : exeAccount) {
			if(code.getCodeIdx() == 1)
				user = code.getCodeVal();
			if(code.getCodeIdx() == 2)
				password = code.getCodeVal();
		}
		if(user==null || password ==null)
			return studentList;
		TokenInfo tokenInfo = userService.getTokenInfo(user, password);
		String exeApiUrl = sysvarDAO.getValueFromName("m_EXEApiUrl");
		if(exeApiUrl == null)
			return studentList;
		RestClient restClient = new RestClient(new RestTemplate(), exeApiUrl,
				tokenInfo.getAccessToken());
		// Update Classes
		HashMap<String, Integer> args = new HashMap<>();
		args.put("campusId", Integer.parseInt(campusCode.getCodeVal()));
		ResponseEntity<ClassInfo[]> responseCls = restClient.getRequestWithArgs(ClassInfo[].class, "/caterer-service/classes?campusId={campusId}", args);
		if (RestClientErrorHandler.hasError(responseCls.getStatusCode())) {
			return null;
		}
		for (ClassInfo clsInfo : responseCls.getBody()) {
			// insert Class
			ClassLst cls = new ClassLst();
			cls.setClassId(clsInfo.getId());
			cls.setGradeId(clsInfo.getYearLevelID());
			cls.setName(clsInfo.getName());
			classDAO.save(cls);

			// insert Grade
			Grade grade = new Grade();;
			grade.setGradeId(clsInfo.getYearLevelID());
			grade.setName(clsInfo.getYearLevelName());
			gradeDAO.save(grade);

		}
		// Update Students
		args = new HashMap<>();
		args.put("active", 1);
		args.put("campusId", 1);
		ResponseEntity<StudentDTO> response = restClient.getRequestWithArgs(StudentDTO.class,
				"/caterer-service/students?active={active}&campusId={campusId}&limit=5000", args);
		if (RestClientErrorHandler.hasError(response.getStatusCode())) {
			return studentList;
		}
		StudentDTO dto = response.getBody();
		System.out.println(dto.getRows().size());
		Map<Integer, Integer> classMap = classDAO.findClassGradeMap();
		Map<String, String> prCodeMap = allCodeDAO.getCodeMap(ConstantKey.ALLCODE.PRCODE);
		Integer gradeId;
		String prCode;
		String defaultPrCode = allCodeDAO.getCodeById(ConstantKey.ALLCODE.PRCODE_DEFAULT).getCodeVal();
		if (dto.getTotal() > 0) {
			for (StudentInfo studentInfo : dto.getRows()) {
				Customer customer = new Customer();
				customer.setCusId(studentInfo.getStudentID());
				customer.setCusTypeId(ConstantKey.CUSTOMER_TYPE.STUDENT);
				customer.setFamilyName(studentInfo.getFamilyName());
				customer.setGivenName(studentInfo.getGivenName());
				customer.setGender(studentInfo.getGender());
				customer.setNationality(studentInfo.getNationality());
				customer.setClassId(studentInfo.getClassId());
				customer.setActive(studentInfo.isActive());
				customer.setBalance(new BigDecimal(0));
				gradeId = classMap.get(studentInfo.getClassId());
				customer.setCardId(27);
//				if(gradeId ==null) {
//					prCode = prCodeMap.get(String.valueOf(gradeId));
//					if(prCode ==null)
//						customer.setPrCode(defaultPrCode);
//					else
//						customer.setPrCode(prCode);
//				}
//				else {
//					customer.setPrCode(defaultPrCode); //Prcode Default	
//				}	
				customer.setPrCode(defaultPrCode);
				try {
					customerDAO.saveOrUpdate(customer);
					CustomerDTO sDto = new CustomerDTO();
					sDto.model2DTO(customer);
					studentList.add(sDto);	
				} catch (Exception ex) {
					ex.printStackTrace();
					CustomerDTO eDto = new CustomerDTO();
					eDto.model2DTO(customer);
					errorList.add(eDto);	
				}
			}
		}
		
		System.out.println(errorList.size());
		return studentList;
	}

	@SuppressWarnings("unused")
	@Override
	public List<CustomerDTO> insertCustomerFromFile(String url) {
		CSVReader reader = null;
		List<CustomerDTO> cusList = new ArrayList<>();
		Map<String, Integer> classMap = classDAO.findAllClassMap();
		try {
			reader = new CSVReader(new FileReader(url));
			String[] content;
			if(reader == null)
				return cusList;			
			reader.readNext(); 
			reader.readNext(); 
			while ((content = reader.readNext()) != null) {
				CustomerDTO cusDto = new CustomerDTO();
				if(content.length ==14 && content[1] !=null)
				{	
					cusDto.setCusId(content[1]);
					cusDto.setFirstName(content[2]);
					cusDto.setLastName(content[3]);
					cusDto.setCusName(content[2] + " " +content[3]);
					// Gender
					if(content[4].equals("M"))
					{
						cusDto.setGender(0);
						cusDto.setSex("Male");
					}else {
						cusDto.setGender(1);
						cusDto.setSex("Female");						
					}
					
					if(content[5].equals(ConstantKey.CUSTOMER_TYPE.STUDENT))// if student
					{
						cusDto.setCusTypeId(ConstantKey.CUSTOMER_TYPE.STUDENT);
						cusDto.setClassId(classMap.get(content[6]));
						//cusDto.setClassCode(content[6]);
					}else if(content[5].equals(ConstantKey.CUSTOMER_TYPE.TEACHER)) {
						cusDto.setCusTypeId(ConstantKey.CUSTOMER_TYPE.TEACHER);
						cusDto.setClassId(classMap.get(content[6]));
					}else {
						cusDto.setCusTypeId(ConstantKey.CUSTOMER_TYPE.OTHER);
						cusDto.setClassId(classMap.get(content[6]));
					}
					CardInfo cardInfo = new CardInfo();
					CardInfoId id = new CardInfoId();
					id.setCardCode(content[7]);
					cardInfo.setId(id);
					cusDto.setCardCode(content[7]);
					cusDto.setNationality(content[8]);
					cusDto.setAddress(content[9]);
					cusDto.setPhoneNum(content[10]);
					cusDto.setDueDate(content[11]);
					cusDto.setEmail(content[12]);
					cusDto.setPrCode(content[13]);
					cusDto.setBalance(0);
					cusDto.setActive(true);
					cusList.add(cusDto);
				}
			}
		}catch(FileNotFoundException ex)
		{
			
		}catch (IOException e) {
			
			e.printStackTrace();
		}
		return cusList;
	}

	@Override
	public int newCustomer(CustomerDTO dto) {
		if(cardInfoDAO.getCardInfoByCode(dto.getCardCode()) !=null)
			return CommonKey.RESPONSE_CODE.EXIST;
		CardInfo cardInfo = new CardInfo();
		CardInfoId cId = new CardInfoId();
		cId.setCardId(null);
		cId.setCardCode(dto.getCardCode());

		cardInfo.setBalance(0L);
		cardInfo.setId(cId);
		//cardInfoDAO.insert(cardInfo);
		
		if(customerDAO.getCustomerById(dto.getCusId(), false)!=null)
			return CommonKey.RESPONSE_CODE.EXIST;
		
		Customer customer = dto.dto2Model();
		customer.setJoinDate(DateConverter.getCurrentDate());
		customer.setCardId(1);		
		customerDAO.insert(customer);
		
		return CommonKey.RESPONSE_CODE.SUCCESS;
	}
	@Override
	public CustomerDTO findCustomerById(String cusId) {
		Customer customer = customerDAO.getCustomerById(cusId, true);		
		CustomerDTO dto = new CustomerDTO();
		if(customer !=null)
			dto.model2DTO(customer);
		CardInfo cardInfo = cardInfoDAO.getCardInfoById(customer.getCardId());
		if (cardInfo != null) {
			dto.setCardCode(cardInfo.getId().getCardCode());
		}
		return dto;
	}
	@Override
	public int transferMoney(TransferingDTO dto, int userId) {
		double srcBalance = dto.getSrcBalance();
		double dstBalance = dto.getDstBalance();
	
		srcBalance = srcBalance - dto.getAmount();
		dstBalance +=  dto.getAmount();
		int ret = cardInfoDAO.updateCardBalance(srcBalance, dto.getSrcCardId());
		if(ret <= 0)
			return CommonKey.RESPONSE_CODE.ERROR;
		ret = cardInfoDAO.updateCardBalance(srcBalance, dto.getDstCardId());
		if(ret<=0)
		{
			// Roll back status 
			ret = cardInfoDAO.updateCardBalance(dto.getSrcBalance(), dto.getSrcCardId());
			return CommonKey.RESPONSE_CODE.ERROR;
		}
		// update customer banlance
		ret = customerDAO.updateCustomerBalance(dto.getSrcCusId(), srcBalance - dto.getAmount());
		if(ret <=0)
		{
			//roll back
			ret = cardInfoDAO.updateCardBalance(dto.getSrcBalance(), dto.getSrcCardId());
			ret = cardInfoDAO.updateCardBalance(dto.getDstBalance(), dto.getSrcCardId());
			return CommonKey.RESPONSE_CODE.ERROR;
		}
		// Add all transaction to CusTrans Table
		CusTrans cusTrans = new CusTrans();
		String transNum = idGeneratorDAO.getTransNum();
		CusTransId id = new CusTransId(transNum, "CARD", "VND");
		cusTrans.setId(id);
		cusTrans.setAmount(new BigDecimal(dto.getAmount()));
		cusTrans.setTransDate(DateConverter.getCurrentDate());
		cusTrans.setTransTime(DateConverter.getCurrentDate());
		cusTrans.setTransType(ConstantKey.TRANS_TYPE.TRASFER);
		cusTrans.setCusId(dto.getSrcCusId());
		cusTrans.setUserId(userId);
		cusTrans.setStatus(true);
		cusTrans.setCurBalance(new BigDecimal(srcBalance - dto.getAmount()));
		cusTransDAO.newCusTran(cusTrans);
		
		////////////////////////////////////////////////////////////////
		ret = customerDAO.updateCustomerBalance(dto.getDstCusId(), dstBalance);
		if(ret <=0)
		{
			//roll back
			ret = cardInfoDAO.updateCardBalance(dto.getSrcBalance(), dto.getSrcCardId());
			ret = cardInfoDAO.updateCardBalance(dto.getDstBalance(), dto.getSrcCardId());
			ret = customerDAO.updateCustomerBalance(dto.getSrcCusId(), dto.getSrcBalance());
			return CommonKey.RESPONSE_CODE.ERROR;
		}
		// Add all transaction to CusTrans Table
		cusTrans = new CusTrans();
	    transNum = idGeneratorDAO.getTransNum();
		id = new CusTransId(transNum, "CARD", "VND");
		cusTrans.setId(id);
		cusTrans.setAmount(new BigDecimal(dto.getAmount()));
		cusTrans.setTransDate(DateConverter.getCurrentDate());
		cusTrans.setTransTime(DateConverter.getCurrentDate());
		cusTrans.setTransType(ConstantKey.TRANS_TYPE.TOP_UP);
		cusTrans.setCusId(dto.getDstCusId());
		cusTrans.setUserId(userId);
		cusTrans.setStatus(true);

		cusTrans.setCurBalance(new BigDecimal(dstBalance + dto.getAmount()));
		cusTransDAO.newCusTran(cusTrans);
		////////////////////////////////////////////////////////////////
		return CommonKey.RESPONSE_CODE.SUCCESS;
	}
	@Override
	public List<CampusDTO> getCampusList() {
		List<AllCode> exeAccount = allCodeDAO.getAllCodeByType(ConstantKey.ALLCODE.EXE);
		if(exeAccount.size()!=2)
			return new ArrayList<>();
		String user =null, password =null;
		for(AllCode code : exeAccount) {
			if(code.getId().getCodeName().equals("USER"))
				user = code.getCodeVal();
			if(code.getId().getCodeName().equals("PASSWORD"))
				password = code.getCodeVal();
		}
		if(user==null || password ==null)
			return new ArrayList<>();
		TokenInfo tokenInfo = userService.getTokenInfo(user, password);
		String exeApiUrl = sysvarDAO.getValueFromName("m_EXEApiUrl");
		if(exeApiUrl == null)
			return new ArrayList<>();
		RestClient restClient = new RestClient(new RestTemplate(), "http://cafeteria.eca.dev.exe.com.vn/api",
				tokenInfo.getAccessToken());
		ResponseEntity<CampusDTO[]> response = restClient.getRequest(CampusDTO[].class, "/caterer-service/campuses");
		if (RestClientErrorHandler.hasError(response.getStatusCode())) {
			return new ArrayList<>();
		}
		return Arrays.asList(response.getBody());
	}
	@Override
	public int updateCampusEnv(CampusDTO dto) {
		AllCode code = new AllCode();
		AllCodeId id = new AllCodeId();
		id.setCodeName(ConstantKey.ALLCODE.CAMPUS);
		code.setId(id);
		code.setCodeVal(String.valueOf(dto.getId()));		
		code.setCodeIdx(2);
		code.setModify(true);
		code.setType(ConstantKey.ALLCODE.LOCATION_TYPE);
		return allCodeDAO.saveOrUpdate(code);
	}
	@Override
	public List<GradeDTO> getGradeList() {
		List<Grade> list = gradeDAO.findAllGrades();
		List<GradeDTO> dtos = new ArrayList<>();
		for(Grade grade : list)
		{
			GradeDTO dto = new GradeDTO();
			dto.model2DTO(grade);
			dtos.add(dto);
		}
		return dtos;
	}
	@Override
	public List<CustomerDTO> newCustomerList(List<CustomerDTO> dtos) {
		List<CustomerDTO> errorList = new ArrayList<>();
		String defaultPrCode = allCodeDAO.getCodeById(ConstantKey.ALLCODE.PRCODE_DEFAULT).getCodeVal();
		for(CustomerDTO dto : dtos) {
			if(customerDAO.getCustomerById(dto.getCusId(), null) !=null)
			{
				dto.setRemark("Duplicated Customer ID");
				errorList.add(dto);
				continue;
			}
			if(cardInfoDAO.getCardInfoByCode(dto.getCardCode()) !=null)
			{
				dto.setRemark("Duplicated Card Code");
				errorList.add(dto);
				continue;
			}
			if(dto.getPrCode() == null || dto.getPrCode().equals(""))
				dto.setPrCode(defaultPrCode);			
			try {
				CardInfo cardInfo = new CardInfo();
				CardInfoId cId = new CardInfoId();
				cId.setCardId(null);
				cId.setCardCode(dto.getCardCode());

				cardInfo.setBalance(0L);
				cardInfo.setId(cId);
				int cardId = cardInfoDAO.insert(cardInfo);
				dto.setCardId(cardId);
				Customer customer = dto.dto2Model();
				customer.setJoinDate(DateConverter.getCurrentDate());		
				customerDAO.insert(customer);
			}catch(Exception ex){
				System.out.println(ex);
				dto.setRemark("System error");
				errorList.add(dto);
			}
		}
		return errorList;
	}
	@Override
	public CustomerDTO findCustomerByCardCode(String cardCode) {
		CustomerDTO dto = new CustomerDTO();
		Customer customer = customerDAO.findCustomerByCardCode(cardCode);
		if (customer != null) {
			dto.model2DTO(customer);
		}
		return dto;
	}
	@Override
	public List<CustomerDTO> getListByClass(Integer classId, String type) {
		LOGGER.info("Start service getListByClass with classId = {}", classId);
		List<CustomerDTO> result = new ArrayList<>();
		List<Object[]> lst = null;
		if (type.equals(ConstantKey.ALL)) {
			lst = customerDAO.getCusInfoByClassId(classId);
		} else if (type.equals(ConstantKey.ACTIVE)) {
			lst = customerDAO.getCusInfoByClassId(classId, true);
		} else if (type.equals(ConstantKey.UNACTIVE)) {
			lst = customerDAO.getCusInfoByClassId(classId, false);
		}
		if (lst != null && !lst.isEmpty()) {
			for (int i =0 ; i < lst.size(); i++) {
				Object[] customer = lst.get(i);
				CustomerDTO dto = new CustomerDTO();
				dto.setCusId(customer[0].toString());
				dto.setFirstName(customer[1].toString());
				dto.setLastName(customer[2].toString());
				dto.setCusName(dto.getFirstName() + " " + dto.getLastName());
				if (customer[3] != null)
					dto.setAddress(customer[3].toString());
				if (customer[4] != null) 
					dto.setPhoneNum(customer[4].toString());
				if (customer[5] != null)
					dto.setEmail(customer[5].toString());
				if (customer[6] != null)
					dto.setRemark(customer[6].toString());
				dto.setCardCode(customer[7].toString());
				dto.setActive((boolean) customer[8]);
				result.add(dto);
			}
		}
		LOGGER.info("End service getListByClass, result : data = {}", MappingUtil.writeValueAsString(result));
		return result;
	}
	@Override
	public ResponseDTO updateCardId(List<CustomerDTO> dtos) {
		if (dtos != null && !dtos.isEmpty()) {
			List<String> cardExist = new ArrayList<>();
			List<CustomerDTO> lst = new ArrayList<>();
			for (CustomerDTO dto : dtos) {
				Customer cus = customerDAO.findCustomerByCusIdAndCardCode(dto.getCusId(), dto.getCardCode());
				if (cus == null) {
					lst.add(dto);
				}
			}
			if (!lst.isEmpty()) {
				for (CustomerDTO dto : lst) {
					Customer cus = customerDAO.findCustomerByCardCode(dto.getCardCode());
					if (cus != null ) {
						cardExist.add(dto.getCardCode());
					}
				}
			}
			if (!cardExist.isEmpty()) {
				return new ResponseDTO(false, CommonKey.RESPONSE_CODE.EXIST, "Card Code is existed", cardExist);
			} else {
				for (CustomerDTO dto : lst) {
					int cardId = 0;
					CardInfo cardInfo = cardInfoDAO.getCardInfoByCode(dto.getCardCode());
					if (cardInfo != null) {
						cardId = cardInfo.getId().getCardId();
					} else {
						cardInfo = new CardInfo();
						CardInfoId cId = new CardInfoId();
						cId.setCardId(null);
						cId.setCardCode(dto.getCardCode());
						cardInfo.setBalance(0L);
						cardInfo.setId(cId);
						cardId = cardInfoDAO.insert(cardInfo);
					}
					if (!customerDAO.updateCardCustomer(dto.getCusId(), cardId)) {
						return new ResponseDTO(false, CommonKey.RESPONSE_CODE.ERROR);
					}
				}
				return new ResponseDTO(true, CommonKey.RESPONSE_CODE.SUCCESS);
			}
		}
		return new ResponseDTO(false, CommonKey.RESPONSE_CODE.ERROR);
	}
	
	@Override
	public ResponseDTO disableStudent(List<CustomerDTO> dtos) {
		if (dtos != null && !dtos.isEmpty()) {
			for (CustomerDTO dto : dtos) {
				customerDAO.disableStudent(dto.getCusId(), false);
			}
			return new ResponseDTO(true, CommonKey.RESPONSE_CODE.SUCCESS);
		}
		return new ResponseDTO(false, CommonKey.RESPONSE_CODE.ERROR);
	}
	@Override
	public List<CustomerDTO> processCardList(String url) {
		List<CustomerDTO> errorList = new ArrayList<>();
		CSVReader reader = null;
		CustomerDTO dto;
		CardInfoDTO cardDTO;
		try {
			// STT CusID CardCode Amount
			reader = new CSVReader(new FileReader(url));
			String[] content;
			reader.readNext();		
			reader.readNext();		
			int id = 0;
			Customer customer;
			while ((content = reader.readNext()) != null) {
				try {
					CardInfo cardInfo = cardInfoDAO.getCardInfoByCode(content[2]);
					if(cardInfo == null) {
						cardDTO = new CardInfoDTO();
						cardDTO.setCardCode(content[2]);
						cardDTO.setBalance(Double.parseDouble(content[3]));
						cardDTO.setActive(true);
						CardInfo card = cardDTO.dto2Model();
						id = cardInfoDAO.insert(card);	
					}else {
						dto = new CustomerDTO();
						dto.setCusId(content[1]);
						dto.setCardCode(content[2]);
						dto.setBalance(Double.parseDouble(content[3]));
						dto.setRemark("Card Code existed!");
						errorList.add(dto);
						continue;
					}
				}catch(Exception ex){
					dto = new CustomerDTO();
					dto.setCusId(content[1]);
					dto.setCardCode(content[2]);
					dto.setBalance(Double.parseDouble(content[3]));
					dto.setRemark("System Error");
					errorList.add(dto);
					continue;
				}
			    customer = customerDAO.getCustomerById(content[1], true);
				try {
					if(customer !=null)
					{
						customer.setCardId(id);
						customer.setBalance(new BigDecimal(Double.parseDouble(content[3])));
						customerDAO.saveOrUpdate(customer);
					}else {
						cardInfoDAO.delete(id);
						cardInfoDAO.delete(id);
						dto = new CustomerDTO();
						dto.setCusId(content[1]);
						dto.setCardCode(content[2]);
						dto.setBalance(Double.parseDouble(content[3]));
						dto.setRemark("Customer code is not existed!");
						errorList.add(dto);
					}
				}catch(Exception ex){
					cardInfoDAO.delete(id);
					dto = new CustomerDTO();
					dto.setCusId(content[1]);
					dto.setCardCode(content[2]);
					dto.setBalance(Double.parseDouble(content[3]));
					dto.setRemark("System Error");
					errorList.add(dto);
				}
			}
		}catch(FileNotFoundException ex) {
			LOGGER.error(ex.getMessage()  + "File: "+ url);
		}catch(IOException ex) {
			LOGGER.error(ex.getMessage());
		}
		return errorList;
	}
	
	@Override
	public List<CustomerDTO> getAllChildrentByParentId(int parentId, String accessToken, String refreshToken) {
		List<CustomerDTO>  customerDTOs = new ArrayList<>();
		String exeApiUrl = sysvarDAO.getValueFromName(ConstantKey.EXE_API_URL);
		RestClient restClient = new RestClient(new RestTemplate(), exeApiUrl, accessToken);	
		HashMap<String, Integer> args = new HashMap<>();
	    args.put("active", 1);
	    args.put("parentID", parentId);
		ResponseEntity<StudentDTO> response = restClient.getRequestWithArgs(StudentDTO.class, "/caterer-service/students?active={active}&parentID={parentID}&limit=5000", args);
	    if (RestClientErrorHandler.hasError(response.getStatusCode())) {
			TokenInfo tokenInfo = userService.refreshToken(refreshToken);
			if(tokenInfo == null)
				return null;
			restClient = new RestClient(new RestTemplate(), exeApiUrl, tokenInfo.getAccessToken());	
			response = restClient.getRequestWithArgs(StudentDTO.class, "/caterer-service/students?active={active}&parentID={parentID}&limit=5000", args);
			if (RestClientErrorHandler.hasError(response.getStatusCode()))
			{
				return null;
			}
	    } 
	    StudentDTO dto = response.getBody();
	    if(dto.getTotal() >0)
	    {
	    	for(StudentInfo studentInfo : dto.getRows()) {
	    		Customer customer = customerDAO.getCustomerById(studentInfo.getStudentID(), true);
	    		if(customer ==null)
	    			continue;
	    		CustomerDTO customerDTO = new CustomerDTO();
	    		customerDTO.setCusId(studentInfo.getStudentID());
	    		customerDTO.setCusName(studentInfo.getGivenName() + " " + studentInfo.getFamilyName());
	    		customerDTO.setCardId(customer.getCardId());

	    		CardInfo cardInfo = cardInfoDAO.getCardInfoById(customer.getCardId());
	    		if(cardInfo != null) {
	    			customerDTO.setCardCode(cardInfo.getId().getCardCode());
	    			customerDTO.setBalance(cardInfo.getBalance());    			
	    		}else {
	    			customerDTO.setCardCode(null);
	    			customerDTO.setBalance(0);   
	    		}
	    		customerDTOs.add(customerDTO);
	    	}
	    }
		return customerDTOs;
	}
	@Override
	public boolean verifyToken(int parentId, String accessToken, String refreshToken) {
		String exeApiUrl = sysvarDAO.getValueFromName(ConstantKey.EXE_API_URL);
		RestClient restClient = new RestClient(new RestTemplate(), exeApiUrl, accessToken);	
		HashMap<String, Integer> args = new HashMap<>();
	    args.put("active", 1);
	    args.put("parentID", parentId);
		ResponseEntity<StudentDTO> response = restClient.getRequestWithArgs(StudentDTO.class, "/caterer-service/students?active={active}&parentID={parentID}", args);
	    if (RestClientErrorHandler.hasError(response.getStatusCode())) {
			return false;
	    }
	    return true;
	}
}
