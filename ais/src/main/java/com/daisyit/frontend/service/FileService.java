package com.daisyit.frontend.service;

import java.util.List;

public interface FileService {
	public List<String> findAllImageFilePath(); 
	public String getImageDir();
	public List<String> findAllImageFiles();
	public boolean deleteImageFileByName(String name);
}
