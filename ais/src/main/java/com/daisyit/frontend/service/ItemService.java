package com.daisyit.frontend.service;

import java.util.List;

import com.daisyit.dto.AllCodeDTO;
import com.daisyit.dto.GradeDTO;
import com.daisyit.dto.ItemGrpDTO;
import com.daisyit.dto.ItemListDTO;
import com.daisyit.dto.ItemListInfo;
import com.daisyit.dto.PrCodeDTO;
import com.daisyit.dto.RtPriceDTO;

public interface ItemService {	
	int saveItemList(ItemListInfo dto, boolean isNew);
	int newPricePolicy(PrCodeDTO dto);
	int modifyPricePolicy(PrCodeDTO dto);
	int disablePricePolicy(String prCode);
	List<ItemGrpDTO> findAllItemList(); 
	List<ItemListDTO> findAllItemListByGrpId(int itemGrpId);
	double convertCurrency(double rate, double amount);
	List<PrCodeDTO> getAllPriceCode ();
	List<AllCodeDTO> getLstPriceUnit();
	ItemListInfo findItemtLstBySkuCode (String skuCode);
	RtPriceDTO findRtPrice (String prCode, Integer skuId);
	int updatePricePolicyForGrade(List<GradeDTO> gradeDTOs);
	List<AllCodeDTO> getCurrencyList();
}
