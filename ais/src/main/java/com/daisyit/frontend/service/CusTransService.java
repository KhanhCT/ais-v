package com.daisyit.frontend.service;
import java.util.List;

import com.daisyit.dto.CusTransDTO;

public interface CusTransService {
	public List<CusTransDTO>getCusTransHistory(int customerId, String fromDate, String toDate);
	
}
