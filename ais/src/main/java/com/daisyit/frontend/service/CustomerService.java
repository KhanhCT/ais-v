package com.daisyit.frontend.service;

import java.util.List;

import com.daisyit.dto.CommonDTO;
import com.daisyit.dto.CustomerDTO;
import com.daisyit.dto.GradeDTO;
import com.daisyit.dto.ResponseDTO;
import com.daisyit.dto.TransferingDTO;
import com.springlib.core.dto.CampusDTO;

public interface CustomerService {
	
	List<CustomerDTO> getAllChildrentByParentId(String parentId);
	List<CustomerDTO> insertCustomerFromFile(String url);
	int newCustomer(CustomerDTO dto);
	List<CustomerDTO> newCustomerList(List<CustomerDTO> dtos);
	public CommonDTO getCommonDTO();
	public List<CustomerDTO> updateStudentList();
	CustomerDTO findCustomerById(String cusId);
	public int transferMoney(TransferingDTO dto, int userId);
	List<CampusDTO> getCampusList();
	int updateCampusEnv(CampusDTO dto);
	List<GradeDTO> getGradeList();
	CustomerDTO findCustomerByCardCode(String cardCode);
	List<CustomerDTO> getListByClass (Integer classId, String type);
	ResponseDTO updateCardId (List<CustomerDTO> dtos);
	ResponseDTO disableStudent (List<CustomerDTO> dtos);
	public List<CustomerDTO> processCardList(String url);
	
	/***************public ****************************/
	List<CustomerDTO> getAllChildrentByParentId(int parentId, String accessToken, String refreshToken);
	public boolean verifyToken(int parentId, String accessToken, String refreshToken);
}
