package com.daisyit.frontend.service;

import java.util.Date;

import com.nimbusds.jwt.JWTClaimsSet;

public interface JwtService {
	String generateTokenLogin(String username);
	JWTClaimsSet getClaimsFromToken(String token);
	Date generateExpirationDate();
	Date getExpirationDateFromToken(String token);
	String getUsernameFromToken(String token);
	byte[] generateShareSecret();
	Boolean isTokenExpired(String token);
	Boolean validateTokenLogin(String token);
}
