package com.daisyit.frontend.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springlib.core.utils.CommonKey;
import org.springlib.core.utils.MappingUtil;

import com.daisyit.backend.dao.AllCodeDAO;
import com.daisyit.backend.dao.ItemGrpDAO;
import com.daisyit.backend.dao.ItemListDAO;
import com.daisyit.backend.dao.PrCodeDAO;
import com.daisyit.backend.dao.RtPriceDAO;
import com.daisyit.backend.model.AllCode;
import com.daisyit.backend.model.AllCodeId;
import com.daisyit.backend.model.ItemGrp;
import com.daisyit.backend.model.ItemList;
import com.daisyit.backend.model.PrCode;
import com.daisyit.backend.model.RtPrice;
import com.daisyit.dto.AllCodeDTO;
import com.daisyit.dto.GradeDTO;
import com.daisyit.dto.ItemGrpDTO;
import com.daisyit.dto.ItemListDTO;
import com.daisyit.dto.ItemListInfo;
import com.daisyit.dto.PrCodeDTO;
import com.daisyit.dto.RtPriceDTO;
import com.daisyit.frontend.service.ItemService;
import com.daisyit.utils.ConstantKey;

@Service(value = "itemService")
public class ItemServiceImpl implements ItemService {
	private Logger logger = LoggerFactory.getLogger(ItemServiceImpl.class);
	@Autowired
	private ItemListDAO itemDAO;
	@Autowired
	private PrCodeDAO prCodeDAO;
	@Autowired
	private RtPriceDAO rtPriceDAO;
	@Autowired
	private ItemGrpDAO itemGrpDAO;
	@Autowired
	private AllCodeDAO allCodeDAO;

	@Override
	public int saveItemList(ItemListInfo dto, boolean isNew) {
		logger.info("Start service newItemList with dto = {}, isNew = {} ", MappingUtil.writeValueAsString(dto), isNew);
		if (dto != null) {
			try {
				ItemListDTO itemListDTO = dto.getItemListDTO();
				List<RtPriceDTO> list = dto.getListRtPrice();
				ItemList item = itemListDTO.dto2Model();
				if (isNew) {
					item.setOpenDate(new Date());
				}
				itemDAO.save(item);
				List<RtPrice> listPrice = new ArrayList<>();
				for (RtPriceDTO obj : list) {
					RtPrice price = obj.dto2Model();
					if (isNew) {
						price.setOpenDate(new Date());
					} else {
						price.setModiDate(new Date());						
					}
					listPrice.add(price);
				}
				if (!listPrice.isEmpty() && !rtPriceDAO.saveList(listPrice)) {
					return CommonKey.RESPONSE_CODE.ERROR;
				}
				logger.info("End service newItemList, result : data = {} ", true);
				return CommonKey.RESPONSE_CODE.SUCCESS;
			} catch (Exception ex) {
				logger.error("Exception:{}, Cause {}", ex.getMessage(), ex.getCause().toString());
				return CommonKey.RESPONSE_CODE.ERROR;
			}
		}
		logger.info("End service newItemList, result : data = {} ", false);
		return CommonKey.RESPONSE_CODE.ERROR;
	}

	@Override
	public int newPricePolicy(PrCodeDTO dto) {
		if(prCodeDAO.checkExistPrCode(dto.getPrCode()))
			return CommonKey.RESPONSE_CODE.EXIST;
		PrCode policy = dto.dto2Model();
		try {
			prCodeDAO.insert(policy);
			return CommonKey.RESPONSE_CODE.SUCCESS;
		} catch (Exception ex) {
			logger.error("Exception:{}, Cause {}", ex.getMessage(), ex.getCause().toString());
		}
		return CommonKey.RESPONSE_CODE.ERROR;
	}

	@Override
	public int modifyPricePolicy(PrCodeDTO dto) {
		PrCode policy = dto.dto2Model();
		try {
			prCodeDAO.update(policy);
			return CommonKey.RESPONSE_CODE.SUCCESS;
		} catch (Exception ex) {
			logger.error("Exception:{}, Cause {}", ex.getMessage(), ex.getCause().toString());
		}
		return CommonKey.RESPONSE_CODE.ERROR;
	}

	@Override
	public List<ItemGrpDTO> findAllItemList() {
		List<ItemGrpDTO> dtos = new ArrayList<ItemGrpDTO>();
		List<ItemGrp> list = itemGrpDAO.findAll();
		for (ItemGrp itemGrp : list) {
			ItemGrpDTO dto = new ItemGrpDTO();
			dto.model2DTO(itemGrp);
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public List<ItemListDTO> findAllItemListByGrpId(int itemGrpId) {
		List<ItemListDTO> dtos = new ArrayList<ItemListDTO>();
		List<ItemList> list = itemDAO.getAllItemsByGrpId(itemGrpId);
		for (ItemList itemList : list) {
			ItemListDTO dto = new ItemListDTO();
			dto.model2DTO(itemList);
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public double convertCurrency(double rate, double amount) {
		return amount / rate;
	}

	@Override
	public List<PrCodeDTO> getAllPriceCode() {
		logger.info("Start service getAllPriceCode");
		List<PrCodeDTO> result = null;
		List<PrCode> prCodeList = prCodeDAO.findAll();
		if (prCodeList != null && !prCodeList.isEmpty()){
			result = new ArrayList<>();
			for (PrCode obj : prCodeList){
				PrCodeDTO dto = new PrCodeDTO();
				dto.model2Dto(obj);
				result.add(dto);
			}
		}
		logger.info("End service getAllPriceCode, result : data = {}", MappingUtil.writeValueAsString(result));
		return result;
	}

	@Override
	public List<AllCodeDTO> getLstPriceUnit() {
		logger.info("Start service getLstPriceUnit");
		List<AllCodeDTO> result = null;
		List<AllCode> lst = allCodeDAO.getAllCodeByType(CommonKey.ALL_CODE.PRICE_UNIT);
		if (lst != null && !lst.isEmpty()) {
			result = new ArrayList<>();
			for (AllCode obj : lst) {
				AllCodeDTO bean = new AllCodeDTO(obj.getId().getIdx(), obj.getId().getCodeName(), obj.getType(),
						obj.getCodeIdx(), obj.getCodeVal(), obj.getModify());
				result.add(bean);
			}
		}
		logger.info("End service getLstPriceUnit, result : data = {}", MappingUtil.writeValueAsString(result));
		return result;
	}

	@Override
	public ItemListInfo findItemtLstBySkuCode(String skuCode) {
		logger.info("Start service findItemtLstBySkuCode with skuCode = {} ", skuCode);
		ItemListInfo result = new ItemListInfo();
		ItemList obj = itemDAO.findItemFromCode(skuCode);
		ItemListDTO itemListDTO = null;
		List<RtPriceDTO> lst = null;
		if (obj != null){
			itemListDTO = new ItemListDTO();
			itemListDTO.model2DTO(obj);
			List<RtPrice> listRtPrice = rtPriceDAO.getLstRtPriceBySkuId(obj.getId().getSkuId());
			if (listRtPrice != null && !listRtPrice.isEmpty()){
				lst = new ArrayList<>();
				for (RtPrice rtPrice : listRtPrice) {
					RtPriceDTO rtPriceDTO = new RtPriceDTO();
					rtPriceDTO.model2DTO(rtPrice);
					lst.add(rtPriceDTO);
				}
			}
		}
		result.setItemListDTO(itemListDTO);
		result.setListRtPrice(lst);
		logger.info("End service findItemtLstBySkuCode, result : data = {} ", MappingUtil.writeValueAsString(result));
		return result;
	}

	@Override
	public RtPriceDTO findRtPrice(String prCode, Integer skuId) {
		logger.info("Start service findRtPrice with prCode = {}, skuId = {} ", prCode, skuId);
		if (prCode != null && skuId != null) {
			RtPrice obj = rtPriceDAO.findRtPrice(prCode, skuId);
			if (obj != null) {
				RtPriceDTO bean = new RtPriceDTO();
				bean.model2DTO(obj);
				return bean;
			}
		}
		return null;
	}
	@Override
	public int updatePricePolicyForGrade(List<GradeDTO> gradeDTOs) {
		int i = 1;
		int ret = 0;
		for(GradeDTO dto : gradeDTOs) {
			AllCode code =new AllCode();
			AllCodeId id = new AllCodeId();
			id.setCodeName(String.valueOf(dto.getGradeId()));
			code.setId(id);
			code.setType(ConstantKey.ALLCODE.PRCODE);
			code.setCodeIdx(i++);
			code.setCodeVal(dto.getPrCode().getPrCode());
			code.setModify(true);
			ret +=allCodeDAO.saveOrUpdate(code);
		}
		return ret;
	}

	@Override
	public int disablePricePolicy(String prCode) {		
		return prCodeDAO.disablePrCode(prCode);
	}

	@Override
	public List<AllCodeDTO> getCurrencyList() {
		List<AllCodeDTO> dtos = new ArrayList<>();
		List<AllCode> codes = allCodeDAO.getAllCodeByType(ConstantKey.ALLCODE.CURRENCY);
		for(AllCode code :codes) {
			AllCodeDTO dto = new AllCodeDTO();
			dto.model2DTO(code);
			dtos.add(dto);
		}
		return dtos;
	}
}
