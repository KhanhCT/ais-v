package com.daisyit.frontend.service;

import java.util.List;

import com.daisyit.dto.CusTransDTO;
import com.daisyit.dto.CustomerDTO;
import com.daisyit.dto.LocationDTO;
import com.daisyit.dto.ReportDTO;
import com.daisyit.dto.SaleTransDTO;
import com.daisyit.dto.TransTypeDTO;

public interface SaleTransService {
	public List<SaleTransDTO> newSaleTransaction(List<SaleTransDTO> dtos, int userId);
	public List<SaleTransDTO> getHistoryByCusId(String fromDate, String toDate, int customerId);
	public List<SaleTransDTO> processStudentOrderFromFile(String url);
	public List<SaleTransDTO> initStudentOrders( int classId);
	public List<ReportDTO<List<SaleTransDTO>>> getTransactionList(String fromDate, String toDate);
	public ReportDTO<List<SaleTransDTO>> getCardTransHistory(String fromDate, String toDate, String custId);
	public List<ReportDTO<List<CustomerDTO>>> getCardBalance();
	public List<CusTransDTO> processToUpList(String url, int userId);
	public List<CusTransDTO> processTopUpList(List<CusTransDTO> dtos, int userId);
	public int pay(List<SaleTransDTO> dtos, int userId);
	public List<LocationDTO> findAllCanteens();
	public List<TransTypeDTO> getAllTransationType();
	public ReportDTO<List<SaleTransDTO>> getSaleTransHistory(String customerId, String fromDate, String toDate);
}
