package com.daisyit.frontend.intercepter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.daisyit.utils.ConstantKey;

@Component
public class AuthInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String uri = request.getRequestURI();
		if (!uri.endsWith("login") && !uri.endsWith("authentication")) {
			String token = (String) request.getSession().getAttribute(ConstantKey.ACCESS_TOKEN);
			String role = (String) request.getSession().getAttribute(ConstantKey.ROLE.NAME);
			if (role == null) {				
				response.sendRedirect(request.getContextPath() + "/login");
				return false;
			}else {
				if(role.equals(ConstantKey.ROLE.ROLE_ADMIN))
				{
					return true;
				}else {
					if(token == null || token.equals(""))
					{
						response.sendRedirect(request.getContextPath() + "/login");
						return false;
					}else
						return true;
				}
			}
		}
		return true;
	}
}
