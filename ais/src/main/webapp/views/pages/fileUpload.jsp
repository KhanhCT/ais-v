<%--
  Created by IntelliJ IDEA.
  User: chien
  Date: 27-Jun-18
  Time: 9:59 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<style>
.custom-file-label, .custom-file-label::after {
	line-height: 1.75;
	cursor: pointer;
}
#image-input {
	display: none;
}
#loader-wrapper {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 1000;
	background: rgba(255,255,255,0.8);
	display: none;
}
#loader {
	display: block;
	position: relative;
	left: 50%;
	top: 50%;
	width: 150px;
	height: 150px;
	margin: -75px 0 0 -75px;
	border-radius: 50%;
	border: 3px solid transparent;
	border-top-color: #3498db;
	-webkit-animation: spin 2s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
	animation: spin 2s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
}

#loader:before {
	content: "";
	position: absolute;
	top: 5px;
	left: 5px;
	right: 5px;
	bottom: 5px;
	border-radius: 50%;
	border: 3px solid transparent;
	border-top-color: #e74c3c;
	-webkit-animation: spin 3s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
	animation: spin 3s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
}

#loader:after {
	content: "";
	position: absolute;
	top: 15px;
	left: 15px;
	right: 15px;
	bottom: 15px;
	border-radius: 50%;
	border: 3px solid transparent;
	border-top-color: #f9c922;
	-webkit-animation: spin 1.5s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
	animation: spin 1.5s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
}

@-webkit-keyframes spin {
	0%   {
		-webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
		-ms-transform: rotate(0deg);  /* IE 9 */
		transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
	}
	100% {
		-webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
		-ms-transform: rotate(360deg);  /* IE 9 */
		transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
	}
}
@keyframes spin {
	0%   {
		-webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
		-ms-transform: rotate(0deg);  /* IE 9 */
		transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
	}
	100% {
		-webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
		-ms-transform: rotate(360deg);  /* IE 9 */
		transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
	}
}
</style>
<script>
	var contextPath = '${pageContext.servletContext.contextPath}';
	var URI_UPLOAD = 'uploadMultipleFile';
	var GET_ALL_IMAGE = 'findAllImageFiles'
	var DELETE_IMAGE = 'deleteImageFileByName?imageName='
    $(document).ready(function () {
		var fileGlobal = [];
		getAllImage();
		$(document).on("change", "#image-input:file", function () {
			var input = $(this);
			if (input.get(0).files) {
				if (fileGlobal.length === 0) {
					for (var i = 0; i < input.get(0).files.length; i++) {
						fileGlobal.push(input.get(0).files[i]);
					}
				} else {
					for (var i = 0; i < input.get(0).files.length; i++) {
					    var isFind = false;
						fileGlobal.forEach(function (item) {
							if (item.name === input.get(0).files[i].name
								&& item.size === input.get(0).files[i].size) {
								isFind = true;
							}
						});
						if (!isFind) {
							fileGlobal.push(input.get(0).files[i]);
						}
					}
				}
				var data = new FormData();
				fileGlobal.forEach(function (file) {
					data.append('file', file);
				});
				$.ajax({
					url: contextPath + CONTSTANT_SERVICE_URI.URI_FILE + URI_UPLOAD,
					type: 'POST',
					contentType: false,
					processData: false,
					data: data,
                    beforeSend: function() {
                        $('#loader-wrapper').fadeIn('fast');
                    },
					success: function (result) {
					    getAllImage();
                        $('#loader-wrapper').fadeOut('slow');
					},
					error: function (xhr, result, errorThrown) {
                        $('#loader-wrapper').fadeOut('slow');
						alert("Cannot upload files : " + result + " .Please try again!");
					}
				});
			}
		});
		$(document).on("click", ".delete-file-btn", function () {
			if ($(this).attr('indexFile')) {
				var nameOfFile = $(this).attr('nameOfFile');
				var index = Number($(this).attr('indexFile'));
				$.ajax({
					url: contextPath + CONTSTANT_SERVICE_URI.URI_FILE + DELETE_IMAGE + nameOfFile,
					type: 'GET',
					dataType: 'json',
					async: false,
                    beforeSend: function() {
                        $('#loader-wrapper').fadeIn('fast');
                    },
					success: function (data) {
						getAllImage();
                        $('#loader-wrapper').fadeOut('slow');
					},
                    error: function (xhr, result, errorThrown) {
                        $('#loader-wrapper').fadeOut('slow');
                        alert("Cannot delete files : " + result + " .Please try again!");
                    }
				});
			}
		});
		$("#image-input:file").on("fileselect", function (event, numFiles, label) {
			var input = $(this).parents(".input-group").find(":text"), log = numFiles > 1 ? numFiles + " files selected" : label;
			if (input.length) {
				input.val(log);
			} else {
				if (log) alert(log);
			}
		});
    });

    function getAllImage() {
        $.ajax({
			url: contextPath + CONTSTANT_SERVICE_URI.URI_FILE + GET_ALL_IMAGE,
			type: 'GET',
			dataType: 'json',
			async: false,
			success: function (data) {
				var htmlTable = '';
				for (var i = 0; i < data.length; i++) {
					htmlTable += ['<tr class="row m-0"><td class="d-inline-block col-1 text-center">', (i + 1), '</td>',
					'<td class="d-inline-block col-9 text-center">', data[i], '</td>',
					'<td class="d-inline-block col-2 text-center">',
					'<button class="btn btn-outline-danger delete-file-btn" indexFile=', i, ' nameOfFile = ', data[i], '>',
					'<span class="fa fa-trash"></span>',
					'</td></tr>'].join('');
				}
				$('#tableFile tbody').html(htmlTable);
			}
		});
    }
</script>
<c:if test="${sessionScope.UserRoleValue == 'Admin'}">
<div class="container">
	<div id="loader-wrapper">
		<div id="loader"></div>
	</div>
	<div class="row col-12">
		<div class="input-group">

			<input type="text" class="form-control rounded-0" readonly
				placeholder=<spring:message code="label.select_image"/>> 
			<label
				class="input-group-btn my-0"> <span
				class="btn btn-large btn-outline-primary rounded-0" id="browse">
					<spring:message code="label.btn_browse"/>&hellip; <input id="image-input" type="file" name="file"
					accept="image/*" multiple>
			</span>
			</label>
		</div>
	</div>
	</br>
	<div class="row col-12">
		<table class="table table-bordered" id="tableFile">
			<thead>
				<tr class="row m-0">
					<th class="d-inline-block col-1 text-center"><spring:message code="label.no"/></th>
					<th class="d-inline-block col-9 text-center"><spring:message code="label.image_name"/></th>
					<th class="d-inline-block col-2 text-center"><spring:message code="label.image_action"/></th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</div>
</c:if>