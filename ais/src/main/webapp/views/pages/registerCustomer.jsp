<%--
  Created by IntelliJ IDEA.
  User: chien
  Date: 09-Jul-18
  Time: 9:07 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/registerCustomer.css"></c:url>">
<script>var contextPath = '${pageContext.servletContext.contextPath}';</script>
<div class="container wrap-register-user" ng-controller="registerCustomerCtrl" ng-init="initializeData()">
    <div class="loader-wrapper" ng-if="showLoaderAllPage">
        <div class="loader"></div>
    </div>
    <div class="row m-t-30 m-b-20">
        <div class="col-md-8 offset-md-2 text-left font-mont">
            <span class="tag text-center"><spring:message code="cum.rc.title_page"/></span>
        </div>
    </div>
    <div class="row wrap-form-register" ng-form="formRegister">
        <div class="col-md-6 offset-md-3 wrap-from-content">
            <div class="loader-wrapper" ng-if="showLoaderCustomer" ng-cloak>
                <div class="loader"></div>
            </div>
            <div class="form-group row">
                <label for="cusId" class="col-sm-5 col-form-label text-left"><spring:message code="cum.cus_id"/></label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="text" id="cusId" ng-model="customerObject.cusId"
                               class="form-control" required="required" autocomplete="off"  >
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="firstName" class="col-sm-5 col-form-label text-left"><spring:message code="cum.first_name"/></label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="text" id="firstName" ng-model="customerObject.firstName"
                               class="form-control" required="required" autocomplete="off"  >
                    </div>
                </div>
                
            </div>
             <div class="form-group row">
                <label for="lastName" class="col-sm-5 col-form-label text-left"><spring:message code="cum.last_name"/></label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="text" id="lastName" ng-model="customerObject.lastName"
                               class="form-control" required="required" autocomplete="off"  >
                    </div>
                </div>
                
            </div>
            <div class="form-group row">
                <label for="Gender" class="col-sm-5 col-form-label text-left"><spring:message code="cum.gender"/></label>
                <div class="col-sm-7" id="Gender">
                    <span class="checkbox-switch-label-first"
                          ng-class="{disabled : customerObject['gender'] != 1 || customerObject['gender'] == 0}"><spring:message code="cum.male"/></span>
                    <input class="checkboxSwitch" type="checkbox"
                           ng-model="customerObject['gender']"
                           ng-true-value="1"
                           ng-false-value="0"
                           ng-checked="customerObject['gender'] == 1"
                           id="inputSwitchGender"/>
                           <label class="label-checkboxSwitch" for="inputSwitchGender"></label>
                    <span class="checkbox-switch-label"
                          ng-class="{disabled : customerObject['gender'] == 1}"><spring:message code="cum.female"/></span>
                </div>
            </div>
           <!--  <div class="form-group row">
                <label for="isStudent" class="col-sm-5 col-form-label text-left">Self:</label>
                <div class="col-sm-7" id="isStudent">
                    <span class="checkbox-switch-label-first"
                          ng-class="{disabled : customerObject['isStudent'] != true || customerObject['isStudent'] == false}">Student</span>
                    <input class="checkboxSwitch" type="checkbox"
                           ng-model="customerObject['isStudent']"
                           ng-true-value="true"
                           ng-false-value="false"
                           ng-checked="customerObject['isStudent'] == true"
                           id="inputSwitchBool"/><label class="label-checkboxSwitch"
                                                          for="inputSwitchBool"></label>
                    <span class="checkbox-switch-label"
                          ng-class="{disabled : customerObject['isStudent'] == 1}">Other</span>
                </div>
            </div> -->
            <div class="form-group row">
                <label for="cusType" class="col-sm-5 col-form-label text-left"><spring:message code="cm.type"/></label>
                <div class="col-sm-7">
					<select id="cusType" ng-model="selectedCusType"
						class="form-control custom-select custom-select-sm" ng-cloak
						ng-options="cusTypeObj as cusTypeObj.codeName for cusTypeObj in cusTypeList track by cusTypeObj.codeName">
					</select>
				</div>
            </div>
            <div class="form-group row">
                <label for="gradeId" class="col-sm-5 col-form-label text-left"><spring:message code="cum.grade"/></label>
                <div class="col-sm-7">
					<select id="gradeId" ng-model="selectedGrade"
						class="form-control custom-select custom-select-sm" ng-cloak
						ng-options="gradeObj as gradeObj.name for gradeObj in gradeList track by gradeObj.gradeId">
					</select>
				</div>
            </div>
            <div class="form-group row">
                <label for="classId" class="col-sm-5 col-form-label text-left"><spring:message code="cum.class"/></label>
                <div class="col-sm-7">
					<select id="classId" ng-model="selectedClass"
						class="form-control custom-select custom-select-sm" ng-cloak
						ng-options="classObj as classObj.name for classObj in classFilter track by classObj.classId">
					</select>
				</div>
            </div>
            <div class="form-group row">
                <label for="prCode" class="col-sm-5 col-form-label text-left"><spring:message code="im.price_policy"/></label>
                <div class="col-sm-7">
					<select id="prCode" ng-model="selectedPrCode"
						class="form-control custom-select custom-select-sm" ng-cloak
						ng-options="prCodeObj as prCodeObj.name for prCodeObj in prCodeList track by prCodeObj.prCode">
					</select>
				</div>
            </div>
            <div class="form-group row">
                <label for="cardCode" class="col-sm-5 col-form-label text-left"><spring:message code="cum.card_num"/></label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="text" id="cardCode" ng-model="customerObject.cardCode" class="form-control" required="required" autocomplete="off">
                    </div>
                </div>
            </div>
<!--             <div class="form-group row">
                <label for="balance" class="col-sm-5 col-form-label text-left">Balance:</label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="text" id="balance" ng-model="customerObject.balance" class="form-control" required="required" autocomplete="off">
                    </div>
                </div>
            </div> -->
            <div class="form-group row">
                <label for="national" class="col-sm-5 col-form-label text-left"><spring:message code="cum.nationality"/></label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="text" id="national" ng-model="customerObject.nationality" class="form-control">
                    </div>
                </div>
            </div>
             <div class="form-group row">
                <label for="addresss" class="col-sm-5 col-form-label text-left"><spring:message code="cum.address"/></label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="text" id="addresss" ng-model="customerObject.address" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="phone" class="col-sm-5 col-form-label text-left"><spring:message code="cum.phone"/></label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="text" id="phone" ng-model="customerObject.phoneNum" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="email" class="col-sm-5 col-form-label text-left"><spring:message code="cum.email"/></label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="text" id="email" ng-model="customerObject.email" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-5 offset-4 text-center">
                    <button class="btn btn-outline-secondary" ng-cloak ng-if="formRegister.$invalid"><spring:message code="cm.register"/></button>
                    <button class="btn btn-outline-success" ng-cloak ng-if="formRegister.$valid" ng-click="registerCustomer()"><spring:message code="cm.register"/></button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<c:url value="/assets/js/registerCustomer.js"></c:url>"></script>