<%--
  Created by IntelliJ IDEA.
  User: chien
  Date: 19-Jul-18
  Time: 10:37 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<style>
    .main-content-template{
        margin: 0;
    }
    .wrapper-error-page{
        width: 100%;
        height: 100%;
        position: absolute;
        background: rgba(241, 241, 241, 0.5);
    }
    .wrapper-icon-error{
        width: 100%;
        height: 200px;
        margin-top: 100px;
        float: left;
    }
    .wrapper-icon-error img{
        display: block;
        height: 100%;
        margin: 0 auto;
    }
    .wrapper-bottom-message{
        width: 100%;
        display: block;
        text-align: center;
        margin-top: 30px;
        float: left;
    }
    .mess-normal-font{
        font-size: 18pt;
        color: rgb(166,166,166);
        font-family: Montserrat, Helvetica, arial, helvetica, sans-serif;
    }
    @media screen and (max-width: 479px){
        .mess-normal-font{
            font-size: 12pt;
        }
        .wrapper-icon-error{
            margin-top: 50px;
        }
    }
</style>
<div class="wrapper-error-page">
    <div class="wrapper-icon-error">
        <img src="../assets/images/error-icon.png" alt="error page">
    </div>
    <div class="wrapper-bottom-message">
        <span class="mess-normal-font">SORRY, The page you tried cannot be found or your permission denied!</span>
    </div>
</div>
