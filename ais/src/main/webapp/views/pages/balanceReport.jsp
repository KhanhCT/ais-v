<%--
  Created by IntelliJ IDEA.
  User: chien
  Date: 03-Jul-18
  Time: 00:16 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/balanceReport.css"></c:url>">
<script>
    var contextPath = '${pageContext.servletContext.contextPath}';
</script>
<script src="<c:url value="/assets/js/balanceInit.js"></c:url>"></script>
<div class="container" ng-controller="balanceCtrl">
    <div class="row m-t-30 m-b-20">
        <div class="col-md-12 text-left font-mont">
            <span class="tag text-center">Balance Report</span>
        </div>
    </div>
    <div class="row wrap-filter">

    </div>
    <div class="row wrap-table">
        <div class="col-md-12">
            <table ng-cloak class="table table-custom-balance">
                <thead>
                    <tr>
                        <th class="text-center"><span>No.</span></th>
                        <th ng-repeat="key in listKeyTable" class="text-center">
                            <span ng-bind="key['name']"></span>
                        </th>
                    </tr>
                </thead>
                <tbody ng-repeat="balanceObj in dataBalance">
                    <tr ng-if="dataBalance.length > 0" class="custom-row-info">
                        <td colspan="{{(listKeyTable.length + 1)}}">Class/Title: <span ng-bind="balanceObj.gradeId"></span> - <span ng-bind="balanceObj.gradeName"></span></td>
                    </tr>
                    <tr ng-if="dataBalance.length > 0" class="row-info-dot" ng-repeat="data in balanceObj.data">
                        <td><span ng-bind="{{$index + 1}}"></span></td>
                        <td ng-repeat="key in listKeyTable" ng-class="key.iClass">
                            <span ng-bind="getStringDataByKey(key['code'], data)"></span>
                        </td>
                    </tr>
                    <tr ng-if="dataBalance.length > 0" class="custom-row-info">
                        <td class="border-right-none"></td>
                        <td colspan="2" class="border-right-none border-left-none">Total by Class/Title: <span ng-bind="balanceObj.gradeId"></span> - <span ng-bind="balanceObj.gradeName"></span></td>
                        <td class="border-right-none border-left-none text-right">(Total Addition)</td>
                        <td class="border-right-none border-left-none text-right">(Total Consumpsion)</td>
                        <td class="border-right-none border-left-none text-right">(Total Balance)</td>
                        <td class="border-left-none"></td>
                    </tr>
                    <tr ng-if="dataBalance.length === 0">
                        <td class="text-center" colspan="{{(listKeyTable.length + 1)}}"><span><spring:message code="label.no_data"/></span></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>