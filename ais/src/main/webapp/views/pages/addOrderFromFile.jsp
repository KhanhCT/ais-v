<%--
  Created by IntelliJ IDEA.
  User: KhanhCT
  Date: 04/06/2018
  Time: 23:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/orderFromFile.css"></c:url>">
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/libs/mcustomscroll/jquery.mCustomScrollbar.min.css"></c:url>">
<script>var contextPath = '${pageContext.servletContext.contextPath}';</script>
<script src="<c:url value="/assets/libs/mcustomscroll/jquery.mCustomScrollbar.concat.min.js"></c:url>"></script>
<div class="container wrapper-customer-order" ng-controller="orderFromFile" ng-init="initializeData()">
    <div id="loader-wrapper" ng-if="showLoaderAllPage" ng-cloak>
        <div id="loader"></div>
    </div>
    <div class="row m-t-5 m-b-10">
        <div class="col-md-12 text-left font-mont">
            <span class="tag text-center"><spring:message code="ngo.title_page"/></span>
        </div>
    </div>
    <div class="row m-l-10 m-r-10">
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 row m-t-10">
			<label for="gradeId" class="col-4 col-form-label text-left"><spring:message code="cum.grade"/></label>
			<div class="col-8">
				<select id="gradeId" ng-model="selectedGrade" ng-change="gradeChanged()"
					class="form-control custom-select custom-select-sm" ng-cloak
					ng-options="grade as grade.name for grade in gradeList track by grade.gradeId">
				</select>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 row m-t-10">
			<label for="classId" class="col-4 col-form-label text-left"><spring:message code="cum.class"/></label>
			<div class="col-8">
				<select id="classId" ng-model="selectedClass"
					class="form-control custom-select custom-select-sm" ng-cloak
					ng-options="classObj as classObj.name for classObj in classFilter track by classObj.classId">
				</select>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 row m-t-10">
			<label for="cantenId" class="col-4 col-form-label text-left"><spring:message code="ngo.canteen"/></label>
			<div class="col-8">
				<select id="cantenId" ng-model="selectedCanteen"
					class="form-control custom-select custom-select-sm" ng-cloak
					ng-options="canteen as canteen.name for canteen in canteenList track by canteen.locationId">
				</select>
			</div>
		</div>
	</div>
	
	<div class="row m-l-10 m-r-10 m-b-10 ">
		<div class="col-md-12">
			<div class="row">
				<label for="mealId" class="col-md-2 text-left pull-left m-t-10"><spring:message code="om.item_name"/></label>
				<div class="col-md-6 pull-left m-t-10">
					<select id="mealId" ng-model="selectedMeal"
							class="form-control custom-select custom-select-sm" required ng-cloak
							ng-options="meal as meal.name for meal in mealList track by meal.skuId">
					</select>
				</div>
				<div class="col-md-2 pull-right m-t-10">
					<button class="btn btn-primary btn-block pull-right" ng-disabled="!selectedGrade || !selectedClass || !selectedCanteen || !selectedMeal || fileToUpload" type="button" ng-click="applyFunc()"><spring:message code="cm.apply"/></button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" ng-form="uploadForm" ng-cloak>
			<div ngf-drop ngf-select ng-model="fileToUpload" class="drop-box"
				 ngf-drag-over-class="'dragover'" ngf-multiple="false" ngf-allow-dir="true"
				 ngf-pattern="'.xls,.csv,.xlsx'" accept=".xls,.csv,.xlsx" ngf-max-size="20MB">
				<div class="content-drag">
					<i class="fa fa-cloud-upload custom-drag"></i>
					<div class="titlte-drag" ng-cloak>
						<span ng-if="!fileToUpload">Browser file to upload...</span>
						<span ng-if="fileToUpload" class="text-primary" ng-bind="fileToUpload.name"></span>
					</div>
					<button class="btn btn-primary custom-button" ng-disabled="!fileToUpload" ng-click="uploadFile($event)"><spring:message code="cm.upload"/></button>
				</div>
			</div>
		</div>
	</div>
	<div class="row wrap-table-config">
		<div class="col-md-12 col-lg-12 col-xs-12 col-md-12 row-custom table">
			<div class="wrap-table-config row">
				<div class="col-md-12 col-lg-12 col-xs-12 col-md-12 row-custom table-custom-config-wrap">
					<table ng-cloak class="table table-condensed table-bordered table-striped table-custom-config"
						   ng-class="{'nomarginbottom': tableTransaction.total() == 0}">
						<thead>
						<tr >
							<th>No.</th>
							<th ng-repeat="key in listKeyTable">
								<div class="name-header" >
									<span ng-bind="key['name']"></span>
								</div>
								<div class="wrap-filter-sort">
									<div class="sort-icon" ng-click="changeSort(key)">
										<img ng-if="!key['sortAZ'] && !key['sortZA']" class="img-sort" ng-src="../assets/images/sort-couple-gray.png">
										<img ng-if="key['sortAZ']" class="img-sort" ng-src="../assets/images/sort-desc-white.png">
										<img ng-if="key['sortZA']" class="img-sort" ng-src="../assets/images/sort-asc-white.png">
									</div>
									<div class="filter-table" ng-click="showFilter(key)">
										<img class="img-filter" ng-if="key['textFilter']" ng-src="../assets/images/filter-white.png">
										<img class="img-filter" ng-if="!key['textFilter']" ng-src="../assets/images/filter-gray.png">
									</div>
								</div>
								<div class="wrap-input-filter" ng-if="key['openFilter']" tw-click-outside="hideFilter(key)">
									<input class="input-filter" placeholder="Search..." type="text" ng-model="key['textFilter']">
									<span class="fa fa-search p-l-6 color-black" ng-if="!key['textFilter']"></span>
									<span class="fa fa-close close-custom" ng-click="clearFilter(key)" ng-if="key['textFilter']"></span>
								</div>
							</th>
							<th style="text-align: center;"><spring:message code="label.action"/></th>
						</tr>
						</thead>
						<tbody>
						<tr dir-paginate="objectData in studentOrders | orderByCustom:sortColumn | customFilterText:listKeyTable | itemsPerPage:itemGap">
							<td><span ng-bind="{{$index + 1}}"></span></td>
							<td ng-repeat="key in listKeyTable" ng-class="key.iClass">
								<span ng-bind="objectData[key['code']]"></span>
							</td>
							<td style="text-align: center" >
								<div class="btn btn-info btn-sm"  tooltip title="Edit" data-toggle="modal" data-target="#editModal" ng-click="editStudentOrder(objectData)">
									<i class="fa fa-pencil" aria-hidden="true"></i>
								</div>
								<div class="btn btn-danger btn-sm" tooltip title="Delete"  ng-click="showDeleteStudentOrder(objectData)">
									<i class="fa fa-trash" aria-hidden="true"></i>
								</div>
							</td>
						</tr>
						<tr ng-if="studentOrders.length === 0">
							<td class="text-center" colspan="{{listKeyTable.length + 2}}"><span><spring:message code="label.no_data"/></span></td>
						</tr>
						</tbody>
						<tfoot>
						<tr>
							<td></td>
							<td ng-repeat="key in listKeyTable" ng-class="key.iClass">
								<span ng-bind="getTotalOfBoard(key)"></span>
							</td>
							<td></td>
						</tr>
						</tfoot>
					</table>
					<div class="row">
						<div class="col-md-6">
							<dir-pagination-controls
									template-url="../assets/html/dirPagination.tpl.html"
									max-size="10"
									direction-links="true"
									boundary-links="true" >
							</dir-pagination-controls>
						</div>
						<div class="col-md-6">
							<ul class="pagination pull-right" ng-if="studentOrders.length > 0" ng-cloak>
								<li ng-repeat="gap in listGap" ng-class="{ active : gap === itemGap }">
									<a href="" ng-click="setItemGap(gap)"><span ng-bind="gap"></span></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<button class="btn btn-warning btn-block" type="button" ng-click="saveOrderList()" ng-disabled="studentOrders.length <= 0"><spring:message code="cm.save"/></button>
		</div>
	</div>
	<div class="modal animated zoomIn" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="editModalLongTitle"><spring:message code="ngo.edit_order"/></h5>
					<button type="button" style="outline: none;" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="col-md-12">
						<div class="form-group row">
							<label for="cusCode" class="col-sm-5 col-form-label text-left"><spring:message code="cum.cus_id"/></label>
							<div class="col-sm-7">
								<div class="input-group">
									<span type="text" id="cusCode" class="form-control" ng-bind="selectedStudentClone.cusId"></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label for="nameID" class="col-sm-5 col-form-label text-left"><spring:message code="no.name"/></label>
							<div class="col-sm-7">
								<div class="input-group">
									<span type="text" id="nameID" class="form-control" ng-bind="selectedStudentClone.cusName"></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label for="mealCloneId" class="col-sm-5 col-form-label text-left"><spring:message code="om.item_name"/></label>
							<div class="col-sm-7">
								<select id="mealCloneId" ng-model="edittedMeal"
										class="form-control custom-select custom-select-sm" required ng-cloak
										ng-options="meal.name for meal in mealList">
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="quantityID" class="col-sm-5 col-form-label text-left"><spring:message code="om.item_qty"/></label>
							<div class="col-sm-4">
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<button class="btn btn-outline-danger" ng-click="minusQuantity()"><span class="fa fa-minus"></span></button>
									</div>
									<input type="text" id="quantityID" class="form-control custom-formcontrol" ng-model="selectedStudentClone.qty" integer-only>
									<div class="input-group-append">
										<button class="btn btn-outline-success" ng-click="plusQuantity()" type="button"><span class="fa fa-plus"></span></button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><spring:message code="cm.cancel"/></button>
					<button type="button" class="btn btn-success" ng-click="submitEdit()"><spring:message code="cm.save"/></button>
				</div>
			</div>
		</div>
	</div>
	<alert-message type-alert="mesObj.type" message-alert="mesObj.text" show-alert="mesObj.active"></alert-message>
</div>
<script src="<c:url value="/assets/js/orderFromFile.js"></c:url>"></script>