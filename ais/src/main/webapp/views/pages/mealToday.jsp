<%--
  Created by IntelliJ IDEA.
  User: chien
  Date: 15/06/2018
  Time: 18:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/libs/css/jquery.bxslider.css"></c:url>">
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/mealTodayStyle.css"></c:url>">
<script>
    var contextPath = '${pageContext.servletContext.contextPath}';
</script>
<div class="container wrapper-meal-today" ng-controller="mealTodayController" ng-init="initData()">
    <div id="loader-wrapper" ng-if="loaderPreviewPage" ng-cloak>
        <div id="loader"></div>
    </div>
    <ul class="slider-meal-today" ng-cloak>
        <li class="item-meal-today" ng-repeat="slider in sliders">
            <img class="image-item" ng-click="changeZoomImage(slider, $event)" ng-src="{{slider.imageUrl}}" />
        </li>
    </ul>
    <div class="wrap-all-page-preview" ng-if="showImageZoom" ng-cloak>
        <div id="cp-header" class="ais-group">
            <div id="cp-title-container" class="aui-item expanded">
                <div>
                    <span class="cp-image-icon size-24 cp-file-icon"></span>
                    <span ng-bind="imageZoomTitle"></span>
                </div>
            </div>
            <div id="cp-file-controls" class="aui-item">
                <span class="fv-close-button">
                    <button role="button" ng-click="hidePreviewZoom($event)" id="cp-control-panel-close" href="#" class="cp-icon">Close</button>
                </span>
            </div>
        </div>
        <div id="cp-body" class="ais-group">
            <div id="cp-file-body" class="meta-banner">
                <%--<div class="wrap-image-show-all">--%>
                    <%--<button type="button" class="close-button-custom close" ng-click="hidePreviewZoom($event)" aria-hidden="true">&times;</button>--%>
                    <%--<div class="wrap-image-show">--%>
                        <%--<img class="image-show-zoom" ng-src="{{imageZoomURL}}" />--%>
                    <%--</div>--%>
                <%--</div>--%>
                <div class="cp-viewer-layer">
                    <div id="cp-image-preview">
                        <div class="cp-scale-info" ng-show="isShowPercent" ng-cloak>
                            <span ng-bind="zoomPercent"></span>%
                        </div>
                        <div class="cp-image-container">
                            <img id="cp-img" ng-src="{{imageZoomURL}}" ng-style="styleZoom"/>
                        </div>
                        <span class="cp-baseline-extension"></span>
                    </div>
                </div>
                <div class="cp-toolbar cp-content">
                    <button class="cp-toolbar-minus cp-icon" ng-click="zoomOut($event)">Zoom out</button>
                    <button class="cp-toolbar-plus cp-icon" ng-click="zoomIn($event)">Zoom in</button>
                </div>
            </div>
        </div>
        <div id="cp-footer">
            <div id="cp-info">
                <div class="thumb-img" ng-repeat="slider in sliders" ng-click="changeZoomImage(slider, $event)">
                    <div class="mask-thumb"></div>
                    <img ng-src="{{slider.imageUrl}}" />
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<c:url value="/assets/libs/js/jquery.bxslider.js"></c:url>"></script>
<script src="<c:url value="/assets/js/mealTodayInit.js"></c:url>"></script>