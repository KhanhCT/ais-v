<%--
  Created by IntelliJ IDEA.
  User: chien
  Date: 03-Jul-18
  Time: 10:14 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/consumptionSumary.css"></c:url>">
<script>
    var contextPath = '${pageContext.servletContext.contextPath}';
</script>
<script src="<c:url value="/assets/js/consumptionSumary.js"></c:url>"></script>
<c:if test="${sessionScope.UserRoleValue == 'Admin'}">
<div class="container" ng-controller="consumptionSumartyCtrl">
    <div class="row m-t-30 m-b-20">
        <div class="col-md-12 text-left font-mont">
            <span class="tag text-center">Consumption Sumary Report</span>
        </div>
    </div>
    <div class="row wrap-table">
        <div class="col-md-12">
            <table ng-cloak class="table table-custom-balance">
                <thead>
                    <tr>
                        <th class="text-center"><span>No.</span></th>
                        <th ng-repeat="key in listKeyTable" class="text-center">
                            <span ng-bind="key['name']"></span>
                        </th>
                    </tr>
                </thead>
                <tbody ng-repeat="dataConsump in dataConsumptionLst">
                    <tr ng-if="dataConsumptionLst.length > 0" class="custom-row-info custom-row-head">
                        <td colspan="4" class="border-right-none">
                            Card ID: <span ng-bind="dataConsump.cardId"></span> (<span ng-bind="dataConsump.cardNum"></span>) <span ng-bind="dataConsump.cardName"></span>
                        </td>
                        <td colspan="4" class="border-right-none border-left-none">Class/Title: <span ng-bind="dataConsump.gradeId"></span> - <span ng-bind="dataConsump.gradeName"></span></td>
                        <td class="border-left-none">Balance: <span ng-bind="dataConsump.balance"></span></td>
                    </tr>
                    <tr ng-if="dataConsumptionLst.length > 0" class="row-info-dot" ng-repeat="data in dataConsump.data">
                        <td><span ng-bind="{{$index + 1}}"></span></td>
                        <td ng-repeat="key in listKeyTable" ng-class="key.iClass">
                            <span ng-bind="data[key['code']]"></span>
                        </td>
                    </tr>
                    <tr ng-if="dataConsumptionLst.length > 0" class="custom-row-info">
                        <td colspan="5" class="border-right-none">Total by Card ID: <span ng-bind="dataConsump.cardId"></span> (<span ng-bind="dataConsump.cardNum"></span>) <span ng-bind="dataConsump.cardName"></span></td>
                        <td class="border-right-none border-left-none text-right">(Total Quantity)</td>
                        <td colspan="2" class="border-right-none border-left-none text-right">(Total Amount)</td>
                        <td class="border-left-none"></td>
                    </tr>
                    <tr ng-if="dataConsumptionLst.length === 0">
                        <td class="text-center" colspan="{{listKeyTable.length + 1}}"><span><spring:message code="label.no_data"/></span></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</c:if>