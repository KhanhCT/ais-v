<%--
  Created by IntelliJ IDEA.
  User: chien
  Date: 04/06/2018
  Time: 23:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/customerStyle.css"></c:url>">
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/libs/mcustomscroll/jquery.mCustomScrollbar.min.css"></c:url>">
<script>var contextPath = '${pageContext.servletContext.contextPath}';</script>
<script src="<c:url value="/assets/libs/mcustomscroll/jquery.mCustomScrollbar.concat.min.js"></c:url>"></script>
<div class="container-fluid wrapper-customer-order" ng-controller="customerOrderCtrl" ng-init="initData()">
    <div class="loader-wrapper" ng-if="showLoaderGetItem" ng-cloak>
        <div class="loader"></div>
    </div>
    <div class="row m-t-5 m-r-5 ">
        <div class="col-12">
            <div class="form-group row">
                <label for="cardID" class="col-1 col-form-label text-left" ><spring:message code="no.card_code" /></label>
                <div class="col-2">
                    <div class="input-group">
                        <input type="text" id="cardID" class="form-control" placeholder="<spring:message code="no.card_code"/>" ng-keydown="$event.keyCode === 13 && findAccount()" required="required" ng-model="cardCode" >
                    </div>
                </div>
                <label for="nameID" class="col-1 col-form-label text-left"><spring:message code="no.name"/></label>
                <div class="col-2">
                    <div class="input-group">
                        <input type="text" id="nameID" class="form-control" placeholder="<spring:message code="no.name"/>" ng-model="customerObj.cusName" readonly="true">
                    </div>
                </div>
                <label for="balanceID" class="col-1 col-form-label text-left"><spring:message code="no.balance"/></label>
                <div class="col-2">
                    <div class="input-group">
                        <input type="text" id="balanceID" class="form-control" readonly="true" ng-model="customerObj.balance">
                    </div>
                </div>
                <label for="cantenId" class="col-1 col-form-label text-left">Canteen</label>
               	<select id="cantenId" ng-model="selectedCanteen"
					class="form-control custom-select custom-select-sm col-2" ng-cloak
					ng-options="canteen as canteen.name for canteen in canteenList track by canteen.locationId">
				</select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-6 row-custom table">
            <div class="wrap-table-config row">
                <div class="col-md-12 col-lg-12 col-xs-12 col-md-12 row-custom table-custom-config-wrap">
                    <table ng-cloak class="table table-condensed table-bordered table-striped table-custom-config"
                           ng-class="{'nomarginbottom': tableTransaction.total() == 0}">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th ng-repeat="key in listKeyTable">
                                    <div class="name-header" >
                                        <span ng-bind="key['name']"></span>
                                    </div>
                                    <div class="wrap-filter-sort">
                                        <div class="sort-icon" ng-click="changeSort(key)">
                                            <img ng-if="!key['sortAZ'] && !key['sortZA']" class="img-sort" ng-src="../assets/images/sort-couple-gray.png">
                                            <img ng-if="key['sortAZ']" class="img-sort" ng-src="../assets/images/sort-desc-white.png">
                                            <img ng-if="key['sortZA']" class="img-sort" ng-src="../assets/images/sort-asc-white.png">
                                        </div>
                                        <div class="filter-table" ng-click="showFilter(key)">
                                            <img class="img-filter" ng-if="key['textFilter']" ng-src="../assets/images/filter-white.png">
                                            <img class="img-filter" ng-if="!key['textFilter']" ng-src="../assets/images/filter-gray.png">
                                        </div>
                                    </div>
                                    <div class="wrap-input-filter" ng-if="key['openFilter']" tw-click-outside="hideFilter(key)">
                                        <input class="input-filter" placeholder="Search..." type="text" ng-model="key['textFilter']">
                                        <span class="fa fa-search p-l-6 color-black" ng-if="!key['textFilter']"></span>
                                        <span class="fa fa-close close-custom" ng-click="clearFilter(key)" ng-if="key['textFilter']"></span>
                                    </div>
                                </th>
                                <th style="text-align: center;"><spring:message code="label.action"/></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr dir-paginate="objectData in arrData | orderByCustom:sortColumn | customFilterText:listKeyTable | itemsPerPage:itemGap">
                                <td><span ng-bind="{{$index + 1}}"></span></td>
                                <td ng-repeat="key in listKeyTable" ng-class="key.iClass">
                                    <span ng-bind="objectData[key['code']]"></span>
                                </td>
                                <td style="text-align: center;">
                                    <div class="btn btn-info btn-sm" tooltip title="Edit" data-toggle="modal" data-target="#editModal" ng-click="editMeal(objectData)">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </div>
                                    <div class="btn btn-danger btn-sm" tooltip title="Delete" data-toggle="modal" data-target="#deleteModal" ng-click="showDeleteMealOrder(objectData)">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </div>
                                </td>
                            </tr>
                            <tr ng-if="arrData.length === 0">
                                <td class="text-center" colspan="{{listKeyTable.length + 2}}"><span><spring:message code="label.no_data"/></span></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td></td>
                                <td ng-repeat="key in listKeyTable" ng-class="key.iClass">
                                    <span ng-bind="getTotalOfBoard(key)"></span>
                                </td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="row">
                        <div class="col-md-6">
                            <dir-pagination-controls
                                    template-url="../assets/html/dirPagination.tpl.html"
                                    max-size="10"
                                    direction-links="true"
                                    boundary-links="true" >
                            </dir-pagination-controls>
                        </div>
                        <div class="col-md-6">
                            <ul class="pagination pull-right" ng-if="arrData.length > 0" ng-cloak>
                                <li ng-repeat="gap in listGap" ng-class="{ active : gap === itemGap }">
                                    <a href="" ng-click="setItemGap(gap)"><span ng-bind="gap"></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">
        <div class="modal animated zoomIn col-6" id="addNewItemModal" tabindex="-1" role="dialog" aria-labelledby="addNewItemModalCenterTitle" aria-hidden="true" >
	        <div class="modal-dialog modal-dialog-centered" role="document">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <h5 class="modal-title" id="addNewItemModalLongTitle"><spring:message code="om.new_item_title"/></h5>
	                    <button type="button" style="outline: none;" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                </div>
	                <div class="modal-body">
	                    <div class="col-md-12">
	                        <div class="form-group row">
	                            <label for="quantityID" class="col-sm-5 col-form-label text-left"><spring:message code="om.item_qty"/></label>
	                            <div class="col-sm-4">
	                                <div class="input-group mb-3">
	                                    <div class="input-group-prepend">
	                                        <button class="btn btn-outline-danger" ng-click="minusQuantity()"><span class="fa fa-minus"></span></button>
	                                    </div>
	                                    <input type="text" id="quantityID" class="form-control custom-formcontrol" ng-model="quantityItem" integer-only
	                                           ng-model-options="{getterSetter: true}">
	                                    <div class="input-group-append">
	                                        <button class="btn btn-outline-success" ng-click="plusQuantity()" type="button"><span class="fa fa-plus"></span></button>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	
	                    </div>
	                </div>
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-danger" data-dismiss="modal"><spring:message code="cm.cancel"/></button>
	                    <button type="button" class="btn btn-success" data-dismiss="modal" ng-click="addQty()"><spring:message code="cm.add"/></button>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="wrap-group-items row" ng-cloak  m-cus-scroll
	         ng-if="isShowItemList || isShowGroupList">
	        <div class="wrap-button-back row m-b-10 m-l-0 m-r-0" ng-cloak>
	            <div class="col-3" >
	                <div class="pull-left button-back" ng-show="isShowItemList" ng-click="changeViewToGroup()">
	                    <i class="fa fa-chevron-circle-left p-r-5"></i>Back
	                </div>
	            </div>
	        </div>
	        <div class="wrap-all-item">
	            <div class="col-md-2 item-wrap hov-pointer" ng-click="changeViewToItems(group)"
	                 ng-if="isShowGroupList && !isShowItemList"
	                 ng-repeat="group in groupItems">
	                <div class="item-content hvr-float-shadow">
	                    <span ng-bind="group.name"></span>
	                </div>
	            </div>
	            <div class="col-md-2 item-wrap hov-pointer" ng-click="changeSelectedItem(item)"
	                 ng-repeat="item in listItemsSelected"  ng-if="isShowItemList && isShowGroupList">
	                <div class="item-content hvr-float-shadow" >
	                    <span ng-bind="item.name"></span>
	                </div>
	            </div>
	        </div>
	    </div>
	    </div>
    </div>

    <div class="row m-t-10 m-l-5" ng-cloak>
			<label for="total2ID" class="col-1 col-form-label text-left "><spring:message
					code="cm.total" /></label>
			<div class="col-2">
				<div class="input-group">
					<input type="text" id="total2ID" class="form-control"
						placeholder="Total" readonly="true" ng-model="total">
				</div>
			</div>
			<label for="totalID" class="col-1 col-form-label text-left"><spring:message
					code="no.balance" /></label>
			<div class="col-2">
				<div class="input-group">
					<input type="text" id="totalID" class="form-control"
						placeholder="Balance" readonly="true"
						ng-model="customerObj.balance">
				</div>
			</div>
			<label for="discountID" class="col-1 col-form-label text-left">Discount</label>
			<div class="col-2">
				<div class="input-group">
					<input type="text" id="discountID" class="form-control"
						placeholder="Discount" ng-model="discount"
						ng-keydown="discountEvent($event)">
				</div>
			</div>
			<div class="col-3">
				<button class="btn btn-primary btn-custom-pay" ng-click="pay()"
					ng-disabled="arrData.length <=0 || customerObj.balance <=-300000">
					<spring:message code="cm.pay" />
				</button>
			</div>
	</div>

    <div class="wrap-accept-delete">
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLongTitle"><spring:message code="cm.comfirmation"/></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                      <spring:message code="om.notice_1"></spring:message>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><spring:message code="cm.close"/></button>
                        <button type="button" class="btn btn-primary" ng-click="deleteMeal()"><spring:message code="cm.accept"/></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" >Edit Meal</h5>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="quantityID" class="col-sm-5 col-form-label text-left"><spring:message code="om.item_qty"/></label>
                        <div class="col-sm-4">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <button class="btn btn-outline-danger" ng-click="minusQuantity1()"><span class="fa fa-minus"></span></button>
                                </div>
                                <input type="text" id="modifiedQtyId" class="form-control custom-formcontrol" ng-model="editteddMeal.qty" integer-only
                                       ng-model-options="{getterSetter: true}" >
                                <div class="input-group-append">
                                    <button class="btn btn-outline-success" ng-click="plusQuantity1()" type="button"><span class="fa fa-plus"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><spring:message code="cm.close"/></button>
                    <button type="button" class="btn btn-primary" ng-click="updateQty()"><spring:message code="cm.save"/></button>
                </div>
            </div>
        </div>
    </div>
    <alert-message type-alert="mesObj.type" message-alert="mesObj.text"
		show-alert="mesObj.active"></alert-message>
</div>
<script>
    var listKeyTable = [
         <%--{--%>
            <%--code: 'skuCode',--%>
            <%--name: '<spring:message code="label.item_code"/>',--%>
            <%--isSortFilter: true,--%>
            <%--width: '50px',--%>
            <%--iClass: "text-center"--%>
        <%--},--%>
        <%--{--%>
            <%--code: 'barcode',--%>
            <%--name: '<spring:message code="label.barcode"/>',--%>
            <%--isSortFilter: true,--%>
            <%--width: '150px',--%>
            <%--iClass: "text-center"--%>
        <%--}, --%>
        {
            code: 'skuName',
            name: '<spring:message code="label.item_name"/>',
            isSortFilter: true,
            width: '150px',
            iClass: "text-right"
        },
        {
            code: 'qty',
            name: '<spring:message code="label.quantity"/>',
            isSortFilter: true,
            width: '150px',
            isTotal: true,
            iClass: "text-right"
        },
        <%--{--%>
            <%--code: 'rtPrice',--%>
            <%--name: '<spring:message code="label.price"/>',--%>
            <%--isSortFilter: true,--%>
            <%--width: '50px',--%>
            <%--iClass: "text-center"--%>
        <%--}, --%>
        {
            code: 'amount',
            name: '<spring:message code="label.total"/>',
            isSortFilter: true,
            width: '150px',
            isTotal: true,
            iClass: "text-right"
        }
    ];
</script>
<script src="<c:url value="/assets/js/initCustomerOrder.js"></c:url>"></script>