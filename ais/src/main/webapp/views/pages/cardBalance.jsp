<%--
  Created by IntelliJ IDEA.
  User: chien
  Date: 04-Jul-18
  Time: 12:08 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/cardBalance.css"></c:url>">
<script>
    var contextPath = '${pageContext.servletContext.contextPath}';
</script>
<script src="<c:url value="/assets/js/cardBalance.js"></c:url>"></script>
<c:if test="${sessionScope.UserRoleValue == 'Admin'}">
<div class="container" ng-controller="cardBalanceCtrl" ng-init="initializeData()">
	<div id="loader-wrapper" ng-if="showLoaderAllPage" ng-cloak>
        <div id="loader"></div>
    </div>
    <div class="row m-t-30 m-b-20">
        <div class="col-md-12 text-left font-mont">
            <span class="tag text-center">Card Balance</span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table ng-cloak class="table table-custom-balance">
                <thead>
                    <tr>
                        <th class="text-center"><span>No.</span></th>
                        <th ng-repeat="key in listKeyTable" class="text-center">
                            <span ng-bind="key['name']"></span>
                        </th>
                    </tr>
                </thead>
                <tbody ng-repeat="cardObj in reportData">
                    <tr ng-if="reportData.length > 0" class="custom-row-info">
                        <td colspan="{{(listKeyTable.length + 1)}}">Class/Title: <span ng-bind="cardObj.classId"></span> - <span ng-bind="cardObj.className"></span></td>
                    </tr>
                    <tr ng-if="reportData.length > 0" ng-repeat="data in cardObj.data" class="row-info-dot">
                        <td><span ng-bind="{{$index + 1}}"></span></td>
                        <td ng-repeat="key in listKeyTable" ng-class="key.iClass">
                            <span ng-bind="data[key['code']]"></span>
                        </td>
                    </tr>
                    <tr ng-if="reportData.length > 0" class="custom-row-info">
                        <td colspan="3" class="border-right-none">Total by Class/Title: <span ng-bind="cardObj.classId"></span> - <span ng-bind="cardObj.className"></span></td>
                        <td class="text-right border-right-none border-left-none"><span ng-bind="cardObj.totalAmount"></td>
                        <td class="border-left-none"></td>
                    </tr>
                    <tr ng-if="reportData.length === 0">
                        <td class="text-center" colspan="{{(listKeyTable.length + 1)}}"><span><spring:message code="label.no_data"/></span></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row m-t-10">
        <div class="dropup col-md-12 m-r-15">
            <button class="btn btn-success btn-lg  dropdown-toggle pull-right" type="button" id="dropdownExport" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                EXPORT
                <span class="glyphicon glyphicon-download-alt p-l-10"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownExport">
                <li><div class="li-export" ng-click="exportData('xls')"><span class="export-item">Excel export</span><img class="pull-right" src='<c:url value="/assets/images/excel_icon.png"></c:url>'></div></li>
                <li role="separator" class="custom-separate"></li>
                <li><div class="li-export" ng-click="exportData('pdf')"><span class="export-item ">PDF export</span><img class="pull-right" src='<c:url value="/assets/images/pdf_icon.png"></c:url>'></div></li>
            </ul>
        </div>
    </div>
</div>
</c:if>
