<%--customerId
  Created by IntelliJ IDEA.
  User: chien
  Date: 29/05/2018
  Time: 23:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/homePageStyle.css"></c:url>">
<script>
    var contextPath = '${pageContext.servletContext.contextPath}';
</script>
<script src="<c:url value="/assets/js/homePage.js"></c:url>"></script>
<c:if test="${sessionScope.UserRoleValue == 'Parent'}">
<div class="container wrapper-homePage" ng-controller="homePageCtrl" ng-init="initData()">
    <div id="loader-wrapper" ng-if="showLoaderAllPage" ng-cloak>
        <div id="loader"></div>
    </div>
    <div class="row m-t-30 m-b-20">
        <div class="col-md-12 text-left font-mont">
            <span class="tag text-center"><spring:message code="label.home_header"/></span>
        </div>
    </div>
    <div class="row">
        <div class="left-parent-info offset-md-3 col-md-6">
            <div class="form-group row">
                <label for="staticEmail" class="bold_label col-sm-5 col-form-label p-l-20" ><spring:message code="label.child_account"/></label>
                <div class="col-sm-7">
                    <select id="staticEmail" class="form-control custom-select custom-select-sm"
                            ng-model="childSelected"
                            ng-change="changeChildSelected()"
                            ng-options="child.cusName for child in parentChildLst">
                    </select>
                </div>
            </div>
        </div>
        <div class="offset-md-3 col-md-6">
            <div class="form-group row custom-row-effect">
                <div class="col-5"><div class="pass-effect custom-span"><span><spring:message code="label.date"/></span></div></div>
                <div class="col-7"><div class="pass-effect"><span ng-bind="curDate"></span></div></div>
            </div>
            <div class="form-group row custom-row-effect">
                <div class="col-5"><div class="pass-effect custom-span"><span><spring:message code="label.account_owner"/></span></div></div>
                <div class="col-7"><div class="pass-effect"><span ng-bind="childSelected.cusName"></span></div></div>
            </div>
            <div class="form-group row custom-row-effect">
                <div class="col-5"><div class="pass-effect custom-span"><span><spring:message code="label.bank_num"/></span></div></div>
                <div class="col-7"><div class="pass-effect"><span ng-bind="childSelected.cusId"></span></div></div>
            </div>
            <div class="form-group row custom-row-effect">
                <div class="col-5"><div class="pass-effect custom-span"><span><spring:message code="label.balance"/></span></div></div>
                <div class="col-7"><div class="pass-effect"><span ng-bind="getNumberInComma(childSelected.balance)"></span></div></div>
            </div>
            <div class="form-group row custom-row-effect">
                <div class="col-5"><div class="pass-effect custom-span"><span>Currency Unit</span></div></div>
                <div class="col-7"><div class="pass-effect"><span>VND</span></div></div>
            </div>
        </div>
        <div class="right-parent-history col-md-12">
            <div class="wrap-filter-date row">
                <div class="col-md-4 offset-md-2">
                    <div class="form-group row">
                        <span class="col-sm-5 col-form-label text-center normal_label"><spring:message code="label.from_date"/></span>
                        <div class="col-sm-7">
                            <div class="input-group">
                                <input type="text" id="fromDate" class="form-control normal_label" placeholder="dd/mm/yyyy" ng-model="fromDate"
                                       ng-class="{'invalid-input': !fromDate}"
                                       date-picker aria-describedby="fromDateAddon">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="fromDateAddon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group row">
                        <span class="col-sm-5 col-form-label text-center normal_label"><spring:message code="label.to_date"/></span>
                        <div class="col-sm-7">
                            <div class="input-group">
                                <input type="text" id="toDate" class="form-control normal_label" placeholder="dd/mm/yyyy"
                                       ng-class="{'invalid-input': !toDate}"
                                       ng-model="toDate" date-picker  aria-describedby="toDateAddon">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="toDateAddon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="col-md-2">
                    <button class="btn btn-warning custom-view" type="button" ng-click="applyView()">
                    <spring:message code="label.btn_view"/>
                    </button>
                </div>
            </div>
        </div>
       <%--  <div class="col-md-6 offset-md-2">
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-5 col-form-label text-right normal_label"><spring:message code="label.action"/></label>
                <div class="col-sm-7">
                    <select id="actionInfo" class="form-control custom-select custom-select-sm"
                            ng-model="transTypeFilter"
                            ng-options="transType.transTypeName for transType in lstTransType">
                    </select>
                </div>
            </div>
        </div> --%>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="wrap-table-config row">
                <div class="col-md-12 col-lg-12 col-xs-12 col-md-12 row-custom table-custom-config-wrap">
                    <table ng-cloak class="table table-condensed table-bordered table-striped table-custom-config"
                           ng-class="{'nomarginbottom': tableTransaction.total() == 0}">
                        <thead>
                            <tr>
                                <th ng-repeat="key in listKeyTable">
                                    <div class="name-header" >
                                        <span ng-bind="key['name']"></span>
                                    </div>
                                    <div class="wrap-filter-sort">
                                        <div class="sort-icon" ng-click="changeSort(key)">
                                            <img ng-if="!key['sortAZ'] && !key['sortZA']" class="img-sort" ng-src="assets/images/sort-couple-gray.png">
                                            <img ng-if="key['sortAZ']" class="img-sort" ng-src="assets/images/sort-desc-white.png">
                                            <img ng-if="key['sortZA']" class="img-sort" ng-src="assets/images/sort-asc-white.png">
                                        </div>
                                        <div class="filter-table" ng-click="showFilter(key)">
                                            <img class="img-filter" ng-if="key['textFilter']" ng-src="assets/images/filter-white.png">
                                            <img class="img-filter" ng-if="!key['textFilter']" ng-src="assets/images/filter-gray.png">
                                        </div>
                                    </div>
                                    <div class="wrap-input-filter" ng-if="key['openFilter']" tw-click-outside="hideFilter(key)">
                                        <input class="input-filter" placeholder="Search..." type="text" ng-model="key['textFilter']">
                                        <span class="fa fa-search p-l-6 color-black" ng-if="!key['textFilter']"></span>
                                        <span class="fa fa-close close-custom" ng-click="clearFilter(key)" ng-if="key['textFilter']"></span>
                                    </div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr dir-paginate="transObj in arrData | orderByCustom:sortColumn | customFilterText:listKeyTable
                            | filter:{transType:transTypeFilter.transTypeId} | itemsPerPage:itemGap">
                               <td ng-repeat="key in listKeyTable" ng-class="key.iClass">
                                   <span ng-bind="transObj[key['code']]"></span>
                               </td>
                            </tr>
                             <tr ng-if="arrData.length > 0">
		                    	<td colspan="3" class="border-right-0">Total by Card ID:</td>
			                    <td class="text-right border-right-none border-left-none">
			                    	<span ng-bind="totalCashIn"></span>
			                    </td>
			                    <td class="text-right border-right-none border-left-none">
			                    	<span ng-bind="totalCashOut"></span>
			                    </td>   
			                    <td class="text-right border-right-none border-left-none">
			                    	<span ></span>
			                    </td>   
			                    <td class="text-right border-right-none border-left-none">
			                    	<span ></span>
			                    </td> 
               				</tr>
                            <tr ng-if="arrData.length === 0 || arrFilter.length === 0">
                                <td class="text-center" colspan="{{listKeyTable.length}}"><span><spring:message code="label.no_data"/></span></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-6">
                            <dir-pagination-controls
                                    template-url="assets/html/dirPagination.tpl.html"
                                    max-size="10"
                                    direction-links="true"
                                    boundary-links="true" >
                            </dir-pagination-controls>
                        </div>
                        <div class="col-6">
                            <ul class="pagination pull-right" ng-if="arrData.length > 0" ng-cloak>
                                <li ng-repeat="gap in listGap" ng-class="{ active : gap === itemGap }">
                                    <a href="" ng-click="setItemGap(gap)"><span ng-bind="gap"></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
           <%--  <div class="wrap-export-button row">
                <div class="col-md-12">
                    <button class="btn btn-primary pull-right m-l-10" type="button" ng-click="exportTrans('pdf')">
                        Export PDF
                    </button>
                    <button class="btn btn-primary pull-right m-l-10" type="button" ng-click="exportTrans('excel')">
                        Export Excel
                    </button>
                </div>
            </div> --%>
        </div>
    </div>
    <div ng-if="showAlertNoChild" class="modal fade" id="hasNochildAlertModal" tabindex="-1" role="dialog"
         ng-click="resetSession()" aria-labelledby="hasNochildModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content" ng-click="$event.stopPropagation()">
                <div class="modal-header">
                    <h5 class="modal-title" id="hasNochildModalCenterTitle">ERROR REQUEST</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="resetSession()">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Something went wrong: Your request interrupted or transaction list is not available please logout and try again!
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" ng-click="resetSession()">Reset Session</button>
                </div>
            </div>
        </div>
    </div>
</div>
</c:if>
