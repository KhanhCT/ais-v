<%--
  Created by IntelliJ IDEA.
  User: chien
  Date: 10/06/2018
  Time: 12:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/itemMasterStyle.css"></c:url>">
<script>var contextPath = '${pageContext.servletContext.contextPath}';</script>
<div class="container wrapper-item-master" ng-controller="itemMasterCtrl" ng-init="initData()">
    <div class="row">
        <div class="col-md-12 text-left font-mont">
            <span class="tag text-center"><spring:message code="im.title_page" /></span>
        </div>
    </div>
    <div class="row m-t-20">
        <div class="col-sm-6">
            <div class="form-group row">
                <label for="itemID" class="col-sm-5 col-form-label text-left"><spring:message code="im.item_id" /></label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="text" id="itemID" class="form-control" ng-model = "itemObj.skuCode" placeholder="Item ID" >
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group row">
                <label for="barcodeID" class="col-sm-5 col-form-label text-left"><spring:message code="im.barcode" /></label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="text" id="barcodeID" class="form-control" ng-model = "itemObj.barcode" placeholder="<spring:message code="im.barcode" />" >
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-t-20">
        <div class="col-sm-6">
            <div class="form-group row">
                <label for="shortName" class="col-sm-5 col-form-label text-left"><spring:message code="im.short_name" /></label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="text" id="shortName" class="form-control" ng-model = "itemObj.name" placeholder="<spring:message code="im.short_name" />" >
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group row">
                <label for="o_priceID" class="col-sm-5 col-form-label text-left"><spring:message code="im.org_price" /></label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="number" id="o_priceID" class="form-control" placeholder="<spring:message code="im.org_price" />" ng-model = "itemObj.lastPrice">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-t-20">
        <div class="col-sm-6">
            <div class="form-group row">
                <label for="unitID" class="col-sm-5 col-form-label text-left"><spring:message code="im.unit" /></label>
                <div class="col-sm-7">
                    <select id="unitID" class="form-control custom-select custom-select-sm" ng-model = "itemObj.priceUnit">
                        <option value="{{priceU.codeName}}" ng-repeat="priceU in priceUnitLst">{{priceU.codeVal}}</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group row">
                <label for="itemTypeID" class="col-sm-5 col-form-label text-left"><spring:message code="cm.type" /></label>
                <div class="col-sm-7">
                    <select id="itemTypeID" class="form-control custom-select custom-select-sm" ng-model="itemObj.itemGrpId"
                    	ng-options="itemGrp.itemGrpId as itemGrp.name for itemGrp in itemGrpLst">
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-t-20">
        <div class="col-sm-6">
            <div class="form-group row">
                <label for="taxRate" class="col-sm-5 col-form-label text-left"><spring:message code="im.tax_rate" /></label>
                <div class="col-sm-7">
                    <select id="taxRate" class="form-control custom-select custom-select-sm">
                        <option value="NT">NT</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group row">
                <label for="statusID" class="col-sm-5 col-form-label text-left"><spring:message code="cm.status" /></label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <select id="statusID" class="form-control custom-select custom-select-sm" >
	                        <option value="true">Active</option>
	                        <option value="false">Inactive</option>
	                    </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-t-20">
        <div class="col-sm-4">
            <div class="form-group">
                <label class="wrap-checkbox">
                    <input type="checkbox" name="check" ng-model = "isSameAll" ng-change="checkedFunction()"> <span class="label-text"><spring:message code="im.same_for_all_cards" /></span>
                </label>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="form-group row">
                <label for="price2Id" class="col-3 col-form-label text-center"><spring:message code="om.item_price" /></label>
                <div class="col-9">
                    <div class="row">
                        <div class="col-7">
                            <input type="number" id="price2Id" class="form-control" placeholder="Price" ng-model = "valueSamePrice">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-t-20" ng-if = "showTable != false">
        <div class="col-md-12 col-lg-12 col-xs-12 col-md-12 row-custom table">
            <div class="wrap-table-config row">
                <div class="col-md-12 col-lg-12 col-xs-12 col-md-12 row-custom table-custom-config-wrap">
                    <table ng-cloak class="table table-condensed table-bordered table-striped table-custom-config"
                           ng-class="{'nomarginbottom': tableTransaction.total() == 0}">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th ng-repeat="key in listKeyTable">
                                <div class="name-header" >
                                    <span ng-bind="key['name']"></span>
                                </div>
                                <div class="wrap-filter-sort">
                                    <div class="sort-icon" ng-click="changeSort(key)">
                                        <img ng-if="!key['sortAZ'] && !key['sortZA']" class="img-sort" ng-src="../assets/images/sort-couple-gray.png">
                                        <img ng-if="key['sortAZ']" class="img-sort" ng-src="../assets/images/sort-desc-white.png">
                                        <img ng-if="key['sortZA']" class="img-sort" ng-src="../assets/images/sort-asc-white.png">
                                    </div>
                                    <div class="filter-table" ng-click="showFilter(key)">
                                        <img class="img-filter" ng-if="key['textFilter']" ng-src="../assets/images/filter-white.png">
                                        <img class="img-filter" ng-if="!key['textFilter']" ng-src="../assets/images/filter-gray.png">
                                    </div>
                                </div>
                                <div class="wrap-input-filter" ng-if="key['openFilter']" tw-click-outside="hideFilter(key)">
                                    <input class="input-filter" placeholder="Search..." type="text" ng-model="key['textFilter']">
                                    <span class="fa fa-search p-l-6 color-black" ng-if="!key['textFilter']"></span>
                                    <span class="fa fa-close close-custom" ng-click="clearFilter(key)" ng-if="key['textFilter']"></span>
                                </div>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr dir-paginate="objectData in rtPriceLst | orderByCustom:sortColumn | customFilterText:listKeyTable | itemsPerPage:itemGap">
                            <td><span ng-bind="{{$index + 1}}"></span></td>
                            <td ng-repeat="key in listKeyTable" ng-class="key.iClass">
                                <span ng-bind="objectData[key['code']]" ng-if="key['code']!=='rtPrice'"></span>
                                <input type="number" class="input-table" integer-only left-align="true" ng-model="objectData[key['code']]" ng-if="key['code']==='rtPrice'">
                            </td>
                        </tr>
                        <tr ng-if="rtPriceLst.length === 0">
                            <td class="text-center" colspan="{{listKeyTable.length + 1}}"><span><spring:message code="label.no_data"/></span></td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-md-6">
                            <dir-pagination-controls
                                    template-url="../assets/html/dirPagination.tpl.html"
                                    max-size="10"
                                    direction-links="true"
                                    boundary-links="true" >
                            </dir-pagination-controls>
                        </div>
                        <div class="col-md-6">
                            <ul class="pagination pull-right" ng-if="rtPriceLst.length > 0" ng-cloak>
                                <li ng-repeat="gap in listGap" ng-class="{ active : gap === itemGap }">
                                    <a href="" ng-click="setItemGap(gap)"><span ng-bind="gap"></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-t-20">
    	<div class="col-12"><div class="separate-line"></div></div>
    </div>
    <div class="row m-t-20">
    	<div class="col-12">
            <div class="form-group row">
                <label for="remark" class="col-sm-2 col-form-label text-left"><spring:message code="cm.remark" /></label>
                <div class="col-sm-10">
                    <div class="input-group">
                        <input type="text" id="remark" class="form-control" ng-model = "itemObj.description" placeholder="Description" >
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-t-20">
    	<div class="col-md-4 offset-md-5">
    		<button class="col-3 btn btn-success m-l-15" ng-click = "saveItem()"><spring:message code="cm.new" /></button>
	    	<button class="col-3 btn btn-danger m-l-15" ng-click = "updateItem()"><spring:message code="cm.modify" /></button>
	    	<button class="col-3 btn btn-warning  m-l-15" ng-click = "findItem()"><spring:message code="cm.find" /></button>
    	</div>
    </div>
</div>
<script src="<c:url value="/assets/js/initItemMaster.js"></c:url>"></script>

