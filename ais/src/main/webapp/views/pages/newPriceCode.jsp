<%--
  Created by IntelliJ IDEA.
  User: KhanhCT
  Date: 04/06/2018
  Time: 23:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/newPriceCode.css"></c:url>">
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/libs/mcustomscroll/jquery.mCustomScrollbar.min.css"></c:url>">
<script>var contextPath = '${pageContext.servletContext.contextPath}';</script>
<script src="<c:url value="/assets/libs/mcustomscroll/jquery.mCustomScrollbar.concat.min.js"></c:url>"></script>
<div class="container wrapper-customer-order" ng-controller="newPriceCode" ng-init="initializeData()">
    <div id="loader-wrapper" ng-if="showLoaderAllPage" ng-cloak>
        <div id="loader"></div>
    </div>
    <div class="row m-t-5 m-b-10">
        <div class="col-md-12 text-left font-mont">
            <span class="tag text-center"><spring:message code="npc.title_page"/></span>
        </div>
    </div>
	<div class="col-md-8 offset-md-2 wrapper-content">
		<div class="transfer-header"><spring:message code="npc.title_page"/></div>
		<div class="transfer-content">
			<div class="form-group row">
				<label for="sourceId"
					class="col-sm-5 col-form-label text-right-custom"><spring:message code="im.pr_code"/></label>
				<div class="col-sm-7">
					<input class="form-control" ng-model="prCode">
				</div>
			</div>
			
			<div class="form-group row" ng-form="srcForm">
				<label for="accountID"
					class="col-sm-5 col-form-label text-right-custom"><spring:message code="im.pr_code_name"/></label>
				<div class="col-sm-7">
					<input class="form-control"  placeholder="<spring:message code="im.pr_code_name"/>"
						ng-model="prCodeName" required="required" >
				</div>
			</div>
			<div class="form-group row">
				<label for="sourceId"
					class="col-sm-5 col-form-label text-right-custom"><spring:message code="im.cash"/></label>
				<div class="col-sm-7">
					<input class="form-control" disable  ng-model="cash">
				</div>
			</div>
			<div class="form-group row" >
				<button class="btn btn-primary text-center offset-md-5 col-md-4"  ng-click="newPriceCode()" ng-disabled="!prCode || !prCodeName"><spring:message code="cm.new"/></button>
			</div>			
		</div>
	</div>
	<div class="row wrap-table-config">
		<div class="col-md-12 col-lg-12 col-xs-12 col-md-12 row-custom table">
			<div class="wrap-table-config row">
				<div class="col-md-12 col-lg-12 col-xs-12 col-md-12 row-custom table-custom-config-wrap">
					<table ng-cloak class="table table-condensed table-bordered table-striped table-custom-config"
						   ng-class="{'nomarginbottom': tableTransaction.total() == 0}">
						<thead>
						<tr >
							<th>No.</th>
							<th ng-repeat="key in listKeyTable">
								<div class="name-header" >
									<span ng-bind="key['name']"></span>
								</div>
								<div class="wrap-filter-sort">
									<div class="sort-icon" ng-click="changeSort(key)">
										<img ng-if="!key['sortAZ'] && !key['sortZA']" class="img-sort" ng-src="../assets/images/sort-couple-gray.png">
										<img ng-if="key['sortAZ']" class="img-sort" ng-src="../assets/images/sort-desc-white.png">
										<img ng-if="key['sortZA']" class="img-sort" ng-src="../assets/images/sort-asc-white.png">
									</div>
									<div class="filter-table" ng-click="showFilter(key)">
										<img class="img-filter" ng-if="key['textFilter']" ng-src="../assets/images/filter-white.png">
										<img class="img-filter" ng-if="!key['textFilter']" ng-src="../assets/images/filter-gray.png">
									</div>
								</div>
								<div class="wrap-input-filter" ng-if="key['openFilter']" tw-click-outside="hideFilter(key)">
									<input class="input-filter" placeholder="Search..." type="text" ng-model="key['textFilter']">
									<span class="fa fa-search p-l-6 color-black" ng-if="!key['textFilter']"></span>
									<span class="fa fa-close close-custom" ng-click="clearFilter(key)" ng-if="key['textFilter']"></span>
								</div>
							</th>
							<th style="text-align: center;"><spring:message code="label.action"/></th>
						</tr>
						</thead>
						<tbody>
						<tr dir-paginate="objectData in prCodeList | orderByCustom:sortColumn | customFilterText:listKeyTable | itemsPerPage:itemGap">
							<td><span ng-bind="{{$index + 1}}"></span></td>
							<td ng-repeat="key in listKeyTable" ng-class="key.iClass">
								<span ng-bind="objectData[key['code']]"></span>
							</td>
							<td style="text-align: center">
								<div class="btn btn-info btn-sm"  tooltip title="Edit" data-toggle="modal" data-target="#editModal" ng-click="showEditingPrCode(objectData)">
									<i class="fa fa-pencil" aria-hidden="true"></i>
								</div>
								<div class="btn btn-info btn-sm"  tooltip title="Delete" data-toggle="modal" data-target="#deleteModal" ng-click="deletePrCode(objectData)">
									<i class="fa fa-trash" aria-hidden="true"></i>
								</div>
							</td>
						</tr>
						
						<tr ng-if="prCodeList.length === 0">
							<td class="text-center" colspan="{{listKeyTable.length + 2}}"><span><spring:message code="label.no_data"/></span></td>
						</tr>
						</tbody>
					</table>
					<div class="row">
						<div class="col-md-6">
							<dir-pagination-controls
									template-url="../assets/html/dirPagination.tpl.html"
									max-size="10"
									direction-links="true"
									boundary-links="true" >
							</dir-pagination-controls>
						</div>
						<div class="col-md-6">
							<ul class="pagination pull-right" ng-if="prCodeList.length > 0" ng-cloak>
								<li ng-repeat="gap in listGap" ng-class="{ active : gap === itemGap }">
									<a href="" ng-click="setItemGap(gap)"><span ng-bind="gap"></span></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		<div class="modal animated zoomIn" id="editModal" tabindex="-1"
		role="dialog" aria-labelledby="editModalCenterTitle"
		aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="editModalLongTitle"><spring:message code="im.edit_pr_code"/></h5>
					<button type="button" style="outline: none;" class="close"
						data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="row wrap-form-register" ng-form="formRegister">
					<div class="col-10 offset-md-1 m-t-5 wrap-from-content">
						<div class="loader-wrapper" ng-if="showLoaderCustomer" ng-cloak>
							<div class="loader"></div>
						</div>
						<div class="form-group row">
							<label for="prCode" class="col-sm-5 col-form-label text-left"><spring:message code="im.pr_code"/></label>
							<div class="col-sm-7">
								<div class="input-group">
									<input type="text" id="prCode" ng-model="selectedPrCode.prCode"
										class="form-control" required="required" disabled autocomplete="off">
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label for="name" class="col-sm-5 col-form-label text-left"><spring:message code="im.pr_code_name"/></label>
							<div class="col-sm-7">
								<div class="input-group">
									<input type="text" id="name"
										ng-model="selectedPrCode.name" class="form-control"
										required="required" autocomplete="off">
								</div>
							</div>

						</div>
						<div class="form-group row">
							<div class="col-5 offset-4 text-center">
								<button class="btn btn-outline-secondary" ng-cloak
									ng-if="formRegister.$invalid"><spring:message code="cm.save"/></button>
								<button class="btn btn-outline-success" ng-cloak
									ng-if="formRegister.$valid" ng-click="editPriceCode()"><spring:message code="cm.save"/></button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="wrap-accept-delete">
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLongTitle"><spring:message code="cm.comfirmation"/></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                      <spring:message code="om.notice_1"></spring:message>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><spring:message code="cm.close"/></button>
                        <button type="button" class="btn btn-primary" ng-click="disablePrCode()"><spring:message code="cm.accept"/></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<alert-message type-alert="mesObj.type" message-alert="mesObj.text" show-alert="mesObj.active"></alert-message>
</div>
<script src="<c:url value="/assets/js/newPriceCode.js"></c:url>"></script>