<%--
  Created by IntelliJ IDEA.
  User: KhanhCT
  Date: 04/06/2018
  Time: 23:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/assets/css/uploadTopUpList.css"></c:url>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/assets/libs/mcustomscroll/jquery.mCustomScrollbar.min.css"></c:url>">
<script>
	var contextPath = '${pageContext.servletContext.contextPath}';
</script>
<script
	src="<c:url value="/assets/libs/mcustomscroll/jquery.mCustomScrollbar.concat.min.js"></c:url>"></script>
<div class="container wrapper-customer-order"
	ng-controller="uploadTopUpCtl" ng-init="initializeData()">
	<div id="loader-wrapper" ng-if="showLoaderAllPage" ng-cloak>
		<div id="loader"></div>
	</div>
	<div class="row m-t-5 m-b-10">
		<div class="col-md-12 text-left font-mont">
			<span class="tag text-center"><spring:message
					code="cum.top_up_title_page" /></span>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" ng-form="uploadForm" ng-cloak>
			<div ngf-drop ngf-select ng-model="fileToUpload" class="drop-box"
				ngf-drag-over-class="'dragover'" ngf-multiple="false"
				ngf-allow-dir="true" ngf-pattern="'.xls,.csv,.xlsx'"
				accept=".xls,.csv,.xlsx" ngf-max-size="20MB">
				<div class="content-drag">
					<i class="fa fa-cloud-upload custom-drag"></i>
					<div class="titlte-drag" ng-cloak>
						<span ng-if="!fileToUpload">Browser file to upload...</span> <span
							ng-if="fileToUpload" class="text-primary"
							ng-bind="fileToUpload.name"></span>
					</div>
					<button class="btn btn-primary custom-button"
						ng-disabled="!fileToUpload" ng-click="uploadFile($event)"><spring:message code="cm.upload"/></button>
				</div>
			</div>
		</div>
	</div>
	<div class="row wrap-table-config">
		<div class="col-md-12 col-lg-12 col-xs-12 col-md-12 row-custom table">
			<div class="wrap-table-config row">
				<div
					class="col-md-12 col-lg-12 col-xs-12 col-md-12 row-custom table-custom-config-wrap">
					<table ng-cloak
						class="table table-condensed table-bordered table-striped table-custom-config"
						ng-class="{'nomarginbottom': tableTransaction.total() == 0}">
						<thead>
							<tr>
								<th>No.</th>
								<th ng-repeat="key in listKeyTable">
									<div class="name-header">
										<span ng-bind="key['name']"></span>
									</div>
									<div class="wrap-filter-sort">
										<div class="sort-icon" ng-click="changeSort(key)">
											<img ng-if="!key['sortAZ'] && !key['sortZA']"
												class="img-sort"
												ng-src="assets/images/sort-couple-gray.png"> <img
												ng-if="key['sortAZ']" class="img-sort"
												ng-src="assets/images/sort-desc-white.png"> <img
												ng-if="key['sortZA']" class="img-sort"
												ng-src="assets/images/sort-asc-white.png">
										</div>
										<div class="filter-table" ng-click="showFilter(key)">
											<img class="img-filter" ng-if="key['textFilter']"
												ng-src="assets/images/filter-white.png"> <img
												class="img-filter" ng-if="!key['textFilter']"
												ng-src="assets/images/filter-gray.png">
										</div>
									</div>
									<div class="wrap-input-filter" ng-if="key['openFilter']"
										tw-click-outside="hideFilter(key)">
										<input class="input-filter" placeholder="Search..."
											type="text" ng-model="key['textFilter']"> <span
											class="fa fa-search p-l-6 color-black"
											ng-if="!key['textFilter']"></span> <span
											class="fa fa-close close-custom" ng-click="clearFilter(key)"
											ng-if="key['textFilter']"></span>
									</div>
								</th>
								<th style="text-align: center;"><spring:message
										code="label.action" /></th>
							</tr>
						</thead>
						<tbody>
							<tr
								dir-paginate="objectData in customerList | orderByCustom:sortColumn | customFilterText:listKeyTable | itemsPerPage:itemGap">
								<td><span ng-bind="{{$index + 1}}"></span></td>
								<td ng-repeat="key in listKeyTable" ng-class="key.iClass">
									<span ng-bind="objectData[key['code']]"></span>
								</td>
								<td style="text-align: center">
									<div class="btn btn-danger btn-sm" tooltip title="Delete"
										ng-click="showDeleteCustomer(objectData)">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</div>
								</td>
							</tr>
							<tr ng-if="customerList.length === 0">
								<td class="text-center" colspan="{{listKeyTable.length + 2}}"><span><spring:message
											code="label.no_data" /></span></td>
							</tr>
						</tbody>
					</table>
					<div class="row">
						<div class="col-md-6">
							<dir-pagination-controls
								template-url="assets/html/dirPagination.tpl.html"
								max-size="10" direction-links="true" boundary-links="true">
							</dir-pagination-controls>
						</div>
						<div class="col-md-6">
							<ul class="pagination pull-right"
								ng-if="customerList.length > 0" ng-cloak>
								<li ng-repeat="gap in listGap"
									ng-class="{ active : gap === itemGap }"><a href=""
									ng-click="setItemGap(gap)"><span ng-bind="gap"></span></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<button class="btn btn-warning btn-block" type="button" ng-disabled="customerList.length <= 0"
				ng-click="saveCustomerList()">
				<spring:message code="cm.save" />
			</button>
		</div>
	</div>
	<alert-message type-alert="mesObj.type" message-alert="mesObj.text"
		show-alert="mesObj.active"></alert-message>
</div>

<script
	src="<c:url value="/assets/js/uploadTopUpList.js"></c:url>"></script>