<%--
  Created by IntelliJ IDEA.
  User: chien
  Date: 02-Jul-18
  Time: 11:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/transactionListStyle.css"></c:url>">
<script>
    var contextPath = '${pageContext.servletContext.contextPath}';
</script>
<script src="<c:url value="/assets/js/transactionList.js"></c:url>"></script>
<c:if test="${sessionScope.UserRoleValue == 'Admin'}">
<div class="container" ng-controller="transactionListCtrl" ng-init="initializeData()">
	<div id="loader-wrapper" ng-if="showLoaderAllPage" ng-cloak>
        <div id="loader"></div>
    </div>
    <div class="row m-t-30 m-b-20">
        <div class="col-md-12 text-left font-mont">
            <span class="tag text-center">Transaction List Report</span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 offset-md-2">
            <div class="form-group row">
                <span class="col-sm-5 col-form-label text-left">From date</span>
                <div class="col-sm-7">
                    <div class="input-group input-date">
                        <input type="text" id="fromDate" class="form-control" date-picker ng-model="fromDate"
                               ng-class="{'invalid-input': !fromDate}"
                               placeholder="dd/mm/yyyy" aria-describedby="dateIDAddon">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group row">
                <span class="col-sm-5 col-form-label text-left">To Date</span>
                <div class="col-sm-7">
                    <div class="input-group input-date">
                        <input type="text" id="toDate" class="form-control" date-picker ng-model="toDate"
                               ng-class="{'invalid-input': !toDate}"
                               placeholder="dd/mm/yyyy" aria-describedby="dateIDAddon">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <button class="btn btn-success btn-custom" type="button" ng-click="applyView()">
                Apply
            </button>
        </div>
    </div>
    <div class="row wrapper-scroll" custom-scroll>
        <table ng-cloak class="table table-bordered table-custom-trans">
            <thead>
                <tr>
                    <th ng-repeat="key in listKeyTable" class="text-center">
                        <span ng-bind="key['name']"></span>
                    </th>
                </tr>
            </thead>
            <tbody ng-repeat="transObj in reportData">
                <tr ng-if="reportData.length > 0" class="custom-row-info">
                    <td colspan="{{listKeyTable.length}}">Số thẻ/Card ID: (<span ng-bind="transObj.cardId"></span>) <span ng-bind="transObj.cusName"></span></td>
                </tr>
                <tr ng-if="reportData.length > 0" ng-repeat="data in transObj.data">
                    <td ng-repeat="key in listKeyTable" ng-class="key.iClass">
                        <span ng-bind="data[key['code']]"></span>
                    </td>
                </tr>
                <tr ng-if="reportData.length > 0">
                    <td colspan="4" class="border-right-0">Total by Card ID:</td>
                    <td class="text-right border-right-none border-left-none">
                    	<span ng-bind="transObj.totalQty"></span>
                    </td>
                    <td colspan="2" class="text-right border-right-none border-left-none">
                    	<span ng-bind="transObj.totalAmount"></span>
                    </td>
                    <td class="border-left-none"></td>
                </tr>
                <tr ng-if="reportData.length === 0 || arrFilter.length === 0">
                    <td class="text-center" colspan="{{listKeyTable.length}}"><span><spring:message code="label.no_data"/></span></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="row m-t-10">
        <div class="dropup col-md-12 m-r-15">
            <button class="btn btn-success btn-lg  dropdown-toggle pull-right" type="button" id="dropdownExport" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                EXPORT
                <span class="glyphicon glyphicon-download-alt p-l-10"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownExport">
                <li><div class="li-export" ng-click="exportData('xls')"><span class="export-item">Excel export</span><img class="pull-right" src='<c:url value="/assets/images/excel_icon.png"></c:url>'></div></li>
                <li role="separator" class="custom-separate"></li>
                <li><div class="li-export" ng-click="exportData('pdf')"><span class="export-item ">PDF export</span><img class="pull-right" src='<c:url value="/assets/images/pdf_icon.png"></c:url>'></div></li>
            </ul>
        </div>
    </div>
</div>
</c:if>