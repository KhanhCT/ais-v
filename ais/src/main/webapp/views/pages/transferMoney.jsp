<%--
  Created by IntelliJ IDEA.
  User: chien
  Date: 11-Jul-18
  Time: 9:43 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/transferMoney.css"></c:url>">
<script>var contextPath = '${pageContext.servletContext.contextPath}';</script>
<div class="container" ng-controller="transferMoneyCtrl" ng-init="initializeData()">
    <div class="row m-t-30 m-b-20">
        <div class="col-md-8 offset-md-2 text-left font-mont">
            <span class="tag text-center"><spring:message code="cum.tm.title_page"/></span>
        </div>
    </div>
    <div class="col-md-8 offset-md-2 wrapper-content">
        <div class="transfer-header">
            <spring:message code="tm.src_title"/>
        </div>
        <div class="transfer-content">
            <div class="form-group row" ng-form="srcForm">
                <label for="accountID" class="col-sm-5 col-form-label text-right-custom"><spring:message code="tm.src_account"/></label>
                 <div class="col-sm-7">
                    <input class="form-control" placeholder="<spring:message code="tm.src_account"/>" ng-model="srcObj.cusId" ng-keydown="$event.keyCode === 13 && findSrcAccount()" required="required">
                </div>
            </div>
             <div class="form-group row">
                <label for="sourceId" class="col-sm-5 col-form-label text-right-custom"><spring:message code="tm.remitter_name"/></label>
                <div class="col-sm-7">
                    <input class="form-control" placeholder="<spring:message code="tm.remitter_name"/>" disabled ng-model="srcObj.cusName">
                </div>
            </div>
            <div class="form-group row">
                <label for="balanceID" class="col-sm-5 col-form-label text-right-custom"><spring:message code="tm.balance"/></label>
                <div class="col-sm-7">
                    <input class="form-control" placeholder="O VND" disabled ng-model="srcObj.balance">             
                </div>
            </div>
        </div>
        <div class="transfer-header">
            <spring:message code="tm.dst_account"/>
        </div>
        <div class="transfer-content"> 
            <div class="form-group row" ng-form="dstForm">
                <label for="balanceID" class="col-sm-5 col-form-label text-right-custom"><spring:message code="tm.dst_account"/></label>
                <div class="col-sm-7">
                    <input class="form-control" placeholder="<spring:message code="tm.dst_account"/>" ng-model="dstObj.cusId" ng-keydown="$event.keyCode === 13 && findDstAccount()">
                </div>
            </div>
            <div class="form-group row">
                <label for="balanceID" class="col-sm-5 col-form-label text-right-custom"><spring:message code="tm.recipient_name"/></label>
                <div class="col-sm-7">
                    <input class="form-control" placeholder="<spring:message code="tm.recipient_name"/>" ng-model="dstObj.cusName" disabled >
                </div>
            </div>
        </div>
        <div class="transfer-header">
        <spring:message code="tm.transfer"/>
        </div>
		<div class="transfer-content">
			<div class="form-group row">
				<label for="accountID"
					class="col-sm-5 col-form-label text-right-custom"> <spring:message code="cm.amount"/></label>
				<div class="col-sm-7">
					<input class="form-control" placeholder="<spring:message code="cm.amount"/>" ng-model="amount" required="required">
					<span ></span>
				</div>
			</div>
			<div class="form-group row">
				<button class="col-4 offset-md-1 btn btn-secondary" ng-cloak ng-click="clearData()" > <spring:message code="cm.clear"/></button>
				<button class="col-4 btn offset-md-2 btn-success " ng-cloak ng-disabled="!srcObj.cusId || !dstObj.cusId"
					ng-click="transfer()"> <spring:message code="cm.transfer"/></button>
			</div>
		</div>
	</div>
	<alert-message type-alert="mesObj.type" message-alert="mesObj.text" show-alert="mesObj.active"></alert-message>
</div>
<script src="<c:url value="/assets/js/transferMoney.js"></c:url>"></script>