myApp.controller('userManagementCtrl', ['$scope', 'NgTableParams', 'ngTableEventsChannel', '$http', '$q' ,'$timeout',
    function($scope, NgTableParams, ngTableEventsChannel, $http, $q, $timeout) {
    $scope.selectedUser = {
        gender: 1
    };
    $scope.listUser = [];
    $scope.showLoaderAllPage = false;
    $scope.initData = function(){
        $scope.showLoaderAllPage = true;
        $http({
            method: 'GET',
            url: contextPath + "/api/v0.1/userController/getLstUserActive",
            responseType: 'json'
        }).then(function successCallback(response) {
            if(response.data){
                $scope.listUser = response.data
            }
            drawTable();
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {
            $scope.showLoaderAllPage = false;
        });
    };
    $scope.modifiUser = function(row){
        $scope.showLoaderUser = true;
        $timeout(function () {
            $scope.selectedUser = row;
            $scope.selectedUserClone = angular.copy($scope.selectedUser);
            $scope.showLoaderUser = false;
        }, 1000);
    };
    $scope.clearModifiUser = function (){
        $scope.selectedUser = undefined;
        $scope.selectedUserClone = undefined;
    };
    $scope.showDeleteUser = function (user){
        $scope.userToDelete = user;
    };
    $scope.saveModifiUser = function(){
        $http({
            method: 'POST',
            url: "saveUser",
            params:{user:$scope.selectedUserClone},
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {
            if(response.data){
                $scope.selectedUser = angular.copy($scope.selectedUserClone)
            }
        }, function errorCallback(response) {
            alert('Save User Error');
        });
    };
    $scope.validateDeleteUser = function(){
        $scope.showLoaderAllPage = true;
        $http({
            method: 'POST',
            url: "deleteUser",
            params:{userId:$scope.userToDelete.userCode},
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {
            $scope.selectedUser = undefined;
            $scope.selectedUserClone = undefined;
            $scope.userToDelete = undefined;
            $scope.initData();
        }, function errorCallback(response) {
            alert('Delete User Error');
            $scope.showLoaderAllPage = false;
        });
    };
    function drawTable(){
        $scope.tableUserManagement = new NgTableParams({
            page: 1,
            count: 10
        }, {
            counts: [1, 5,10,50],
            paginationMaxBlocks: 5,
            paginationMinBlocks: 2,
            dataset: $scope.listUser
        });
    }

}]);
myApp.directive('mCusScroll', function() {
    return{
        restrict: 'A',
        link: function(scope, element, attrs) {
            setTimeout(function() {
                element.mCustomScrollbar({
                    axis:"y",
                    theme:"dark",
                    autoExpandScrollbar:true,
                    scrollbarPosition: "outside",
                    autoHideScrollbar: true,
                    advanced:{
                        autoExpandVerticalScroll:true
                    }});
            }, 100);
        }
    };
});