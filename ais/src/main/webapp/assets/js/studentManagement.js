myApp.controller('studentManagement', ['$scope', '$http', '$timeout', '$q', 'Upload','$window', function($scope, $http, $timeout, $q, Upload) {
	$scope.gradeList = [];
	$scope.classList = [];
	$scope.classFilter = [];
	$scope.customers = []
	
	$scope.selectedGrade = undefined;
	$scope.selectedClass = undefined;
	$scope.orderTable = undefined;
	
	$scope.showLoaderAllPage = false;
	
    var URI_GET_PAGE_DATA = 'getCommonDTO',
    FIND_CUSTOMER_BY_ID = 'findCustomerById',
    FIND_CUSTOMER_BY_CLASS_ID = 'findByClassId',
    DISABLE_CUSTOMER = 'disableStudent';
	$scope.progressPercentage = 0;
	$scope.mesObj = undefined;
	$scope.fileToUpload = undefined;
	function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
	}

	/*TABLE*/
	$scope.itemGap = 10;
	$scope.listGap = [1,5,10,20];
	$scope.setItemGap = function(gap){
		$scope.itemGap = gap;
	};
	$scope.showFilter =function(key){
		key.openFilter = true;
	};
	$scope.clearFilter = function(key){
		key.textFilter = '';
	};
	$scope.changeSort = function(key){
		if(!$scope.sortColumn || $scope.sortColumn !== key) {
			if($scope.sortColumn){
				$scope.sortColumn['sortAZ'] = false;
				$scope.sortColumn['sortZA'] = false;
			}
			key['sortAZ'] = true;
			key['sortZA'] = false;
			$scope.sortColumn = key;
		} else if($scope.sortColumn && $scope.sortColumn === key) {
			if($scope.sortColumn['sortAZ']){
				$scope.sortColumn['sortAZ'] = false;
				$scope.sortColumn['sortZA'] = true;
			} else if(key['sortZA']){
				$scope.sortColumn['sortAZ'] = true;
				$scope.sortColumn['sortZA'] = false;
			} else {
				$scope.sortColumn['sortAZ'] = true;
				$scope.sortColumn['sortZA'] = false;
			}
		}
	};
	$scope.hideFilter = function (key){
		key.openFilter = false;
	};
	$scope.changeGrade = function(){
		var gradeIdTemp = $scope.selectedGrade.gradeId;
		$scope.classFilter = [];
		angular.forEach($scope.classList, function (item) {
			if(item.gradeId === gradeIdTemp){
                $scope.classFilter.push(item)
			}
        })
	};
	$scope.uploadFile = function ($event) {
		$scope.showLoaderAllPage = true;
		$event.stopPropagation();
		$event.preventDefault();
		$scope.progressPercentage = 0;
		var URI_UPLOAD_FILE = "uploadCardList";
		var URL_UPLOAD = contextPath + CONTSTANT_SERVICE_URI.URI_FILE + URI_UPLOAD_FILE;
		Upload.upload({
			url: URL_UPLOAD,
			method: 'POST',
			data: {file: $scope.fileToUpload}
		}).then(function (resp) {
			$scope.customers = resp.data
			$scope.showLoaderAllPage = false;
		}, function (resp) {
			$scope.showLoaderAllPage = false;
		}, function (evt) {
			$scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + $scope.progressPercentage + '% ' );
			$scope.showLoaderAllPage = false;
		});
	}
    $scope.initializeData = function()
    {
    	$scope.showLoaderAllPage = true;
    	$scope.mesObj = {
    			type: true,
    			text: '',
    			active: false
    	}

	  	var URL_API_PAGE_DATA =  contextPath + CONTSTANT_SERVICE_URI.URI_CUSTOMER + URI_GET_PAGE_DATA;
	  	$http({
            method: 'GET',
            url: URL_API_PAGE_DATA,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	var pageData = response.data;
            	$scope.gradeList = angular.copy(pageData.gradeList);
            	$scope.classList = angular.copy(pageData.classList);
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log(URL_API_PAGE_DATA + ' [ERROR]');
        }); 	
    };
    $scope.find = function(){
    	if($scope.cusId && $scope.cusId !=''){
    		findCusById();
    	}else{
    		findByClass();
    	}
    }
    findCusById = function () {
    	$scope.customers = [];
    	var URL_API_PAGE_DATA =  contextPath + CONTSTANT_SERVICE_URI.URI_CUSTOMER + FIND_CUSTOMER_BY_ID;
	  	$http({
            method: 'GET',
            url: URL_API_PAGE_DATA,
            params:{cusId: $scope.cusId},
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	var pageData = response.data;
            	$scope.customers.push (pageData);
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log(FIND_CUSTOMER_BY_ID + ' [ERROR]');
        }); 	
    }
    findByClass = function () {
    	var URL_API_PAGE_DATA =  contextPath + CONTSTANT_SERVICE_URI.URI_CUSTOMER + FIND_CUSTOMER_BY_CLASS_ID;
	  	$http({
            method: 'GET',
            url: URL_API_PAGE_DATA,
            params:{classId: $scope.selectedClass.classId,
            	type : "ALL"},
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	var pageData = response.data;
            	$scope.customers = angular.copy(pageData);
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log(FIND_CUSTOMER_BY_CLASS_ID + ' [ERROR]');
        }); 	
    }
    
    $scope.changeStatus = function (checAll) {
        angular.forEach($scope.customers, function(item){
            if(checAll == true){
                item.active = 0;
            } else if(checAll == false){
                item.active = 1;
            }
        });
    };
    
    $scope.checkStatus = function(){
        var countStatus = 0, countNumber = 0;
        angular.forEach($scope.customers, function(item){
            if(item.active == 0){
                countStatus++;
            }
            countNumber++;
        });
        if(countNumber === countStatus && countNumber !== 0){
            $scope.checAll = true;
            $('#select_all').prop('checked',true);
        } else {
            $scope.checAll = false;
            $('#select_all').prop('checked',false);
        }
    }
    
    $scope.saveCusList = function () {
    	var students;
    	angular.forEach($scope.customers, function(item){
            if(item.active == 0){
            	students.push(item);
            }
        });
    	var URL_API_PAGE_DATA =  contextPath + CONTSTANT_SERVICE_URI.URI_CUSTOMER + DISABLE_CUSTOMER;
	  	$http({
            method: 'POST',
            url: URL_API_PAGE_DATA,
            data: students,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	$scope.showLoaderAllPage = false;
            	var pageData = response.data;
            	if (pageData.success == false){
            		if (pageData.code == 0) {
            			showMessage ("ERROR");
            		} else if (pageData.code == 2) {
            			showMessage (pageData.description + " :" + pageData.data);
            		} 
            	} else {
            		showMessage ("SUCCESS!");
            	}
            }
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log(FIND_CUSTOMER_BY_CLASS_ID + ' [ERROR]');
        }); 	
    }
	$scope.listKeyTable = [{
		code : 'cusId',
		name : 'Customer ID',
		isSortFilter : true,
		width : '100px',
		iClass : "text-center"
	},{
		code : 'cusName',
		name : 'Customer Name',
		isSortFilter : true,
		width : '150px',
		iClass : "text-center"
	}, {
		code : 'cardCode',
		name : 'Card Number',
		isSortFilter : true,
		width : '40px',
		iClass : "text-center"
	}, {
		code : 'address',
		name : 'Address',
		isSortFilter : true,
		width : '55px',
		iClass : "text-center"
	}, {
		code : 'phoneNum',
		name : 'Phone Number',
		isSortFilter : true,
		width : '55px',
		iClass : "text-center"
	},{
		code : 'email',
		name : 'Email',
		isSortFilter : true,
		width : '55px',
		iClass : "text-center"
	},{
		code : 'remark',
		name : 'Remark',
		isSortFilter : true,
		width : '55px',
		iClass : "text-center"
	}];			
}]);
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return 1;
                        else if (Number(tempA) < Number(tempB)) return -1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return 1;
                        else if (tempA < tempB) return -1;
                        else if (tempA === tempB) return 0;
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return -1;
                        else if (Number(tempA) < Number(tempB)) return 1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return -1;
                        else if (tempA < tempB) return 1;
                        else if (tempA === tempB) return 0;
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});