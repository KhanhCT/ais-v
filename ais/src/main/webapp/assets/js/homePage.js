myApp.controller('homePageCtrl', ['$scope', '$http', '$timeout', '$q', '$window', function($scope, $http, $timeout,  $q, $window) {
    $scope.parentChildLst = [];
    $scope.showAlertNoTransactionFound = false;
    $scope.showAlertNoChild = false;
    $scope.showLoaderAllPage = false;
    $scope.curDate = moment().format('DD/MM/YYYY');
    $scope.childSelected = undefined;
    $scope.fromDate = moment().format('DD/MM/YYYY');
    $scope.toDate = moment().format('DD/MM/YYYY');
    $scope.arrFilter = undefined;
    $scope.lstTransType = [];
    $scope.itemGap = 10;
    $scope.listGap = [1,5,10,20];
    $scope.totalCashIn = 0;
    $scope.totalCashOut = 0;
    
    $scope.transTypeFilter = {
        transTypeId: "",
        transTypeCode: "",
        transTypeName: "ALL"
    };
    $scope.arrData = [];
    var URI_GET_CUSTOMER_TRANSACTION = 'getSaleTransHistory',
        URI_EXPORT_TRANSACTION = 'transactionHistoryReport',
        URI_GET_ALL_TRANS_TYPE = 'getAllTransType';
    angular.element(document).ready(function () {
        createEventForPassEffectClass();
    });
    $scope.setItemGap = function(gap){
        $scope.itemGap = gap;
    };
    $scope.showFilter =function(key){
        key.openFilter = true;
    };
    $scope.clearFilter = function(key){
        key.textFilter = '';
    };
    $scope.changeSort = function(key){
        if(!$scope.sortColumn || $scope.sortColumn !== key) {
            if($scope.sortColumn){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = false;
            }
            key['sortAZ'] = true;
            key['sortZA'] = false;
            $scope.sortColumn = key;
        } else if($scope.sortColumn && $scope.sortColumn === key) {
            if($scope.sortColumn['sortAZ']){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = true;
            } else if(key['sortZA']){
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            } else {
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            }
        }
    };
    $scope.hideFilter = function (key){
        key.openFilter = false;
    };
    $scope.listKeyTable = [
        {
            code: 'transDate',
            name: 'Date',
            isSortFilter: true,
            width: '100px',
            iClass: "text-center"
        },
        {
            code: 'transTime',
            name: 'Time',
            isSortFilter: true,
            width: '50px',
            iClass: "text-center"
        },
        {
            code: 'transNum',
            name: 'Service Code',
            isSortFilter: true,
            width: '150px',
            iClass: "text-center"
        },
        {
            code: 'cashIn',
            name: 'Cash In',
            isSortFilter: true,
            width: '150px',
            isTotal: true,
            iClass: "text-right"
        },
        {
            code: 'cashOut',
            name: 'Cash Out',
            isSortFilter: true,
            width: '150px',
            isTotal: true,
            iClass: "text-right"
        },
        {
            code: 'action',
            name: 'Action',
            isSortFilter: true,
            width: '50px',
            iClass: "text-center"
        },
        {
            code: 'remark',
            name: 'Remark',
            isSortFilter: true,
            width: '150px',
            isTotal: true,
            iClass: "text-center"
        }
    ];
    $scope.getTotalOfBoard = function(key){
        var total= 0;
        if(key['code'] === 'transDate'){
            return 'Total By CardID(CardId)';
        }
        if(key.isTotal === true){
            angular.forEach($scope.arrData, function (item) {
                if(!isNaN(item[key['code']])){
                    total += Number(item[key['code']]);
                }
            })
        } else {
            total = '';
        }
        return total;
    };
    $scope.initData = function(){
        $scope.showLoaderAllPage = true;
        var  URI_GET_ALL_CHILD = 'getChildrentByParentId';
        var URL_API_GET_CHILD =  contextPath + CONTSTANT_SERVICE_URI.URI_CUSTOMER + URI_GET_ALL_CHILD;
        var URL_API_GET_ALL_TRANS_TYPE =  contextPath + CONTSTANT_SERVICE_URI.URI_SALE_TRANS + URI_GET_ALL_TRANS_TYPE;
        $http({
            method: 'GET',
            url: URL_API_GET_CHILD,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {
            if(response.data){
                $scope.parentChildLst = response.data;
                $scope.childSelected = $scope.parentChildLst[0];
                getTransactionInfor();
            } 
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {
            // $scope.showLoaderAllPage = false;
            $scope.showAlertNoChild = true;
             $timeout(function () {
                 $('#hasNochildAlertModal').modal('show');
             }, 500);
             $scope.showLoaderAllPage = false;
        });
        
    };
    $scope.applyView = function () {
        if($scope.fromDate && moment($scope.fromDate, 'DD/MM/YYYY', true).isValid()
            && $scope.toDate && moment($scope.toDate, 'DD/MM/YYYY', true).isValid()){
            $scope.showLoaderAllPage = true;
            var momentFrom = moment($scope.fromDate, 'DD/MM/YYYY', true),
                momentTo = moment($scope.toDate, 'DD/MM/YYYY', true);
            if(momentTo.diff(momentFrom) >= 0){
                getTransactionInfor();
            } else {
                $scope.toDate = $scope.fromDate;
                getTransactionInfor();
            }
        }
    };
    $scope.exportTrans = function(type) {
        var URL_REPORT =  contextPath + CONTSTANT_SERVICE_URI.URI_API_REPORT + URI_EXPORT_TRANSACTION;
        var FROM_DATE = moment($scope.fromDate, 'DD/MM/YYYY').format('DD/MM/YYYY'),
            TO_DATE = moment($scope.toDate, 'DD/MM/YYYY').format('DD/MM/YYYY');
        URL_REPORT += '?customerId='+ $scope.childSelected.cusId
                + '&fromDate=' + FROM_DATE + '&toDate=' + TO_DATE
                + '&type='+type;
        $window.open(URL_REPORT);
        // $http({
        //     method: 'GET',
        //     url: URL_REPORT,
        //     params:{
        //         customerId:$scope.childSelected.cusId,
        //         fromDate:FROM_DATE,
        //         toDate:TO_DATE,
        //         type:type
        //     },
        //     headers: {'Content-Type': 'application/json; charset=utf-8'}
        // }).then(function successCallback(response) {
        //
        // }, function errorCallback(response) {
        //     console.log(URI_EXPORT_TRANSACTION + ' [ERROR]');
        // });
    };
    $scope.changeChildSelected = function () {
        $scope.showLoaderAllPage = true;
        getTransactionInfor();
    };
    $scope.getNumberInComma = function(number){
        return numberWithCommas(number);
    }
    function getTransactionInfor(){
        var URL_API_GET_TRANS =  contextPath + CONTSTANT_SERVICE_URI.URI_SALE_TRANS + URI_GET_CUSTOMER_TRANSACTION;
        var FROM_DATE = moment($scope.fromDate, 'DD/MM/YYYY').format('DD/MM/YYYY'),
            TO_DATE = moment($scope.toDate, 'DD/MM/YYYY').format('DD/MM/YYYY');
        if($scope.childSelected && $scope.fromDate && $scope.toDate){
            $http({
                method: 'GET',
                url: URL_API_GET_TRANS,
                params:{customerId:$scope.childSelected.cusId, fromDate:FROM_DATE, toDate: TO_DATE},
                headers: {'Content-Type': 'application/json; charset=utf-8'}
            }).then(function successCallback(response) {
                if(response.data){
                    var payload = response.data
                    $scope.totalCashIn = payload.totalCashIn;
                    $scope.totalCashOut = payload.totalCashOut;
                    $scope.arrData = payload.data;
                }
                $scope.showLoaderAllPage = false;
            }, function errorCallback(response) {
                $timeout(function () {
                    $scope.showLoaderAllPage = false;
                    $scope.showAlertNoTransactionFound = true;
                }, 500);
                console.log(URI_GET_CUSTOMER_TRANSACTION + ' [ERROR]');
            });
        } else{
        	alert('Student is not available!')
        }
    }
    $scope.resetSession = function () {
        window.location.href = contextPath;
    };
}]);
function createEventForPassEffectClass() {
    $(document).on('mouseenter','.pass-effect', function() {
        applyPassEffect(this);
    });

    $(document).on('mouseleave','.pass-effect', function() {
        stopPassEffect(this);
    });
}

function applyPassEffect(div) {
    var span = $(div).children('span');
    var parentWidth = $(div).width();
    var childWidth = $(span).width();

    if (childWidth > parentWidth) {
        repeatAnimation(span, childWidth, parentWidth);
    }
}

function repeatAnimation(span, childWidth, parentWidth) {
    $(span).animate({
        marginLeft: (-childWidth) + 'px'
    }, 6000, "linear");

    $(span).animate({
        marginLeft: parentWidth + 'px'
    }, 0, function () {
        repeatAnimation(span, childWidth, parentWidth);
    });
}

function stopPassEffect(div) {
    var span = $(div).children('span');
    $(span).clearQueue().stop();
    $(span).css('margin-left',0);
}
myApp.filter('startFrom', function () {
    return function (input, start) {
        if (input) {
            start = +start;
            return input.slice(start);
        }
        return [];
    };
});
function numberWithCommas(num) {
    if(num != null){
        var parts = num.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    } else {
        return '';
    }
}
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;

                    if(keySort['code'] === 'transDate' || keySort['code'] === 'transTime' ){
                        // function Sort Date
                    } else {
                        var tempA = a[keySort['code']];
                        var tempB = b[keySort['code']];
                        if(!isNaN(tempA) && !isNaN(tempB)){
                            if(Number(tempA) > Number(tempB)) return 1;
                            else if (Number(tempA) < Number(tempB)) return -1;
                            else if (Number(tempA) === Number(tempB)) return 0;
                        } else{
                            tempA = tempA.toString().toLowerCase();
                            tempB = tempB.toString().toLowerCase();
                            if(tempA > tempB) return 1;
                            else if (tempA < tempB) return -1;
                            else if (tempA === tempB) return 0;
                        }
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    if(keySort['code'] === 'transDate' || keySort['code'] === 'transTime'){
                        // function Sort Date
                    } else {
                        var tempA = a[keySort['code']];
                        var tempB = b[keySort['code']];
                        if(!isNaN(tempA) && !isNaN(tempB)){
                            if(Number(tempA) > Number(tempB)) return -1;
                            else if (Number(tempA) < Number(tempB)) return 1;
                            else if (Number(tempA) === Number(tempB)) return 0;
                        } else{
                            tempA = tempA.toString().toLowerCase();
                            tempB = tempB.toString().toLowerCase();
                            if(tempA > tempB) return -1;
                            else if (tempA < tempB) return 1;
                            else if (tempA === tempB) return 0;
                        }
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});
