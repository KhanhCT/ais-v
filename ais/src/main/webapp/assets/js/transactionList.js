myApp.controller('transactionListCtrl', ['$scope', '$http', '$timeout', '$q', '$window', function($scope, $http, $timeout,  $q, $window) {
	$scope.reportData = [];
	$scope.fromDate = moment(new Date()).format('DD/MM/YYYY');
	$scope.toDate = moment(new Date()).format('DD/MM/YYYY');
	$scope.showLoaderAllPage = true;
	var URI_GET_REPORT_DATA = 'getTransactionList';
	var URI_API_GET_REPORT_DATA =  contextPath + CONTSTANT_SERVICE_URI.URI_SALE_TRANS + URI_GET_REPORT_DATA;
	$scope.initializeData = function(){
		var _fromDate = moment($scope.fromDate, 'DD/MM/YYYY').format('DD/MM/YYYY');
        var _toDate = moment($scope.toDate, 'DD/MM/YYYY').format('DD/MM/YYYY');
        $http({
            method: 'GET',
            url: URI_API_GET_REPORT_DATA,
            params:{fromDate:_fromDate, toDate: _toDate},
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	$scope.reportData = response.data;
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log(URI_API_GET_REPORT_DATA + ' [ERROR]');
        });
		$timeout(function () {
            $scope.showLoaderAllPage = false;
        }, 500);
	};
	$scope.applyView = function(){
		$scope.showLoaderAllPage = true;
		var _fromDate = moment($scope.fromDate, 'DD/MM/YYYY').format('DD/MM/YYYY');
        var _toDate = moment($scope.toDate, 'DD/MM/YYYY').format('DD/MM/YYYY');
       
		$http({
            method: 'GET',
            url: URI_API_GET_REPORT_DATA,
            params:{fromDate:_fromDate, toDate: _toDate},
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	$scope.reportData = response.data;
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log(URI_API_GET_REPORT_DATA + ' [ERROR]');
        });
	}

	$scope.exportData = function (typeExport) {
		var typeBlod;
		var fileName;
		if (typeExport === 'xls') {
			typeBlod = 'application/vnd.ms-excel';
			fileName = 'TransactionList.xls';
		} else if (typeExport === 'pdf') {
			typeBlod = 'application/pdf';
			fileName = 'TransactionList.pdf';
		}
		$http({
            method: 'POST',
            url: contextPath + "/ais/report/exportTransactionList?fromDate=" + $scope.fromDate +"&toDate="+ $scope.toDate + "&typeExport=" + typeExport,
            responseType: 'arraybuffer',
            data: $scope.reportData
        }).then(function successCallback(response) {
     
            var linkElement = document.createElement('a');
            try {
                var blob = new Blob([response.data], { type: typeBlod });
                var url = window.URL.createObjectURL(blob);
     
                linkElement.setAttribute('href', url);
                linkElement.setAttribute("download", fileName);
     
                var clickEvent = new MouseEvent("click", {
                    "view": window,
                    "bubbles": true,
                    "cancelable": false
                });
                linkElement.dispatchEvent(clickEvent);
            } catch (ex) {
                console.log(ex);
            }
        }, function errorCallback(response) {
        });
	}
	
    $scope.listKeyTable = [
        {
            code: 'transDate',
            name: 'Date',
            isSortFilter: true,
            width: '100px',
            iClass: "text-center"
        },
        {
            code: 'transTime',
            name: 'Time',
            isSortFilter: true,
            width: '50px',
            iClass: "text-center"
        },
        {
            code: 'transNum',
            name: 'Service number',
            isSortFilter: true,
            width: '150px',
            iClass: "text-center"
        },
        {
            code: 'transType',
            name: 'Type',
            isSortFilter: true,
            width: '150px',
            isTotal: true,
            iClass: "text-right"
        },
        {
            code: 'qty',
            name: 'Quantity',
            isSortFilter: true,
            width: '150px',
            isTotal: true,
            iClass: "text-right"
        },
        {
            code: 'rtPrice',
            name: 'Unit price',
            isSortFilter: true,
            width: '50px',
            iClass: "text-center"
        },
        {
            code: 'amount',
            name: 'Amount',
            isSortFilter: true,
            width: '150px',
            isTotal: true,
            iClass: "text-right"
        },
        {
            code: 'skuName',
            name: 'Item',
            isSortFilter: true,
            width: '150px',
            isTotal: true,
            iClass: "text-right"
        }
    ];

}]);
