var sliderImageAuto = undefined;
myApp.controller('mealTodayController',['$scope','$timeout', '$http', function ($scope, $timeout, $http) {
    var sliderList = [];
    $scope.showImageZoom = false;
    var URI_GET_DAILY_MEAL = 'findAllImageFiles';
    $scope.initData = function () {
        $scope.loaderPreviewPage = true;
        var URL_API_GET =  contextPath + CONTSTANT_SERVICE_URI.URI_FILE + URI_GET_DAILY_MEAL;
        // ASYN FOR LIST MEAL TODAY -> IT BE HIDDEN TO MAKE VIEW SMOOTHER
        $('.slider-meal-today').css({visibility: 'hidden', opacity: 0});
        $http({
            method: 'GET',
            url: URL_API_GET,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {
            if(response.data){
                sliderList = response.data
            }
            setSliderPreview(sliderList);
        }, function errorCallback(response) {
            setSliderPreview(undefined);
        });
    };
    $scope.changeZoomImage = function (slider, $event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.imageZoomURL = slider.imageUrl;
        $scope.imageZoomTitle = slider.titleMeal;
        $scope.showImageZoom = true;
        heightImage = undefined;
        widthImage = undefined;
        scaleImage = undefined;
        $scope.styleZoom = undefined;
        $scope.zoomPercent = 100;
        if(sliderImageAuto){
            sliderImageAuto.stopAuto();
        }
    };
    $scope.hidePreviewZoom = function ($event){
        $event.preventDefault();
        $event.stopPropagation();
        $scope.showImageZoom = false;
        if(sliderImageAuto){
            sliderImageAuto.startAuto();
        }
    };
    var heightImage = undefined;
    var widthImage = undefined;
    var scaleImage = undefined;
    $scope.zoomPercent = 100;
    $scope.zoomIn = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if(!heightImage || !widthImage || !scaleImage){
            widthImage = $('.cp-image-container').width();
            heightImage = $('.cp-image-container').height();
            scaleImage = widthImage / heightImage;
            $scope.zoomPercent = 115;
            $scope.styleZoom = {
                width: (widthImage * $scope.zoomPercent / 100) +'px',
                height: (heightImage * $scope.zoomPercent / 100) + 'px'
            }
        } else {
            if($scope.zoomPercent <= 185){
                $scope.zoomPercent = $scope.zoomPercent + 15;
            }
            $scope.styleZoom = {
                width: (widthImage * $scope.zoomPercent / 100) +'px',
                height: (heightImage * $scope.zoomPercent / 100) + 'px'
            }
        }
        $scope.isShowPercent = true;
        showPercentAuto();
    };
    function showPercentAuto(){
        $timeout(function () {
            $scope.isShowPercent = false;
        }, 1000);
    }
    $scope.zoomOut = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if(!heightImage || !widthImage){
            widthImage = $('.cp-image-container').width();
            heightImage = $('.cp-image-container').height();
            scaleImage = widthImage / heightImage;
            $scope.zoomPercent = 85;
            $scope.styleZoom = {
                width: (widthImage * $scope.zoomPercent / 100) +'px',
                height: (heightImage * $scope.zoomPercent / 100) + 'px'
            }
        } else {
            if($scope.zoomPercent >= 15){
                $scope.zoomPercent = $scope.zoomPercent - 15;
            }
            $scope.styleZoom = {
                width: (widthImage * $scope.zoomPercent / 100) +'px',
                height: (heightImage * $scope.zoomPercent / 100) + 'px'
            }
        }
        $scope.isShowPercent = true;
        showPercentAuto();
    };
    function setSliderPreview(slider){
        if(slider && slider.length > 0){
            $scope.sliders = [];
            for(var i = 0; i < slider.length; i++){
                $scope.sliders.push({
                    imageUrl: contextPath + '/uploads/'  + slider[i], titleMeal: slider[i]
                })
            }
        } else {
            $scope.sliders = [
                {imageUrl: contextPath+ '/assets/images/no-image.png', titleMeal: 'not-found.png'},
            ];
        }
        angular.element(document).ready(function () {
            sliderImageAuto =  $('.slider-meal-today').bxSlider({
                mode: 'horizontal',
                speed: 500,
                auto: true,
                pager: true,
                pagerType: 'full',
                infiniteLoop: true,
                adaptiveHeight: false,
                autoControls: true,
                autoControlsCombine: true,
                autoHover: true
            });
            $timeout(function () {
                $('.slider-meal-today').css({visibility: 'visible', opacity: 1});
                $scope.loaderPreviewPage = false;
            }, 500);
        });
    }
}]);