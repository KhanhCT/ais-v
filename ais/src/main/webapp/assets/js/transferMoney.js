myApp.controller('transferMoneyCtrl', ['$scope', '$http', '$q' ,'$timeout',
function($scope, $http, $q, $timeout) {
	$scope.srcObj = undefined;
	$scope.dstObj = undefined;
	$scope.amount = undefined;
	$scope.mesObj = undefined;
	
	var URI_FIND_CUSTOMER = 'findCustomerById';
	$scope.initializeData = function(){
		$scope.mesObj = {
			type: true,
			text: '',
			active: false
		}
		$scope.srcObj = {
			cusId : undefined,
			cusName: undefined,
			balance: 0
		};
	}
	function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
	}
	$scope.findSrcAccount= function(){
		var URL_API_FIND_CUSTOMER = contextPath + CONTSTANT_SERVICE_URI.URI_CUSTOMER + URI_FIND_CUSTOMER;
		$http({
            method: 'GET',
            url: URL_API_FIND_CUSTOMER,
            params: {cusId: $scope.srcObj.cusId},
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	$scope.srcObj= response.data;
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {		
            console.log(URL_API_FIND_CUSTOMER + ' [ERROR]');
            
            $scope.showLoaderAllPage = false;
        });
	}
	$scope.findDstAccount= function(){
		$scope.showLoaderAllPage = true;
		var URL_API_FIND_CUSTOMER = contextPath + CONTSTANT_SERVICE_URI.URI_CUSTOMER + URI_FIND_CUSTOMER;
		$http({
            method: 'GET',
            url: URL_API_FIND_CUSTOMER,
            params: {cusId: $scope.dstObj.cusId},
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	$scope.dstObj= response.data;
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {		
            console.log(URL_API_FIND_CUSTOMER + ' [ERROR]');
            
            $scope.showLoaderAllPage = false;
        });
	}
	$scope.clearData = function(){
		$scope.srcObj = {
				cusId : undefined,
				cusName: undefined,
				balance: 0,
				cardId: 0
		};
		$scope.dstObj = {
				cusId : undefined,
				cusName: undefined,
				balance: 0,
				cardId: 0
		};
		$scope.amount = 0;
	}
	$scope.transfer = function(){
		$scope.showLoaderAllPage = true;
		if($scope.srcObj.balance < $scope.amount){
			showMessage('Balance is not enough!');
			return;
		}
		if($scope.amount < 10000)
		{
			showMessage('Amount required more than 10,000 VND');
			return;
		}
		$scope.transferObj = {
			srcCusId: undefined,
			srcCardId: undefined,
			srcBalance: 0,
			dstCusId: undefined,
			dstCardId: undefined,
			dstBalance: 0,
			amount: 0
		};
		
		if($scope.amount && $scope.srcObj && $scope.dstObj){
			$scope.transferObj.srcCusId = $scope.srcObj.cusId;
			$scope.transferObj.srcCardId = $scope.srcObj.cardId;
			$scope.transferObj.srcBalance = $scope.srcObj.balance;
			$scope.transferObj.dstCusId = $scope.dstObj.cusId;
			$scope.transferObj.dstCardId = $scope.dstObj.cardId;
			$scope.transferObj.dstBalance = $scope.dstObj.balance;
			$scope.transferObj.amount = $scope.amount;
		}
		var URI_TRANSFER_MONEY = 'transferMoney';
		var URL_API_TRANSFER_MONEY = contextPath + CONTSTANT_SERVICE_URI.URI_CUSTOMER + URI_TRANSFER_MONEY;
		$http({
            method: 'POST',
            url: URL_API_TRANSFER_MONEY, 
            headers: {'Content-Type': 'application/json; charset=utf-8'},
		 	data: $scope.transferObj
        }).then(function successCallback(response) {	
            if(response.data){
            	switch(response.data){
            	case 0:
            		showMessage('Failed to transfer. Try again!');
            		break;
            	case 1:
            		showMessage('Transfer money successfully!');
            		break;
            	case 2:
            		break;
            	}
            	$scope.clearData();
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {		
            console.log(URL_API_TRANSFER_MONEY + ' [ERROR]');
            showMessage('Transfer money successfully!');
            $scope.showLoaderAllPage = false;
        });
	}
}]);