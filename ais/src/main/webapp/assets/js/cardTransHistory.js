myApp.controller('cardTranHistCtrl', ['$scope', '$http', '$timeout', '$q', '$window', function($scope, $http, $timeout,  $q, $window) {
	$scope.reportData = []
	$scope.fromDate = moment(new Date()).format('DD/MM/YYYY');
	$scope.toDate = moment(new Date()).format('DD/MM/YYYY');
	$scope.showLoaderAllPage = true;
	$scope.customerId = undefined;
	$scope.dataExport = undefined;
	$scope.initializeData = function(){
		
		$scope.showLoaderAllPage = true;
		
		$timeout(function () {
            $scope.showLoaderAllPage = false;
        }, 500);
		
	};
	$scope.applyView = function(){
		$scope.showLoaderAllPage = true;
		$scope.reportData = [];
		var _fromDate = moment($scope.fromDate, 'DD/MM/YYYY').format('DD/MM/YYYY');
        var _toDate = moment($scope.toDate, 'DD/MM/YYYY').format('DD/MM/YYYY');
        var URI_GET_REPORT_DATA = 'getCardTransHistory';
		var URI_API_GET_REPORT_DATA =  contextPath + CONTSTANT_SERVICE_URI.URI_SALE_TRANS + URI_GET_REPORT_DATA;
		$http({
            method: 'GET',
            url: URI_API_GET_REPORT_DATA,
            params:{fromDate:_fromDate, toDate: _toDate, cusId: $scope.customerId},
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	$scope.reportData = response.data;
            	$scope.dataExport = response.data;
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log(URI_API_GET_REPORT_DATA + ' [ERROR]');
        });
		
	}
	
	$scope.exportData = function (typeExport) {
		var typeBlod;
		var fileName;
		if (typeExport === 'xls') {
			typeBlod = 'application/vnd.ms-excel';
			fileName = 'CardTransactionHistory.xls';
		} else if (typeExport === 'pdf') {
			typeBlod = 'application/pdf';
			fileName = 'CardTransactionHistory.pdf';
		}
		$http({
            method: 'POST',
            url: contextPath + "/ais/report/exportCardTransactionHistory?fromDate=" + $scope.fromDate +"&toDate="+ $scope.toDate + "&typeExport=" + typeExport,
            responseType: 'arraybuffer',
            data: $scope.dataExport
        }).then(function successCallback(response) {
     
            var linkElement = document.createElement('a');
            try {
                var blob = new Blob([response.data], { type: typeBlod });
                var url = window.URL.createObjectURL(blob);
     
                linkElement.setAttribute('href', url);
                linkElement.setAttribute("download", fileName);
     
                var clickEvent = new MouseEvent("click", {
                    "view": window,
                    "bubbles": true,
                    "cancelable": false
                });
                linkElement.dispatchEvent(clickEvent);
            } catch (ex) {
                console.log(ex);
            }
        }, function errorCallback(response) {
        });
	}
	

    $scope.listKeyTable = [
        {
            code: 'transDate',
            name: 'Date',
            isSortFilter: true,
            width: '100px',
            iClass: "text-center"
        },
        {
            code: 'transTime',
            name: 'Time',
            isSortFilter: true,
            width: '50px',
            iClass: "text-center"
        },
        {
            code: 'transNum',
            name: 'Service number',
            isSortFilter: true,
            width: '150px',
            iClass: "text-center"
        },
        {
            code: 'transType',
            name: 'Type',
            isSortFilter: true,
            width: '150px',
            isTotal: true,
            iClass: "text-center"
        },
        {
            code: 'qty',
            name: 'Quantity',
            isSortFilter: true,
            width: '150px',
            isTotal: true,
            iClass: "text-center"
        },
        {
            code: 'cashIn',
            name: 'Cash In',
            isSortFilter: true,
            width: '50px',
            iClass: "text-right"
        },
        {
            code: 'cashOut',
            name: 'Cash Out',
            isSortFilter: true,
            width: '150px',
            isTotal: true,
            iClass: "text-right"
        },
        {
            code: 'skuName',
            name: 'Item',
            isSortFilter: true,
            width: '150px',
            isTotal: true,
            iClass: "text-center"
        }
    ];

}]);
