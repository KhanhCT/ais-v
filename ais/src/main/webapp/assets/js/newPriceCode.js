myApp.controller('newPriceCode', ['$scope', '$http', '$timeout', '$q', 'Upload',
		'$window', function($scope, $http, $timeout, $q,  Upload) {
	
	$scope.showLoaderAllPage = false;
	$scope.mesObj = undefined;
	$scope.prCode = undefined;
	$scope.prCodeName = undefined;
	$scope.cash = 'VND';
	$scope.selectedPrCode = undefined;
	$scope.clonePrCode = undefined;
	$scope.prCodeList = [] ;


	function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
	}

	/*TABLE*/
	$scope.itemGap = 10;
	$scope.listGap = [1,5,10,20];
	$scope.setItemGap = function(gap){
		$scope.itemGap = gap;
	};
	$scope.showFilter =function(key){
		key.openFilter = true;
	};
	$scope.clearFilter = function(key){
		key.textFilter = '';
	};
	$scope.changeSort = function(key){
		if(!$scope.sortColumn || $scope.sortColumn !== key) {
			if($scope.sortColumn){
				$scope.sortColumn['sortAZ'] = false;
				$scope.sortColumn['sortZA'] = false;
			}
			key['sortAZ'] = true;
			key['sortZA'] = false;
			$scope.sortColumn = key;
		} else if($scope.sortColumn && $scope.sortColumn === key) {
			if($scope.sortColumn['sortAZ']){
				$scope.sortColumn['sortAZ'] = false;
				$scope.sortColumn['sortZA'] = true;
			} else if(key['sortZA']){
				$scope.sortColumn['sortAZ'] = true;
				$scope.sortColumn['sortZA'] = false;
			} else {
				$scope.sortColumn['sortAZ'] = true;
				$scope.sortColumn['sortZA'] = false;
			}
		}
	};
	
	$scope.hideFilter = function (key){
		key.openFilter = false;
	};
	

    $scope.initializeData = function()
    {
    	$scope.showLoaderAllPage = true;
    	$scope.mesObj = {
    			type: true,
    			text: '',
    			active: false
    	}
    	showPrCodeList();
    	$timeout(function () {
            $scope.showLoaderAllPage = false;
        }, 500);
    	showMessage("On Starting .....");
    };
   
    $scope.newPriceCode = function(){
    	var URI_NEW_PRICE_POLICY = 'newPricePolicy';
	  	var URL_API_NEW_PRICE_POLICY = contextPath + CONTSTANT_SERVICE_URI.URI_ITEMS + URI_NEW_PRICE_POLICY;
	  	$scope.showLoaderAllPage = true;
	  	var newPrCoce = {
        		prCode : $scope.prCode,
        		type: '',
        		name: $scope.prCodeName,
        		cys: 'VND',
        		status: true
        }
    	$http({
            method: 'POST',
            url: URL_API_NEW_PRICE_POLICY,
            data: newPrCoce,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            switch(response.data){
            case 0:
            	showMessage("Failed to insert price code:(. Try again!");
            	break;
            case 1:{           	
            	var newPrCoce = {
            		prCode : $scope.prCode,
            		type: '',
            		name: $scope.prCodeName,
            		cys: 'VND',
            		status: true
            	}
            	$scope.prCodeList.push(newPrCoce);
            	$scope.prCode = undefined;
            	$scope.prCodeName = undefined;
            	showMessage("Insert new price code successfully :).");
            }break;
            case 2:{
            	$scope.prCode = undefined;
            	$scope.prCodeName = undefined;
            	showMessage("This price code is available. Try again!");
            }break;
            default:{
            	$scope.prCode = undefined;
            	$scope.prCodeName = undefined;
            	showMessage("Insert new price code successfully :).");
            	}break;
            }
            $scope.showLoaderAllPage = false;
            
        }, function errorCallback(response) {
            $timeout(function () {
                $scope.showLoaderAllPage = false;
            }, 500);
            showMessage("Failed to update env :(. Try again!");
        });
    };
    
    function showPrCodeList(){
    	var URI_SHOW_PRCODE = 'getAllPriceCode';
	  	var URL_API_SHOW_PRCODE = contextPath + CONTSTANT_SERVICE_URI.URI_ITEMS + URI_SHOW_PRCODE;	
	  	$scope.showLoaderAllPage = true;
    	$http({
            method: 'GET',
            url: URL_API_SHOW_PRCODE,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	$scope.prCodeList = response.data;	
            }
            $scope.showLoaderAllPage = false;
            showMessage("Finish to get price code list")
        }, function errorCallback(response) {
            $timeout(function () {
                $scope.showLoaderAllPage = false;
            }, 500);
            showMessage("Failed to get price code list :(. Try again!")
        });
    };
    $scope.showEditingPrCode = function(priceCode){
    	$scope.selectedPrCode = priceCode;
    	$scope.clonePrCode = priceCode;    	
    }
    $scope.deletePrCode = function(priceCode){
    	$scope.selectedPrCode = priceCode;
    }
    $scope.disablePrCode =  function(){
    	var URI_DELETE_PRCODE = 'disablePricePolicy';
	  	var URL_API_DELETE_PRCODE = contextPath + CONTSTANT_SERVICE_URI.URI_ITEMS + URI_DELETE_PRCODE;	
	  	$scope.showLoaderAllPage = true;
    	$http({
            method: 'DELETE',
            url: URL_API_DELETE_PRCODE,
            params:{prCode:$scope.selectedPrCode.prCode},
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
        	 switch(response.data){
             case 0:
             	showMessage("Failed to disable price code:(. Try again!");
             	break;
             case 1:{           	
             	showMessage("Disable price code successfully :).");
             	$('#deleteModal').modal('hide');
             }break;
             default:{
             	showMessage("Disable price code successfully :).");
             	$('#deleteModal').modal('hide');
             }break;
             }
        	$('#deleteModal').modal('hide');
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {
        	showMessage("Failed to disable price code:(. Try again!");
            $('#deleteModal').modal('hide');
            $timeout(function () {
                $scope.showLoaderAllPage = false;
            }, 500);
            
        });
    }
    $scope.editPriceCode = function(){
    	var URI_MODIFY_PRICE_POLICY = 'modifyPricePolicy';
	  	var URL_API_MODIFY_PRICE_POLICY = contextPath + CONTSTANT_SERVICE_URI.URI_ITEMS + URI_MODIFY_PRICE_POLICY;
	  	$scope.showLoaderAllPage = true;
	  	var newPrCoce = {
        		prCode : $scope.selectedPrCode.prCode,
        		type: '',
        		name: $scope.selectedPrCode.name,
        		cys: 'VND',
        		status: true
        }
    	$http({
            method: 'POST',
            url: URL_API_MODIFY_PRICE_POLICY,
            data: $scope.selectedPrCode,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            switch(response.data){
            case 0:
            	showMessage("Failed to update price code:(. Try again!");
            	break;
            case 1:{           	
            	var prCode = $scope.prCodeList.indexOf($scope.clonePrCode);
            	if(prCode){
            		$scope.prCodeList[prCode].name = $scope.selectedPrCode.name;
            	}            	
            	$scope.selectedPrCode = undefined;
            	showMessage("Update price code successfully :).");
            }break;
            default:{
            	var prCode = $scope.prCodeList.indexof($scope.clonePrCode);
            	if(prCode){
            		$scope.prCodeList[prCode].name = $scope.selectedPrCode.name;
            	}            	
            	$scope.selectedPrCode = undefined;
            	showMessage("Update price code successfully :).");           	
            }break;
            }
            $('#editModal').modal('hide');
            $scope.showLoaderAllPage = false;    
        }, function errorCallback(response) {
        	$('#editModal').modal('hide');
            $timeout(function () {
                $scope.showLoaderAllPage = false;
            }, 500);
            showMessage("Failed to update price code :(. Try again!");
        });
    }
	angular.element(document).ready(function () {
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		});
		$(document).on('change', '#quantityID', function () {
			if(!isNaN($(this).val())){
				$scope.selectedStudentClone.qty = Number($(this).val());
				$('#quantityID').val($scope.selectedStudentClone.qty);
			} else{
				$scope.selectedStudentClone.qty = 0;
				$('#quantityID').val($scope.selectedStudentClone.qty);
			}
		});
	});

	$scope.listKeyTable = [{
		code : 'prCode',
		name : 'Price Code',
		isSortFilter : true,
		width : '100px',
		iClass : "text-center"
	},{
		code : 'name',
		name : 'Description',
		isSortFilter : true,
		width : '100px',
		iClass : "text-center"
	}];		
}]);
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return 1;
                        else if (Number(tempA) < Number(tempB)) return -1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return 1;
                        else if (tempA < tempB) return -1;
                        else if (tempA === tempB) return 0;
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return -1;
                        else if (Number(tempA) < Number(tempB)) return 1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return -1;
                        else if (tempA < tempB) return 1;
                        else if (tempA === tempB) return 0;
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});