myApp.controller('balanceCtrl', ['$scope', '$http', '$timeout', '$q', '$window', function($scope, $http, $timeout,  $q, $window) {
    $scope.dataBalance = [];
    for(var i =0; i < 10; i++){
        var objectGrade = {
            gradeId: '00'+i,
            gradeName: 'grade '+i,
            data:[]
        };
        var data = [];
        for(var j= 0; j < Math.floor(Math.random() * (20 - 1)) + 1; j++){
            data.push(
                {
                    cardId: 'A00058'+j*i,
                    cardNum: Math.floor(Math.random() * (500000 - 100000)) + 100000,
                    cardName: 'CHENG Jahe'+j*i,
                    openAmount: '',
                    addTopup: 100000+j*i,
                    consumpsion: -Math.floor(Math.random() * (500000 - 100000)) + 100000,
                    balance: Math.floor(Math.random() * (500000 - 100000)) + 100000,
                    remark: j*i % 8 ===1 ? 'New card 10/11/2018': ''
                }
            )
        }
        objectGrade.data = data;
        $scope.dataBalance.push(objectGrade);
    };
    $scope.getStringDataByKey = function(key, data){
        if(key === 'name'){
            return data['cardId']+' ('+data['cardNum']+') ' +data['cardName']
        } else {
            return data[key];
        }
    };
    $scope.listKeyTable = [
        {
            code: 'name',
            name: 'Name',
            iClass: "text-left"
        },
        {
            code: 'openAmount',
            name: 'Opening Amount',
            iClass: "text-right"
        },
        {
            code: 'addTopup',
            name: 'Additional top up',
            iClass: "text-right"
        },
        {
            code: 'consumpsion',
            name: 'Consumpsion',
            isTotal: true,
            iClass: "text-right"
        },
        {
            code: 'balance',
            name: 'Balance',
            isTotal: true,
            iClass: "text-right"
        },
        {
            code: 'remark',
            name: 'Remark',
            iClass: "text-center"
        }
    ];

}]);
