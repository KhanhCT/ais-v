myApp.controller('consumptionSumartyCtrl', ['$scope', '$http', '$timeout', '$q', '$window', function($scope, $http, $timeout,  $q, $window) {
    $scope.dataConsumptionLst = [];
    function randomDate(start, end) {
        return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    }
    function makeRandomID() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 10; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
    function makeRandomName() {
        var text = "";
        var possibleFirst = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var possibleName = "aeijouvy";

        text += possibleFirst.charAt(Math.floor(Math.random() * possibleFirst.length));
        for (var i = 0; i < 5; i++)
            text += possibleName.charAt(Math.floor(Math.random() * possibleName.length));
        text += ' ';
        text += possibleFirst.charAt(Math.floor(Math.random() * possibleFirst.length));
        for (var i = 0; i < 5; i++)
            text += possibleName.charAt(Math.floor(Math.random() * possibleName.length));

        return text;
    }

    for(var i =0; i < 10; i++){
        var objectData = {
            cardId: makeRandomID(),
            cardNum: Math.floor(Math.random() * (999999 - 100000)) + 100000 + i,
            cardName: makeRandomName(),
            gradeId: '00'+i,
            gradeName: 'grade '+Math.floor(Math.random() * (12 - 1)) + 1,
            balance:  Math.floor(Math.random() * (99999999 - 1000000)) + 1000000,
            data:[]
        };
        var data = [];
        for(var j= 0; j < Math.floor(Math.random() * (20 - 1)) + 1; j++){
            var quantityTemp = Math.floor(Math.random() * (10 - 1)) + 1;
            var unitPriTemp = Math.floor(Math.random() * (50000 - 10000)) + 10000
            data.push(
                {
                    date: randomDate(new Date(2012, 0, 1), new Date()),
                    time: '00:00',
                    name: i*j % 5 ===0 ?'Noodle': i*j % 8 ===0 ? 'Winter melon tea' : 'Salad White China Plate',
                    serviceNum: makeRandomID(),
                    quantity: quantityTemp,
                    unitPri: unitPriTemp,
                    amount: quantityTemp * unitPriTemp,
                    remark: j*i % 8 ===1 ? 'Kiosk': 'Canteen'
                }
            )
        }
        objectData.data = data;
        $scope.dataConsumptionLst.push(objectData);
    }
    $scope.listKeyTable = [
        {
            code: 'date',
            name: 'Date',
            iClass: "text-left"
        },
        {
            code: 'time',
            name: 'Time',
            iClass: "text-right"
        },
        {
            code: 'name',
            name: 'Name',
            iClass: "text-right"
        },
        {
            code: 'serviceNum',
            name: 'Service number',
            isTotal: true,
            iClass: "text-right"
        },
        {
            code: 'quantity',
            name: 'Quantity',
            isTotal: true,
            iClass: "text-right"
        },
        {
            code: 'unitPri',
            name: 'Unit price',
            iClass: "text-center"
        },
        {
            code: 'amount',
            name: 'Amount',
            iClass: "text-center"
        }
        ,
        {
            code: 'remark',
            name: 'Remark',
            iClass: "text-center"
        }
    ];

}]);
