myApp.controller('viewCampusList', ['$scope', '$http', '$timeout', '$q',  'Upload',
		'$window', function($scope, $http, $timeout, $q,  Upload) {
	
	$scope.showLoaderAllPage = false;
	$scope.mesObj = undefined;
	$scope.campusVar = 'CAMPUS';
	$scope.campusName = '';
	$scope.campusId = undefined;
	$scope.campusList = [] ;
	$scope.gradeList = [];
	$scope.prCodeList = [];

	function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
	}

	/*TABLE*/
	$scope.itemGap = 10;
	$scope.listGap = [1,5,10,20];
	$scope.setItemGap = function(gap){
		$scope.itemGap = gap;
	};
	$scope.showFilter =function(key){
		key.openFilter = true;
	};
	$scope.clearFilter = function(key){
		key.textFilter = '';
	};
	$scope.changeSort = function(key){
		if(!$scope.sortColumn || $scope.sortColumn !== key) {
			if($scope.sortColumn){
				$scope.sortColumn['sortAZ'] = false;
				$scope.sortColumn['sortZA'] = false;
			}
			key['sortAZ'] = true;
			key['sortZA'] = false;
			$scope.sortColumn = key;
		} else if($scope.sortColumn && $scope.sortColumn === key) {
			if($scope.sortColumn['sortAZ']){
				$scope.sortColumn['sortAZ'] = false;
				$scope.sortColumn['sortZA'] = true;
			} else if(key['sortZA']){
				$scope.sortColumn['sortAZ'] = true;
				$scope.sortColumn['sortZA'] = false;
			} else {
				$scope.sortColumn['sortAZ'] = true;
				$scope.sortColumn['sortZA'] = false;
			}
		}
	};
	
	$scope.hideFilter = function (key){
		key.openFilter = false;
	};
	$scope.getTotalOfBoard = function(key){
		var total= 0;
		if(key['code'] === 'transDate'){
			return 'Total By CardID(CardId)';
		}
		if(key.isTotal === true){
			angular.forEach($scope.arrData, function (item) {
				if(!isNaN(item[key['code']])){
					total += Number(item[key['code']]);
				}
			})
		} else {
			total = '';
		}
		return total;
	};

	$scope.convertObject2String = function(key, data){     
		return data[key];
    };
    
    $scope.initializeData = function()
    {
    	$scope.showLoaderAllPage = true;
    	$scope.mesObj = {
    			type: true,
    			text: '',
    			active: false
    	}

    	$timeout(function () {
            $scope.showLoaderAllPage = false;
        }, 500);
    	showMessage("On Starting .....");
    };
   
    $scope.updateCampusEnv = function(){
    	var URI_UPDATE_CAMPUS = 'updateCampusEnv';
	  	var URL_API_UPDATE_CAMPUS = contextPath + CONTSTANT_SERVICE_URI.URI_CUSTOMER + URI_UPDATE_CAMPUS;
	  	$scope.showLoaderAllPage = true;
	  	var env = {
		  		id: $scope.campusId ,
		  		name: $scope.campusName
		  	};
    	$http({
            method: 'POST',
            url: URL_API_UPDATE_CAMPUS,
            data: env,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
        	alert(response.data);
            switch(response.data){
            case 0:
            	showMessage("Failed to update env :(. Try again!");
            	break;
            case 1:{
            	$scope.campusName = undefined;
            	$scope.campusId = undefined;
            	showMessage("Update env successfully :).");
            }break;
            case 2:{
            	$scope.campusName = undefined;
            	$scope.campusId = undefined;
            	showMessage("Update env successfully :).");
            }break;
            default:{
            	$scope.campusName = undefined;
            	$scope.campusId = undefined;
            	showMessage("Update env successfully :).");
            	}break;
            }
            $scope.showLoaderAllPage = false;
            
        }, function errorCallback(response) {
            $timeout(function () {
                $scope.showLoaderAllPage = false;
            }, 500);
            showMessage("Failed to update env :(. Try again!");
        });
    };

    $scope.showCampusList = function(){
    	var URI_SHOW_CAMPUS = 'getCampusList';
	  	var URL_API_SHOW_CAMPUS = contextPath + CONTSTANT_SERVICE_URI.URI_CUSTOMER + URI_SHOW_CAMPUS;	
	  	$scope.showLoaderAllPage = true;
    	$http({
            method: 'GET',
            url: URL_API_SHOW_CAMPUS,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	$scope.campusList = response.data;	
            }
            $scope.showLoaderAllPage = false;
            showMessage("Finish to get campus list")
        }, function errorCallback(response) {
            $timeout(function () {
                $scope.showLoaderAllPage = false;
            }, 500);
            showMessage("Failed to get campus list :(. Try again!")
        });
    };
    $scope.showGradeList = function(){
    	var URI_SHOW_GRADE = 'getGradeList';
	  	var URL_API_SHOW_GRADE = contextPath + CONTSTANT_SERVICE_URI.URI_CUSTOMER + URI_SHOW_GRADE;	
	  	$scope.showLoaderAllPage = true;
    	$http({
            method: 'GET',
            url: URL_API_SHOW_GRADE,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	$scope.gradeList = response.data;	
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {
            $timeout(function () {
                $scope.showLoaderAllPage = false;
            }, 500);
            showMessage("Failed to get grade list :(. Try again!")
        });
    	var URI_SHOW_PRCODE = 'getAllPriceCode';
	  	var URL_API_SHOW_PRCODE = contextPath + CONTSTANT_SERVICE_URI.URI_ITEMS + URI_SHOW_PRCODE;	
	  	$scope.showLoaderAllPage = true;
    	$http({
            method: 'GET',
            url: URL_API_SHOW_PRCODE,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	$scope.prCodeList = response.data;	
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {
            $timeout(function () {
                $scope.showLoaderAllPage = false;
            }, 500);
            showMessage("Failed to get grade list :(. Try again!")
        });
    }
   $scope.updatePricePolicy = function(){
	   var URI_UPDATE_PRCODE = 'updatePricePolicy';
	   var URL_API_UPDATE_PRCODE = contextPath + CONTSTANT_SERVICE_URI.URI_ITEMS + URI_UPDATE_PRCODE;	
	   $scope.showLoaderAllPage = true;
	   $http({
           method: 'POST',
           url: URL_API_UPDATE_PRCODE,
           data: $scope.gradeList,
           headers: {'Content-Type': 'application/json; charset=utf-8'}
       }).then(function successCallback(response) {	
           if(response.data == $scope.gradeList.length){
        	   showMessage("Update price policy successfully :)")
           }
           $scope.showLoaderAllPage = false;
       }, function errorCallback(response) {
           $timeout(function () {
               $scope.showLoaderAllPage = false;
           }, 500);
           showMessage("Failed to update price policy:(. Try again!")
       });
   }
    $scope.selectCampus = function(objectData){
    	$scope.campusName = objectData.name;
    	$scope.campusId = objectData.id;
    }

	angular.element(document).ready(function () {
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		});
		$(document).on('change', '#quantityID', function () {
			if(!isNaN($(this).val())){
				$scope.selectedStudentClone.qty = Number($(this).val());
				$('#quantityID').val($scope.selectedStudentClone.qty);
			} else{
				$scope.selectedStudentClone.qty = 0;
				$('#quantityID').val($scope.selectedStudentClone.qty);
			}
		});
	});

	$scope.listKeyTable = [{
		code : 'id',
		name : 'Campus Id',
		isSortFilter : true,
		width : '100px',
		iClass : "text-center"
	},{
		code : 'name',
		name : 'Campus Name',
		isSortFilter : true,
		width : '100px',
		iClass : "text-center"
	}];		

	$scope.listGradeTable = [{
		code : 'gradeId',
		name : 'Grade ID',
		isSortFilter : true,
		width : '100px',
		iClass : "text-center"
	},{
		code : 'name',
		name : 'Grade Name',
		isSortFilter : true,
		width : '100px',
		iClass : "text-center"
	}];	
}]);