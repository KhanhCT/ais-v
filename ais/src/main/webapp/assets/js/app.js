"use strict";
var myApp = angular.module('AisApplication', ['ngTable','tw.directives.clickOutside'
    ,'angularUtils.directives.dirPagination', 'ngFileUpload']);
moment.locale('en');
var CONTSTANT_SERVICE_URI = {
    URI_SALE_TRANS: '/api/v0.1/saleTrans/',
    URI_ITEMS: '/api/v0.1/itemController/',
    URI_CUSTOMER: '/api/v0.1/customerController/',
    URI_USER: '/api/v0.1/userController/',
    URI_FILE: '/api/v0.1/fileController/'
};

myApp.directive('datePicker', function() {
    return{
        restrict: 'A',
        require:'ngModel',
        link: function(scope, element, attribute, ngModel) {
            setTimeout(function(){
                element.datepicker({
                    format:'dd/mm/yyyy',
                    autoclose: true
                })
            }, 500);
        }
    };
});
myApp.directive('integerOnly', function() {
    return{
        restrict: 'A',
        require:'ngModel',
        scope:{
            leftAlign: '=?'
        },
        link: function(scope, element, attribute, ngModel) {
            setTimeout(function(){
                $(element).inputmask({
                    alias: "integer",
                    allowPlus: false,
                    allowMinus: false,
                    rightAlign: scope.leftAlign == true ?  false: true
                })
            }, 500);
        }
    };
});
myApp.directive('alertMessage', function() {
    return{
        restrict: 'E',
        scope:{
            typeAlert: '=',
            messageAlert: '=',
            showAlert: '='
        },
        template: [
            '<div class="wrap-message-alert" ng-class="classAlert" ng-show="showAlert" ng-cloak>',
                '<div class="message-alert" ng-bind="messageAlert">',
                '</div>',
            '</div>'
        ].join(''),
        replace: true,
        link: function(scope, element, attrs) {
            scope.classAlert = 'alert-error';
            if(scope.typeAlert == true){
                scope.classAlert = 'alert-success';
            }
        }
    };
});
myApp.directive('customScroll', function() {
    return{
        restrict: 'A',
        link: function(scope, element, attrs) {
            setTimeout(function() {
                element.niceScroll({
                    cursorwidth: "5px",
                    cursoropacitymin: 0.1,
                    cursorborderradius: "5px",
                    background: "rgba(0,0,0,0.1)",
                    cursorborder: "0",
                    autohidemode: true
                });
            }, 100);
        }
    };
});



