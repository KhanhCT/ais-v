myApp.controller('uploadTopUpCtl', ['$scope', '$http', '$timeout', '$q', 'Upload',
		'$window', function($scope, $http, $timeout, $q, Upload) {
	
	$scope.showLoaderAllPage = false;
	$scope.mesObj = undefined;
	$scope.customerList = []
	$scope.fileToUpload = undefined;
	$scope.progressPercentage = 0;
	
	$scope.gradeList = [];
	$scope.classList = [];
	$scope.prCodeList = [];
	$scope.classFilter = [];
	$scope.cusTypeList = []
	
	$scope.selectedGrade = undefined;
	$scope.selectedClass = undefined;
	$scope.selectedPrCode = undefined;
	
	$scope.selectedCusType = undefined;
	$scope.uploadFile = function ($event) {
		$scope.showLoaderAllPage = true;
		var URI_UPLOAD_FILE = 'uploadTopUpList';
		$event.stopPropagation();
		$event.preventDefault();
		$scope.progressPercentage = 0;
		var URL_API_UPLOAD = contextPath + CONTSTANT_SERVICE_URI.URI_FILE + URI_UPLOAD_FILE;
		Upload.upload({
			url: URL_API_UPLOAD,
			method: 'POST',
			data: {file: $scope.fileToUpload}
		}).then(function (resp) {
			$scope.customerList = resp.data
			$timeout(function(){
				$scope.showLoaderAllPage = false;
			}, 500);
			
		}, function (resp) {
			$scope.showLoaderAllPage = false;
			showMessage('Failed to upload the file. Try again!');
		}, function (evt) {
			$scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + $scope.progressPercentage + '% ' );
			$scope.showLoaderAllPage = false;
		});
	}
	function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
	}

	/*TABLE*/
	$scope.itemGap = 10;
	$scope.listGap = [1,5,10,20];
	$scope.setItemGap = function(gap){
		$scope.itemGap = gap;
	};
	$scope.showFilter =function(key){
		key.openFilter = true;
	};
	$scope.clearFilter = function(key){
		key.textFilter = '';
	};
	$scope.changeSort = function(key){
		if(!$scope.sortColumn || $scope.sortColumn !== key) {
			if($scope.sortColumn){
				$scope.sortColumn['sortAZ'] = false;
				$scope.sortColumn['sortZA'] = false;
			}
			key['sortAZ'] = true;
			key['sortZA'] = false;
			$scope.sortColumn = key;
		} else if($scope.sortColumn && $scope.sortColumn === key) {
			if($scope.sortColumn['sortAZ']){
				$scope.sortColumn['sortAZ'] = false;
				$scope.sortColumn['sortZA'] = true;
			} else if(key['sortZA']){
				$scope.sortColumn['sortAZ'] = true;
				$scope.sortColumn['sortZA'] = false;
			} else {
				$scope.sortColumn['sortAZ'] = true;
				$scope.sortColumn['sortZA'] = false;
			}
		}
	};
	$scope.$watch('selectedGrade', function(){
    	
    	for(var i = 0; i< $scope.classList.length; i++){
    		if($scope.classList[i].gradeId == $scope.selectedGrade.gradeId){
    			$scope.classFilter.push($scope.classList[i]);
    		}		
    	}
    });
	$scope.hideFilter = function (key){
		key.openFilter = false;
	};
	$scope.getTotalOfBoard = function(key){
		var total= 0;
		if(key['code'] === 'transDate'){
			return 'Total By CardID(CardId)';
		}
		if(key.isTotal === true){
			angular.forEach($scope.arrData, function (item) {
				if(!isNaN(item[key['code']])){
					total += Number(item[key['code']]);
				}
			})
		} else {
			total = '';
		}
		return total;
	};
	$scope.changeGrade = function(){
		var gradeIdTemp = $scope.selectedGrade.gradeId;
		$scope.classFilter = [];
		angular.forEach($scope.classList, function (item) {
			if(item.gradeId === gradeIdTemp){
                $scope.classFilter.push(item)
			}
        })
	};
	$scope.convertObject2String = function(key, data){     
		return data[key];
    };
    
    $scope.initializeData = function()
    {
    	$scope.showLoaderAllPage = true;
    	$timeout(function(){
    		$scope.showLoaderAllPage = false;
    	}, 500);	  	
    };
   
    $scope.saveCustomerList = function(){
    	var URL_SAVE_CUS_LIST = 'processTopUpList';
	  	var URL_API_SAVE_CUS_LIST = contextPath + CONTSTANT_SERVICE_URI.URI_SALE_TRANS + URL_SAVE_CUS_LIST
    	var meal = undefined;
	  	$scope.showLoaderAllPage = true;
    	$http({
            method: 'POST',
            url: URL_API_SAVE_CUS_LIST,
            data: $scope.customerList,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	 $scope.customerList = response.data;
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {
            $timeout(function () {
                $scope.showLoaderAllPage = false;
            }, 500);
            console.log(URL_API_SAVE_CUS_LIST + ' [ERROR]');
        });
    };
    $scope.saveCustomer = function(){
	  	var URL_API_NEW_TRANS = contextPath + CONTSTANT_SERVICE_URI.URI_SALE_TRANS + URI_NEW_TRANS;	
	  	$scope.showLoaderAllPage = true;
	  	var payload = {
	  		payload : $scope.studentOrders
	  	};
	  	$http({
	  		method: 'POST',
	  		url: URL_API_NEW_TRANS,
	  		headers: {
                contentType: "application/json; charset=utf-8",
                dataType: 'JSON'
            },
            data: $scope.studentOrders
	  	}).then(function successCallBack(response){
	  		if(response.data){
                $scope.studentOrders = response.data;
			}
	  		$scope.showLoaderAllPage = false;
	  	}, function errorCallBack(response){
	  		$scope.showLoaderAllPage = false;
	  	})
    };

	angular.element(document).ready(function () {
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		});
		$(document).on('change', '#quantityID', function () {
			if(!isNaN($(this).val())){
				$scope.selectedStudentClone.qty = Number($(this).val());
				$('#quantityID').val($scope.selectedStudentClone.qty);
			} else{
				$scope.selectedStudentClone.qty = 0;
				$('#quantityID').val($scope.selectedStudentClone.qty);
			}
		});
	});
	$scope.showDeleteCustomer = function(objectData){
        $.confirm({
			title: 'Confirm!',
            icon: 'fa fa-info-circle',
            theme: 'material',
			content: 'Are you sure to delete this record?',
			scope: $scope,
            animation: 'scale',
            closeAnimation: 'scale',
            type: 'red',
            scrollToPreviousElement: false,
			buttons: {
				deleteBtn: {
					text: 'Delete',
					btnClass: 'btn-danger',
					action: function(scope, button){
                        var index = scope.customerList.indexOf(objectData);
                        scope.customerList.splice(index,1);
                        scope.$apply();
					}
				},
				close: function(scope, button){

				}
			}
		});
	}

	$scope.listKeyTable = [{
		code : 'cusId',
		name : 'Customer Code',
		isSortFilter : true,
		width : '100px',
		iClass : "text-center"
	},{
		code : 'cusName',
		name : 'Customer Name',
		isSortFilter : true,
		width : '100px',
		iClass : "text-center"
	},{
		code : 'amount',
		name : 'Amount',
		isSortFilter : true,
		width : '55px',
		isTotal : true,
		iClass : "text-center"
	},{
		code : 'remark',
		name : 'Remark',
		isSortFilter : true,
		width : '55px',
		isTotal : true,
		iClass : "text-center"
	}];			
}]);
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;

                    if(keySort['code'] === 'transDate' || keySort['code'] === 'transTime' ){
                        // function Sort Date
                    } else {
                        var tempA = a[keySort['code']];
                        var tempB = b[keySort['code']];
                        if(!isNaN(tempA) && !isNaN(tempB)){
                            if(Number(tempA) > Number(tempB)) return 1;
                            else if (Number(tempA) < Number(tempB)) return -1;
                            else if (Number(tempA) === Number(tempB)) return 0;
                        } else{
                            tempA = tempA.toString().toLowerCase();
                            tempB = tempB.toString().toLowerCase();
                            if(tempA > tempB) return 1;
                            else if (tempA < tempB) return -1;
                            else if (tempA === tempB) return 0;
                        }
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    if(keySort['code'] === 'transDate' || keySort['code'] === 'transTime'){
                        // function Sort Date
                    } else {
                        var tempA = a[keySort['code']];
                        var tempB = b[keySort['code']];
                        if(!isNaN(tempA) && !isNaN(tempB)){
                            if(Number(tempA) > Number(tempB)) return -1;
                            else if (Number(tempA) < Number(tempB)) return 1;
                            else if (Number(tempA) === Number(tempB)) return 0;
                        } else{
                            tempA = tempA.toString().toLowerCase();
                            tempB = tempB.toString().toLowerCase();
                            if(tempA > tempB) return -1;
                            else if (tempA < tempB) return 1;
                            else if (tempA === tempB) return 0;
                        }
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});