myApp.controller('orderFromFile', ['$scope', '$http', '$timeout', '$q',
		'$window', function($scope, $http, $timeout, $q) {
	$scope.mealList = [];
	$scope.canteenList = [];
	$scope.gradeList = [];
	$scope.classList = [];
	$scope.classFilter = [];
	$scope.studentOrders = [];
	
	$scope.selectedMeal = undefined;
	$scope.selectedCanteen = undefined;
	$scope.selectedGrade = undefined;
	$scope.selectedClass = undefined;
	$scope.orderTable = undefined;
	
	$scope.showLoaderAllPage = false;
	
	var URI_GET_ITEMLIST = 'findAllItemListByGrpId',
    URI_GET_PAGE_DATA = 'getCommonDTO',
    URI_GET_STUDENT_ORDER = 'initStudentOrders',
    URI_NEW_TRANS = 'newSaleTrans';

	/*TABLE*/
	$scope.itemGap = 10;
	$scope.listGap = [1,5,10,20];
	$scope.setItemGap = function(gap){
		$scope.itemGap = gap;
	};
	$scope.showFilter =function(key){
		key.openFilter = true;
	};
	$scope.clearFilter = function(key){
		key.textFilter = '';
	};
	$scope.changeSort = function(key){
		if(!$scope.sortColumn || $scope.sortColumn !== key) {
			if($scope.sortColumn){
				$scope.sortColumn['sortAZ'] = false;
				$scope.sortColumn['sortZA'] = false;
			}
			key['sortAZ'] = true;
			key['sortZA'] = false;
			$scope.sortColumn = key;
		} else if($scope.sortColumn && $scope.sortColumn === key) {
			if($scope.sortColumn['sortAZ']){
				$scope.sortColumn['sortAZ'] = false;
				$scope.sortColumn['sortZA'] = true;
			} else if(key['sortZA']){
				$scope.sortColumn['sortAZ'] = true;
				$scope.sortColumn['sortZA'] = false;
			} else {
				$scope.sortColumn['sortAZ'] = true;
				$scope.sortColumn['sortZA'] = false;
			}
		}
	};
	$scope.hideFilter = function (key){
		key.openFilter = false;
	};
	$scope.getTotalOfBoard = function(key){
		var total= 0;
		if(key['code'] === 'transDate'){
			return 'Total By CardID(CardId)';
		}
		if(key.isTotal === true){
			angular.forEach($scope.arrData, function (item) {
				if(!isNaN(item[key['code']])){
					total += Number(item[key['code']]);
				}
			})
		} else {
			total = '';
		}
		return total;
	};
	$scope.changeGrade = function(){
		var gradeIdTemp = $scope.selectedGrade.gradeId;
		$scope.classFilter = [];
		angular.forEach($scope.classList, function (item) {
			if(item.gradeId === gradeIdTemp){
                $scope.classFilter.push(item)
			}
        })
	};
	$scope.convertObject2String = function(key, data){     
		return data[key];
    };
    
    $scope.initializeData = function()
    {
    	$scope.showLoaderAllPage = true;
	  	var URL_API_ITEM_LIST =  contextPath + CONTSTANT_SERVICE_URI.URI_ITEMS + URI_GET_ITEMLIST;
	  	var URL_API_PAGE_DATA =  contextPath + CONTSTANT_SERVICE_URI.URI_SALE_TRANS + URI_GET_PAGE_DATA;
	  	$http({
            method: 'GET',
            url: URL_API_PAGE_DATA,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	var pageData = response.data;
            	$scope.canteenList = angular.copy(pageData.canteenList);
            	$scope.gradeList = angular.copy(pageData.gradeList);
            	$scope.classList = angular.copy(pageData.classList);
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log(URL_API_PAGE_DATA + ' [ERROR]');
        }); 	
	  	$http({
	            method: 'GET',
	            url: URL_API_ITEM_LIST,
	            params:{itemGrpId: 1},
	            headers: {'Content-Type': 'application/json; charset=utf-8'}
	        }).then(function successCallback(response) {	
	            if(response.data){
	                $scope.mealList = response.data;
	            }
	            $scope.showLoaderAllPage = false;
	        }, function errorCallback(response) {
				$scope.showLoaderAllPage = false;
	            console.log(URL_API_ITEM_LIST + ' [ERROR]');
	     });  
    };
   
    $scope.applyFunc = function(){
	  	var URL_API_INIT_STUDENT_ORDER = contextPath + CONTSTANT_SERVICE_URI.URI_SALE_TRANS + URI_GET_STUDENT_ORDER;
    	var meal = undefined;
	  	$scope.showLoaderAllPage = true;
    	$http({
            method: 'GET',
            url: URL_API_INIT_STUDENT_ORDER,
            params:{classId: 1},
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	var skuId = $scope.selectedMeal.skuId;
            	$scope.studentOrders = response.data;
            	for(var i = 0; i<$scope.mealList.length; i++){
            		if($scope.mealList[i].skuId == skuId){
            			meal = $scope.mealList[i];
            			break;
            		}
            	}
            	for(var i = 0; i< $scope.studentOrders.length; i++){		
            		$scope.studentOrders[i].skuName = meal.name;
            		$scope.studentOrders[i].rtPrice = meal.rtPrice;
            		$scope.studentOrders[i].total = $scope.studentOrders[i].rtPrice * $scope.studentOrders[i].qty;
            	}  		
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {
            $timeout(function () {
                $scope.showLoaderAllPage = false;
            }, 500);
            console.log(URL_API_INIT_STUDENT_ORDER + ' [ERROR]');
        });
    	 $timeout(function () {
             $scope.showLoaderAllPage = false;
         }, 1000);
    };
    $scope.saveFunc = function(){
	  	var URL_API_NEW_TRANS = contextPath + CONTSTANT_SERVICE_URI.URI_SALE_TRANS + URI_NEW_TRANS;	
	  	$scope.showLoaderAllPage = true;
	  	var payload = {
	  		payload : $scope.studentOrders
	  	};
	  	$http({
	  		method: 'POST',
	  		url: URL_API_NEW_TRANS,
	  		headers: {
                contentType: "application/json; charset=utf-8",
                dataType: 'JSON'
            },
            data: $scope.studentOrders
	  	}).then(function successCallBack(response){
	  		if(response.data){
                $scope.studentOrders = response.data;
			}
	  		$scope.showLoaderAllPage = false;
	  	}, function errorCallBack(response){
	  		$scope.showLoaderAllPage = false;
	  	})
    };
    $scope.editStudentOrder = function(objectData){
    	$scope.selectedStudent = objectData;
        $scope.selectedStudentClone = angular.copy(objectData);
	};
    $scope.subbmitEdit = function(){
        angular.forEach($scope.selectedStudentClone, function (value, key) {
            $scope.selectedStudent[key] = value;
        });
        $('#editModal').modal('hide');
	};
	$scope.plusQuantity = function () {
		$scope.selectedStudentClone.qty++;
		$('#quantityID').val($scope.selectedStudentClone.qty);
	};
	$scope.minusQuantity = function () {
		if($scope.selectedStudentClone.qty > 0){
			$scope.selectedStudentClone.qty--;
			$('#quantityID').val($scope.selectedStudentClone.qty);
		} else {
			$scope.selectedStudentClone.qty = 0;
			$('#quantityID').val($scope.selectedStudentClone.qty);
		}
	};
	angular.element(document).ready(function () {
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		});
		$(document).on('change', '#quantityID', function () {
			if(!isNaN($(this).val())){
				$scope.selectedStudentClone.qty = Number($(this).val());
				$('#quantityID').val($scope.selectedStudentClone.qty);
			} else{
				$scope.selectedStudentClone.qty = 0;
				$('#quantityID').val($scope.selectedStudentClone.qty);
			}
		});
	});
	$scope.showDeleteStudentOrder = function(objectData){
        $.confirm({
			title: 'Confirm!',
            icon: 'fa fa-info-circle',
            theme: 'material',
			content: 'Are you sure to delete this record?',
			scope: $scope,
            animation: 'scale',
            closeAnimation: 'scale',
            type: 'red',
            scrollToPreviousElement: false,
			buttons: {
				deleteBtn: {
					text: 'Delete',
					btnClass: 'btn-danger',
					action: function(scope, button){
                        var index = scope.studentOrders.indexOf(objectData);
                        scope.studentOrders.splice(index,1);
                        scope.$apply();
					}
				},
				close: function(scope, button){

				}
			}
		});
	}
    $scope.deleteOrder = function(selectedOrder){
    	var index = $scope.studentOrders.indexOf($scope.selectedOrder);
    	$scope.studentOrders.splice(index,1);
    };
    $scope.modifyOrder = function(selectedOrder){
    	
    };
	$scope.listKeyTable = [{
		code : 'cusName',
		name : 'Student Name',
		isSortFilter : true,
		width : '100px',
		iClass : "text-center"
	},{
		code : 'skuName',
		name : 'Item',
		isSortFilter : true,
		width : '150px',
		iClass : "text-center"
	}, {
		code : 'qty',
		name : 'Quantity',
		isSortFilter : true,
		width : '40px',
		iClass : "text-center"
	}, {
		code : 'rtPrice',
		name : 'Price',
		isSortFilter : true,
		width : '70px',
		iClass : "text-center"
	}, {
		code : 'total',
		name : 'Total',
		isSortFilter : true,
		width : '55px',
		isTotal : true,
		iClass : "text-center"
	}];			
}]);
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;

                    if(keySort['code'] === 'transDate' || keySort['code'] === 'transTime' ){
                        // function Sort Date
                    } else {
                        var tempA = a[keySort['code']];
                        var tempB = b[keySort['code']];
                        if(!isNaN(tempA) && !isNaN(tempB)){
                            if(Number(tempA) > Number(tempB)) return 1;
                            else if (Number(tempA) < Number(tempB)) return -1;
                            else if (Number(tempA) === Number(tempB)) return 0;
                        } else{
                            tempA = tempA.toString().toLowerCase();
                            tempB = tempB.toString().toLowerCase();
                            if(tempA > tempB) return 1;
                            else if (tempA < tempB) return -1;
                            else if (tempA === tempB) return 0;
                        }
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    if(keySort['code'] === 'transDate' || keySort['code'] === 'transTime'){
                        // function Sort Date
                    } else {
                        var tempA = a[keySort['code']];
                        var tempB = b[keySort['code']];
                        if(!isNaN(tempA) && !isNaN(tempB)){
                            if(Number(tempA) > Number(tempB)) return -1;
                            else if (Number(tempA) < Number(tempB)) return 1;
                            else if (Number(tempA) === Number(tempB)) return 0;
                        } else{
                            tempA = tempA.toString().toLowerCase();
                            tempB = tempB.toString().toLowerCase();
                            if(tempA > tempB) return -1;
                            else if (tempA < tempB) return 1;
                            else if (tempA === tempB) return 0;
                        }
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});