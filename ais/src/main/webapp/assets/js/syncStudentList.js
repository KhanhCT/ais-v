myApp.controller('syncStudentList', ['$scope', '$http', '$timeout', '$q', 'Upload',
		'$window', function($scope, $http, $timeout, $q, Upload) {
	
	$scope.showLoaderAllPage = false;
	$scope.mesObj = undefined;
	$scope.studentList = []


	function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
	}

	/*TABLE*/
	$scope.itemGap = 10;
	$scope.listGap = [1,5,10,20];
	$scope.setItemGap = function(gap){
		$scope.itemGap = gap;
	};
	$scope.showFilter =function(key){
		key.openFilter = true;
	};
	$scope.clearFilter = function(key){
		key.textFilter = '';
	};
	$scope.changeSort = function(key){
		if(!$scope.sortColumn || $scope.sortColumn !== key) {
			if($scope.sortColumn){
				$scope.sortColumn['sortAZ'] = false;
				$scope.sortColumn['sortZA'] = false;
			}
			key['sortAZ'] = true;
			key['sortZA'] = false;
			$scope.sortColumn = key;
		} else if($scope.sortColumn && $scope.sortColumn === key) {
			if($scope.sortColumn['sortAZ']){
				$scope.sortColumn['sortAZ'] = false;
				$scope.sortColumn['sortZA'] = true;
			} else if(key['sortZA']){
				$scope.sortColumn['sortAZ'] = true;
				$scope.sortColumn['sortZA'] = false;
			} else {
				$scope.sortColumn['sortAZ'] = true;
				$scope.sortColumn['sortZA'] = false;
			}
		}
	};

	$scope.hideFilter = function (key){
		key.openFilter = false;
	};

	$scope.convertObject2String = function(key, data){     
		return data[key];
    };
    
    $scope.initializeData = function()
    {
    	$scope.showLoaderAllPage = true;
    	$scope.mesObj = {
    			type: true,
    			text: '',
    			active: true
    	}

    	$timeout(function () {
            $scope.showLoaderAllPage = false;
        }, 500);
    	showMessage("Synchronizing............");
    };
   
    $scope.syncStudent = function(){
    	showMessage("Synchronizing............");
    	var URI_SYNC_STUDENT = 'updateStudentList';
	  	var URL_API_SYNC_STUDENT = contextPath + CONTSTANT_SERVICE_URI.URI_CUSTOMER + URI_SYNC_STUDENT;
    	var meal = undefined;
	  	$scope.showLoaderAllPage = true;
    	$http({
            method: 'GET',
            url: URL_API_SYNC_STUDENT,
            params:{classId: 1},
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	$scope.studentList = response.data;	
            }
            $scope.showLoaderAllPage = false;
            showMessage("Finish to synchronize student list!")
        }, function errorCallback(response) {
            $timeout(function () {
                $scope.showLoaderAllPage = false;
            }, 500);
            showMessage("Failed to synchronize student list!")
        });
    };
  
	angular.element(document).ready(function () {
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		});
		$(document).on('change', '#quantityID', function () {
			if(!isNaN($(this).val())){
				$scope.selectedStudentClone.qty = Number($(this).val());
				$('#quantityID').val($scope.selectedStudentClone.qty);
			} else{
				$scope.selectedStudentClone.qty = 0;
				$('#quantityID').val($scope.selectedStudentClone.qty);
			}
		});
	});
	
	$scope.listKeyTable = [{
		code : 'cusId',
		name : 'Customer Code',
		isSortFilter : true,
		width : '100px',
		iClass : "text-center"
	},{
		code : 'cusName',
		name : 'Customer Name',
		isSortFilter : true,
		width : '100px',
		iClass : "text-center"
	},{
		code : 'gender',
		name : 'Gender',
		isSortFilter : true,
		width : '50px',
		iClass : "text-center"
	}, {
		code : 'cusType',
		name : 'Type',
		isSortFilter : true,
		width : '40px',
		iClass : "text-center"
	}, {
		code : 'cardCode',
		name : 'Card Number',
		isSortFilter : true,
		width : '70px',
		iClass : "text-center"
	}, {
		code : 'nationality',
		name : 'Nationality',
		isSortFilter : true,
		width : '55px',
		isTotal : true,
		iClass : "text-center"
	}];			
}]);
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;

                    if(keySort['code'] === 'transDate' || keySort['code'] === 'transTime' ){
                        // function Sort Date
                    } else {
                        var tempA = a[keySort['code']];
                        var tempB = b[keySort['code']];
                        if(!isNaN(tempA) && !isNaN(tempB)){
                            if(Number(tempA) > Number(tempB)) return 1;
                            else if (Number(tempA) < Number(tempB)) return -1;
                            else if (Number(tempA) === Number(tempB)) return 0;
                        } else{
                            tempA = tempA.toString().toLowerCase();
                            tempB = tempB.toString().toLowerCase();
                            if(tempA > tempB) return 1;
                            else if (tempA < tempB) return -1;
                            else if (tempA === tempB) return 0;
                        }
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    if(keySort['code'] === 'transDate' || keySort['code'] === 'transTime'){
                        // function Sort Date
                    } else {
                        var tempA = a[keySort['code']];
                        var tempB = b[keySort['code']];
                        if(!isNaN(tempA) && !isNaN(tempB)){
                            if(Number(tempA) > Number(tempB)) return -1;
                            else if (Number(tempA) < Number(tempB)) return 1;
                            else if (Number(tempA) === Number(tempB)) return 0;
                        } else{
                            tempA = tempA.toString().toLowerCase();
                            tempB = tempB.toString().toLowerCase();
                            if(tempA > tempB) return -1;
                            else if (tempA < tempB) return 1;
                            else if (tempA === tempB) return 0;
                        }
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});