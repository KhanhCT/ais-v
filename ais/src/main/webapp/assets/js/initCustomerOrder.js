myApp.controller('customerOrderCtrl', ['$scope', 'NgTableParams', 'ngTableEventsChannel', '$http', '$timeout','$q', '$interval',
    function($scope, NgTableParams, ngTableEventsChannel, $http, $timeout, $q, $interval) {
    $scope.isShowItemList = false;
    $scope.quantityItem = 1;
    $scope.arrData = [];
    $scope.groupItems = [];
    $scope.showLoaderGetItem = false;
    $scope.fromDate = moment().format('DD/MM/YYYY');
    $scope.toDate = moment().format('DD/MM/YYYY');
    $scope.isShowGroupList = true;
    $scope.itemGap = 10;
    $scope.listGap = [1,5,10,20];
    $scope.currencyList = [];
    $scope.cardCode = '';
    $scope.customerObj = undefined;
    $scope.discount =0;
    $scope.total = 0;
    $scope.locationId = undefined;
    $scope.canteenList = [];
    $scope.selectedCanteen = undefined;
    $scope.mesObj = undefined;
    $scope.selectedCurrency = undefined;
    $scope.deletedMeal = undefined;
    $scope.editteddMeal= undefined;
    $scope.clock = {
        time: "",
        interval: 1000
    };
    $scope.listKeyTable = angular.copy(listKeyTable);
    $interval(function () {
            $scope.clock.time = Date.now();
        },
        $scope.clock.interval
    );
    function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
	}
    var URI_SERVICE_HISTORY = 'getHistoryByCusId',
        URI_SERVICE_GROUPS = 'findAllItemGrp',
        URI_SERVICE_ITEM_BY_GROUP = 'findAllItemListByGrpId';
    $scope.initData = function() {
    	
        var FROM_DATE = moment($scope.fromDate, 'DD/MM/YYYY').format('DDMMYYYY'),
            TO_DATE = moment($scope.fromDate, 'DD/MM/YYYY').format('DDMMYYYY');
        $scope.mesObj = {
				type: true,
				text: '',
				active: false
		}
        $scope.showLoaderGetItem = true;
        findAllCanteens();
        $http({
            method: 'GET',
            url: contextPath + CONTSTANT_SERVICE_URI.URI_ITEMS + URI_SERVICE_GROUPS,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {
            if(response.data){
                $scope.groupItems = response.data;
            }
            $scope.showLoaderGetItem = false;
        }, function errorCallback(response) {
            console.log('ERROR CALL : ['+URI_SERVICE_GROUPS+']');
            $scope.showLoaderGetItem = false;
        });
        var URI_GET_CURRENCY_LIST = 'getCurrencyList';
        var URI_API_GET_CURRENCY_LIST = contextPath + CONTSTANT_SERVICE_URI.URI_ITEMS + URI_GET_CURRENCY_LIST;
        $http({
            method: 'GET',
            url: URI_API_GET_CURRENCY_LIST,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {
            if(response.data){
                $scope.currencyList = response.data;
                $scope.selectedCurrency = $scope.currencyList[0];
            }
            $scope.showLoaderGetItem = false;
        }, function errorCallback(response) {
            console.log('ERROR CALL : ['+URI_API_GET_CURRENCY_LIST+']');
            $scope.showLoaderGetItem = false;
        })
    };
    $scope.setItemGap = function(gap){
        $scope.itemGap = gap;
    };
    $scope.showFilter =function(key){
        key.openFilter = true;
    };
    $scope.clearFilter = function(key){
        key.textFilter = '';
    };
    $scope.changeSort = function(key){
        if(!$scope.sortColumn || $scope.sortColumn !== key) {
            if($scope.sortColumn){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = false;
            }
            key['sortAZ'] = true;
            key['sortZA'] = false;
            $scope.sortColumn = key;
        } else if($scope.sortColumn && $scope.sortColumn === key) {
            if($scope.sortColumn['sortAZ']){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = true;
            } else if(key['sortZA']){
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            } else {
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            }
        }
    };
    $scope.hideFilter = function (key){
        key.openFilter = false;
    };
    $scope.getTotalOfBoard = function(key){
        var total= 0;
        if(key['code'] === 'transDate'){
            return 'Total By CardID(CardId)';
        }
        if(key.isTotal === true){
            angular.forEach($scope.arrData, function (item) {
                if(!isNaN(item[key['code']])){
                    total += Number(item[key['code']]);
                }
            })
        } else {
            total = '';
        }
        return total;
    };
    function errorCustomerNone(){
        $.confirm({
            title: 'Encountered an error!',
            icon: 'fa fa-warning',
            content: 'Please insert your card-id and information to do this.',
            type: 'red',
            scrollToPreviousElement: false,
            buttons: {
                close: function(){
                    $('.main-content-template').scrollTop(0);
                    $('#cardID').focus();
                }
            }
        });
    }
    $scope.changeViewToItems = function (group) {
        if(!$scope.customerObj){
            errorCustomerNone();
        } else {
            $scope.showLoaderGetItem = true;
            $scope.groupSelected = group;
            $http({
                method: 'GET',
                url: contextPath + CONTSTANT_SERVICE_URI.URI_ITEMS + URI_SERVICE_ITEM_BY_GROUP,
                params:{itemGrpId:group.itemGrpId},
                headers: {'Content-Type': 'application/json; charset=utf-8'}
            }).then(function successCallback(response) {
                if(response.data){
                    $scope.listItemsSelected = response.data;
                    $scope.isShowItemList = true;
                    $scope.showLoaderGetItem = false;
                }
            }, function errorCallback(response) {
                console.log('ERROR CALL : ['+URI_SERVICE_GROUPS+']');
                $scope.showLoaderGetItem = false;
            })
        }
    };
    $scope.addQty = function(){
    	var item = {
    			'skuId' : $scope.itemSelected.skuId,
    			'skuCode' : $scope.itemSelected.skuCode,
    			'barcode' : $scope.itemSelected.barcode,
    			'skuName' : $scope.itemSelected.name,
    			'qty' : $scope.quantityItem,
    			'prCode': $scope.customerObj.prCode,
    			'rtPrice' : $scope.itemSelected.price,
    			'amount' : $scope.quantityItem * $scope.itemSelected.price,
    			'cusId' : $scope.customerObj.cusId,
    			'locationId' : $scope.locationId,
    	}
    	if ($scope.arrData && $scope.arrData.length > 0) {
    		var isExist = false;
    		for (var i = 0; i < $scope.arrData.length; i++) {
    			if ($scope.arrData[i].skuCode === item.skuCode) {
    				$scope.arrData[i].qty += item.qty;
    				$scope.arrData[i].amount += item.amount;
    				isExist = true;
    				break;
    			}
    		}	
    		if (!isExist) {
    			$scope.arrData.push (item);
    		}
    	} else {
    		$scope.arrData.push (item);
    	}
    	if ($scope.total) {
    		$scope.total += item.amount;
    	} else {
    		$scope.total = item.amount;
    	}
    };
    $scope.findAccount = function(){
    	var URI_FIND_CUSTOMER = 'findCustomerByCardCode';
    	var URL_API_FIND_CUSTOMER = contextPath + CONTSTANT_SERVICE_URI.URI_CUSTOMER + URI_FIND_CUSTOMER;
		$http({
            method: 'GET',
            url: URL_API_FIND_CUSTOMER,
            params:{cardCode: $scope.cardCode},
            headers: {'Content-Type': 'application/json; charset=utf-8'},
        }).then(function successCallback(response) {	
            if(response.data){
            	$scope.customerObj = response.data;
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {		
            console.log(URL_API_FIND_CUSTOMER + ' [ERROR]');
            
            $scope.showLoaderAllPage = false;
        });
    }
    
    function findAllCanteens(){
    	var URI_FIND_CANTEEN = 'findAllCanteens';
    	var URL_API_FIND_CANTEEN = contextPath + CONTSTANT_SERVICE_URI.URI_SALE_TRANS + URI_FIND_CANTEEN;
		$http({
            method: 'GET',
            url: URL_API_FIND_CANTEEN,
            headers: {'Content-Type': 'application/json; charset=utf-8'},
        }).then(function successCallback(response) {	
            if(response.data){
            	$scope.canteenList = response.data;
            	$scope.selectedCanteen = $scope.canteenList[0];
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {		
            console.log(URL_API_FIND_CUSTOMER + ' [ERROR]');
            
            $scope.showLoaderAllPage = false;
        });
    }
    $scope.pay = function(){
    	var URI_PAY = 'pay';
    	var URI_API_PAY = contextPath + CONTSTANT_SERVICE_URI.URI_SALE_TRANS + URI_PAY;
    	$http({
            method: 'POST',
            url: URI_API_PAY,
            data: $scope.arrData,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {
        	
            switch(response.data){
	            case 0:{
	            	showMessage("Not enought balance")
	            }break;
	            case 1:{
	            	showMessage("Order successfully")
	            	$scope.customerObj = undefined;
	            	$scope.arrData = [];
	           	 	$scope.cardCode = ''
	            }break;
	            case 2:{
	            	showMessage("Not enought balance")
	            }break;
            }
        }, function errorCallback(response) {
            console.log('ERROR CALL : ['+URI_SERVICE_GROUPS+']');
            $scope.showLoaderGetItem = false;
        })
        $scope.showLoaderGetItem = false;
        $scope.total = 0;
    };
    $scope.changeSelectedItem = function (item) {
        if(!$scope.customerObj){
            errorCustomerNone()
        } else {
            $scope.itemSelected = item;
            var URI_FIND_ITEM = 'findRtPrice';
            var URL_API_FIND_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_ITEMS + URI_FIND_ITEM;
            $http({
                method: 'GET',
                url: URL_API_FIND_ITEM,
                params: {prCode: $scope.customerObj.prCode, skuId: item.skuId},
                headers: {'Content-Type': 'application/json; charset=utf-8'}
            }).then(function successCallback(response) {
                if (response.data) {
                    $scope.itemSelected.price = response.data.rtPrice;
                }
            }, function errorCallback(response) {
                console.log('ERROR CALL : [' + URL_API_FIND_ITEM + ']');
            })
            $scope.quantityItem = 1;
            $scope.addQty();
        }
    };
    $scope.showAddItem = function () {
        $scope.isShowGroupList = true;
    };
    $scope.plusQuantity = function () {
        $scope.quantityItem++;
        $('#quantityID').val($scope.quantityItem);
    };
    $scope.minusQuantity = function () {
        if($scope.quantityItem > 0){
            $scope.quantityItem--;
            $('#quantityID').val($scope.quantityItem);
        } else {
            $scope.quantityItem = 0;
            $('#quantityID').val($scope.quantityItem);
        }
    };
    $scope.plusQuantity1 = function () {
        $scope.editteddMeal.qty++;
//        $('#modifiedQtyId').val($scope.modifiedQty);
    };
    $scope.minusQuantity1 = function () {
        if($scope.editteddMeal.qty > 0){
        	$scope.editteddMeal.qty--;
//            $('#modifiedQtyId').val($scope.modifiedQty);
        } else {
        	$scope.editteddMeal.qty = 0;
//            $('#modifiedQtyId').val($scope.modifiedQty);
        }
    }
    $scope.editMeal = function(item){
    	$scope.editteddMeal = item;
    }
    $scope.updateQty = function(){
    	var index = $scope.arrData.indexOf($scope.editteddMeal);
    	$scope.arrData[index].qty = $scope.editteddMeal.qty;
    	$scope.arrData[index].amount = $scope.editteddMeal.qty * $scope.arrData[index].rtPrice;
    	$('#editModal').modal('hide');
    }
    
    $scope.discountEvent = function (event) {
        if(event.keyCode == 13) {   // '13' is the key code for enter
        	if ($scope.discount && $scope.total) {
        		$scope.total -=$scope.discount;
        	}
        }
    }
    $scope.showDeleteMealOrder = function(item){
    	$scope.deletedMeal = item;
    }
    $scope.deleteMeal = function(){
    	var index = $scope.arrData.indexOf($scope.deletedMeal);
    	$scope.arrData.splice(index, 1);
    	$scope.deletedMeal = undefined;
    	$('#deleteModal').modal('hide');
    }
   
    $scope.convertCurrency = function () {
    	if ($scope.total2 && $scope.rate) {
    		$http({
                method: 'GET',
                url: contextPath + '/api/v0.1/itemController/convertCurrency?rate=' + $scope.rate + '&amount=' + $scope.total2,
                headers: {'Content-Type': 'application/json; charset=utf-8'}
            }).then(function successCallback(response) {
                if(response.data){
                	$scope.total3 = response.data;
                }
            }, function errorCallback(response) {
                console.log('ERROR CALL : [convertCurrency]');
            })
    	}
    }
    angular.element(document).ready(function () {
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });
       $(document).on('change', '#quantityID', function () {
           if(!isNaN($(this).val())){
               $scope.quantityItem = Number($(this).val());
               $('#quantityID').val($scope.quantityItem);
           } else{
               $scope.quantityItem = 0;
               $('#quantityID').val($scope.quantityItem);
           }
       });
    });
    $scope.changeViewToGroup = function () {
        $scope.listItemsSelected = undefined;
        $scope.itemSelected = undefined;
        $scope.isShowItemList = false;
        $scope.quantityItem = 0;
    };
}]);
myApp.directive('mCusScroll', function() {
    return{
        restrict: 'A',
        link: function(scope, element, attrs) {
            setTimeout(function() {
                element.mCustomScrollbar({
                    axis:"y",
                    theme:"dark",
                    autoExpandScrollbar:true,
                    scrollbarPosition: "outside",
                    autoHideScrollbar: true,
                    advanced:{
                        autoExpandVerticalScroll:true
                    }});
            }, 100);
        }
    };
});
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return 1;
                        else if (Number(tempA) < Number(tempB)) return -1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return 1;
                        else if (tempA < tempB) return -1;
                        else if (tempA === tempB) return 0;
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return -1;
                        else if (Number(tempA) < Number(tempB)) return 1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return -1;
                        else if (tempA < tempB) return 1;
                        else if (tempA === tempB) return 0;
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});