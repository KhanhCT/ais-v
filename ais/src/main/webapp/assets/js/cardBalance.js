myApp.controller('cardBalanceCtrl', ['$scope', '$http', '$timeout', '$q', '$window', function($scope, $http, $timeout,  $q, $window) {
	$scope.reportData = [];
	$scope.showLoaderAllPage = true;
    function randomDate(start, end) {
        return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    }
    
    $scope.initializeData = function(){
    	$scope.showLoaderAllPage = true;
        var URI_GET_REPORT_DATA = 'getCardBalance';
		var URI_API_GET_REPORT_DATA =  contextPath + CONTSTANT_SERVICE_URI.URI_SALE_TRANS + URI_GET_REPORT_DATA;
		$http({
            method: 'GET',
            url: URI_API_GET_REPORT_DATA,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	$scope.reportData = response.data;
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log(URI_API_GET_REPORT_DATA + ' [ERROR]');
        });
		$timeout(function () {
            $scope.showLoaderAllPage = false;
        }, 500);
    }
    
    $scope.exportData = function (typeExport) {
		var typeBlod;
		var fileName;
		if (typeExport === 'xls') {
			typeBlod = 'application/vnd.ms-excel';
			fileName = 'CardBalance.xls';
		} else if (typeExport === 'pdf') {
			typeBlod = 'application/pdf';
			fileName = 'CardBalance.pdf';
		}
		$http({
            method: 'POST',
            url: contextPath + "/ais/report/exportCardBalance?typeExport=" + typeExport,
            responseType: 'arraybuffer',
            data: $scope.reportData
        }).then(function successCallback(response) {
     
            var linkElement = document.createElement('a');
            try {
                var blob = new Blob([response.data], { type: typeBlod });
                var url = window.URL.createObjectURL(blob);
     
                linkElement.setAttribute('href', url);
                linkElement.setAttribute("download", fileName);
     
                var clickEvent = new MouseEvent("click", {
                    "view": window,
                    "bubbles": true,
                    "cancelable": false
                });
                linkElement.dispatchEvent(clickEvent);
            } catch (ex) {
                console.log(ex);
            }
        }, function errorCallback(response) {
        });
	}
	
    
    function makeRandomID() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 10; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
    $scope.listKeyTable = [
        {
            code: 'cusId',
            name: 'Customer Id',
            iClass: "text-center"
        },
        {
            code: 'cusName',
            name: 'Customer Name',
            iClass: "text-left"
        },
        {
            code: 'balance',
            name: 'Balance',
            iClass: "text-right"
        },
        {
            code: 'remark',
            name: 'Remark',
            iClass: "text-left"
        }
    ];

}]);
