myApp.controller('itemMasterCtrl', ['$scope', 'NgTableParams', 'ngTableEventsChannel', '$http', '$q', function($scope, NgTableParams, ngTableEventsChannel, $http, $q) {
    $scope.priceCodeLst = [];
    $scope.itemGrpLst = [];
    $scope.priceUnitLst = [];
    $scope.rtPriceLst = [];
    $scope.itemObj = {};
    $scope.showTable = true;
    $scope.itemObj.openDate = moment().format('DD/MM/YYYY');
    $scope.valueSamePrice = 0;
    $scope.listKeyTable = [{
        code : 'prCode',
        name : 'Price Code',
        isSortFilter : true,
        iClass : "text-center"
    },
//    {
//        code : 'name',
//        name : 'Name',
//        isSortFilter : true,
//        iClass : "text-center"
//    },
    {
        code : 'name',
        name : 'Description',
        isSortFilter : true,
        iClass : "text-center"
    }, {
        code : 'rtPrice',
        name : 'Price',
        iClass : "text-center"
    }];
    /*TABLE*/
    $scope.itemGap = 10;
    $scope.listGap = [1,5,10,20];
    $scope.setItemGap = function(gap){
        $scope.itemGap = gap;
    };
    $scope.showFilter =function(key){
        key.openFilter = true;
    };
    $scope.clearFilter = function(key){
        key.textFilter = '';
    };
    $scope.changeSort = function(key){
        if(!$scope.sortColumn || $scope.sortColumn !== key) {
            if($scope.sortColumn){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = false;
            }
            key['sortAZ'] = true;
            key['sortZA'] = false;
            $scope.sortColumn = key;
        } else if($scope.sortColumn && $scope.sortColumn === key) {
            if($scope.sortColumn['sortAZ']){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = true;
            } else if(key['sortZA']){
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            } else {
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            }
        }
    };

    $scope.hideFilter = function (key){
        key.openFilter = false;
    };
    $scope.initData = function () {
    	$q.all([
			$http({
			    method: 'GET',
			    url: contextPath + '/api/v0.1/itemController/findAllItemGrp',
			    headers: {'Content-Type': 'application/json; charset=utf-8'}
			}).then(function successCallback(response) {
			    if(response.data){
			        $scope.itemGrpLst  = response.data;
			        if($scope.itemGrpLst && $scope.itemGrpLst.length > 0){
			        	$scope.itemObj.itemGrpId = $scope.itemGrpLst[0].itemGrpId;
			        }
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllItemGrp]');
			}),
			$http({
	            method: 'GET',
	            url: contextPath + '/api/v0.1/itemController/getLstPriceUnit',
	            headers: {'Content-Type': 'application/json; charset=utf-8'}
	        }).then(function successCallback(response) {
	            if(response.data){
	                $scope.priceUnitLst  = response.data;
	                if($scope.priceUnitLst && $scope.priceUnitLst.length > 0){
	                	$scope.itemObj.priceUnit = $scope.priceUnitLst[0].codeName;
	                }
	            }
	        }, function errorCallback(response) {
	            console.log('ERROR CALL : [getLstPriceUnit]');
	        }),
	        getPriceCode()
        ]).then (function () {
        	
        })
    };
    
    function getPriceCode (){
    	return $http({
            method: 'GET',
            url: contextPath + '/api/v0.1/itemController/getAllPriceCode',
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {
            if(response.data){
                $scope.priceCodeLst  = response.data;
                if($scope.priceCodeLst && $scope.priceCodeLst.length > 0){
                	$scope.priceCodeSelected = $scope.priceCodeLst[0].prCode;
                	for (var i = 0; i < $scope.priceCodeLst.length; i++){
                		$scope.rtPriceLst.push({
                			'prCode' : $scope.priceCodeLst[i].prCode,
                			'rtPrice' : 0,
                			'name' : getPriceCodeName ($scope.priceCodeLst[i].prCode)
                		})
                	}
                }
            }
        }, function errorCallback(response) {
            console.log('ERROR CALL : [getAllPriceCode]');
        })
    }
    $scope.saveItem = function () {
    	if ($scope.itemObj) {
    		$scope.itemObj.status = true;
    		var listRtPrice = [];
    		if ($scope.isSameAll) {
    			for (var i = 0; i < $scope.rtPriceLst.length; i++) {
        			listRtPrice.push ({
        				'prCode' : $scope.rtPriceLst[i].prCode,
        				'skuId' : $scope.itemObj.skuId,
        				'rtPrice' : $scope.valueSamePrice
        			})
        		}
        	} else {
        		for (var i = 0; i < $scope.rtPriceLst.length; i++) {
        			listRtPrice.push ({
        				'prCode' : $scope.rtPriceLst[i].prCode,
        				'skuId' : $scope.itemObj.skuId,
        				'rtPrice' : $scope.rtPriceLst[i].rtPrice
        			})
        		}
        	}
    		
    		$http({
                method: 'POST',
                url: contextPath + '/api/v0.1/itemController/newItemList',
                responseType: 'json',
                headers: {
                    contentType: "application/json; charset=utf-8",
                    dataType: 'JSON'
                },
                data: {'itemListDTO' : $scope.itemObj, 'listRtPrice' : listRtPrice}
            }).then(function successCallback(response) {
                $scope.initData();
            }, function errorCallback(response) {
            })
    	}
    }
    
    $scope.updateItem = function () {
    	if ($scope.itemObj) {
    		$scope.itemObj.status = true;
    		var listRtPrice = [];
    		if ($scope.isSameAll) {
    			for (var i = 0; i < $scope.rtPriceLst.length; i++) {
        			listRtPrice.push ({
        				'prCode' : $scope.rtPriceLst[i].prCode,
        				'skuId' : $scope.itemObj.skuId,
        				'rtPrice' : $scope.valueSamePrice
        			})
        		}
        	} else {
        		for (var i = 0; i < $scope.rtPriceLst.length; i++) {
        			listRtPrice.push ({
        				'prCode' : $scope.rtPriceLst[i].prCode,
        				'skuId' : $scope.itemObj.skuId,
        				'rtPrice' : $scope.rtPriceLst[i].rtPrice
        			})
        		}
        	}
    		
    		$http({
                method: 'PUT',
                url: contextPath + '/api/v0.1/itemController/updateItemList',
                responseType: 'json',
                headers: {
                    contentType: "application/json; charset=utf-8",
                    dataType: 'JSON'
                },
                data: {'itemListDTO' : $scope.itemObj, 'listRtPrice' : listRtPrice}
            }).then(function successCallback(response) {
                $scope.initData();
            }, function errorCallback(response) {
            })
    	}
    }
    
    $scope.findItem = function () {
    	var skuCode = $scope.itemObj.skuCode;
    	if (skuCode) {
			$http({
			    method: 'GET',
			    url: contextPath + '/api/v0.1/itemController/findItemtLstBySkuCode?skuCode=' + skuCode,
			    responseType: 'json'
			}).then(function successCallback(response) {
				var data = response.data;
				if (data) {
					$scope.itemObj = data.itemListDTO;
					if (data.listRtPrice) {
						$scope.rtPriceLst = data.listRtPrice;
					}
					if($scope.rtPriceLst && $scope.rtPriceLst.length > 0){
						for (var i = 0; i < $scope.rtPriceLst.length; i++) {
							$scope.rtPriceLst[i]['name'] = getPriceCodeName ($scope.rtPriceLst[i].prCode);
						}
			        }
				}
			}, function errorCallback(response) {
			})
    	}
    }
    
    $scope.checkedFunction = function () {
    	if ($scope.isSameAll) {
    		$scope.showTable = false;
    	} else {
    		$scope.showTable = true;
    	}
    }
    
    function getPriceCodeName(prCode) {
    	var prName = '';
        for(var i = 0 ; i< $scope.priceCodeLst.length; i++){
            if($scope.priceCodeLst[i].prCode == prCode){
            	prName =  $scope.priceCodeLst[i].name;
            }
        }
        return prName;
    }
    
}]);
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return 1;
                        else if (Number(tempA) < Number(tempB)) return -1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return 1;
                        else if (tempA < tempB) return -1;
                        else if (tempA === tempB) return 0;
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return -1;
                        else if (Number(tempA) < Number(tempB)) return 1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return -1;
                        else if (tempA < tempB) return 1;
                        else if (tempA === tempB) return 0;
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});