myApp.controller('registerCustomerCtrl', ['$scope', '$http', '$q' ,'$timeout',
    function($scope, $http, $q, $timeout) {
	$scope.gradeList = [];
	$scope.classList = [];
	$scope.prCodeList = [];
	$scope.classFilter = [];
	$scope.cusTypeList = [];
	$scope.selectedGrade = undefined;
	$scope.selectedClass = undefined;
	$scope.selectedPrCode = undefined;
	$scope.selectedCusType = undefined;
	$scope.selectedCusType = undefined;
	
    var URI_REGISTER_CUSTOMER = 'newCustomer'
    , URI_GET_PAGE_DATA = 'getCommonDTO';
    
    $scope.customerObject = undefined;
    $scope.showLoaderAllPage = true;
    $scope.initializeData = function (){
        $scope.showLoaderAllPage = true;
        var URL_API_PAGE_DATA = contextPath + CONTSTANT_SERVICE_URI.URI_CUSTOMER + URI_GET_PAGE_DATA;
	  	$http({
            method: 'GET',
            url: URL_API_PAGE_DATA,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	var pageData = response.data;
            	$scope.gradeList = angular.copy(pageData.gradeList);
            	$scope.classList = angular.copy(pageData.classList);
            	$scope.cusTypeList = angular.copy(pageData.cusTypeList);
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log(URL_API_PAGE_DATA + ' [ERROR]');
        });
	  	var URL_GET_PRCODE = 'getAllPriceCode';
	  	var URL_API_PRCODE =  contextPath + CONTSTANT_SERVICE_URI.URI_ITEMS + URL_GET_PRCODE;
	  	$http({
            method: 'GET',
            url: URL_API_PRCODE,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	var pageData = response.data;
            	$scope.prCodeList = response.data;
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log(URL_API_PRCODE + ' [ERROR]');
        });
	  	
        $scope.customerObject = {
            cusId: '',
            firstName: '',
            lastName: '',
            gender: 0,
            cusTypeId: '',
            classId: '',
            prCode:'',
            cardCode: '',
            Balance:0,
            nationally: '',
            address: '',
            phoneNum:'',
            email: ''
        };
        $timeout(function () {
            $scope.showLoaderAllPage = false;
        }, 1000);
        
        
    };
    $scope.$watch('selectedGrade', function(){
    	
    	for(var i = 0; i< $scope.classList.length; i++){
    		if($scope.classList[i].gradeId == $scope.selectedGrade.gradeId){
    			$scope.classFilter.push($scope.classList[i]);
    		}		
    	}
    });

    $scope.registerCustomer = function () {
        $scope.showLoaderCustomer = true;
        var URL_NEW_CUSTOMER = contextPath + CONTSTANT_SERVICE_URI.URI_CUSTOMER + URI_REGISTER_CUSTOMER;
        $scope.customerObject.classId = $scope.selectedClass.classId;
        $scope.customerObject.prCode = $scope.selectedPrCode.prCode;
        $scope.customerObject.cusTypeId = $scope.selectedCusType.codeVal;
        $http({
            method: 'POST',
            url: URL_NEW_CUSTOMER,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        	,data: $scope.customerObject
        }).then(function successCallback(response) {
            if(response.data){
            	$scope.selectedGrade = undefined;
            	$scope.classFilter = [];
            	$scope.selectedPrCode = undefined;
            	$scope.customerObject = {
            			cusId: '',
                        firstName: '',
                        lastName: '',
                        gender: 0,
                        cusTypeId: '',
                        classId: '',
                        prCode:'',
                        prCode:'',
                        cardCode: '',
                        Balance:0,
                        nationally: '',
                        address: '',
                        phoneNum:'',
                        email: ''
                };
            }
            alert('SUCCESS');
            $scope.showLoaderCustomer = false;
        }, function errorCallback(response) {
            alert('Register Customer Error');
            $scope.showLoaderCustomer = false;
        });
    }
}]);