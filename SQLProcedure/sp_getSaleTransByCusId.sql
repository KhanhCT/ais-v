USE [AIS]
GO
/****** Object:  StoredProcedure [dbo].[sp_getSaleTransByCusId]    Script Date: 06/27/2018 9:54:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
** SP Name      : sp_getSaleTransHistory
** Group        :
** Purpose      : Get SaleTransHistory from YM to YM
** By		    : Chu Trong Khanh
** Created      : 26/03/2018
** Modified     :
** Notes	    :
*/

ALTER PROCEDURE [dbo].[sp_getSaleTransByCusId]
	 @frDate       date
	,@toDate	   date
	,@customerId   int
	,@CondExp  varchar(max) = NULL
    --WITH ENCRYPTION
AS

BEGIN
	SET NOCOUNT ON

	-- Tao bang tong hop du lieu giao dich kho
	IF object_id('#SaleTransTmp') IS NOT NULL DROP TABLE #SaleTransTmp
	SELECT * INTO #SaleTransTmp FROM SaleTrans WHERE 0 = 1

	DECLARE
		 @m_MEClose	char(6)
		,@YM                  char(6)
		,@SaleTransTblName    char(60)
		,@_Date              date
		,@_Cond             varchar(max)
		,@_strExec         nvarchar(max)
		,@tmp date

	SET @_Date = @frDate
	SET @toDate = DATEADD(DAY, 1,@toDate) 
	SET @tmp = (SELECT convert(date,Value,103) FROM Sysvar WHERE VarName ='m_MEClose')
	SET @m_MEClose = CONVERT(char(7),@tmp,126)
	--PRINT @m_MEClose
	IF @CondExp IS NULL
		BEGIN
			SET @_Cond = ' TransDate >= ' + CHAR(39) + CONVERT(CHAR(10), @FrDate,126) + CHAR(39) +
						 ' AND TransDate < ' + CHAR(39) +  CONVERT(CHAR(10), @ToDate,126) + CHAR(39)  + ' AND cusId=' + CAST(@customerId as VARCHAR(50))
		END
	ELSE
		BEGIN
			SET @_Cond = '(TransDate >= ' + CHAR(39) + CONVERT(CHAR(10), @FrDate,126) + CHAR(39) +
						 ' AND TransDate < ' + CHAR(39) + CONVERT(CHAR(10), @ToDate,126) + CHAR(39) + ')' + ' AND ' + RTrim(@CondExp) + + ' AND cusId=' +   CAST(@customerId as VARCHAR(50))
		END

	WHILE (CONVERT(char(7),@_Date,126) <=  @m_MEClose)
		BEGIN
			SET @YM         = CONVERT(char(7),@_Date,126)

			SET @SaleTransTblName = 'MounthlySaleTrans_' + @YM

			SET @_Date      = DATEADD(month,1,@_Date)

			IF object_id(@SaleTransTblName) IS NOT NULL
				BEGIN
					SET @_strExec =
							'INSERT INTO #SaleTransTmp' + CHAR(13) +
							'	SELECT * FROM ' + RTRim(@SaleTransTblName) + CHAR(13) +
							'   WHERE ' + RTRIM(@_Cond)
					EXECUTE(@_strExec)
				END
			CONTINUE
		END

	IF CONVERT(char(7),@toDate,126) > @m_MEClose
	BEGIN
		SET @_strExec =
				'INSERT INTO #SaleTransTmp' + CHAR(13) +
				'	SELECT * FROM  SaleTrans' + CHAR(13) +
				'   WHERE ' + RTRIM(@_Cond)
		EXECUTE(@_strExec)

		IF object_id('MounthlySaleTrans') IS NOT NULL
				BEGIN
					SET @_strExec =
							'INSERT INTO #SaleTransTmp' + CHAR(13) +
							'	SELECT * FROM MounthlySaleTrans ' + CHAR(13) +
							'   WHERE ' + RTRIM(@_Cond)
					EXECUTE(@_strExec)
				END

	END

	SELECT * FROM #SaleTransTmp
	DROP TABLE #SaleTransTmp

END

