USE [AIS]
GO
/****** Object:  StoredProcedure [dbo].[sp_getCusTransHistory]    Script Date: 06/23/2018 12:43:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
** SP Name      : sp_getCusTransHistory
** Group        :
** Purpose      : Get Catered from YM to YM
** By		    : Chu Trong Khanh
** Created      : 26/03/2018
** Modified     :
** Notes	    :
*/

ALTER PROCEDURE [dbo].[sp_getCusTransHistory]
	 @frDate       date
	,@toDate	   date
	,@customerId   int
	,@CondExp  varchar(max) = NULL
    --WITH ENCRYPTION
AS

BEGIN
	SET NOCOUNT ON

	-- Tao bang tong hop du lieu giao dich kho
	IF object_id('#CusTransTmp') IS NOT NULL DROP TABLE #CusTransTmp
	SELECT * INTO #CusTransTmp FROM CusTrans WHERE 0 = 1

	DECLARE
		 @m_MEClose	char(8)
		,@YM                  char(6)
		,@CusTransTblName    char(60)
		,@_Date              date
		,@_Cond             varchar(max)
		,@_strExec         nvarchar(max)
		,@tmp date
	SET @toDate = DATEADD(DAY, 1,@toDate)
	SET @_Date = @frDate

	SET @tmp = (SELECT convert(date,Value,103) FROM Sysvar WHERE VarName ='m_MEClose')
	SET @m_MEClose = CONVERT(char(7),@tmp,126)
	--PRINT @m_MEClose
	IF @CondExp  IS NULL
		BEGIN
			SET @_Cond = ' TransDate >= ' + CHAR(39) + CONVERT(CHAR(10), @FrDate,126) + CHAR(39) +
						 ' AND TransDate < ' + CHAR(39) +  CONVERT(CHAR(10), @ToDate,126) + CHAR(39)  + ' AND cusId=' + CAST(@customerId as VARCHAR(50))
		END
	ELSE
		BEGIN
			SET @_Cond = '(TransDate >= ' + CHAR(39) + CONVERT(CHAR(10), @FrDate,126) + CHAR(39) +
						 ' AND TransDate < ' + CHAR(39) + CONVERT(CHAR(10), @ToDate,126) + CHAR(39) + ')' + ' AND ' + RTrim(@CondExp) +  ' AND cusId=' +   CAST(@customerId as VARCHAR(50))
		END

	WHILE (CONVERT(char(7),@_Date,126) <=  @m_MEClose)
		BEGIN
			SET @YM         = CONVERT(char(6),@_Date,112)

			SET @CusTransTblName = 'MounthlyCusTrans_' + @YM
			
			SET @_Date      = DATEADD(month,1,@_Date)

			IF object_id(@CusTransTblName) IS NOT NULL
				BEGIN
					SET @_strExec =
							'INSERT INTO #CusTransTmp' + CHAR(13) +
							'	SELECT * FROM ' + RTRim(@CusTransTblName) + CHAR(13) +
							'   WHERE ' + RTRIM(@_Cond)
					EXECUTE(@_strExec)
				END
			CONTINUE
		END

	IF CONVERT(char(7),@toDate,126) > @m_MEClose
	BEGIN
		SET @_strExec =
				'INSERT INTO #CusTransTmp' + CHAR(13) +
				'	SELECT * FROM  CusTrans' + CHAR(13) +
				'   WHERE ' + RTRIM(@_Cond)
		PRINT @_strExec
		EXECUTE(@_strExec)

		IF object_id('MounthlyCusTrans') IS NOT NULL
				BEGIN
					SET @_strExec =
							'INSERT INTO #CusTransTmp' + CHAR(13) +
							'	SELECT * FROM MounthlyCusTrans ' + CHAR(13) +
							'   WHERE ' + RTRIM(@_Cond)
					EXECUTE(@_strExec)
				END

	END

	SELECT * FROM #CusTransTmp
	DROP TABLE #CusTransTmp

END


