USE [ais]
GO
/****** Object:  StoredProcedure [dbo].[MonthlyTask]    Script Date: 3/31/2018 8:05:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[MonthlyTask]
	WITH ENCRYPTION
AS
BEGIN
    SET NOCOUNT ON;
	execute [dbo].[spS_BackUp] 'ais' , 'D:\', 'monthly'
	execute [dbo].[spS_GenerateMonthlyTable] 'CusTrans', 'MonthlyCusTrans'
    -- The interval between cleanup attempts
    DECLARE @tmp date
	DECLARE @_curdate date
	DECLARE	@_destDate date
	SELECT @_curDate = (SELECT convert(date,VarValue,112) FROM Sysvar WHERE VarName ='m_CurDate')
	SET @_destDate = DATEADD(MONTH, 1, convert(date,@_curDate,112))
	WHILE 1 = 1
	BEGIN 
		SELECT @_curDate = (SELECT convert(date,VarValue,112) FROM Sysvar WHERE VarName ='m_CurDate')
		SET @tmp = CONVERT(date,@_curDate,112)
		IF CONVERT(CHAR(6),@tmp,112) = CONVERT(CHAR(6),@_destDate,112)
		BEGIN
			SET @_destDate = DATEADD(MONTH, 1, convert(date,@_curDate,112))
			execute [dbo].[spS_BackUp] 'ais' , 'D:\', 'monthly'
			execute [dbo].[spS_GenerateMonthlyTable] 'CusTrans', 'MonthlyCusTrans'
		END
	END
END

