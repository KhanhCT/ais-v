USE [nsrp]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetLstAndCount]    Script Date: 3/31/2018 7:59:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
** SP Name      : sp_GetLstAndCount
** Group        :
** Purpose      : Get Catered from YM to YM
** By		    : Chu Trong Khanh
** Created      : 26/03/2018
** Modified     :
** Notes	    :
*/

ALTER PROCEDURE [dbo].[sp_GetLstAndCount]
	 @frDate       date
	,@toDate	      date
	,@CondExp  varchar(max) = ''
	WITH ENCRYPTION
AS

BEGIN
	SET NOCOUNT ON

	-- Tao bang tong hop du lieu giao dich kho
	IF object_id('Tmp') IS NOT NULL DELETE FROM dbo.Tmp
	--SELECT * INTO #Cater FROM CATERING WHERE 0 = 1

	DECLARE
		 @m_MEClose	char(6)
		,@YM                  char(6)
		,@CaterTblName    char(60)
		,@_Date              date
		,@_Cond             varchar(max)
		,@_strExec         nvarchar(max)
		,@tmp date

	SET @_Date = @frDate

	SET @tmp = (SELECT convert(date,VarValue,103) FROM Sysvar WHERE VarName ='m_MEClose')
	SET @m_MEClose = CONVERT(char(6),@tmp,112)
	--PRINT @m_MEClose
	--PRINT CONVERT(char(6),@toDate,112)
	--PRINT CONVERT(char(6),@_Date,112)
	IF @CondExp = ''
		BEGIN
			SET @_Cond = ' CateringDate >=' + CHAR(39) + CONVERT(CHAR(8), @FrDate,112) + CHAR(39) +
						 ' AND CateringDate < ' + CHAR(39) +  CONVERT(CHAR(8), @ToDate,112) + CHAR(39)
		END
	ELSE
		BEGIN
			SET @_Cond = ' CateringDate >=' + CHAR(39) + CONVERT(CHAR(8), @FrDate,112) + CHAR(39) +
						 ' AND CateringDate < ' + CHAR(39) +  CONVERT(CHAR(8), @ToDate,112) + CHAR(39) + ' AND ' + RTrim(@CondExp)
		END
	
	WHILE (CONVERT(char(6),@_Date,112) <=  @m_MEClose)
		BEGIN
			SET @YM         = CONVERT(char(6),@_Date,112)
			
			SET @CaterTblName = 'Catered_' + @YM
			--PRINT  @CaterTblName
			
			SET @_Date      = DATEADD(month,1,@_Date)

			IF object_id(@CaterTblName) IS NOT NULL
				BEGIN
					SET @_strExec =
							'INSERT INTO dbo.Tmp' + CHAR(13) +
							'	SELECT CateringDate, DeptId, MealId, MealTimeId, LocationId, COUNT(*) FROM ' + RTRim(@CaterTblName) + CHAR(13) +
							'   WHERE ' + RTRIM(@_Cond)
					EXECUTE(@_strExec)
				END
			CONTINUE
		END
	
	IF CONVERT(char(6),@toDate,112) > @m_MEClose
	BEGIN
		SET @_strExec =
				'INSERT INTO dbo.Tmp' + CHAR(13) +
				'	SELECT CateringDate, DeptId, MealId, MealTimeId, LocationId, COUNT(*) FROM  Catering' + CHAR(13) +
				'   WHERE ' + RTRIM(@_Cond)
		--PRINT @_strExec
		EXECUTE(@_strExec)
		

		IF object_id('Catered') IS NOT NULL
				BEGIN
					SET @_strExec =
							'INSERT INTO dbo.Tmp' + CHAR(13) +
							'	SELECT CateringDate, DeptId, MealId, MealTimeId, LocationId, COUNT(*) FROM Catered ' + CHAR(13) +
							'   WHERE ' + RTRIM(@_Cond)
					EXECUTE(@_strExec)
					--PRINT @_strExec
				END

	END

	SELECT * FROM dbo.Tmp
	DELETE FROM dbo.Tmp

END


