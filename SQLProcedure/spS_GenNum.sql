
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].spS_GenNum') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spS_GenNum]
GO


CREATE PROCEDURE [dbo].[spS_GenNum]
	 @depCode char(2)
	,@tranDate date
    ,@trCode char(2)
    ,@stationCode char(2)
	,@transNum  char(18) OUTPUT
--WITH ENCRYPTION     
AS
/*
** Khai bao cac bien xu ly loi
*/
Declare 
@RetCode int
,@transType char(12)
,@lastNum  int
,@yyMMdd char(6) 
,@m_POSMode int

select @lastNum = 1
Select @RetCode = 0	

select @m_CurDate = (select convert(datetime, value,103)  from sysvar where VarName ='m_CurDate')
select @m_OrgMode = (select convert(int,value)  from sysvar where VarName ='m_OrgMode')
 
Begin Tran
Save Tran Sv_spS_GenNum
	
	IF @TranDate < @M_CurDate 
		Begin
		    PRINT 'Transaction : ' + CONVERT(char(8),@m_CurDate,112)
	    END

	SET @yyMMdd   = Substring(CONVERT(char(),@TranDate,112),3,4)
			
	IF @m_OrgMode = 1
		BEGIN 
				SET @transType =  @trCode + @stationCode + @depCode + @yyMMdd
        END
	ELSE /*-- @m_OrgMode = 2 */
		BEGIN
			Set @WS_ALIAS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
			--SET @transType = @m_BuID + @DepCode + SubString(@WS_ALIAS, @WsID, 1) + @YearMonth
		END
		
	
	If not exists (SELECT * FROM TRANNUM WITH(LOCK) WHERE TRANS_TYPE = @transType)
		Begin
		    INSERT INTO TRANNUM(TRANS_TYPE,LAST_NUM) VALUES(@transType,1)
            
			Select @Retcode = @@error
            If @RetCode<>0 Rollback Tran Sv_spS_GenNum

		    select @transType = @transType +  '000001'
		end
	Else
		Begin
		    select @lastNum = (select last_num from TRANNUM WITH(LOCK) WHERE TRANS_TYPE = @TransType)
			select @transNum = @transType +  + RIGHT('000000' + LTRIM(STR(@lastNum + 1)), 6)
			update TRANNUM  SET LAST_NUM = LAST_NUM + 1 WHERE TRANS_TYPE = @transType
		End
	
/* 
** procedure to return
*/
PROC_Return:
    If @@trancount <> 0 Commit Tran
    Return @RetCode

