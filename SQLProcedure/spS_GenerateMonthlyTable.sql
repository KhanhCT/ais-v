USE [ais]
GO
/****** Object:  StoredProcedure [dbo].[spS_GenerateMonthlyTable]    Script Date: 3/31/2018 8:09:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spS_GenerateMonthlyTable]
	 @srcTableName	SYSNAME			-- Input parameter which will be taking in the existing table name
	,@destTableName		SYSNAME
    WITH ENCRYPTION
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @_curDate varchar(6)
	DECLARE @_tableName varchar(50)
	DECLARE @_strExec    nvarchar(MAX)
	DECLARE @tmp date
	SELECT @tmp = (SELECT convert(date,VarValue,103) FROM Sysvar WHERE VarName ='m_CurDate')
	SET @_curDate = CONVERT(char(6), dateadd(MONTH, -1,@tmp),112)
	SET @_tableName = @destTableName + '_' + @_curDate
	
	IF OBJECT_ID(@_tableName) IS NOT NULL
	SET @_strExec = 'DROP TABLE ' + @_tableName
	EXECUTE(@_strExec)
	-- execute [dbo].[spS_CreateTable] @existingTableName, @newTableName 

	SET @_strExec = '[dbo].[spS_CreateTable] ' + @srcTableName + ' , ' + CHAR(39) + @_tableName + CHAR(39)  + CHAR(13)
	EXECUTE(@_strExec)
	-- SET @_strExec = 'ALTER TABLE Catered_201803 ' + ' ADD CONSTRAINT PK_' +  @newTableName + '_' + @_curDate  + ' PRIMARY KEY  (StaffId,MealTimeId,CateringDate)'
	-- EXECUTE(@_strExec)
	SET @_strExec = 'INSERT INTO ' + RTRIM(@_tableName)  + CHAR(13) + 
								'	SELECT * FROM '  + CHAR(13) + @destTableName 
	EXECUTE(@_strExec)
	
	SET @_strExec = 'DELETE FROM ' + @destTableName 
	EXECUTE(@_strExec)

	UPDATE dbo.Sysvar SET VarValue = CONVERT(datetime,DATEADD(DAY,-1, @tmp) , 101) WHERE VarName = 'm_MEClose'
				
END
