USE [ais]
GO
/****** Object:  StoredProcedure [dbo].[spS_GenerateDailyTable]    Script Date: 3/31/2018 8:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spS_GenerateDailyTable]
	 @srcTableName	SYSNAME			-- Input parameter which will be taking in the existing table name
	,@destTableName		SYSNAME
	WITH ENCRYPTION 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @_curdate date
	DECLARE	@_nextDate datetime

    SELECT @_curDate = (SELECT convert(date,VarValue,103) FROM Sysvar WHERE VarName ='m_CurDate')
	--PRINT @_curDate
	INSERT INTO @destTableName SELECT * FROM @srcTableName WHERE TransDate = @_curdate
	DELETE @srcTableName  WHERE TransDate = @_curdate
	SELECT @_nextDate = (SELECT convert(date,VarValue,103) FROM Sysvar WHERE VarName ='m_CurDate') 
	SET @_nextDate = @_nextDate + 1
	UPDATE dbo.Sysvar SET VarValue = CONVERT(datetime,@_nextDate , 101) WHERE VarName = 'm_CurDate'
END
