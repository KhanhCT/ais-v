USE [ais]
GO
/****** Object:  StoredProcedure [dbo].[spS_BackUp]    Script Date: 3/31/2018 8:07:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE  [dbo].[spS_BackUp]
   @db_name 			varchar(20)
   ,@bak_path  			varchar(100)
   ,@ext_name			varchar(100)=''				/* = 1: phan mo rong theo thu trong tuan, = 2: phan mo rong theo thu va gio */ 
--   ,@option  			int 		= 0	
--						= 0 : TO DISK
--						= 1 : TO PIPE
--						= 2 : TO TAPE
	WITH ENCRYPTION  
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare 
	@RetCode 	int
	,@bu_filename 	varchar(100)
	,@name 		varchar(50)


	if  @ext_name = '' 
	select @bu_filename = @bak_path + @db_name + CONVERT(CHAR(8),CONVERT(date, GETDATE(), 112), 112) + '.bak'
	else 
	select @bu_filename = @bak_path + @db_name + '_'+ @ext_name + '_' + CONVERT(CHAR(8),CONVERT(date, GETDATE(), 112), 112) + '.bak'

	select @name = 'Full Backup of ' + @db_name

	BACKUP DATABASE @db_name
    TO DISK = @bu_filename
    WITH FORMAT,
    NAME = @name

END
