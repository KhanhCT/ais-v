USE [ais]
GO
/****** Object:  StoredProcedure [dbo].[DailyTask]    Script Date: 3/31/2018 8:03:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[DailyTask]
	WITH ENCRYPTION
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
	execute [dbo].[spS_BackUp] 'ais' , 'D:\', 'daily'
	execute [dbo].[spS_GenerateDailyTable] 'CusTrans', 'MonthlyCusTrans'

    -- The interval between cleanup attempts
    DECLARE @timeToRun nvarchar(50)
	DECLARE @_strExec  nvarchar(MAX)

    set @timeToRun = '00:00:01'
    WHILE 1 = 1
    BEGIN
        WAITFOR TIME @timeToRun;
        begin	
			execute [dbo].[spS_BackUp] 'nsrp' , 'D:\', 'daily'
			execute  [dbo].[spS_GenerateDailyTable] 'CusTrans', 'MonthlyCusTrans'
        end
    END
END

