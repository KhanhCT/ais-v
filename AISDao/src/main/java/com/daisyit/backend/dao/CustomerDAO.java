package com.daisyit.backend.dao;

import java.util.List;

import com.daisyit.backend.model.Customer;

public interface CustomerDAO {
	
	Customer getCustomerById(String customerId, Boolean status) ;
	List<Customer> findAllStudentByClassId(int classId);
	void insert(Customer customer);
	void saveOrUpdate(Customer customer);
	int updateCustomerBalance(String cusId, double balance);
	Object[] getCustomerInfo(String cusId);
	List<Object[]> getCardBalance();
	Customer findCustomerByCardCode(String cardCode);
	Customer findCustomerByCardId(int cardId);
	Customer findCustomerByCusIdAndCardCode(String cusId, String cardCode);
	List<Object[]> getCusInfoByClassId(Integer classId, boolean active);
	List<Object[]> getCusInfoByClassId(Integer classId);
	Boolean updateCardCustomer (String cusId, int cardId);
	Boolean disableStudent (String cusId, boolean active);
}
