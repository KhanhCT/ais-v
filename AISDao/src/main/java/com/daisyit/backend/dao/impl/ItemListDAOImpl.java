package com.daisyit.backend.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.internal.SessionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.daisyit.backend.dao.ItemListDAO;
import com.daisyit.backend.model.ItemList;

@Repository("itemDAO")
@Transactional
public class ItemListDAOImpl implements ItemListDAO {
	@Autowired
	protected SessionFactory sessionFactory;	
	@Override
	public int save(ItemList item) {
		Session session = sessionFactory.getCurrentSession();
		String sqlQuery = "INSERT ItemList(skuCode, name, itemGrpId, itemType, priceUnit, unitDesc, openDate, rtPrice, status) VALUES( :skuCode, :name,  :itemGrpId, :itemType, "
				+ ":priceUnit, :unitDesc,:openDate, :rtPrice, :status)";
		Query query = session.createSQLQuery(sqlQuery);
		query.setString("skuCode", item.getId().getSkuCode());
		query.setString("name", item.getName());
		query.setInteger("itemGrpId", item.getItemGrpId());
		query.setString("itemType", item.getItemType());
		query.setString("priceUnit", item.getPriceUnit());
		query.setString("unitDesc", item.getUnitDesc());
		query.setDate("openDate", item.getOpenDate());
		query.setBigDecimal("rtPrice", item.getRtPrice());
		query.setBoolean("status", true);
		return query.executeUpdate();
//		String tableName = item.getClass().getSimpleName();
//	    boolean identityInsertSetToOn = false;
//	    SessionImpl sessionImpl = (SessionImpl) sessionFactory.openSession();
//	    try {
//	    	sessionImpl.connection().createStatement().execute("SET IDENTITY_INSERT "+tableName+" ON");
//	        identityInsertSetToOn = true;
//	        sessionImpl.beginTransaction();
//	        sessionImpl.saveOrUpdate(item);
//	        sessionImpl.getTransaction().commit();
//	    } catch (SQLException e) {
//	    	sessionImpl.getTransaction().rollback();
//	        return false;
//	    } finally {
//	        if (identityInsertSetToOn) {
//	            try {
//	            	sessionImpl.connection().createStatement().execute("SET IDENTITY_INSERT "+tableName+" OFF");
//	            } catch (SQLException e) {
//	                throw new RuntimeException(e);
//	            }
//	        }
//	    }
//	    return true;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ItemList> findAll() {
		List<ItemList> list = new ArrayList<>();
		Query query = this.sessionFactory.getCurrentSession().createQuery("FROM ItemList");
		list = query.list();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, String> findItemMap() {
		Map<Integer, String> itemMap = new HashMap<>();
		List<ItemList> list = new ArrayList<>();
		Query query = this.sessionFactory.getCurrentSession().createQuery("FROM ItemList");
		list = query.list();
		for(ItemList item : list)
		{
			itemMap.put(item.getId().getSkuId(),item.getName());
		}
		return itemMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ItemList> getAllItemsByGrpId(int itemGrpId) {
		List<ItemList> list = new ArrayList<>();
		String sqlQuery = "FROM ItemList WHERE itemGrpId = :itemGrpId";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(sqlQuery);
		query.setInteger("itemGrpId", itemGrpId);
		list = query.list();
		return list;
	}

	@Override
	public ItemList findItemFromCode(String skuCode) {
		String sqlQuery = "FROM ItemList c WHERE c.id.skuCode = :skuCode";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(sqlQuery);
		query.setString("skuCode", skuCode);
		return (ItemList) query.uniqueResult();
	}

	@Override
	public ItemList findItemFromId(int skuId) {
		String sqlQuery = "SELECT * FROM ItemList WHERE id.skuId = :skuId";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(sqlQuery);
		query.setInteger("skuId", skuId);
		return (ItemList) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, ItemList> findAllItemMap() {
		Map<String, ItemList> itemMap = new HashMap<>();
		List<ItemList> list = new ArrayList<>();
		Query query = this.sessionFactory.getCurrentSession().createQuery("FROM ItemList");
		list = query.list();
		for(ItemList item : list)
		{
			itemMap.put(item.getId().getSkuCode(),item);
		}
		return itemMap;
	}

	
	
}
