package com.daisyit.backend.dao;

import java.util.List;

import com.daisyit.backend.model.Account;


public interface UserDAO {
	 Account getUserByName(String userName);
	 Account getUser(String username, String password);
	 boolean deleteUserById(int userId);
	 int addUser(Account user);
	 Account getUserById(int id);
	 List<Account> getAllUser();
}
