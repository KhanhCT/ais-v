package com.daisyit.backend.dao.impl;

import java.math.BigDecimal;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.CardInfoDAO;
import com.daisyit.backend.model.CardInfo;

@Repository("cardInfoDAO")
@Transactional
public class CardInfoDAOImpl implements CardInfoDAO{
	@Autowired
	protected SessionFactory sessionFactory;
	@Override
	public CardInfo getCardInfoById(int cardId) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM CardInfo c WHERE c.id.cardId = :cardId";
		Query query = session.createQuery(sqlQuery);
		query.setInteger("cardId", cardId);
		return (CardInfo) query.uniqueResult();
	}
	@Override
	public CardInfo getCardInfoByCode(String cardCode) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM CardInfo c WHERE c.id.cardCode = :cardCode";
		Query query = session.createQuery(sqlQuery);
		query.setString("cardCode", cardCode);
		return (CardInfo) query.uniqueResult();
	}
	@Override
	public int insert(CardInfo cardInfo) {
		Session session = this.sessionFactory.getCurrentSession();	
		String sqlQuery = "exec sp_insertCardInfo :cardCode, :balance, :cardType, :issDate";
		Query query = session.createSQLQuery(sqlQuery);
		query.setString("cardCode", cardInfo.getId().getCardCode());
		query.setBigDecimal("balance", new BigDecimal(cardInfo.getBalance()));
		query.setString("cardType", cardInfo.getCardType());
		query.setDate("issDate", cardInfo.getIssDate());
		int cardId = (int) query.list().get(0);
		return cardId;
	}
	@Override
	public int updateCardBalance(double amount, int cardId)
	{
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "UPDATE CardInfo SET balance = :balance WHERE id.cardId = :cardId";
		Query query = session.createQuery(sqlQuery);
		query.setInteger("cardId", cardId);
		query.setBigDecimal("balance", new BigDecimal(amount));
		return query.executeUpdate();
	}
	@Override
	public int delete(int cardId) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "DELETE CardInfo WHERE id.cardId= :cardId";
		Query query = session.createQuery(sqlQuery);
		query.setInteger("cardId", cardId);
		return query.executeUpdate();
	}

}
