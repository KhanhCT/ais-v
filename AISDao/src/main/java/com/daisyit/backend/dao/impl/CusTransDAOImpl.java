package com.daisyit.backend.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.CusTransDAO;
import com.daisyit.backend.model.CusTrans;
import com.daisyit.backend.model.WebTopUp;;

@Repository("cusTransDAO")
@Transactional
public class CusTransDAOImpl implements CusTransDAO {
	
	@Autowired
	protected SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CusTrans> getCusTransHistory(int customerId, Date fromDate, Date toDate, String sql) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "exec sp_GetCusTransHistory :fromDate, :toDate, :customerId, :condExp ";
		Query query = session.createSQLQuery(sqlQuery).addEntity(CusTrans.class);		
		query.setDate("fromDate", fromDate);
		query.setDate("toDate", toDate);
		query.setInteger("customerId", customerId);
		query.setString("condExp", sql);
		return query.list();
	}

	@Override
	public int newCusTran(CusTrans cusTrans) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(cusTrans);
		return 1;
	}

	@Override
	public int newWebTopUp(WebTopUp webTopUp) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(webTopUp);
		return 1;
	}

}
