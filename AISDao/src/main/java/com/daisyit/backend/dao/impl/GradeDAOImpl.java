package com.daisyit.backend.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.GradeDAO;
import com.daisyit.backend.model.Grade;

@Repository(value="gradeDAO")
@Transactional
public class GradeDAOImpl implements GradeDAO{
	@Autowired
	protected SessionFactory sessionFactory;
	@SuppressWarnings("unchecked")
	@Override
	public List<Grade> findAllGrades() {
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM Grade");
		return query.list();
	}
	@Override
	public void save(Grade grade) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(grade);
	}
}
