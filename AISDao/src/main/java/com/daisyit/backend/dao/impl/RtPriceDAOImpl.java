package com.daisyit.backend.dao.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.daisyit.backend.dao.RtPriceDAO;
import com.daisyit.backend.model.RtPrice;

@Repository("rtPriceDAO")
@Transactional
public class RtPriceDAOImpl implements RtPriceDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public boolean save(RtPrice price) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(price);
		return true;
	}

	@Override
	public List<RtPrice> getLstRtPriceBySkuId(Integer skuId) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM RtPrice c WHERE c.id.skuId = :skuId");
		query.setParameter("skuId", skuId);
		return query.list();
	}

	@Override
	public boolean saveList(List<RtPrice> listRtPrice) {
		Session session = sessionFactory.openSession();
		try {
			session.beginTransaction();
			int batchSize = 20;
			for (int i = 0; i < listRtPrice.size(); i++) {
				session.saveOrUpdate(listRtPrice.get(i));
				if (i % batchSize == 0 && i > 0) {
					session.flush();
					session.clear();
				}
			}
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			session.getTransaction().rollback();
			return false;
		}
	}

	@Override
	public RtPrice findRtPrice(String prCode, int skuId) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM RtPrice c WHERE c.id.prCode = :prCode AND c.id.skuId = :skuId AND status = 1");
		query.setParameter("prCode", prCode);
		query.setParameter("skuId", skuId);
		List<RtPrice> list = query.list();
		if (list != null && !list.isEmpty()) {
			return list.get(0);
		}
		return null;
	}

}
