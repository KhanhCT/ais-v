package com.daisyit.backend.dao;

import java.util.List;

import com.daisyit.backend.model.PrCode;

public interface PrCodeDAO {
	public boolean insert(PrCode prCode);
	public boolean update(PrCode prCode);
	public List<PrCode> findAll();
	boolean checkExistPrCode(String prCode);
	int disablePrCode(String prCode);
}
