package com.daisyit.backend.dao;

import java.util.List;
import java.util.Map;

import com.daisyit.backend.model.Location;

public interface LocationDAO {
	Location getServingLocationById(int locationCode, int campusId);
	Location getLocationByName(String Name);
	Map<String, Location> findAllLocationMap();
	public List<Location> getAllCanteen();
	public void save(Location loc);
}
