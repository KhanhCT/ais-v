package com.daisyit.backend.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.LocationDAO;
import com.daisyit.backend.model.Location;
@Repository("locationDAO")
@Transactional
public class LocationDAOImpl implements LocationDAO{
	@Autowired
	protected SessionFactory sessionFactory;
	@Override
	public Location getServingLocationById(int locId, int campusId) { // Canteen
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM Location WHERE id.locationId = :locationCode AND type=:type AND campusId= :campusId";
		Query query = session.createQuery(sqlQuery);
		query.setInteger("locationId", locId);
		query.setInteger("campusId", campusId);
		query.setString("type", "KO1");
		return (Location) query.uniqueResult();
	}

	@Override
	public Location getLocationByName(String name) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM Location WHERE name= :name";
		Query query = session.createQuery(sqlQuery);
		query.setString("name", name);
		return (Location) query.uniqueResult();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Location> getAllCanteen() {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM Location WHERE type= :type";
		Query query = session.createQuery(sqlQuery);
		//TODO Fix Location TYPE for canteen: 01
		query.setString("type", "01");
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Location> findAllLocationMap() {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM Location";
		Map<String, Location> locMap = new HashMap<>();
		List<Location> locList = new ArrayList<>();
		Query query = session.createQuery(sqlQuery);
		locList =  query.list();
		for(Location loc : locList)
		{
			//locMap.put(loc.getLocationId(), loc);
		}
		return locMap;
	}

	@Override
	public void save(Location loc) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(loc);	
	}

}
