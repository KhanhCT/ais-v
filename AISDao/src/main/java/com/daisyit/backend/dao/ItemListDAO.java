package com.daisyit.backend.dao;

import java.util.List;
import java.util.Map;

import com.daisyit.backend.model.ItemList;

public interface ItemListDAO {
	public int save(ItemList item);
	List<ItemList> findAll();
	Map<Integer, String> findItemMap();
	Map<String, ItemList> findAllItemMap();
	List<ItemList> getAllItemsByGrpId(int itemGrpId);
	ItemList findItemFromCode(String itemCode);
	ItemList findItemFromId(int itemId);
}
