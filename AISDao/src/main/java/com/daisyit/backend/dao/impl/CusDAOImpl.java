package com.daisyit.backend.dao.impl;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.CustomerDAO;
import com.daisyit.backend.model.Customer;

@Repository("customerDAO")
@Transactional
public class CusDAOImpl implements CustomerDAO{
	@Autowired
	protected SessionFactory sessionFactory;


	@Override
	public Customer getCustomerById(String customerId, Boolean status) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery ;
		if(status!=null)
			sqlQuery = "FROM Customer WHERE id.cusId = :cusId AND active= :active";
		else 
			sqlQuery = "FROM Customer WHERE id.cusId = :cusId";
		Query query = session.createQuery(sqlQuery);
		query.setString("cusId", customerId);
		if(status!=null)
			query.setBoolean("active", status);
		return (Customer) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Customer> findAllStudentByClassId(int classId) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM Customer WHERE classId = :classId AND active= :active";
		Query query = session.createQuery(sqlQuery);
		query.setInteger("classId", classId);
		query.setBoolean("active", true);
		return query.list();
	}

	@Override
	public void insert(Customer customer) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(customer);		
	}

	@Override
	public void saveOrUpdate(Customer customer) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(customer);		
	}

	@Override
	public int updateCustomerBalance(String cusId, double balance) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "UPDATE Customer SET balance = :balance WHERE id.cusId = :cusId ";
		Query query = session.createQuery(sqlQuery);
		query.setString("cusId", cusId);
		query.setBigDecimal("balance", new BigDecimal(balance));
		return query.executeUpdate();
	}

	@Override
	public Object[] getCustomerInfo(String cusId) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "exec sp_getCustomerInfo  :customerId ";
		Query query = session.createSQLQuery(sqlQuery);		
		query.setString("customerId", cusId);
		return (Object[]) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getCardBalance() {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "exec sp_getCardBalance";
		Query query = session.createSQLQuery(sqlQuery);		
		return (query.list());
	}

	@Override
	public Customer findCustomerByCardCode(String cardCode) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM Customer t1 WHERE t1.cardId = (SELECT t2.id.cardId FROM CardInfo t2 WHERE t2.id.cardCode = :cardCode) AND t1.active = :active";
		Query query = session.createQuery(sqlQuery);
		query.setParameter("cardCode", cardCode);
		query.setParameter("active", true);
		List<Customer> lst = query.list();
		if (lst != null && !lst.isEmpty()) {
			return lst.get(0);
		}
		return null;
	}

	@Override
	public List<Object[]> getCusInfoByClassId(Integer classId, boolean active) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "SELECT t1.CusId, t1.GivenName, t1.FamilyName, t1.Address, t1.PhoneNum, t1.Email, t1.Remark, t2.CardCode, t1.Active "
				+ "FROM Customer t1 LEFT JOIN CardInfo t2 ON t1.CardId = t2.CardId WHERE t1.ClassId = :ClassId AND t1.Active = :active";
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("ClassId", classId);
		query.setBoolean("active", active);
		return  query.list();
	}
	
	@Override
	public List<Object[]> getCusInfoByClassId(Integer classId) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "SELECT t1.CusId, t1.GivenName, t1.FamilyName, t1.Address, t1.PhoneNum, t1.Email, t1.Remark, t2.CardCode, t1.Active "
				+ "FROM Customer t1 LEFT JOIN CardInfo t2 ON t1.CardId = t2.CardId WHERE t1.ClassId = :ClassId";
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("ClassId", classId);
		return  query.list();
	}

	@Override
	public Boolean updateCardCustomer(String cusId, int cardId) {
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery("UPDATE Customer c SET c.cardId = :cardId WHERE c.cusId = :cusId ");
		query.setParameter("cardId", cardId);
		query.setParameter("cusId", cusId);
		int result = query.executeUpdate();
		if (result > 0 ) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Customer findCustomerByCusIdAndCardCode(String cusId, String cardCode) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM Customer t1 WHERE t1.cusId = :cusId AND t1.cardId = (SELECT t2.id.cardId FROM CardInfo t2 WHERE t2.id.cardCode = :cardCode) AND t1.active = 1";
		Query query = session.createQuery(sqlQuery);
		query.setParameter("cusId", cusId);
		query.setParameter("cardCode", cardCode);
		List<Customer> lst = query.list();
		if (lst != null && !lst.isEmpty()) {
			return lst.get(0);
		}
		return null;
	}

	@Override
	public Boolean disableStudent(String cusId, boolean active) {
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery("UPDATE Customer c SET c.active = :active WHERE c.cusId = :cusId ");
		query.setParameter("active", active);
		query.setParameter("cusId", cusId);
		int result = query.executeUpdate();
		if (result > 0 ) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Customer findCustomerByCardId(int cardId) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "SELECT * FROM Customer WHERE CardId= :cardId AND active = 1";
		Query query = session.createQuery(sqlQuery);
		query.setInteger("cardId", cardId);
		return (Customer) query.uniqueResult();
	}
}
