package com.daisyit.backend.dao.impl;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.daisyit.backend.dao.IdGeneratorDAO;

@Repository("idGenerator")
@Transactional
public class IdGeneratorDAOImpl implements IdGeneratorDAO{
	@Autowired
	protected SessionFactory sessionFactory;
	@Override
	public String getTransNum() {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "exec spS_GenNum";
		Query query = session.createSQLQuery(sqlQuery);
		return  (String) query.list().get(0);
	}

}
