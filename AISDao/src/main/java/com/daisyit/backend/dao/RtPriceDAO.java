package com.daisyit.backend.dao;

import java.util.List;

import com.daisyit.backend.model.RtPrice;

public interface RtPriceDAO {
	boolean save (RtPrice price);
	List<RtPrice> getLstRtPriceBySkuId (Integer skuId);
	boolean saveList (List<RtPrice> listRtPrice);
	RtPrice findRtPrice (String prCode, int skuId);
}
