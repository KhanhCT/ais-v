package com.daisyit.backend.dao;

import java.util.List;

import com.daisyit.backend.model.Grade;

public interface GradeDAO {
	public List<Grade> findAllGrades();
	public void save(Grade grade);

}
