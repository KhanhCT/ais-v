package com.daisyit.backend.dao;

import com.daisyit.backend.model.CardInfo;

public interface CardInfoDAO {
	public CardInfo getCardInfoById(int id);
	public CardInfo getCardInfoByCode(String cardCode);
	public int insert(CardInfo cardInfo);
	public int updateCardBalance(double amount, int cardId);
	public int delete(int cardId);
}
