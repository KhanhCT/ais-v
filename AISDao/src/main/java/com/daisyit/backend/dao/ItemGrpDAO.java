package com.daisyit.backend.dao;

import java.util.List;

import com.daisyit.backend.model.ItemGrp;

public interface ItemGrpDAO {
	List<ItemGrp> findAll();
}
