package com.daisyit.backend.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.daisyit.backend.dao.PrCodeDAO;
import com.daisyit.backend.model.PrCode;

@Repository("prCodeDAO")
@Transactional
public class PrCodeDAOImpl implements PrCodeDAO{
	@Autowired
	private SessionFactory sessionFactory;
	@Override
	public boolean insert(PrCode prCode) {
		Session session =  sessionFactory.getCurrentSession();
		session.save(prCode);
		return true;
	}

	@Override
	public boolean update(PrCode prCode) {
		Session session =  sessionFactory.getCurrentSession();
		session.update(prCode);
		return true;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrCode> findAll() {
		List<PrCode> list = new ArrayList<>();
		Query query = this.sessionFactory.getCurrentSession().createQuery("FROM PrCode WHERE Status = :status");
		query.setBoolean("status", true);
		list =  query.list();
		return list;
	}

	@Override
	public boolean checkExistPrCode(String prCode) {
		Session session =  sessionFactory.getCurrentSession();
		String sql = "FROM PrCode WHERE prCode= :prCode";
		Query query = session.createQuery(sql);
		query.setString("prCode", prCode);
		if(query.list().size() <=0)
			return false;
		return true;
	}
	
	@Override
	public int disablePrCode(String prCode) {
		Session session =  sessionFactory.getCurrentSession();
		String sql = "UPDATE PrCode SET Status = :status WHERE PrCode= :prCode";
		Query query = session.createSQLQuery(sql);
		query.setString("prCode", prCode);
		query.setBoolean("status", false);
		return query.executeUpdate();
	}

}
