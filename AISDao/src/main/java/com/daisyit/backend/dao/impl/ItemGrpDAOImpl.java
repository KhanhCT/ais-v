package com.daisyit.backend.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.daisyit.backend.dao.ItemGrpDAO;
import com.daisyit.backend.model.ItemGrp;

@Repository("itemGrpDAO")
@Transactional
public class ItemGrpDAOImpl implements ItemGrpDAO {
	@Autowired
	protected SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<ItemGrp> findAll() {
		List<ItemGrp> list = new ArrayList<>();
		Query query = this.sessionFactory.getCurrentSession().createQuery("FROM ItemGrp");
		list = query.list();
		return list;
	}

}
