package com.daisyit.backend.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.SaleTransDAO;
import com.daisyit.backend.model.SaleTrans;

@Repository(value="saleTransDAO")
@Transactional
public class SaleTransDAOImpl implements SaleTransDAO{
	@Autowired
	protected SessionFactory sessionFactory;
	
	@Override
	public boolean newSaleTrans(SaleTrans saleTrans) {
		Session session = sessionFactory.getCurrentSession();
		session.save(saleTrans);
		return true;
	}

	@Override
	public boolean addMultiRows(List<SaleTrans> saleTransList) {
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SaleTrans> getSaleTransByCusId(Date fromDate, Date toDate, int customerId, String sql) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "exec sp_getSaleTransByCusId :fromDate, :toDate, :customerId, :condExp ";
		Query query = session.createSQLQuery(sqlQuery).addEntity(SaleTrans.class);
		List<SaleTrans> list = new ArrayList<>();
		query.setDate("fromDate", fromDate);
		query.setDate("toDate", toDate);
		query.setInteger("customerId", customerId);
		query.setString("condExp", sql);
		list = query.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getTransactionList(Date fromDate, Date toDate) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "exec sp_getTransactionList :fromDate, :toDate";
		Query query = session.createSQLQuery(sqlQuery);
		List<Object[]> objList = new ArrayList<>();
		query.setDate("fromDate", fromDate);
		query.setDate("toDate", toDate);
		objList = query.list();
		return objList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getCardTransHistory(Date fromDate, Date toDate, String custId) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "exec sp_getCardTransHistory :fromDate, :toDate, :cusId";
		Query query = session.createSQLQuery(sqlQuery);
		List<Object[]> objList = new ArrayList<>();
		query.setDate("fromDate", fromDate);
		query.setDate("toDate", toDate);
		query.setString("cusId", custId);
		objList = query.list();
		return objList;
	}
}
