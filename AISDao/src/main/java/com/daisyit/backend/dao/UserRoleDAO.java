package com.daisyit.backend.dao;

import java.util.List;

import com.daisyit.backend.model.UserRole;

public interface UserRoleDAO {
	String getUserRoleName(int userRoleId);
	List<UserRole> getAllUserRoles();
}
