package com.daisyit.backend.dao;

import java.util.Date;
import java.util.List;

import com.daisyit.backend.model.CusTrans;
import com.daisyit.backend.model.WebTopUp;

public interface CusTransDAO {
	List<CusTrans> getCusTransHistory(int customerId, Date fromDate, Date toDate, String sql);
	int newCusTran(CusTrans cusTrans);
	int newWebTopUp(WebTopUp webTopUp);
}
