package com.daisyit.backend.dao;

import java.util.Date;
import java.util.List;

import com.daisyit.backend.model.SaleTrans;

public interface SaleTransDAO {
	boolean newSaleTrans(SaleTrans saleTrans);
	boolean addMultiRows(List<SaleTrans> saleTransList);
	List<SaleTrans> getSaleTransByCusId(Date fromDate, Date toDate, int customerId, String sql);
	List<Object[]> getTransactionList(Date fromDate, Date toDate);
	public List<Object[]> getCardTransHistory(Date fromDate, Date toDate, String custId);
}
