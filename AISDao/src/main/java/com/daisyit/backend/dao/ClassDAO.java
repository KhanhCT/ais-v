package com.daisyit.backend.dao;

import java.util.List;
import java.util.Map;

import com.daisyit.backend.model.ClassLst;

public interface ClassDAO {
	public  List<ClassLst> findAllClassByGradeId(int grpId);
	public  List<ClassLst> findAllClasses();
	public Map<String, Integer> findAllClassMap();
	public void save(ClassLst cls);
	public Map<Integer, Integer> findClassGradeMap();
}
