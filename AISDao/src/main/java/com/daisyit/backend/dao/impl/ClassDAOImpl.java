package com.daisyit.backend.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.ClassDAO;
import com.daisyit.backend.model.ClassLst;

@Repository(value="classDAO")
@Transactional
public class ClassDAOImpl implements ClassDAO{
	@Autowired
	protected SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ClassLst> findAllClassByGradeId(int grpId) {
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM ClassLst WHERE gradeId = :grpId");
		query.setInteger("gradeId", grpId);
		return query.list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<ClassLst> findAllClasses() {
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM ClassLst ");
		return query.list();
	}
	@Override
	public Map<String, Integer> findAllClassMap() {
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM ClassLst");
		@SuppressWarnings("unchecked")
		List<ClassLst> classLsts = query.list();
		Map<String, Integer> classMap = new HashMap<>();
		for(ClassLst cls : classLsts)
		{
			classMap.put(String.valueOf(cls.getClassId()), cls.getClassId());
		}
		return classMap;
	}
	@Override
	public void save(ClassLst cls) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(cls);
	}
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Integer> findClassGradeMap() {
		Map<Integer, Integer> map = new HashMap<>();
		String sql = "SELECT t1.ClassId, t2.GradeId FROM ClassLst t1 INNER JOIN Grade t2 ON t1.GradeId = t2.GradeId";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createSQLQuery(sql);
		List<Object[]> list = query.list();
		for(Object[] obj : list) {
			map.put((int)obj[0], (int)obj[1]);
		}
		return map;
	}

}
