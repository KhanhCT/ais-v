package com.daisyit.backend.model;
// Generated Jul 10, 2018 7:56:54 PM by Hibernate Tools 4.3.1


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * SchoolYear generated by hbm2java
 */
@Entity
@Table(name="SchoolYear"
    ,schema="dbo"
    ,catalog="AIS"
)
public class SchoolYear  implements java.io.Serializable {


     private int scYearId;
     private String name;
     private Date fromDate;
     private Date toDate;
     private String description;

    public SchoolYear() {
    }

	
    public SchoolYear(int scYearId) {
        this.scYearId = scYearId;
    }
    public SchoolYear(int scYearId, String name, Date fromDate, Date toDate, String description) {
       this.scYearId = scYearId;
       this.name = name;
       this.fromDate = fromDate;
       this.toDate = toDate;
       this.description = description;
    }
   
     @Id 

    
    @Column(name="ScYearId", unique=true, nullable=false)
    public int getScYearId() {
        return this.scYearId;
    }
    
    public void setScYearId(int scYearId) {
        this.scYearId = scYearId;
    }

    
    @Column(name="Name", length=20)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="FromDate", length=10)
    public Date getFromDate() {
        return this.fromDate;
    }
    
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="ToDate", length=10)
    public Date getToDate() {
        return this.toDate;
    }
    
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    
    @Column(name="Description", length=60)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }




}


