package com.daisyit.backend.model;
// Generated Jul 10, 2018 7:56:54 PM by Hibernate Tools 4.3.1


import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * SaleTrans generated by hbm2java
 */
@Entity
@Table(name="SaleTrans"
    ,schema="dbo"
    ,catalog="AIS"
)
public class SaleTrans  implements java.io.Serializable {


     private SaleTransId id;
     private String transType;
     private String cusId;
     private int cardId;
     private int locationId;
     private Date transDate;
     private Date transTime;
     private BigDecimal qty;
     private BigDecimal rtPrice;
     private BigDecimal curBalance;
     private int userId;
     private String remark;

    public SaleTrans() {
    }

	
    public SaleTrans(SaleTransId id, String transType, String cusId, int cardId, int locationId, Date transDate, Date transTime, BigDecimal qty, BigDecimal rtPrice, int userId) {
        this.id = id;
        this.transType = transType;
        this.cusId = cusId;
        this.cardId = cardId;
        this.locationId = locationId;
        this.transDate = transDate;
        this.transTime = transTime;
        this.qty = qty;
        this.rtPrice = rtPrice;
        this.userId = userId;
    }
    public SaleTrans(SaleTransId id, String transType, String cusId, int cardId, int locationId, Date transDate, Date transTime, BigDecimal qty, BigDecimal rtPrice, BigDecimal curBalance, int userId, String remark) {
       this.id = id;
       this.transType = transType;
       this.cusId = cusId;
       this.cardId = cardId;
       this.locationId = locationId;
       this.transDate = transDate;
       this.transTime = transTime;
       this.qty = qty;
       this.rtPrice = rtPrice;
       this.curBalance = curBalance;
       this.userId = userId;
       this.remark = remark;
    }
   
     @EmbeddedId

    
    @AttributeOverrides( {
        @AttributeOverride(name="transNum", column=@Column(name="TransNum", nullable=false, length=18) ), 
        @AttributeOverride(name="skuId", column=@Column(name="SkuId", nullable=false) ) } )
    public SaleTransId getId() {
        return this.id;
    }
    
    public void setId(SaleTransId id) {
        this.id = id;
    }

    
    @Column(name="TransType", nullable=false, length=2)
    public String getTransType() {
        return this.transType;
    }
    
    public void setTransType(String transType) {
        this.transType = transType;
    }

    
    @Column(name="CusId", nullable=false, length=12)
    public String getCusId() {
        return this.cusId;
    }
    
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    
    @Column(name="CardId")
    public int getCardId() {
        return this.cardId;
    }
    
    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    
    @Column(name="LocationId", nullable=false)
    public int getLocationId() {
        return this.locationId;
    }
    
    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="TransDate", nullable=false, length=10)
    public Date getTransDate() {
        return this.transDate;
    }
    
    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    @Temporal(TemporalType.TIME)
    @Column(name="TransTime", nullable=false, length=16)
    public Date getTransTime() {
        return this.transTime;
    }
    
    public void setTransTime(Date transTime) {
        this.transTime = transTime;
    }

    
    @Column(name="Qty", nullable=false, precision=12, scale=0)
    public BigDecimal getQty() {
        return this.qty;
    }
    
    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    
    @Column(name="RtPrice", nullable=false, precision=18, scale=0)
    public BigDecimal getRtPrice() {
        return this.rtPrice;
    }
    
    public void setRtPrice(BigDecimal rtPrice) {
        this.rtPrice = rtPrice;
    }

    
    @Column(name="CurBalance", precision=18, scale=0)
    public BigDecimal getCurBalance() {
        return this.curBalance;
    }
    
    public void setCurBalance(BigDecimal curBalance) {
        this.curBalance = curBalance;
    }

    
    @Column(name="UserId", nullable=false)
    public int getUserId() {
        return this.userId;
    }
    
    public void setUserId(int userId) {
        this.userId = userId;
    }

    
    @Column(name="Remark")
    public String getRemark() {
        return this.remark;
    }
    
    public void setRemark(String remark) {
        this.remark = remark;
    }




}


