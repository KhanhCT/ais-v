package com.daisyit.backend.model;
// Generated Jul 10, 2018 7:56:54 PM by Hibernate Tools 4.3.1


import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * CardInfo generated by hbm2java
 */
@Entity
@Table(name="CardInfo"
    ,schema="dbo"
    ,catalog="AIS"
)
public class CardInfo  implements java.io.Serializable {


     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CardInfoId id;
     private double balance;
     private String cardType;
     private String passcode;
     private String chrCode;
     private String hexCode;
     private Date issDate;
     private Date dueDate;
     private Double discPc;
     private Date fromDate;
     private Date toDate;
     private boolean active;

    public CardInfo() {
    }

	
    public CardInfo(CardInfoId id, boolean active) {
        this.id = id;
        this.active = active;
    }
    public CardInfo(CardInfoId id, Long balance, String cardType, String passcode, String chrCode, String hexCode, Date issDate, Date dueDate, Double discPc, Date fromDate, Date toDate, boolean active) {
       this.id = id;
       this.balance = balance;
       this.cardType = cardType;
       this.passcode = passcode;
       this.chrCode = chrCode;
       this.hexCode = hexCode;
       this.issDate = issDate;
       this.dueDate = dueDate;
       this.discPc = discPc;
       this.fromDate = fromDate;
       this.toDate = toDate;
       this.active = active;
    }
   
     @EmbeddedId

    
    @AttributeOverrides( {
        @AttributeOverride(name="cardId", column=@Column(name="CardId", nullable=false) ), 
        @AttributeOverride(name="cardCode", column=@Column(name="CardCode", nullable=false, length=13) ) } )
    public CardInfoId getId() {
        return this.id;
    }
    
    public void setId(CardInfoId id) {
        this.id = id;
    }

    
    @Column(name="Balance", precision=12, scale=0, nullable=false)
    public double getBalance() {
        return this.balance;
    }
    
    public void setBalance(double balance) {
        this.balance = balance;
    }

    
    @Column(name="CardType", length=2)
    public String getCardType() {
        return this.cardType;
    }
    
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    
    @Column(name="Passcode", length=13)
    public String getPasscode() {
        return this.passcode;
    }
    
    public void setPasscode(String passcode) {
        this.passcode = passcode;
    }

    
    @Column(name="ChrCode", length=20)
    public String getChrCode() {
        return this.chrCode;
    }
    
    public void setChrCode(String chrCode) {
        this.chrCode = chrCode;
    }

    
    @Column(name="HexCode", length=20)
    public String getHexCode() {
        return this.hexCode;
    }
    
    public void setHexCode(String hexCode) {
        this.hexCode = hexCode;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="IssDate", length=10)
    public Date getIssDate() {
        return this.issDate;
    }
    
    public void setIssDate(Date issDate) {
        this.issDate = issDate;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="DueDate", length=10)
    public Date getDueDate() {
        return this.dueDate;
    }
    
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    
    @Column(name="DiscPc", precision=53, scale=0)
    public Double getDiscPc() {
        return this.discPc;
    }
    
    public void setDiscPc(Double discPc) {
        this.discPc = discPc;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="FromDate", length=10)
    public Date getFromDate() {
        return this.fromDate;
    }
    
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="ToDate", length=10)
    public Date getToDate() {
        return this.toDate;
    }
    
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    
    @Column(name="Active", nullable=false)
    public boolean isActive() {
        return this.active;
    }
    
    public void setActive(boolean active) {
        this.active = active;
    }




}


